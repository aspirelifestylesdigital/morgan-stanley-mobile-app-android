package com.api.aspire.data.restapi;

import com.api.aspire.BuildConfig;
import com.api.aspire.data.entity.oauth.GetTokenAspireResponse;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface AspireOAuthApi {

    @Headers({BuildConfig.WS_ASPIRE_TOKEN_AUTHORIZATION})
    @FormUrlEncoded
    @POST("token/")
    Call<GetTokenAspireResponse> getTokenService(@Field("grant_type") String grant_type,
                                                 @Field("redirect_uri") String redirect_uri,
                                                 @Field("scope") String scope,
                                                 @Field("username") String username,
                                                 @Field("password") String password
    );

    @Headers({BuildConfig.WS_ASPIRE_TOKEN_AUTHORIZATION})
    @FormUrlEncoded
    @POST("token/")
    Call<GetTokenAspireResponse> getTokenUser(@Field("grant_type") String grant_type,
                                              @Field("redirect_uri") String redirect_uri,
                                              @Field("scope") String scope,
                                              @Field("username") String username,
                                              @Field("password") String password
    );

    @Headers({BuildConfig.WS_ASPIRE_TOKEN_AUTHORIZATION})
    @FormUrlEncoded
    @POST("revoke/")
    Call<ResponseBody> revokeToken(@Field("token") String token);
}