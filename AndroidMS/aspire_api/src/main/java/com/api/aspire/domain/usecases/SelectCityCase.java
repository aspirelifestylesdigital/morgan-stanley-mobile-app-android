package com.api.aspire.domain.usecases;

import android.text.TextUtils;

import com.api.aspire.common.constant.ConstantAspireApi;
import com.api.aspire.data.entity.profile.ProfileAspireResponse;
import com.api.aspire.domain.model.ProfileAspire;

import io.reactivex.Completable;

public class SelectCityCase {

    private GetAccessToken getAccessToken;
    private LoadProfile loadProfile;
    private SaveProfile saveProfile;

    private boolean shouldGetProfile;

    public SelectCityCase(GetAccessToken getAccessToken, LoadProfile loadProfile, SaveProfile saveProfile) {
        this.getAccessToken = getAccessToken;
        this.loadProfile = loadProfile;
        this.saveProfile = saveProfile;
    }

    public Completable saveProfileSelectCity(ProfileAspire profileAspire) {
        shouldGetProfile = true;

        if (profileAspire != null && profileAspire.getAppUserPreferences() != null) {

            for (ProfileAspireResponse.AppUserPreferences preference : profileAspire.getAppUserPreferences()) {
                if (ConstantAspireApi.APP_USER_PREFERENCES.SELECTED_CITY_KEY.equals(preference.getPreferenceKey())
                        && !TextUtils.isEmpty(preference.getAppUserPreferenceId())) {
                    shouldGetProfile = false;
                }
            }
        }
        return getAccessToken.execute()
                .flatMapCompletable(accessToken -> {

                    SaveProfile.Params params = new SaveProfile.Params(profileAspire, accessToken);

                    if (shouldGetProfile) {
                        return saveProfile.buildUseCaseCompletable(params)
                                .andThen(loadProfile.buildUseCaseSingle(accessToken).toCompletable());
                    } else {
                        return saveProfile.buildUseCaseCompletable(params);
                    }
                });
    }
}
