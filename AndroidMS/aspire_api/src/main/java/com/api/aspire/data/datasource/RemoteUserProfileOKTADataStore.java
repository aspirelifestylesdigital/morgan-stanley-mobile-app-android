package com.api.aspire.data.datasource;

import com.api.aspire.common.constant.ErrCode;
import com.api.aspire.data.entity.userprofile.UserProfileResponse;
import com.api.aspire.data.retro2client.AppHttpClient;

import io.reactivex.Single;
import retrofit2.Call;
import retrofit2.Response;

public class RemoteUserProfileOKTADataStore {
    public Single<UserProfileResponse> getUserProfile(String clientIdEmail) {
        return Single.create(e -> {
            Call<UserProfileResponse> request = buildApiGetProfile(clientIdEmail);

            Response<UserProfileResponse> response = request.execute();

            if (response == null || !response.isSuccessful()) {
                e.onError(new Exception(ErrCode.USER_PROFILE_OKTA_NOT_EXIST.name()));
            }else{
                e.onSuccess(response.body());
            }
        });
    }

    public Call<UserProfileResponse> buildApiGetProfile(String clientIdEmail) {
        return AppHttpClient.getInstance()
                        .getOktaUserApi()
                        .getProfile(clientIdEmail);
    }
}
