package com.api.aspire.domain.repository;

import io.reactivex.Completable;

public interface ForgotPasswordRepository {

    Completable forgotPassword(String email, String password, String answer);
}
