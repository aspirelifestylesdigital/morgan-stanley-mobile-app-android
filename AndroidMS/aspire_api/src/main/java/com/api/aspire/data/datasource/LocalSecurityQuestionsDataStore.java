package com.api.aspire.data.datasource;

import com.api.aspire.data.entity.securityquestion.SecurityQuestionResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Single;

public class LocalSecurityQuestionsDataStore {
    public Single<SecurityQuestionResponse> getSecurityQuestions() {
        return Single.create(emit -> {
            String dataQuestions =
                    "[\n" +
                            "    {\n" +
                            "        \"question\": \"disliked_food\",\n" +
                            "        \"questionText\": \"What is the food you least liked as a child?\"\n" +
                            "    },\n" +
                            "    {\n" +
                            "        \"question\": \"name_of_first_plush_toy\",\n" +
                            "        \"questionText\": \"What is the name of your first stuffed animal?\"\n" +
                            "    },\n" +
                            "    {\n" +
                            "        \"question\": \"first_award\",\n" +
                            "        \"questionText\": \"What did you earn your first medal or award for?\"\n" +
                            "    },\n" +
                            "    {\n" +
                            "        \"question\": \"favorite_security_question\",\n" +
                            "        \"questionText\": \"What is your favorite security question?\"\n" +
                            "    },\n" +
                            "    {\n" +
                            "        \"question\": \"favorite_toy\",\n" +
                            "        \"questionText\": \"What is the toy/stuffed animal you liked the most as a kid?\"\n" +
                            "    },\n" +
                            "    {\n" +
                            "        \"question\": \"first_computer_game\",\n" +
                            "        \"questionText\": \"What was the first computer game you played?\"\n" +
                            "    },\n" +
                            "    {\n" +
                            "        \"question\": \"favorite_movie_quote\",\n" +
                            "        \"questionText\": \"What is your favorite movie quote?\"\n" +
                            "    },\n" +
                            "    {\n" +
                            "        \"question\": \"first_sports_team_mascot\",\n" +
                            "        \"questionText\": \"What was the mascot of the first sports team you played on?\"\n" +
                            "    },\n" +
                            "    {\n" +
                            "        \"question\": \"first_music_purchase\",\n" +
                            "        \"questionText\": \"What music album or song did you first purchase?\"\n" +
                            "    },\n" +
                            "    {\n" +
                            "        \"question\": \"favorite_art_piece\",\n" +
                            "        \"questionText\": \"What is your favorite piece of art?\"\n" +
                            "    },\n" +
                            "    {\n" +
                            "        \"question\": \"grandmother_favorite_desert\",\n" +
                            "        \"questionText\": \"What was your grandmother's favorite dessert?\"\n" +
                            "    },\n" +
                            "    {\n" +
                            "        \"question\": \"first_thing_cooked\",\n" +
                            "        \"questionText\": \"What was the first thing you learned to cook?\"\n" +
                            "    },\n" +
                            "    {\n" +
                            "        \"question\": \"childhood_dream_job\",\n" +
                            "        \"questionText\": \"What was your dream job as a child?\"\n" +
                            "    },\n" +
                            "    {\n" +
                            "        \"question\": \"place_where_significant_other_was_met\",\n" +
                            "        \"questionText\": \"Where did you meet your spouse/significant other?\"\n" +
                            "    },\n" +
                            "    {\n" +
                            "        \"question\": \"favorite_vacation_location\",\n" +
                            "        \"questionText\": \"Where did you go for your favorite vacation?\"\n" +
                            "    },\n" +
                            "    {\n" +
                            "        \"question\": \"new_years_two_thousand\",\n" +
                            "        \"questionText\": \"Where were you on New Year's Eve in the year 2000?\"\n" +
                            "    },\n" +
                            "    {\n" +
                            "        \"question\": \"favorite_speaker_actor\",\n" +
                            "        \"questionText\": \"Who is your favorite speaker/orator?\"\n" +
                            "    },\n" +
                            "    {\n" +
                            "        \"question\": \"favorite_book_movie_character\",\n" +
                            "        \"questionText\": \"Who is your favorite book/movie character?\"\n" +
                            "    },\n" +
                            "    {\n" +
                            "        \"question\": \"favorite_sports_player\",\n" +
                            "        \"questionText\": \"Who is your favorite sports player?\"\n" +
                            "    }\n" +
                            "]";

            try {
                List<SecurityQuestionResponse.Question> questions = new ArrayList<>();
                JSONArray questionArr = new JSONArray(dataQuestions);
                for (int i = 0; i < questionArr.length(); i++) {
                    JSONObject objQuestion = questionArr.getJSONObject(i);
                    SecurityQuestionResponse.Question question = new SecurityQuestionResponse.Question();
                    question.setQuestion(objQuestion.getString("question"));
                    question.setQuestionText(objQuestion.getString("questionText"));
                    questions.add(question);
//                    if ("favorite_book_movie_character"
//                            .equalsIgnoreCase(objQuestion.getString("question"))) {
//                        questions.add(question);
//                        break;
//                    }
                }

                SecurityQuestionResponse response = new SecurityQuestionResponse();
                response.setQuestions(questions);
                emit.onSuccess(response);
            } catch (JSONException ex) {
                ex.printStackTrace();
                emit.onError(new Exception(""));
            }
        });
    }
}
