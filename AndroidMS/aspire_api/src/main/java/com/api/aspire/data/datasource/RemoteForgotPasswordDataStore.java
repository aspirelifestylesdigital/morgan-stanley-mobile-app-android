package com.api.aspire.data.datasource;

import com.api.aspire.common.constant.ErrCode;
import com.api.aspire.data.entity.forgotpassword.ForgotPasswordRequest;
import com.api.aspire.data.entity.forgotpassword.ForgotPasswordResponse;
import com.api.aspire.data.retro2client.AppHttpClient;

import io.reactivex.Completable;
import retrofit2.Call;
import retrofit2.Response;

public class RemoteForgotPasswordDataStore {

    /*
    {
  "errorCode":"E0000001",
  "errorSummary":"Api validation failed: password.value",
  "errorLink":"E0000001",
  "errorId":"oaeCc2DwRt4RlqhX94hkaEL8g",
  "errorCauses":[
    {
      "errorSummary":"password.value: Password has been used too recently"
    }
  ]
}
{
  "errorCode":"E0000034",
  "errorSummary":"Forgot password not allowed on specified user.",
  "errorLink":"E0000034",
  "errorId":"oaeX8LYbgxMRmKv-xj_LLxpLw",
  "errorCauses":[
    {
      "errorSummary":"User is currently locked out."
    }
  ]
}
    */
    public Completable forgotPassword(String clientIdEmail, ForgotPasswordRequest forgotPasswordRequest) {
        return Completable.create(emitter -> {
            Call<ForgotPasswordResponse> request = AppHttpClient.getInstance()
                    .getOktaUserApi()
                    .forgotPassword(clientIdEmail, forgotPasswordRequest);

            Response<ForgotPasswordResponse> response = request.execute();
            if (!response.isSuccessful()) {
                String error = response.errorBody() == null ? "" : response.errorBody().string();

                if (error.contains("E0000001")
                        && (error.contains("Password has been changed too recently")
                            || error.contains("Password has been used too recently")) ) {

                    emitter.onError(new Exception(ErrCode.FORGOT_PASSWORD_CHANGE_RECENTLY.name()));

                } else if (error.contains("E0000001")
                        && error.contains("Password requirements were not met")) {

                    emitter.onError(new Exception(ErrCode.FORGOT_PASSWORD_PASSWORD_NOT_MEET_RULE.name()));

                } else if (error.contains("E0000014")
                        && error.contains("The credentials provided were incorrect.")) {

                    emitter.onError(new Exception(ErrCode.FORGOT_PASSWORD_WRONG_ANSWER.name()));

                } else if (error.contains("E0000034")
                        && error.contains("Forgot password not allowed on specified user")) {

                    emitter.onError(new Exception(ErrCode.FORGOT_PASSWORD_NOT_ALLOWED.name()));

                } else if (error.contains("E0000001")
                        && error.contains("Password cannot be your current password")) {

                    emitter.onError(new Exception(ErrCode.FORGOT_PASSWORD_CANNOT_CURRENT.name()));

                } else {
                    emitter.onError(new Exception(ErrCode.API_ERROR.name()));
                }
            } else {
                emitter.onComplete();
            }
        });
    }

}
