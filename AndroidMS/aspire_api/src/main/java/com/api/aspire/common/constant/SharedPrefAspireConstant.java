package com.api.aspire.common.constant;

/**
 * Created by tung.phan on 5/5/2017.
 */

public interface SharedPrefAspireConstant {

    String PROFILE = "ppfile";
    String SELECTED_CITY = "cityNameSelected";
    String FORGOT_SECRET_KEY = "hasFP";
    String BIN_CODE = "passCode"; //--  same with name pass code (veraCode security)
    String AUTH_TOKEN = "authToken";
    String REQUEST_CONTENT_STORE = "requestContentStore";
}
