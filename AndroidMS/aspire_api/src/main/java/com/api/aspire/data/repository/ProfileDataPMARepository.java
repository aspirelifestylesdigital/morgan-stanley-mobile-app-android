package com.api.aspire.data.repository;

import android.text.TextUtils;

import com.api.aspire.BuildConfig;
import com.api.aspire.common.constant.ErrCode;
import com.api.aspire.data.entity.userprofile.pma.ProfilePMAResponse;
import com.api.aspire.data.entity.userprofile.pma.SearchProfilePMAResponse;
import com.api.aspire.data.retro2client.AppHttpClient;
import com.api.aspire.domain.repository.ProfilePMARepository;
import com.api.aspire.domain.usecases.MapProfileBase;

import java.util.List;

import io.reactivex.Single;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by vinh.trinh on 5/11/2017.
 */

public class ProfileDataPMARepository implements ProfilePMARepository {

    private MapProfileBase mapApp;

    public ProfileDataPMARepository(MapProfileBase mapApp) {
        this.mapApp = mapApp;
    }

    @Override
    public Single<SearchProfilePMAResponse> searchProfilePMA(String tokenPMA, String email) {
        return Single.create(e -> {
            Call<List<SearchProfilePMAResponse>> requestApi = buildApiSearchProfilePMA(tokenPMA, email);

            Response<List<SearchProfilePMAResponse>> response = requestApi.execute();
            if (response == null) {
                e.onError(new Exception(ErrCode.UNKNOWN_ERROR.name()));
            } else if (!response.isSuccessful()) {
                if (errorResponseNoPartyFound().equalsIgnoreCase(response.errorBody().string())) {
                    e.onError(new Exception(ErrCode.SEARCH_PROFILE_PMA_NO_FOUND_ERROR.name()));
                } else {
                    e.onError(new Exception(ErrCode.UNKNOWN_ERROR.name()));
                }
            } else {
                List<SearchProfilePMAResponse> responseBody = response.body();
                if (responseBody != null && responseBody.size() > 0) {
                    SearchProfilePMAResponse data = responseBody.get(0);
                    e.onSuccess(data);
                } else {
                    e.onError(new Exception(ErrCode.UNKNOWN_ERROR.name()));
                }
            }
        });
    }

    public Call<List<SearchProfilePMAResponse>> buildApiSearchProfilePMA(String tokenPMA, String email) {
        return AppHttpClient.getInstance()
                .getAspirePMAProfileApi().searchPartyPerson(tokenPMA,
                        BuildConfig.WS_ASPIRE_PMA_APP_CODE,
                        mapApp.getXOrganization(),
                        email);
    }

    public String errorResponseNoPartyFound(){
        return "No party result found";
    }

    @Override
    public Single<ProfilePMAResponse> getProfilePMA(String tokenPMA, String partyId) {
        return Single.create(e -> {
            Call<ProfilePMAResponse> requestApi = buildApiGetProfilePMA(tokenPMA, partyId);

            Response<ProfilePMAResponse> response = requestApi.execute();
            if (response == null) {
                e.onError(new Exception(ErrCode.UNKNOWN_ERROR.name()));
            } else if (!response.isSuccessful()) {
                String msgError = response.errorBody().string();
                msgError = TextUtils.isEmpty(msgError) ? "" : msgError;

                if (msgError.contains("PMA_NOTFOUND_001") || msgError.contains("No result found")) {
                    e.onError(new Exception(ErrCode.GET_PROFILE_PMA_NO_FOUND_ERROR.name()));
                } else {
                    e.onError(new Exception(ErrCode.UNKNOWN_ERROR.name()));
                }
            } else {
                ProfilePMAResponse responseBody = response.body();
                e.onSuccess(responseBody);
            }
        });
    }

    public Call<ProfilePMAResponse> buildApiGetProfilePMA(String tokenPMA, String partyId) {
        return AppHttpClient.getInstance()
                .getAspirePMAProfileApi().getProfilePMA(tokenPMA,
                        partyId,
                        BuildConfig.WS_ASPIRE_PMA_APP_CODE,
                        mapApp.getXOrganization()
                );
    }

}



