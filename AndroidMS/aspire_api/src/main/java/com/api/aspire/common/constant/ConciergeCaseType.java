package com.api.aspire.common.constant;

public interface ConciergeCaseType {

    String CREATE = "CREATE";
    String UPDATE = "UPDATE";
    String DELETE = "DELETE";

}
