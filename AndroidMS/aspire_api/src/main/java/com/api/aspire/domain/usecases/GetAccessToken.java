package com.api.aspire.domain.usecases;


import com.api.aspire.domain.model.ProfileAspire;

import io.reactivex.Single;

public class GetAccessToken {

    private LoadProfile loadProfile;
    private GetToken getToken;

    public GetAccessToken(LoadProfile loadProfile, GetToken getToken) {
        this.loadProfile = loadProfile;
        this.getToken = getToken;
    }

    public Single<String> getTokenSignIn(String email, String secret) {
        return getToken.getTokenUser(email, secret);
    }

    /**
     * get Access Token load get profile local
     */
    public Single<String> execute() {
        return loadProfile.loadStorage().flatMap(this::execute);
    }

    /**
     * get Access Token with email & secret
     */
    public Single<String> execute(ProfileAspire profile) {
        return getToken.refreshToken(profile.getEmail(), profile.getSecretKeyDecrypt());
    }

    public Single<Params> executeParams() {
        return loadProfile.loadStorage()
                .flatMap(profile -> {
                    String secretKey = profile.getSecretKeyDecrypt();
                    return getToken.refreshToken(profile.getEmail(), profile.getSecretKeyDecrypt())
                            .zipWith(Single.just(secretKey), Params::new);
                });
    }

    public static final class Params {
        final String accessToken;
        /* value pass decrypt */
        final String secretKeyDecrypt;

        public Params(String accessToken, String secretKeyDecrypt) {
            this.accessToken = accessToken;
            this.secretKeyDecrypt = secretKeyDecrypt;
        }

        public String getAccessToken() {
            return accessToken;
        }

        public String getSecretKeyDecrypt() {
            return secretKeyDecrypt;
        }
    }
}