package com.api.aspire.data.datasource;


import com.api.aspire.data.preference.PreferencesStorageAspire;
import com.api.aspire.domain.model.ProfileAspire;

import io.reactivex.Completable;
import io.reactivex.Single;

public class LocalProfileDataStore {

    private PreferencesStorageAspire preferencesStorageAspire;

    public LocalProfileDataStore(PreferencesStorageAspire preferencesStorage) {
        this.preferencesStorageAspire = preferencesStorage;
    }

    public Single<ProfileAspire> loadProfile() {
        return Single.create(emitter -> {
            try {
                ProfileAspire profile =
                        preferencesStorageAspire.profile();
                emitter.onSuccess(profile);

            } catch (Exception e) {
                emitter.onError(e);
            }
        });
    }

    public Completable saveProfile(ProfileAspire profile) {
        return Completable.create(e -> {
            preferencesStorageAspire.editor().profile(profile).build().save();
            e.onComplete();
        });
    }

}
