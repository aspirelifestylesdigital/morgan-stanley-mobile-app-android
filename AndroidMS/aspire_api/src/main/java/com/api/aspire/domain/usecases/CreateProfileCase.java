package com.api.aspire.domain.usecases;

import com.api.aspire.data.entity.profile.CreateProfileAspireRequest;
import com.api.aspire.domain.model.ProfileAspire;
import com.api.aspire.domain.repository.ProfileAspireRepository;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;

/**
 * flow input binCode before create user
 */
public class CreateProfileCase extends UseCase<ProfileAspire, CreateProfileCase.Params> {

    private ProfileAspireRepository profileRepository;
    private GetToken getToken;
    private ChangeSecurityQuestion useCaseSecurityQuestion;
    private SignInCase signInCase;

    public CreateProfileCase(ProfileAspireRepository profileRepository,
                             ChangeSecurityQuestion useCaseSecurityQuestion,
                             SignInCase signInCase,
                             GetToken getToken) {

        this.profileRepository = profileRepository;
        this.useCaseSecurityQuestion = useCaseSecurityQuestion;
        this.signInCase = signInCase;
        this.getToken = getToken;
    }

    @Override
    Observable<ProfileAspire> buildUseCaseObservable(Params params) {
        return null;
    }

    @Override
    public Single<ProfileAspire> buildUseCaseSingle(Params params) {
        return initProfile(params);
    }

    @Override
    Completable buildUseCaseCompletable(Params params) {
        return null;
    }

    /**
     * Call Api
     * 1. Get token Service
     * 2. Create profile -> ErrCode.USER_EXISTS_ERROR
     * 3. Get Token User
     * 4. Get Profile Aspire
     * 5. Get Profile OKTA ( get id ) -> ErrCode.USER_PROFILE_OKTA_NOT_EXIST
     * 6. Change recovery question -> ErrCode.CHANGE_SECURITY_QUESTION_ERROR
     *
     * -> ErrCode:
     * <br> UNKNOWN_ERROR,
     * <br> USER_EXISTS_ERROR,
     * <br> USER_PROFILE_OKTA_NOT_EXIST,
     * <br> CHANGE_SECURITY_QUESTION_ERROR
     *
     * <br>
     * Note: error (5)or(6) call again from flow 5 -> 6
     */
    public Single<ProfileAspire> initProfile(Params params) {
        return getToken.getTokenService()
                .flatMapCompletable(serviceToken -> profileRepository.createProfile(serviceToken,
                        params.createProfileRequest,
                        params.profile)
                )
                .andThen(
                        signInCase.loadProfileNoSaveStorage(
                                new SignInCase.Params(params.getUserEmail(), params.getUserSecret())
                        ).flatMap(profileLocal ->
                                handleCaseSecurityQuestion(params.securityQuestionParam, profileLocal)
                        )
                );

    }

    public Single<ProfileAspire> handleCaseSecurityQuestion(ChangeSecurityQuestion.Param securityQuestionParam,
                                                  ProfileAspire profileLocal) {

        return useCaseSecurityQuestion.buildUseCaseCompletable(securityQuestionParam)
                //after flow security success save profile
                .andThen(signInCase.saveProfileStorage(profileLocal));
    }

    public static class Params {
        public final ProfileAspire profile;
        final CreateProfileAspireRequest createProfileRequest;
        final ChangeSecurityQuestion.Param securityQuestionParam;

        public Params(ProfileAspire profile,
                      CreateProfileAspireRequest createProfileRequest,
                      ChangeSecurityQuestion.Param securityQuestionParam) {

            this.profile = profile;
            this.createProfileRequest = createProfileRequest;
            this.securityQuestionParam = securityQuestionParam;
        }


        String getUserEmail() {
            return profile.getEmail();
        }

        String getUserSecret() {
            return profile.getSecretKeyDecrypt();
        }
    }

}
