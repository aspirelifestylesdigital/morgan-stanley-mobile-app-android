package com.api.aspire.domain.usecases;

import android.text.TextUtils;

import com.api.aspire.data.entity.profile.ChangePasswordOKTARequest;
import com.api.aspire.data.entity.profile.ChangePasswordOKTAResponse;
import com.api.aspire.data.preference.PreferencesStorageAspire;
import com.api.aspire.data.retro2client.AppHttpClient;
import com.api.aspire.domain.model.ProfileAspire;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import retrofit2.Call;
import retrofit2.Response;

public class ResetPassword extends UseCase<Void, ResetPassword.Params> {

    private PreferencesStorageAspire preferencesStorage;
    private LoadProfile loadProfile;
    private MapProfileBase mapLogic;

    public ResetPassword(MapProfileBase mapLogic,
                         PreferencesStorageAspire preferencesStorage,
                         LoadProfile loadProfile) {

        this.mapLogic = mapLogic;
        this.preferencesStorage = preferencesStorage;
        this.loadProfile = loadProfile;
    }

    @Override
    Observable<Void> buildUseCaseObservable(Params params) {
        return null;
    }

    @Override
    Single<Void> buildUseCaseSingle(Params params) {
        return null;
    }

    @Override
    public Completable buildUseCaseCompletable(Params params) {
        return getProfileLocal().flatMapCompletable(profile ->
                Completable.create(e -> {
                    ChangePasswordOKTARequest body = new ChangePasswordOKTARequest(params.oldPassword, params.newPassword);

                    String email = TextUtils.isEmpty(profile.getEmail()) ? "" : profile.getEmail();
                    String clientIdEmail = mapLogic.convertClientIdEmail(email);

                    Call<ChangePasswordOKTAResponse> request = AppHttpClient.getInstance()
                            .getOktaUserApi().changePassword(clientIdEmail, body);

                    Response<ChangePasswordOKTAResponse> response = request.execute();
                    if (response.isSuccessful() &&
                            response.body() != null &&
                            response.body().isSuccess()) {

                        ProfileAspire profileUpdate = preferencesStorage.profile();
                        profileUpdate.setSecretKey(params.newPassword);

                        // 6/5/2018 save password local after change pass success
                        preferencesStorage.editor().hasForgotPwd(false).profile(profileUpdate).build().save();
                        e.onComplete();

                    } else {
                        e.onError(new Exception(response.errorBody().string()));
                    }
                }));

    }

    private Single<ProfileAspire> getProfileLocal() {
        return loadProfile.loadStorage();
    }

    public static class Params {
        private final String oldPassword;
        private final String newPassword;

        public Params(String oldPassword, String newPassword) {
            this.oldPassword = oldPassword;
            this.newPassword = newPassword;
        }
    }
}
