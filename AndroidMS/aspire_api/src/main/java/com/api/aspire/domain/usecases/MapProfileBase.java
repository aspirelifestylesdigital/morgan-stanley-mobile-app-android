package com.api.aspire.domain.usecases;

import com.api.aspire.data.entity.profile.UpdateProfileAspireRequest;
import com.api.aspire.data.preference.PreferencesStorageAspire;
import com.api.aspire.domain.model.ProfileAspire;

import io.reactivex.Completable;
import io.reactivex.Single;

public interface MapProfileBase {

    /**
     * @param secretKeyCurrent: deEncrypt pass
     */
    Single<ProfileAspire> mapResponse(ProfileAspire profileAspire,
                                      String secretKeyCurrent,
                                      String userEmailCurrent);

    Completable handlePreferenceApp(PreferencesStorageAspire preferencesStorage,
                                    ProfileAspire profileAspire);

    UpdateProfileAspireRequest mapUpdate(PreferencesStorageAspire pref,
                                         ProfileAspire profile);

    String mapSalutation(String profileSalutation);

    String getXAppId();

    String getXOrganization();

    String convertClientIdEmail(String email);

    String getAppUserNamePMA();

    String getAppSecretPMA();

    String getTokenServiceUserName();

    String getTokenServiceSecret();
}
