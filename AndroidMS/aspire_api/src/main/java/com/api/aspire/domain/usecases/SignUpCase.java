package com.api.aspire.domain.usecases;

import android.text.TextUtils;

import com.api.aspire.common.constant.ErrCode;
import com.api.aspire.data.datasource.RemoteUserProfileOKTADataStore;
import com.api.aspire.data.entity.userprofile.UserProfileResponse;
import com.api.aspire.data.entity.userprofile.pma.ProfilePMAMapView;
import com.api.aspire.data.entity.userprofile.pma.ProfilePMAResponse;
import com.api.aspire.data.entity.userprofile.pma.SearchProfilePMAResponse;
import com.api.aspire.data.repository.ProfileDataPMARepository;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.SingleEmitter;
import retrofit2.Call;
import retrofit2.Response;

public class SignUpCase {

    private MapProfileBase mapLogic;
    private RemoteUserProfileOKTADataStore remoteProfileOKTA;
    private GetProfilePMA useCaseProfilePMA;
    private ProfileDataPMARepository pmaRepository;

    public SignUpCase(MapProfileBase mapLogic,
                      RemoteUserProfileOKTADataStore remoteProfileOKTA,
                      GetProfilePMA getProfilePMA,
                      ProfileDataPMARepository pmaRepository) {

        this.mapLogic = mapLogic;
        this.remoteProfileOKTA = remoteProfileOKTA;
        this.useCaseProfilePMA = getProfilePMA;
        this.pmaRepository = pmaRepository;
    }

    //<editor-fold desc="Check OKTA And PMA map profile view">

    /**
     * Check Profile OKTA and Profile PMA
     * <p> 1. check profile OKTA ->
     * <p> i.  have account -> SIGN_UP_PROFILE_OKTA_EXISTS_ERROR
     * <p> ii. no have account -> check profile PMA - GET_TOKEN_PMA_ERROR
     * <p> iii.  profile PMA no exists -> show dialog -> open page register
     * <p> iiii. profile PMA have account -> open page register with map value
     * <p>
     * Error
     * <br/> SIGN_UP_PROFILE_OKTA_EXISTS_ERROR
     * <br/> SIGN_UP_PROFILE_OKTA_AND_PMA_NO_FOUND_ERROR
     *  -> case don't get list profile PMA empty || party id empty || get profile PMA empty
     */
    public Single<ProfilePMAMapView> buildCaseCheckProfileOKTAAndPMA(final String emailOrigin) {
        return Completable.create(e -> {

            String clientIdEmailOKTA = mapLogic.convertClientIdEmail(emailOrigin);

            Call<UserProfileResponse> requestGetProfileOKTA = remoteProfileOKTA.buildApiGetProfile(clientIdEmailOKTA);

            Response<UserProfileResponse> responseGetProfileOKTA = requestGetProfileOKTA.execute();

            if (responseGetProfileOKTA.isSuccessful()) {
                //-- profile okta account exists
                e.onError(new Exception(ErrCode.SIGN_UP_PROFILE_OKTA_EXISTS_ERROR.name()));
            } else {
                e.onComplete();
            }
        }).andThen(getPMAToken()
                .flatMap(tokenPMA -> handleSearchAndGetProfilePMA(emailOrigin, tokenPMA)));
    }

    public Single<String> getPMAToken() {
        return useCaseProfilePMA.getTokenPMA();
    }

    private Single<ProfilePMAMapView> handleSearchAndGetProfilePMA(String emailOrigin, String tokenPMA) {
        return Single.create(e -> {
            Response<List<SearchProfilePMAResponse>> responsePMA =
                    pmaRepository.buildApiSearchProfilePMA(tokenPMA, emailOrigin).execute();

            if (!responsePMA.isSuccessful()) {
                e.onError(new Exception(ErrCode.SIGN_UP_PROFILE_OKTA_AND_PMA_NO_FOUND_ERROR.name()));
            } else {
                List<SearchProfilePMAResponse> responseBody = responsePMA.body();
                if (responseBody == null || responseBody.isEmpty()) {
                    e.onError(new Exception(ErrCode.SIGN_UP_PROFILE_OKTA_AND_PMA_NO_FOUND_ERROR.name()));
                } else {
                    SearchProfilePMAResponse data = responseBody.get(0);
                    String partyId = data.getPartyid();

                    if (TextUtils.isEmpty(partyId)) {
                        e.onError(new Exception(ErrCode.SIGN_UP_PROFILE_OKTA_AND_PMA_NO_FOUND_ERROR.name()));
                    } else {
                        //-- get Profile PMA
                        handleGetProfilePMA(emailOrigin, tokenPMA, partyId, e);
                    }
                }
            }
        });
    }

    private void handleGetProfilePMA(String emailOrigin, String tokenPMA, String partyIdSearch, SingleEmitter<ProfilePMAMapView> e) throws java.io.IOException {
        Response<ProfilePMAResponse> responseProfilePMA = pmaRepository
                .buildApiGetProfilePMA(tokenPMA, partyIdSearch).execute();

        if (responseProfilePMA.isSuccessful()) {

            ProfilePMAResponse profilePMA = responseProfilePMA.body();
            e.onSuccess(convertProfilePMAtoView(emailOrigin, profilePMA));

        } else {
            e.onError(new Exception(ErrCode.SIGN_UP_PROFILE_OKTA_AND_PMA_NO_FOUND_ERROR.name()));
        }
    }

    private ProfilePMAMapView convertProfilePMAtoView(String emailOrigin, ProfilePMAResponse profilePMA) {
        String partyIdUser = "";
        String firstName = "";
        String lastName = "";
        String zipCode = "";
        String phone = "";
        if (profilePMA != null) {
            partyIdUser = TextUtils.isEmpty(profilePMA.getPartyid()) ? "" : profilePMA.getPartyid();

            if (profilePMA.getPartyperson() != null) {
                firstName = profilePMA.getPartyperson().getFirstname();
                firstName = TextUtils.isEmpty(firstName) ? "" : firstName;

                lastName = profilePMA.getPartyperson().getLastname();
                lastName = TextUtils.isEmpty(lastName) ? "" : lastName;
            }

            //zipCode
            if (profilePMA.getLocations() != null
                    && profilePMA.getLocations().size() > 0
                    && profilePMA.getLocations().get(0) != null
                    && profilePMA.getLocations().get(0).getAddress() != null) {
                zipCode = profilePMA.getLocations().get(0).getAddress().getZipcode();
                zipCode = TextUtils.isEmpty(zipCode) ? "" : zipCode;

                zipCode = zipCode.replaceAll("-", "");
            }

            // phone
            if (profilePMA.getContacts() != null
                    && profilePMA.getContacts().getPhones() != null
                    && profilePMA.getContacts().getPhones().size() > 0
                    && profilePMA.getContacts().getPhones().get(0) != null) {

                phone = profilePMA.getContacts().getPhones().get(0).getPhonenumber();
                phone = TextUtils.isEmpty(phone) ? "" : phone;

                phone = phone.replaceAll("-", "");
            }
        }

        return new ProfilePMAMapView(partyIdUser,
                firstName,
                lastName,
                zipCode,
                phone,
                emailOrigin
        );
    }
    //</editor-fold>

}
