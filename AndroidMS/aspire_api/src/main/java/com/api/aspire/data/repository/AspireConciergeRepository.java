package com.api.aspire.data.repository;


import com.api.aspire.data.entity.askconcierge.ConciergeCaseAspireRequest;
import com.api.aspire.data.entity.askconcierge.ConciergeCaseAspireResponse;
import com.api.aspire.data.restapi.AspireConcierge;
import com.api.aspire.data.retro2client.AppHttpClient;

import retrofit2.Call;

public class AspireConciergeRepository implements AspireConcierge {

    @Override
    public Call<ConciergeCaseAspireResponse> createConciergeCase(String accessToken, String xAppId, String xOrganiation, ConciergeCaseAspireRequest request) {
        return AppHttpClient.getInstance().getAspireConcierge().createConciergeCase(accessToken, xAppId, xOrganiation, request);
    }

    @Override
    public Call<ConciergeCaseAspireResponse> updateConciergeCase(String accessToken, ConciergeCaseAspireRequest request) {
        return AppHttpClient.getInstance().getAspireConcierge().updateConciergeCase(accessToken, request);
    }

    @Override
    public Call<ConciergeCaseAspireResponse> deleteConciergeCase(String accessToken, ConciergeCaseAspireRequest request) {
        return AppHttpClient.getInstance().getAspireConcierge().deleteConciergeCase(accessToken, request);
    }
}