package com.api.aspire.domain.usecases;

import android.text.TextUtils;

import com.api.aspire.domain.model.ProfileAspire;
import com.api.aspire.domain.repository.ProfileAspireRepository;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;

public class LoadProfile extends UseCase<ProfileAspire, String> {

    private ProfileAspireRepository profileRepository;

    public LoadProfile(ProfileAspireRepository profileRepository) {
        this.profileRepository = profileRepository;
    }

    @Override
    Observable<ProfileAspire> buildUseCaseObservable(String accessToken) {
        return null;
    }

    @Override
    public Single<ProfileAspire> buildUseCaseSingle(String accessToken) {
        if (TextUtils.isEmpty(accessToken)) {
            return loadStorage();
        } else {
            return profileRepository.loadProfile(accessToken);
        }
    }

    public Single<ProfileAspire> loadStorage() {
        return profileRepository.loadProfile(null);
    }

    public Single<ProfileAspire> loadRemote(GetAccessToken getAccessToken) {
        return getToken(getAccessToken)
                .flatMap(params -> profileRepository.loadProfile(params.getAccessToken()));
    }

    private Single<GetAccessToken.Params> getToken(GetAccessToken getAccessToken) {
        return getAccessToken.executeParams();
    }

    @Override
    Completable buildUseCaseCompletable(String accessToken) {
        return null;
    }

    public Completable refreshProfile(GetAccessToken getAccessToken) {
        return getToken(getAccessToken)
                .flatMapCompletable(params -> profileRepository.refreshProfile(params.getAccessToken()));
    }
}