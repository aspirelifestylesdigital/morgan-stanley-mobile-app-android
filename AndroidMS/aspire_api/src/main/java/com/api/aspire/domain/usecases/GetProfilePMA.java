package com.api.aspire.domain.usecases;


import android.text.TextUtils;

import com.api.aspire.common.constant.ErrCode;
import com.api.aspire.data.entity.oauth.GetTokenAspireRequest;
import com.api.aspire.data.entity.oauth.GetTokenAspireResponse;
import com.api.aspire.data.restapi.AspireTokenPMAApi;
import com.api.aspire.data.retro2client.AppHttpClient;

import io.reactivex.Single;
import retrofit2.Call;
import retrofit2.Response;

public class GetProfilePMA {

    private MapProfileBase mapAppBase;

    public GetProfilePMA(MapProfileBase mapAppBase) {
        this.mapAppBase = mapAppBase;
    }

    public Single<String> getTokenPMA() {
        return Single.create(e -> {
            AspireTokenPMAApi tokenPMAApi = AppHttpClient.getInstance().getAspirePMATokenApi();

            GetTokenAspireRequest rq = new GetTokenAspireRequest(
                    mapAppBase.getAppUserNamePMA(),
                    mapAppBase.getAppSecretPMA()
            );

            Call<GetTokenAspireResponse> requestApi = tokenPMAApi.getTokenPMA(rq.getGrantType(),
                    rq.getRedirectUri(),
                    rq.getScope(),
                    rq.getUsername(),
                    rq.getSecretKey()
            );
            Response<GetTokenAspireResponse> response = requestApi.execute();

            if (response == null || !response.isSuccessful() || response.body() == null) {
                e.onError(new Exception(ErrCode.GET_TOKEN_PMA_ERROR.name()));
            } else {
                String accessToken = response.body().getAccessToken();
                String tokenType = response.body().getTokenType();

                if (TextUtils.isEmpty(accessToken) || TextUtils.isEmpty(tokenType)) {

                    e.onError(new Exception(ErrCode.GET_TOKEN_PMA_ERROR.name()));

                } else {
                    String token = tokenType + " " + accessToken;
                    e.onSuccess(token);
                }
            }
        });
    }


}