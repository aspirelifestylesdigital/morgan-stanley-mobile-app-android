package com.support.mylibrary.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.AppCompatTextView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ScaleXSpan;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;

import com.support.mylibrary.R;

import static android.content.ContentValues.TAG;

/**
 * Created by vinh.trinh on 5/23/2017.
 */

public class LetterSpacingTextView extends AppCompatTextView {
    int pH, pV;
    private float spacing;
    private CharSequence originalText = "";
    private float dpScale = 1f;
    // Pixel scale based on 320 screen resolution
    private float pxScale = 1f;
    private boolean textAllCaps = true;
    private Drawable errorIndicator;
    private Drawable normal;
    private boolean isError;
    public LetterSpacingTextView(Context context) {
        super(context);
    }

    public LetterSpacingTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray attributes = context.obtainStyledAttributes(attrs, R.styleable.LetterSpacingTextView);
        spacing = attributes.getFloat(R.styleable.LetterSpacingTextView_letter_spacing, 0);
        spacing = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, spacing,getResources().getDisplayMetrics());
        attributes.recycle();
        originalText = super.getText();
        applyLetterSpacing();
        setLinkTextColor(Color.parseColor("#0000EE"));

        this.invalidate();
    }

    public float getLetterSpacing() {
        return spacing;
    }

    public void setLetterSpacing_(float letterSpacing) {
        this.spacing = letterSpacing;
        applyLetterSpacing();
    }

    @Override
    public void setText(CharSequence text, BufferType type) {
        originalText = text;
        applyLetterSpacing();
    }
    @Override
    public CharSequence getText() {
        return originalText;
    }

    @Override
    public void setError(CharSequence error) {
        if (normal == null) {
            normal = getBackground();
            pV = this.getPaddingTop();
            pH = this.getPaddingLeft();
        }
        if (error == null) {
            stateNormal();
        } else {
            stateError();
        }
    }

    private void stateError() {
        setBackgroundDrawable(errorIndicator);
        setPadding(pH, pV, pH, pV);
        isError = true;
    }

    private void stateNormal() {
        setBackgroundDrawable(normal);
        setPadding(pH, pV, pH, pV);
        isError = false;
    }


    private void applyLetterSpacing() {
        if (spacing == 0f) {
            super.setText(originalText, BufferType.NORMAL);
            return;
        }

        if (TextUtils.isEmpty(originalText)
                || originalText.length() < 2
                || (spacing >= -1f && spacing <= 1f)) {
            super.setText(originalText, BufferType.NORMAL);
            return;
        }

        // Make ALL CAPS flag false first
        setAllCaps(false);
        // Create text builder with non breaking space letter between
        StringBuilder builder;
        if (textAllCaps) {
            builder = new StringBuilder(originalText.toString().toUpperCase().replaceAll("(.)", "\u00A0" + "$1"));
        } else {
            builder = new StringBuilder(originalText.toString().replaceAll("(.)", "\u00A0" + "$1"));
        }

        float nonBreakingSpace = getPaint().measureText("\u00A0");
        Log.d(TAG, "nonBreakingSpace = " + nonBreakingSpace);

        float scale = spacing * pxScale / nonBreakingSpace;
        Log.d(TAG, "scale = " + scale);

        if (scale > 0.9 && scale < 1f) {
            super.setText(builder, BufferType.NORMAL);
            return;
        }

        // Scale all non breaking space letters by provided spacing value
        SpannableString finalText = new SpannableString(builder);
        if (builder.length() > 1) {
            for (int i = 0; i < builder.length(); i += 2) {
                finalText.setSpan(new ScaleXSpan(scale),
                        i,
                        i + 1,
                        Spannable.SPAN_INCLUSIVE_INCLUSIVE
                );
            }
        }
        // Set text with buffer type SPANNABLE
        super.setText(finalText, BufferType.SPANNABLE);

        setPadding(getPaddingLeft() - (int) (spacing * pxScale - spacing), getPaddingTop(), getPaddingRight(), getPaddingBottom());
    }
}
