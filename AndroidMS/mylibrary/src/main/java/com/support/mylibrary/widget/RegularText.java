package com.support.mylibrary.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ScaleXSpan;
import android.util.AttributeSet;

import com.support.mylibrary.R;


/**
 * Created by hautran on 09/08/17.
 */

public class RegularText extends AppCompatTextView {

    public RegularText(Context context) {
        super(context);
        init(context, null);
    }

    public RegularText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public RegularText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    @SuppressLint("ResourceType")
    private void init(Context ctx, AttributeSet attrs) {
        Typeface typeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/arial.ttf");
        setTypeface(typeface);
        setTextColor(Color.parseColor("#99A1A8"));
        getAttrsText(ctx, attrs);
        setSpacing(1.4f); //Or any float. To reset to normal, use 0 or LetterSpacingTextView.Spacing.NORMAL
    }


    private float spacing = Spacing.NORMAL;
    private CharSequence originalText = "";

    public class Spacing {
        public final static float NORMAL = 0;
    }

    public float getSpacing() {
        return this.spacing;
    }

    public void setSpacing(float spacing) {
        this.spacing = spacing;
        applySpacing();
    }

    @Override
    public void setText(CharSequence text, BufferType type) {
        originalText = text;
        applySpacing();
    }

    @Override
    public CharSequence getText() {
        return originalText;
    }

    private void applySpacing() {
        if (this == null || this.originalText == null) return;
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < originalText.length(); i++) {
            builder.append(originalText.charAt(i));
            if (i + 1 < originalText.length()) {
                builder.append("\u00A0");
            }
        }
        SpannableString finalText = new SpannableString(builder.toString());
        if (builder.toString().length() > 1) {
            for (int i = 1; i < builder.toString().length(); i += 2) {
                finalText.setSpan(new ScaleXSpan((spacing + 1) / 22), i, i + 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
        }
        super.setText(finalText, BufferType.SPANNABLE);
    }


    private void getAttrsText(Context ctx, AttributeSet attrs) {
        TypedArray a = ctx.obtainStyledAttributes(attrs, R.styleable.LetterSpacingTextView);
        String customFont = a.getString(R.styleable.LetterSpacingTextView_letter_spacing);
        originalText = customFont;
    }
}
