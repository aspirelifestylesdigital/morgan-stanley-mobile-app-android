package com.support.mylibrary.widget;

import com.support.mylibrary.R;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.widget.AppCompatButton;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ScaleXSpan;
import android.util.AttributeSet;
import android.util.TypedValue;

/**
 * Created by vinh.trinh on 5/23/2017.
 */

public class ButtonTextSpacing extends AppCompatButton {
    private float spacing;
    private CharSequence originalText = "";

    public ButtonTextSpacing(Context context) {
        super(context);
    }

    public ButtonTextSpacing(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray attributes = context.obtainStyledAttributes(attrs, R.styleable.ButtonTextSpacing);
        spacing = attributes.getFloat(R.styleable.ButtonTextSpacing_text_spacing, 0);
        spacing = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, spacing,getResources().getDisplayMetrics());
        attributes.recycle();
        originalText = super.getText();
        applyLetterSpacing();
        this.invalidate();
    }

    @Override
    public void setText(CharSequence text, BufferType type) {
        originalText = text;
        applyLetterSpacing();
    }

    @Override
    public CharSequence getText() {
        return originalText;
    }

    private void applyLetterSpacing() {
        if (this.originalText == null) return;
        if(spacing == 0) {
            super.setText(originalText, BufferType.SPANNABLE);
            return;
        }
        StringBuilder builder = new StringBuilder();
        for(int i = 0; i < originalText.length(); i++) {
            String c = ""+ originalText.charAt(i);
            builder.append(c.toLowerCase());
            if(i+1 < originalText.length()) {
                builder.append("\u00A0");
            }
        }
        SpannableString finalText = new SpannableString(builder.toString());
        if(builder.toString().length() > 1) {
            for(int i = 1; i < builder.toString().length(); i+=2) {
                finalText.setSpan(new ScaleXSpan((spacing+1)/10), i, i+1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
        }
        super.setText(finalText, BufferType.SPANNABLE);
    }
}
