package com.support.mylibrary.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatEditText;
import android.util.AttributeSet;
import android.util.Log;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;

import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

import com.support.mylibrary.R;

import java.lang.reflect.Field;

/**
 * Created by vinh.trinh on 5/18/2017.
 */

public class ErrorIndicatorEditText extends AppCompatEditText implements View.OnFocusChangeListener {

    private Drawable errorIndicator;
    private Drawable normal;
    private boolean isError;
    private OnFocusChangeListener focusChangeListener;
    private TextInteractListener textInteractListener;
    int pH, pV;
    private boolean blockActionMode;

    public void setTextInteractListener(TextInteractListener textInteractListener) {
        this.textInteractListener = textInteractListener;
    }

    public ErrorIndicatorEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        errorIndicator = ContextCompat.getDrawable(context, R.drawable.error_indicator);
        super.setOnFocusChangeListener(this);
        TypedArray attributes = context.obtainStyledAttributes(attrs, R.styleable.ErrorIndicatorEditText);
        blockActionMode = attributes.getBoolean(R.styleable.ErrorIndicatorEditText_fmenu_disable, true);
        if(blockActionMode) {
            blockContextMenu();
        }
        attributes.recycle();
    }

    @Override
    public void setError(CharSequence error) {
        if (normal == null) {
            normal = getBackground();
            pV = this.getPaddingTop();
            pH = this.getPaddingLeft();
        }
        if (error == null) {
            stateNormal();
        } else {
            stateError();
        }
    }

    @Override
    public void setError(CharSequence error, Drawable icon) {
        if (normal == null) {
            normal = getBackground();
            pV = this.getPaddingTop();
            pH = this.getPaddingLeft();
        }
        if (error == null) {
            stateNormal();
        } else {
            stateError();
        }
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        boolean error = this.isError;
        if (focusChangeListener != null) focusChangeListener.onFocusChange(v, hasFocus);
        this.isError = error;
        if (isError) {
            stateError();
        }
    }

    @Override
    public void setOnFocusChangeListener(OnFocusChangeListener l) {
        focusChangeListener = l;
        super.setOnFocusChangeListener(l);
    }

    private void stateError() {
        setBackgroundDrawable(errorIndicator);
        setPadding(pH, pV, pH, pV);
        isError = true;
    }

    private void stateNormal() {
        setBackgroundDrawable(normal);
        setPadding(pH, pV, pH, pV);
        isError = false;
    }

    @Override
    public boolean onTextContextMenuItem(int id) {
        boolean consumed = super.onTextContextMenuItem(id);
        if (textInteractListener == null) return consumed;
        switch (id) {
            case android.R.id.paste:
                textInteractListener.onTextPaste(this, this.getText().toString());
                return true;
        }
        return consumed;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN && blockActionMode) {
            this.setInsertionDisabled();
        }
        return super.onTouchEvent(event);
    }

    private void setInsertionDisabled() {
        try {
            Field editorField = TextView.class.getDeclaredField("mEditor");
            editorField.setAccessible(true);
            Object editorObject = editorField.get(this);

            Class editorClass = Class.forName("android.widget.Editor");
            Field mInsertionControllerEnabledField = editorClass.getDeclaredField("mInsertionControllerEnabled");
            mInsertionControllerEnabledField.setAccessible(true);
            mInsertionControllerEnabledField.set(editorObject, false);
        }
        catch (Exception ignored) {
        }
    }

    public boolean isError() {
        return isError;
    }

    public void showErrorColorLine(){
        setError("");
    }

    public void hideErrorColorLine(){
        setError(null);
    }

    private void blockContextMenu() {
        this.setCustomSelectionActionModeCallback(new BlockedActionModeCallback());
    }

    public interface TextInteractListener {
        void onTextPaste(AppCompatEditText view, String textPasted);
    }

    private class BlockedActionModeCallback implements ActionMode.Callback {

        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            return false;
        }

        public void onDestroyActionMode(ActionMode mode) {}
    }
}
