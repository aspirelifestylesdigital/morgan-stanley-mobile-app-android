package com.ms.androidapp.presentation.cityguidecategory;

import android.util.Log;

import com.ms.androidapp.domain.model.SubCategoryItem;
import com.ms.androidapp.domain.usecases.StaticCityGuideCategories;
import com.ms.androidapp.presentation.explore.ExplorePresenter;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by tung.phan on 5/31/2017.
 */

public class SubCategoryPresenter implements SubCategory.Presenter {
    private static final String TAG = ExplorePresenter.class.getSimpleName();
    private CompositeDisposable disposables;
    private StaticCityGuideCategories staticCityGuideCategories;
    private SubCategory.View view;

    @Override
    public void attach(SubCategory.View view) {
        this.view = view;
        disposables = new CompositeDisposable();
        staticCityGuideCategories = new StaticCityGuideCategories();
    }

    @Override
    public void detach() {
        disposables.dispose();
        this.view = null;
    }

    @Override
    public void getSubCategories(int categoryId) {
        view.showLoading();
        disposables.add(staticCityGuideCategories.param(new StaticCityGuideCategories.Params(categoryId))
                .on(Schedulers.io(), AndroidSchedulers.mainThread())
                .execute(new GetSubCategoryObserver()));
    }

    private final class GetSubCategoryObserver extends DisposableSingleObserver<List<SubCategoryItem>> {

        @Override
        public void onSuccess(List<SubCategoryItem> subCategoryItems) {
            view.hideLoading();
            view.updateSubCategoryRViewAdapter(subCategoryItems);
        }

        @Override
        public void onError(Throwable e) {
            Log.e(TAG, e.getMessage());
        }
    }
}
