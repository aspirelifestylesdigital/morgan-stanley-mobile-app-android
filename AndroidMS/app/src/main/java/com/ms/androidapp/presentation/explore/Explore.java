package com.ms.androidapp.presentation.explore;

import com.ms.androidapp.domain.model.LatLng;
import com.ms.androidapp.domain.model.explore.ExploreRView;
import com.ms.androidapp.domain.model.explore.ExploreRViewItem;
import com.ms.androidapp.presentation.base.BasePresenter;

import java.util.List;


/**
 * Created by tung.phan on 5/8/2017.
 */

public interface Explore {
    interface View {
        void showLoading(boolean more);
        void hideLoading(boolean more);
        void updateRecommendAdapter(List<ExploreRViewItem> datum);
        List<ExploreRViewItem> historyData();
        void onUpdateFailed(String message);
        void emptyData();
        void onSearchNoData();
        // repeat: true, keep opening SearchActivity
        void loadDataDone();
        void clearSearch(boolean repeat);
        void noInternetMessage();
        void cityGuideNotAvailable();
    }

    interface Presenter extends BasePresenter<Explore.View> {
        void getAccommodationData();
        void handleCitySelection();
        void handleCategorySelection(String category);
        void cityGuideCategorySelection(int index);
        void searchByTerm(String term, boolean withOffers);
        void clearSearch();
        void offHasOffers();
        void loadMore();
        ExploreRView detailItem(ExploreRView.ItemType type, int index);
        void applyDiningSortingCriteria(LatLng location, String cuisine);
    }
}
