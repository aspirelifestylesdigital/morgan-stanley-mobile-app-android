package com.ms.androidapp.presentation.preferences;


import android.content.Context;

import com.api.aspire.data.entity.preference.PreferenceData;
import com.api.aspire.data.preference.PreferencesStorageAspire;
import com.api.aspire.data.repository.ProfileDataAspireRepository;
import com.api.aspire.domain.model.ProfileAspire;
import com.api.aspire.domain.usecases.GetAccessToken;
import com.api.aspire.domain.usecases.GetToken;
import com.api.aspire.domain.usecases.LoadProfile;
import com.api.aspire.domain.usecases.SaveProfile;
import com.ms.androidapp.App;
import com.api.aspire.common.constant.ErrCode;
import com.ms.androidapp.domain.usecases.MapProfileApp;

import io.reactivex.Completable;
import io.reactivex.CompletableEmitter;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableCompletableObserver;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class UserPreferencesPresenter implements UserPreferences.Presenter {

    private CompositeDisposable disposables;
    private UserPreferences.View view;
    private GetAccessToken getAccessToken;
    private LoadProfile loadProfile;
    private SaveProfile saveProfile;

    private ProfileAspire cacheProfile;

    public UserPreferencesPresenter(Context c) {
        disposables = new CompositeDisposable();
        MapProfileApp mapLogic = new MapProfileApp();
        PreferencesStorageAspire preferencesStorage = new PreferencesStorageAspire(c);
        ProfileDataAspireRepository profileRepository = new ProfileDataAspireRepository(preferencesStorage,mapLogic);
        GetToken getToken = new GetToken(preferencesStorage, mapLogic);
        this.loadProfile = new LoadProfile(profileRepository);
        this.getAccessToken = new GetAccessToken(loadProfile, getToken);
        this.saveProfile = new SaveProfile(profileRepository);

    }

    public void attach(UserPreferences.View view) {
        this.view = view;
    }

    public void detach() {
        disposables.dispose();
        view = null;
    }

    @Override
    public void savePreferences(PreferenceData preferenceData) {
        if (!App.getInstance().hasNetworkConnection()) {
            view.showErrorDialog(ErrCode.CONNECTIVITY_PROBLEM, null);
            return;
        }
        view.showLoading();
        disposables.add(getAccessToken.execute(cacheProfile)
                .flatMapCompletable(accessToken -> {
                            cacheProfile.setPreferenceData(preferenceData);
                            return saveProfile.buildUseCaseCompletable(new SaveProfile.Params(cacheProfile, accessToken));
                        }
                ).andThen(loadProfileRemote() // get id preference update
                        .flatMapCompletable(profileLocal -> Completable.create(CompletableEmitter::onComplete))
                )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new UpdatePreferencesObserver())
        );
    }

    private Single<ProfileAspire> loadProfileRemote() {
        return loadProfile.loadRemote(getAccessToken)
                .flatMap(profile -> {
                    cacheProfile = profile;
                    return Single.just(cacheProfile);
                });
    }

    @Override
    public void loadPreferences() {
        view.showLoading();
        disposables.add(
                loadProfileRemote()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new LoadPreferencesObserver())
        );
    }

    public void loadPreferencesLocal() {
        disposables.add(loadProfile.loadStorage()
                .flatMap(profile -> {
                    cacheProfile = profile;
                    return Single.just(cacheProfile);
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new LoadPreferencesLocalObserver())
        );
    }

    /**
     * load data preference local -> still don't hide show loading
     */
    private class LoadPreferencesLocalObserver extends LoadPreferencesObserver {

        @Override
        public void onSuccess(ProfileAspire profile) {
            view.loadPreferencesOnUI(profile.getPreferenceData());
        }

        @Override
        public void onError(Throwable e) {
            super.onError(e);
        }
    }

    private class LoadPreferencesObserver extends DisposableSingleObserver<ProfileAspire> {

        @Override
        public void onSuccess(ProfileAspire profile) {
            if (view != null) {
                view.loadPreferencesOnUI(profile.getPreferenceData());
                view.hideLoading();
            }
        }

        @Override
        public void onError(Throwable e) {
            if (view != null) {
                view.hideLoading();
                view.showErrorDialog(ErrCode.UNKNOWN_ERROR, e.getMessage());
            }
        }
    }

    private class UpdatePreferencesObserver extends DisposableCompletableObserver {

        @Override
        public void onComplete() {
            view.hideLoading();
            view.showPreferenceSavedDialog();
        }

        @Override
        public void onError(Throwable e) {
            if (view == null) {
                return;
            }
            view.hideLoading();
            view.showErrorDialog(ErrCode.UNKNOWN_ERROR, e.getMessage());
        }
    }
}
