package com.ms.androidapp.domain.usecases;

import com.ms.androidapp.datalayer.entity.GetClientCopyResult;
import com.ms.androidapp.domain.repository.B2CRepository;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;

/**
 * Created by ThuNguyen on 6/6/2017.
 */

public class GetMasterCardCopy extends UseCase<GetClientCopyResult, String> {

    private B2CRepository b2CRepository;

    public GetMasterCardCopy(B2CRepository b2CRepository) {
        this.b2CRepository = b2CRepository;
    }

    @Override
    Observable<GetClientCopyResult> buildUseCaseObservable(String params) {
        return null;
    }

    @Override
    public Single<GetClientCopyResult> buildUseCaseSingle(String type) {
        return b2CRepository.getMasterCardCopy(type);
    }

    @Override
    Completable buildUseCaseCompletable(String params) {
        return null;
    }
}
