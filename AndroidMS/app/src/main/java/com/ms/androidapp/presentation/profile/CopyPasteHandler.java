package com.ms.androidapp.presentation.profile;

import android.support.v7.widget.AppCompatEditText;

import com.ms.androidapp.R;
import com.ms.androidapp.common.logic.Validator;
import com.support.mylibrary.widget.ErrorIndicatorEditText;

/**
 * Created by vinh.trinh on 5/29/2017.
 */

final public class CopyPasteHandler implements ErrorIndicatorEditText.TextInteractListener {

    private Validator validator;

    public CopyPasteHandler(Validator validator) {
        this.validator = validator;
    }

    @Override
    public void onTextPaste(AppCompatEditText view, String textPasted) {
        int id = view.getId();
        String validText;
        switch (id) {
            case R.id.edt_first_name:
            case R.id.edt_last_name:
                validText = nameHandling(textPasted);
                break;
            case R.id.edt_phone:
                validText = phoneHandling(textPasted);
                break;
            default:
                validText = textPasted;
                break;
        }
        view.setText(validText);
        view.setSelection(validText.length());
    }

    private String nameHandling(String textPasted) {
        int length = textPasted.length();
        length = length < validator.NAME_MAX_LENGTH ? length : validator.NAME_MAX_LENGTH;
        return textPasted
                .substring(validator.NAME_MIN_LENGTH, length);
    }

    private String phoneHandling(String textPasted) {
        String process = textPasted.substring(2, textPasted.length());
        process = process.replaceAll("-", "");
        int length = process.length();
        if(length > 3) {
            process = process.substring(0, 3) + "-" + process.substring(3, length);
        }
        length = process.length();
        if(length > 7) {
            process = process.substring(0, 7) + "-" + process.substring(7, length);
        }
        length = process.length();
        length = length < validator.PHONE_MAX_LENGTH-2 ? length : validator.PHONE_MAX_LENGTH-2;
        return textPasted.substring(0, 2) + process
                .substring(0, length);
    }

}
