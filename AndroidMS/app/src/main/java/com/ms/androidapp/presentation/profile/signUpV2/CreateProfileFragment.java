package com.ms.androidapp.presentation.profile.signUpV2;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.SwitchCompat;
import android.text.Editable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;


import com.api.aspire.data.entity.userprofile.pma.ProfilePMAMapView;
import com.ms.androidapp.App;
import com.ms.androidapp.R;
import com.ms.androidapp.common.constant.AppConstant;
import com.ms.androidapp.common.constant.IntentConstant;
import com.ms.androidapp.common.logic.MaskerHelper;
import com.ms.androidapp.common.logic.Validator;
import com.ms.androidapp.presentation.base.BaseSwipeBackFragment;
import com.ms.androidapp.presentation.info.MasterCardUtilityActivity;
import com.ms.androidapp.presentation.profile.ProfileComparator;
import com.ms.androidapp.presentation.widget.CustomSpinner;
import com.ms.androidapp.presentation.widget.DropdownAdapter;
import com.ms.androidapp.presentation.widget.ViewKeyboardListener;
import com.ms.androidapp.presentation.widget.ViewUtils;
import com.support.mylibrary.widget.ClickGuard;
import com.support.mylibrary.widget.ErrorIndicatorEditText;
import com.ms.androidapp.domain.model.Profile;
import com.support.mylibrary.widget.LetterSpacingTextView;

import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class CreateProfileFragment extends BaseSwipeBackFragment {

    private static final String ARG_PROFILE_PMA = "profile_pma";
    private final String STATE_INPUTS = "input";

    @BindView(R.id.profile_rootview)
    LinearLayout llProfileParentView;
    @BindView(R.id.scroll_view)
    ScrollView mainScrollView;
    @BindView(R.id.llParent)
    LinearLayout llParent;
    @BindView(R.id.fakeViewToMoveButtonDown)
    View fakeViewToMoveButtonDown;
    @BindView(R.id.edt_first_name)
    ErrorIndicatorEditText edtFirstName;
    @BindView(R.id.edt_last_name)
    ErrorIndicatorEditText edtLastName;
    @BindView(R.id.edt_email)
    ErrorIndicatorEditText edtEmail;
    @BindView(R.id.flEmailMask)
    View emailMask;
    @BindView(R.id.edt_phone)
    ErrorIndicatorEditText edtPhone;
    @BindView(R.id.switch_location)
    SwitchCompat locationSwitchCompat;
    @BindView(R.id.checkbox_acknowledge)
    AppCompatCheckBox chbAcknowledge;
    @BindView(R.id.btn_submit)
    AppCompatButton btnSubmit;
    @BindView(R.id.content_wrapper)
    LinearLayout contentWrapper;
    @BindView(R.id.tvAcknow)
    LetterSpacingTextView tvAcknow;
    @BindView(R.id.tvSelectSalutation)
    LetterSpacingTextView tvSelectSalutation;
    @BindView(R.id.vSalutation)
    View viewSalutation;
    @BindView(R.id.checkbox_container)
    LinearLayout llCheckBoxContainer;

    private CreateProfileEventsListener listener;
    private Profile profile = new Profile();
    private Validator validator = new Validator();
    private FocusChangeListener firstNameMasker = new FocusChangeListener(MaskerHelper.INPUT.NAME);
    private FocusChangeListener lastNameMasker = new FocusChangeListener(MaskerHelper.INPUT.NAME);
    private FocusChangeListener emailMasker = new FocusChangeListener(MaskerHelper.INPUT.EMAIL);
    private FocusChangeListener phoneMasker = new FocusChangeListener(MaskerHelper.INPUT.NAME);
    private TextWatcher phonePattern;
    private ErrorIndicatorEditText editText;
    private String errMessage;
    private int hasFocus;
    private CustomSpinner mSpinnerSalutation;
    private boolean profileFromUpdate = true;
    private boolean isShowLocationAlert = true;
    private ViewKeyboardListener keyboardListener;

    //cache data
    private ProfilePMAMapView profilePMAMapView;

    public static CreateProfileFragment newInstance(ProfilePMAMapView profilePMAMapView) {
        Bundle args = new Bundle();
        args.putParcelable(ARG_PROFILE_PMA, profilePMAMapView);
        CreateProfileFragment fragment = new CreateProfileFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        isShowLocationAlert = true;
        if (context instanceof CreateProfileEventsListener) {
            listener = (CreateProfileEventsListener) context;
        } else {
            throw new ClassCastException(context.toString() + " must implement CreateProfileEventsListener.");
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        storeUp();
        outState.putParcelable(STATE_INPUTS, profile);
        super.onSaveInstanceState(outState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_create_profile, container, false);
        return attachToSwipeBack(view);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setTitle(getString(R.string.register).toUpperCase());
        btnSubmit.setEnabled(false);
        btnSubmit.post(new Runnable() {
            @Override
            public void run() {
                btnSubmit.setClickable(false);
            }
        });
        edtPhone.setImeOptions(EditorInfo.IME_ACTION_NEXT);
        tvSelectSalutation.setVisibility(View.VISIBLE);
        setupTermOfUseClickable();

        mSpinnerSalutation = setUpCustomSpinner(tvSelectSalutation, getSalutation());
        mSpinnerSalutation.setNone(false);
        edtFirstName.setOnFocusChangeListener(firstNameMasker);
        edtLastName.setOnFocusChangeListener(lastNameMasker);
        edtEmail.setOnFocusChangeListener(emailMasker);
        keyboardInteractListener();

        //check box high level button
        chbAcknowledge.setOnCheckedChangeListener((buttonView, isChecked) -> {
            btnSubmit.setEnabled(isChecked);
            btnSubmit.setClickable(isChecked);
        });
        locationSwitchCompat.setOnClickListener(view12 -> {
            boolean on = !locationSwitchCompat.isChecked();
            if (on) listener.onLocationSwitchOn();
        });

        tvAcknow.setOnClickListener(view1 -> chbAcknowledge.setChecked(!chbAcknowledge.isChecked()));

        llCheckBoxContainer.setOnClickListener(view1 -> chbAcknowledge.setChecked(!chbAcknowledge.isChecked()));

        if (savedInstanceState != null) {
            loadProfile(savedInstanceState.getParcelable(STATE_INPUTS), false);
        }
        ClickGuard.guard(btnSubmit);
        llParent.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean focus) {
                if (focus) {
                    ViewUtils.hideSoftKey(llParent);
                }
            }
        });

        phonePattern = new TextWatcher() {
            String strBefore = "";
            int selection;

            @Override
            public void beforeTextChanged(CharSequence s, int index, int before, int count) {
                strBefore = s.toString();
                selection = edtPhone.getSelectionEnd();
            }

            @Override
            public void onTextChanged(CharSequence sequence, int index, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(edtPhone == null) return;
                edtPhone.removeTextChangedListener(phonePattern);
                String text = editable.toString().trim();
                // remove + when input after number
                if (text.lastIndexOf("+") > 0) {
                    edtPhone.setText(strBefore);
                    edtPhone.setSelection(selection);
                }

                if(text.length() > 0 && !text.contains("+")){
                    text = "+" + text;
                    edtPhone.setText(text);
                    edtPhone.setSelection(text.length());
                }else if(text.length() == 1 && text.equalsIgnoreCase("+")){
                    //- for back text empty
                    text = "";
                    edtPhone.setText(text);
                    edtPhone.setSelection(text.length());
                }

                edtPhone.addTextChangedListener(phonePattern);
            }
        };
        edtPhone.addTextChangedListener(phonePattern);
        edtPhone.setOnFocusChangeListener(phoneMasker);

        //setup
        profilePMAMapView = getArguments().getParcelable(ARG_PROFILE_PMA);
        if (profilePMAMapView != null) {
            //default email
            this.profile.setEmail(profilePMAMapView.getEmail());

            //map profilePMA -> profile
            Profile profileConvert = new Profile();
            profileConvert.setFirstName(profilePMAMapView.getFirstName());
            profileConvert.setLastName(profilePMAMapView.getLastName());
            profileConvert.setPhone(profilePMAMapView.getPhone().replace("+",""));
            profileConvert.setEmail(profilePMAMapView.getEmail());

            loadProfile(profileConvert,false);
        }

        edtEmail.setEnabled(false);
        edtEmail.setClickable(false);
        edtEmail.setCursorVisible(false);
        edtEmail.setTextColor(ContextCompat.getColor(App.getInstance(), R.color.heading_gray));

    }

    private List<String> getSalutation() {
        return Arrays.asList(getResources().getStringArray(R.array.cate_salutations));
    }

    private CustomSpinner setUpCustomSpinner(TextView anchor,
                                             List<String> data) {
        CustomSpinner customSpinner = new CustomSpinner(getContext(), anchor);

        DropdownAdapter dropdownAdapter = new DropdownAdapter(getContext(),
                R.layout.item_dropdown,
                data, 42);
        customSpinner.setDropdownAdapter(dropdownAdapter);
        customSpinner.setConfigHeight(data.size());
//        int maxHeight = data.size() * 42;
//        if (maxHeight < 280) {
//            customSpinner.setPobpupHeight(maxHeight);
//        } else {
//            customSpinner.setPobpupHeight(280);
//        }
        return customSpinner;
    }

    /**
     * handle click on back hide keyboard close cursor
     */
    private void keyboardInteractListener() {
        ViewKeyboardListener.KeyboardEvent event = new ViewKeyboardListener.KeyboardEvent() {
            @Override
            public void showKeyboard() {
                //dummy do nothing
                mainScrollView.smoothScrollTo(0, (int) contentWrapper.getTop());
//                scaleView();
            }

            @Override
            public void hideKeyboard() {
                //dummy do nothing
//                if(fakeViewToMoveButtonDown != null){
//                    fakeViewToMoveButtonDown.setVisibility(View.GONE);
//                }
            }

            @Override
            public View getCurrentFocus() {
                if (getActivity() == null) return null;
                return getActivity().getCurrentFocus();
            }
        };
        //-- add listen keyboard
        keyboardListener = new ViewKeyboardListener(llProfileParentView, event);
        keyboardListener.setViewFocusChangeListener(llParent);
    }

    @Override
    public void onStop() {
        ViewUtils.hideSoftKey(getView());
        keyboardListener.removeListener();
        super.onStop();
    }

    @OnClick(R.id.btn_submit)
    void onSubmit(View view) {
        profileFromUpdate = true;
        ViewUtils.hideSoftKey(view);
        storeUp();
        hasFocus = checkHasFocus();
        if (formValidation()) {
            // Replace the "+" sign of phone number
            profile.setPhone(profile.getPhone().replace("+", ""));
            profile.locationToggle(!locationSwitchCompat.isChecked());
            listener.onProfilePMACreate(profilePMAMapView, profile);
        }
    }

    @OnClick({R.id.profile_rootview, R.id.content_wrapper, R.id.flEmailMask, R.id.llParent})
    public void onClick(View view) {
        ViewUtils.hideSoftKey(view);
        if (view.getId() == R.id.flEmailMask) {
            edtEmail.setText(emailMasker.original);
        }
    }

    public void showSoftKey() {
        if (editText != null) {
            editText.requestFocus();
            editText.showErrorColorLine();
            editText.setCursorVisible(true);
            editText.setSelection(editText.getText().toString().length());
            ViewUtils.showSoftKey(editText);
        }
    }

    private boolean formValidation() {
        checkout();
        if (editText != null) {
            editText.requestFocus();
            editText.setError("");
            editText.setSelection(editText.getText().length());
            listener.onProfileInputError(errMessage);
            return false;
        } else if (tvSelectSalutation.getText().toString().equals(getString(R.string.my_profile_text_salutation_place_holder))) {
            errMessage = getString(R.string.input_err_salutation);
            listener.onProfileInputError(errMessage);
            return false;
        }
        return true;
    }

    private void checkout() {
        editText = null;
        StringBuilder messageBuilder = new StringBuilder();
        if (tvSelectSalutation.getText().toString().equals(getString(R.string.my_profile_text_salutation_place_holder))) {
            viewSalutation.setBackgroundColor(Color.parseColor("#ffff4444"));
            if (messageBuilder.length() > 0) {
                messageBuilder.append("\n");
            }
            messageBuilder.append(getString(R.string.input_err_salutation));
        } else {
            viewSalutation.setBackgroundColor(Color.parseColor("#99A1A8"));
        }

        if (!validator.name(profile.getFirstName())) {
            editText = edtFirstName;
            edtFirstName.setError("");
            if (profile.getFirstName().length() > 0) {
                if (messageBuilder.length() > 0) {
                    messageBuilder.append("\n");
                }
                messageBuilder.append(getString(R.string.input_err_invalid_name,
                        "first"));
            }

        } else if (!validator.specialChars(profile.getFirstName())) {
            editText = edtFirstName;
            edtFirstName.setError("");
            if (profile.getFirstName().length() > 0) {
                if (messageBuilder.length() > 0) {
                    messageBuilder.append("\n");
                }
                messageBuilder.append(getString(R.string.input_err_special_char_first_name));
            }
        } else {
            edtFirstName.setError(null);
        }

        if (!validator.name(profile.getLastName())) {
            if (editText == null) editText = edtLastName;
            edtLastName.setError("");
            if (profile.getLastName().length() > 0) {
                if (messageBuilder.length() > 0) {
                    messageBuilder.append("\n");
                }
                messageBuilder.append(getString(R.string.input_err_invalid_name,
                        "last"));
            }

        } else if (!validator.specialChars(profile.getLastName())) {
            if (editText == null) editText = edtLastName;
            edtLastName.setError("");
            if (profile.getLastName().length() > 0) {
                if (messageBuilder.length() > 0) {
                    messageBuilder.append("\n");
                }
                messageBuilder.append(getString(R.string.input_err_special_char_last_name));
            }
        } else {
            edtLastName.setError(null);
        }
        if (!validator.email(profile.getEmail())) {
            if (editText == null) editText = edtEmail;
            edtEmail.setError("");
            if (profile.getEmail().length() > 0) {
                if (messageBuilder.length() > 0) {
                    messageBuilder.append("\n");
                }
                messageBuilder.append(getString(R.string.input_err_invalid_email));
            }
        } else {
            edtEmail.setError(null);
        }
        //-- phone
        String phoneOnView;
        if (edtPhone.hasFocus()) {
            phoneOnView = edtPhone.getText().toString().trim();
        } else {
            phoneOnView = phoneMasker.original.trim();
        }
        if (!validator.phoneFormatValidator(phoneOnView)) {
            if (editText == null) editText = edtPhone;
            edtPhone.setError("");
            if (profile.getPhone()
                    .length() > 0) {
                if (messageBuilder.length() > 0) {
                    messageBuilder.append("\n");
                }
                messageBuilder.append(getString(R.string.input_err_invalid_phone));
            }
        } else {
            String phoneWithOnlyDigit = profile.getPhone().replace("+", "");
            String foundCountryCode = validator.findCountryCodeFromScratch(phoneWithOnlyDigit);
            if (TextUtils.isEmpty(foundCountryCode)) {
                if (editText == null) editText = edtPhone;
                edtPhone.setError("");
                if (profile.getPhone()
                        .length() > 0) {
                    if (messageBuilder.length() > 0) {
                        messageBuilder.append("\n");
                    }
                    messageBuilder.append(getString(R.string.input_err_invalid_country_code));
                }
            } else {
                if (phoneWithOnlyDigit.equalsIgnoreCase(foundCountryCode)) { // Full phone with only country code
                    if (editText == null) editText = edtPhone;
                    edtPhone.setError("");
                    if (profile.getPhone()
                            .length() > 0) {
                        if (messageBuilder.length() > 0) {
                            messageBuilder.append("\n");
                        }
                        messageBuilder.append(getString(R.string.input_err_invalid_phone));
                    }
                } else {
                    edtPhone.setError(null);
                }
            }
        }

//        String password = edtPassword.getText().toString();
//        String reTypePwd = edtConfirmPassword.getText().toString();
//        if (!validator.secretValidator(password)) {
//            if (editText == null) {
//                editText = edtPassword;
//            }
//            edtPassword.setError("");
//            if (password.length() > 0) {
//                if (messageBuilder.length() > 0) {
//                    messageBuilder.append("\n");
//                }
//                messageBuilder.append(getString(R.string.input_err_invalid_password));
//            }
//        } else {
//            edtPassword.setError(null);
//        }
//
//        if (!validator.secretValidator(reTypePwd)) {
//            if (editText == null) {
//                editText = edtConfirmPassword;
//            }
//            edtConfirmPassword.setError("");
//        }
//        boolean emptyPassword = reTypePwd.length() == 0;
//        if (!emptyPassword) {
//            if (!validator.password(password,
//                    reTypePwd)) {
//                if (editText == null) {
//                    editText = edtConfirmPassword;
//                }
//                edtConfirmPassword.setError("");
//
//                if (messageBuilder.length() > 0) {
//                    messageBuilder.append("\n");
//
//                }
//
//                messageBuilder.append(getString(R.string.input_err_pwd_confirmation_failed));
//            } else {
//                edtConfirmPassword.setError(null);
//            }
//        }
//
//        if (editText == null && password.length() == 0) {
//            editText = edtPassword;
//        }
        if (editText != null) {
            String temp = messageBuilder.toString();
            messageBuilder = new StringBuilder("");
            if (profile.anyIsEmpty()) {
                messageBuilder.append(getString(R.string.input_err_required));
            }

            if (messageBuilder.length() > 0) {
                messageBuilder.append("\n");
            }
            messageBuilder.append(temp);

        }
        errMessage = messageBuilder.toString();
    }

    private class FocusChangeListener implements View.OnFocusChangeListener {

        MaskerHelper.INPUT input;
        private String original = "";

        FocusChangeListener(MaskerHelper.INPUT type) {
            input = type;
        }

        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            AppCompatEditText view = (AppCompatEditText) v;
            if (hasFocus) {
                view.setText(original);
                if (view != edtEmail) {
                    edtEmail.setText(MaskerHelper.mask(MaskerHelper.INPUT.EMAIL, emailMasker.original));
                }

            } else {
                original = view.getText().toString().trim();
                if(view.getId() == edtPhone.getId()){
                    edtPhone.removeTextChangedListener(phonePattern);
                    view.setText(MaskerHelper.mask(input, original));
                    edtPhone.addTextChangedListener(phonePattern);
                }else{
                    view.setText(MaskerHelper.mask(input, original));
                }
            }
            view.setCursorVisible(hasFocus);

        }
    }

    private void storeUp() {
        firstNameMasker.original = firstNameMasker.original.trim();
        lastNameMasker.original = lastNameMasker.original.trim();
//        emailMasker.original = emailMasker.original.trim();
        phoneMasker.original = phoneMasker.original.trim();
        String firstName = firstNameMasker.original;
        String lastName = lastNameMasker.original;
//        String email = emailMasker.original;
        String phone = phoneMasker.original;
        profile.setFirstName(firstName);
        profile.setLastName(lastName);
        profile.setPhone(phone);
//        profile.setEmail(email);
        profile.setSalutation(mSpinnerSalutation.getTextSelectedSafe());
    }

    private int checkHasFocus() {
        if (edtFirstName.hasFocus()) {
            profile.setFirstName(edtFirstName.getText().toString().trim());
            return 1;
        }
        if (edtLastName.hasFocus()) {
            profile.setLastName(edtLastName.getText().toString().trim());
            return 2;
        }
        if (edtPhone.hasFocus()) {
            profile.setPhone(edtPhone.getText().toString().trim());
            return 4;
        }
        // zip code
        return 5;
    }

    void loadProfile(Profile profile, boolean fromRemote) {
        if (profile == null) {
            return;
        }

        // Always add "+" at the first place in phone number
        final String phoneOnView = profile.getDisplayCountryCodeAndPhone();

        firstNameMasker.original = profile.getFirstName();
        lastNameMasker.original = profile.getLastName();
        phoneMasker.original = phoneOnView;
        emailMasker.original = profile.getEmail();
        edtFirstName.setText(profile.getFirstName());
        edtLastName.setText(profile.getLastName());
//        edtEmail.setText(profile.getEmail());
        edtPhone.setText(phoneOnView);
        if (profileFromUpdate) {
            edtFirstName.getOnFocusChangeListener().onFocusChange(edtFirstName, false);
            edtLastName.getOnFocusChangeListener().onFocusChange(edtLastName, false);
            edtPhone.getOnFocusChangeListener().onFocusChange(edtPhone, false);
        } else {
            edtFirstName.getOnFocusChangeListener().onFocusChange(edtFirstName, hasFocus == 1);
            edtLastName.getOnFocusChangeListener().onFocusChange(edtLastName, hasFocus == 2);
            edtPhone.getOnFocusChangeListener().onFocusChange(edtPhone, hasFocus == 4);
        }
        edtEmail.setText(MaskerHelper.mask(MaskerHelper.INPUT.EMAIL, emailMasker.original));

        String valSalutation = profile.getSalutation();
        if (!TextUtils.isEmpty(valSalutation)
                && getSalutation().contains(valSalutation)) {
            mSpinnerSalutation.setSelectionPosition(getSalutation().indexOf(valSalutation));
        }

        ProfileComparator profileComparator = new ProfileComparator(
                edtFirstName,
                edtLastName,
                edtEmail,
                edtPhone,
                locationSwitchCompat,
                mSpinnerSalutation);
        profileComparator.original = profile;
        boolean on = profile.isLocationOn();
        locationSwitchCompat.setChecked(!on);
        if (fromRemote) {
            if (on && isShowLocationAlert) {
                listener.onLocationSwitchOn();
            }
            isShowLocationAlert = false;
        }
        resetErrorView();

        btnSubmit.setEnabled(false);
        btnSubmit.setClickable(false);
    }

    public void switchOff() {
        locationSwitchCompat.setChecked(true);
        profile.locationToggle(false);
    }

    public interface CreateProfileEventsListener {

        void onProfileInputError(String message);

        void onLocationSwitchOn();

        void onProfilePMACreate(ProfilePMAMapView profilePMAMapView, Profile profile);
    }

    private void resetErrorView() {
        edtFirstName.hideErrorColorLine();
        edtLastName.hideErrorColorLine();
        edtEmail.hideErrorColorLine();
        edtPhone.hideErrorColorLine();
    }

    private void setupTermOfUseClickable() {
        String s = getString(R.string.acknowledge);
        String termAndCondition = getString(R.string.term_of_use);
        int termStart = s.indexOf(termAndCondition);
        int termEnd = termStart + termAndCondition.length();
        String privacyPolicy = getString(R.string.privacy);
        int privacyStart = s.indexOf(privacyPolicy);
        int privacyEnd = privacyStart + privacyPolicy.length();


        SpannableString ss = new SpannableString(s);
        ClickableSpan span1 = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                openWebView(AppConstant.MASTERCARD_COPY_UTILITY.TermsOfUse);
            }

            @Override
            public void updateDrawState(final TextPaint textPaint) {
                textPaint.setColor(getResources().getColor(R.color.white));
                textPaint.setUnderlineText(true);
            }
        };

        ClickableSpan span2 = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                openWebView(AppConstant.MASTERCARD_COPY_UTILITY.Privacy);
            }

            @Override
            public void updateDrawState(final TextPaint textPaint) {
                textPaint.setColor(getResources().getColor(R.color.white));
                textPaint.setUnderlineText(true);
            }
        };

        ss.setSpan(span1, termStart, termEnd, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss.setSpan(span2, privacyStart, privacyEnd, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        tvAcknow.setText(ss);
        tvAcknow.setMovementMethod(LinkMovementMethod.getInstance());
    }

    void openWebView(AppConstant.MASTERCARD_COPY_UTILITY utilityType) {
        Intent intent = new Intent(this.getActivity(), MasterCardUtilityActivity.class);
        intent.putExtra(IntentConstant.MASTERCARD_COPY_UTILITY, utilityType);
        this.getActivity().startActivity(intent);
    }
}
