package com.ms.androidapp.presentation.requestdetail;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatTextView;
import android.view.Menu;
import android.view.View;

import com.ms.androidapp.App;
import com.ms.androidapp.R;
import com.ms.androidapp.common.constant.IntentConstant;
import com.ms.androidapp.domain.model.RequestDetailData;
import com.ms.androidapp.presentation.base.CommonActivity;
import com.ms.androidapp.presentation.request.AskConciergeActivity;
import com.ms.androidapp.presentation.widget.DialogHelper;

import java.util.List;

import butterknife.BindViews;
import butterknife.OnClick;

/**
 * Created by vinh.trinh on 9/13/2017.
 */

public class RequestDetailActivity extends CommonActivity implements RequestDetail.View {

    @BindViews({ R.id.request_detail_title, R.id.request_detail_status, R.id.request_detail_date,
            R.id.request_detail_details, R.id.label_below, R.id.label_above })
    List<AppCompatTextView> textViews;
    DialogHelper dialogHelper;

    private RequestDetailPresenter presenter;

    private RequestDetailPresenter createPresenter(RequestDetailData rdData) {
        return new RequestDetailPresenter(getApplicationContext(), rdData);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_detail);
        setTitle("My Requests");
        dialogHelper = new DialogHelper(this);
        RequestDetailData rdData = getIntent().getParcelableExtra("rd_data");
        if(!rdData.open) findViewById(R.id.button_wrapper).setVisibility(View.GONE);
        presenter = createPresenter(rdData);
        presenter.attach(this);
        presenter.getRequestDetail();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == 35562 && resultCode == RESULT_OK) {
            setResult(RESULT_OK);
            finish();
            return;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onDestroy() {
        presenter.detach();
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @OnClick(R.id.btn_create)
    void createClick(View view) {
        Intent intent = new Intent(this, AskConciergeActivity.class);
        RequestDetailData data = presenter.getData();
        intent.putExtra(IntentConstant.AC_SUGGESTED_CONCIERGE, presenter.content(true));
        intent.putExtra(IntentConstant.AC_PREF_RESPONSE, data.responseMode);
        startActivityForResult(intent, 35562);
    }

    @OnClick(R.id.btn_change)
    void changeClick(View view) {
        Intent intent = new Intent(this, AskConciergeActivity.class);
        RequestDetailData data = presenter.getData();
        intent.putExtra(IntentConstant.AC_SUGGESTED_CONCIERGE, presenter.content(false));
        intent.putExtra(IntentConstant.AC_TRANSACTION_ID, data.transactionID);
        intent.putExtra(IntentConstant.AC_PREF_RESPONSE, data.responseMode);
        startActivityForResult(intent, 35562);
    }

    @OnClick(R.id.btn_cancel)
    void cancelClick(View view) {
        dialogHelper.action(
                null,
                "Cancelling a request may be subject to a cancellation fee, if a purchase or reservation has already been made.  Do you want to continue?",
                "yes",
                "no",
                (dialogInterface, i) -> presenter.dismissRequest());
    }

    @Override
    public void showProgressDialog() {
        dialogHelper.showProgress();
    }

    @Override
    public void dismissProgressDialog() {
        dialogHelper.dismissProgress();
    }

    @Override
    public void showData(DetailViewData requestDetail) {
        textViews.get(0).setText(requestDetail.title);
        textViews.get(1).setText(requestDetail.isOpening ? "Open" : "Closed");
        textViews.get(2).setText(requestDetail.date);
        textViews.get(3).setText(requestDetail.details);
        textViews.get(4).setText(requestDetail.isOpening ? "Last Changed:" : "Closed:");
        textViews.get(5).setText("Status:");
    }

    @Override
    public void showErrorMessage(String message) {
        dialogHelper.alert(App.getInstance().getString(R.string.errorTitle), "Operation failed.");
    }

    @Override
    public void onCancelDone() {
        setResult(RESULT_OK);
        finish();
    }
}
