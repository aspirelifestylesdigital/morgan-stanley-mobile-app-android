package com.ms.androidapp.presentation.request;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.api.aspire.common.constant.ErrorApi;
import com.ms.androidapp.App;
import com.ms.androidapp.R;
import com.ms.androidapp.common.constant.AppConstant;
import com.ms.androidapp.common.constant.CityData;
import com.api.aspire.common.constant.ErrCode;
import com.ms.androidapp.common.constant.IntentConstant;
import com.ms.androidapp.common.constant.RequestCode;
import com.ms.androidapp.common.constant.ResultCode;
import com.ms.androidapp.presentation.base.CommonActivity;
import com.ms.androidapp.presentation.home.HomeActivity;
import com.ms.androidapp.presentation.selectcity.SelectCityActivity;
import com.ms.androidapp.presentation.widget.DialogHelper;
import com.ms.androidapp.presentation.widget.ViewUtils;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by vinh.trinh on 5/3/2017.
 */

public class AskConciergeActivity extends CommonActivity implements CreateNewConciergeCase.View,
        PlaceRequestFragment.ViewEventsListener,
        ThankYouFragment.ViewEventsListener {

    private final int REQ_CODE_SPEECH_INPUT = 701;

    private AskConciergePresenter presenter;
    private DialogHelper dialogHelper;

    private AskConciergePresenter buildPresenter() {
        return new AskConciergePresenter(this);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.acticity_dummy_content);
        setTitle(R.string.request_title);
        setToolbarColor(R.color.ms_colorPrimary);
        toolbar.setOnClickListener(ViewUtils::hideSoftKey);
        dialogHelper = new DialogHelper(this);

        // Get extra
        String suggestedConcierge = getIntent().getStringExtra(IntentConstant.AC_SUGGESTED_CONCIERGE);
        String prefResponse = getIntent().getStringExtra(IntentConstant.AC_PREF_RESPONSE);
        String transactionId = getIntent().getStringExtra(IntentConstant.AC_TRANSACTION_ID);

        if(savedInstanceState == null) {
            Bundle bundle = new Bundle();
            if(!TextUtils.isEmpty(suggestedConcierge)) {
                bundle.putString(IntentConstant.AC_SUGGESTED_CONCIERGE, suggestedConcierge  + "<br/>" );
            }
            bundle.putString(IntentConstant.AC_PREF_RESPONSE, prefResponse);
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.fragment_place_holder, PlaceRequestFragment.newInstance(bundle),
                            PlaceRequestFragment.class.getSimpleName())
                    .commit();


            // Track GA
            App.getInstance().track(AppConstant.GA_TRACKING_SCREEN_NAME.ASK_CONCIERGE.getValue());
        }
        presenter = buildPresenter();
        presenter.attach(this);
        presenter.param(
                getIntent().getStringExtra(IntentConstant.SELECTED_CITY),
                getIntent().getStringExtra(IntentConstant.SELECTED_CATEGORY)
        );
    }

    @Override
    protected void onDestroy() {
        presenter.detach();
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    private void callTheConcierge(String number) {
        // Show dialog to confirm
        new DialogHelper(this).action(number, "", getString(R.string.text_call), getString(R.string.text_cancel), (dialogInterface, i) -> {
            Intent callIntent = new Intent(Intent.ACTION_VIEW);
            callIntent.setData(Uri.parse("tel:" + number));
            startActivity(callIntent);
        },null);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQ_CODE_SPEECH_INPUT) {
            if (resultCode == ResultCode.RESULT_OK && null != data) {
                ArrayList<String> result = data
                        .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                String text = result.get(0);
                if(!TextUtils.isEmpty(text)) {
                    ((PlaceRequestFragment)getSupportFragmentManager().findFragmentByTag(
                            PlaceRequestFragment.class.getSimpleName()
                    )).speechToTextResult(text);
                }
            }
            return;
        }else if(requestCode == RequestCode.SELECT_CITY){
            if(resultCode == ResultCode.RESULT_OK && null != data){
                toHomeScreen();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override //CreateNewConciergeCase.View::
    public void onRequestSuccessfullySent() {
        setResult(RESULT_OK);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_place_holder, ThankYouFragment.newInstance(),
                        ThankYouFragment.class.getSimpleName())
                .commit();
        setTitle(R.string.request_title2);

        // Track GA with "create request successfully"
        App.getInstance().track(AppConstant.GA_TRACKING_CATEGORY.REQUEST.getValue(),
                AppConstant.GA_TRACKING_ACTION.SUBMIT.getValue(),
                AppConstant.GA_TRACKING_LABEL.REQUEST_ACCEPTED.getValue());
    }

    @Override //CreateNewConciergeCase.View::
    public void showErrorDialog(ErrCode errCode, String extraMsg) {
        if (!dialogHelper.networkUnavailability(errCode, extraMsg)) {
            if (ErrorApi.isGetTokenError(extraMsg)) {
                dialogHelper.showGetTokenError();
            } else {
                String message = getString(R.string.ask_concierge_error);
                dialogHelper.alert(null, message);
            }
        }
        // Track GA with "create request failed"
        App.getInstance().track(AppConstant.GA_TRACKING_CATEGORY.REQUEST.getValue(),
                AppConstant.GA_TRACKING_ACTION.SUBMIT.getValue(),
                AppConstant.GA_TRACKING_LABEL.REQUEST_DENIED.getValue());
    }

    @Override //CreateNewConciergeCase.View::
    public void showProgressDialog() {
        dialogHelper.showProgress();
    }

    @Override
    public void dismissProgressDialog() { //CreateNewConciergeCase.View::
        dialogHelper.dismissProgress();
    }

    @Override // PlaceRequestFragment.ViewEventsListener::
    public void record() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.ENGLISH);
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                getString(R.string.speech_prompt));
        try {
            startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
        } catch (ActivityNotFoundException a) {
            Toast.makeText(getApplicationContext(),
                    getString(R.string.speech_not_supported),
                    Toast.LENGTH_SHORT).show();
        }
    }

    @Override // PlaceRequestFragment.ViewEventsListener::
    public void sendRequest(String content, boolean email, boolean phone) {
        presenter.sendRequest(content, email, phone);
    }

    @Override // PlaceRequestFragment.ViewEventsListener::
    public void makePhoneCall(String number) {
        callTheConcierge(number);
    }

    @Override // ThankYouFragment.ViewEventsListener::
    public void anotherRequest() {
        Bundle bundle = new Bundle();
        bundle.putBoolean(IntentConstant.INVOKE_KEYBOARD, true);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_place_holder, PlaceRequestFragment.newInstance(),
                        PlaceRequestFragment.class.getSimpleName())
                .commit();
        setTitle(R.string.request_title);
    }

    @Override // ThankYouFragment.ViewEventsListener::
    public void toExplore() {
        if (CityData.citySelected()) {
            toHomeScreen();
        } else {
            // Go to select city activity
            Intent intentSelectCity = new Intent(this, SelectCityActivity.class);
            intentSelectCity.putExtra(IntentConstant.ASK_SCREEN_TO_SELECT_CITY, true);
            startActivityForResult(intentSelectCity, RequestCode.SELECT_CITY);
        }
    }

    private void toHomeScreen() {
        setResult(ResultCode.RESULT_OK, null);
        Intent intent = new Intent(this, HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.putExtra(IntentConstant.RESET_EXPLORE_PAGE, true);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        if (placeRequestOnTop()) {
            return;
        }
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            if (!placeRequestOnTop()) {
                super.onBackPressed();
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private boolean placeRequestOnTop() {
        boolean opTop = getSupportFragmentManager()
                .findFragmentByTag(PlaceRequestFragment.class.getSimpleName()) != null;
        if (opTop) {
            dialogHelper.action(null,
                    getString(R.string.ask_quit_confirm),
                    getString(R.string.text_yes_all_caps), getString(R.string.text_no_all_caps),
                    (dialog, which) -> super.onBackPressed());
        }
        return opTop;
    }
}