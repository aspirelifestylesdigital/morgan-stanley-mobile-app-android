package com.ms.androidapp.presentation.profile;

import android.support.v7.widget.SwitchCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.Button;
import android.widget.EditText;

import com.ms.androidapp.domain.model.Profile;
import com.ms.androidapp.presentation.widget.CustomSpinner;

/**
 * Created by vinh.trinh on 7/11/2017.
 */

public class ProfileComparator {

    public Profile original;

    private EditText edtFirstName, edtLastName, edtEmail, edtPhone, edtAnswer;
    private SwitchCompat locationSwitch;
    /*private Spinner salutationSpinner;*/
    private CustomSpinner salutationSpinner;
    private Button btnSubmit, btnCancel;
    private boolean firstName, lastName, email, phone, salutation, answer, question;
    private boolean changed;

    public ProfileComparator(EditText edtFirstName, EditText edtLastName, EditText edtEmail, EditText edtPhone,
                             Button btnSubmit, Button btnCancel, SwitchCompat locationSwitch, CustomSpinner salutationSpinner,
                             EditText edtAnswer) {
        this.edtFirstName = edtFirstName;
        this.edtLastName = edtLastName;
        this.edtEmail = edtEmail;
        this.edtPhone = edtPhone;
        this.btnSubmit = btnSubmit;
        this.btnCancel = btnCancel;
        this.locationSwitch = locationSwitch;
        this.salutationSpinner = salutationSpinner;
        this.edtAnswer = edtAnswer;
        setUp();
    }

    //-- create profile
    public ProfileComparator(EditText edtFirstName, EditText edtLastName, EditText edtEmail, EditText edtPhone,
                             SwitchCompat locationSwitch, CustomSpinner salutationSpinner) {
        this(edtFirstName,
                edtLastName,
                edtEmail,
                edtPhone,
                null,
                null,
                locationSwitch,
                salutationSpinner,
                null
        );
    }

    private void setUp() {
        edtFirstName.addTextChangedListener(new TextWatcher() {
            @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override
            public void afterTextChanged(Editable s) {
                if(edtFirstName.isFocused()) {
                    String string = s.toString();
                    firstName = !string.equals(original.getFirstName().trim());
                    onChanged();
                }
            }
        });
        edtLastName.addTextChangedListener(new TextWatcher() {
            @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override
            public void afterTextChanged(Editable s) {
                if(edtLastName.isFocused()) {
                    String string = s.toString();
                    lastName = !string.equals(original.getLastName().trim());
                    onChanged();
                }
            }
        });
      /*  edtEmail.addTextChangedListener(new TextWatcher() {
            @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override
            public void afterTextChanged(Editable s) {
                String string = s.toString();
                if(string.contains("*")) return;
                email = !string.equals(original.getEmail());
                onChanged();
            }
        });*/
        edtPhone.addTextChangedListener(new TextWatcher() {
            @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override
            public void afterTextChanged(Editable s) {
                if(edtPhone.isFocused()) {
                    /*String string = s.toString();
                    if(string.indexOf("+") == 0){
                        if(string.length() > 1) {
                            string = string.substring(1);
                        }else{
                            string = "";
                        }
                    }*/
                    String originalPhone = original.getPhone().replace("+", "");
                    phone = !s.toString().replace("+", "").equals(originalPhone);
                    onChanged();
                }
            }
        });
        salutationSpinner.setmSpinnerListener(new CustomSpinner.SpinnerListener() {
            @Override
            public void onArchorViewClick() {

            }

            @Override
            public void onItemSelected(final int index) {
                salutation = !(salutationSpinner.getDropdownAdapter().getItem(index)).equals(original.getSalutation());
                onChanged();

            }
        });
      /*  salutationSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                salutation = !(salutationSpinner.getSelectedItem()).equals(original.getSalutation());
                onChanged();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });*/
        if (edtAnswer != null) {
            edtAnswer.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }

                @Override
                public void afterTextChanged(Editable s) {
                    if(edtAnswer.isFocused()) {
                        String string = s.toString();
                        answer = string.trim().length() > 0 ;
                        onChanged();
                    }
                }
            });
        }
    }

    public boolean isAnswerChanged() {
        return answer;
    }


    public boolean isQuestion() {
        return question;
    }

    public void setQuestion(boolean question) {
        this.question = question;
    }


    public boolean isProfileChanged() {
        return firstName || lastName || phone || email || salutation ||
                locationSwitch.isChecked() == original.isLocationOn();
    }

    void onChanged() {

        boolean valueCheck = isProfileChanged();

        //-- create profile
        if (edtAnswer == null) {
            this.changed = valueCheck;
        } else {
            //-- My Profile
            this.changed = valueCheck || answer;
        }

        if(btnSubmit != null){
            btnSubmit.setEnabled(changed);
            btnSubmit.setClickable(changed);
        }

        if(btnCancel != null){
            btnCancel.setEnabled(changed);
            btnCancel.setClickable(changed);
        }
    }

    public void handleSelectQuestion() {
        //-- My Profile
        this.changed = isProfileChanged();

        if(btnSubmit != null){
            btnSubmit.setEnabled(changed);
            btnSubmit.setClickable(changed);
        }

        if(btnCancel != null){
            btnCancel.setEnabled(changed);
            btnCancel.setClickable(changed);
        }
    }
}
