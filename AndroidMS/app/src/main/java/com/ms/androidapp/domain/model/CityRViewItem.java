package com.ms.androidapp.domain.model;

/**
 * Created by vinh.trinh on 5/12/2017.
 */

public class CityRViewItem {
    private int imageResources;
    private String city;//can also be region name
    private String state;//can also be country

    public CityRViewItem(int imageResources, String city, String state) {
        this.imageResources = imageResources;
        this.city = city;
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public String getState() {
        return state;
    }
    public int getImageResources() {
        return imageResources;
    }

    public void setImageResources(int imageResources) {
        this.imageResources = imageResources;
    }
}
