package com.ms.androidapp.domain.mapper;


import android.text.TextUtils;

import com.ms.androidapp.common.constant.CityData;
import com.ms.androidapp.datalayer.entity.SearchContent;
import com.ms.androidapp.datalayer.entity.Tiles;

import java.util.ArrayList;
import java.util.List;

public class FilterMapper {

    /** filter ccaSubCategory and geographic region*/
    public static void handleFilterGeographicRegion(List<Tiles> originalData) {
        try {
            List<Tiles> toBeRemoved = new ArrayList<>();
            boolean isCity = CityData.isUSCity();
            String[] geographicRegions = CityData.geographicRegion().split("\\.");
            for (Tiles tiles: originalData) {
                boolean isNeedRemove = isNeedRemoveItem(isCity, geographicRegions, tiles.category(), tiles.subCategory(), tiles.geographicRegion());
                if (isNeedRemove) {
                    toBeRemoved.add(tiles);
                }//do nothing, show this item => continue
            }
            originalData.removeAll(toBeRemoved);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /** Page Search filter ccaSubCategory and geographic region*/
    public static void handleFilterGeographicRegionSearch(List<SearchContent> originalData) {
        try {
            List<SearchContent> toBeRemoved = new ArrayList<>();
            boolean isCity = CityData.isUSCity();
            String[] geographicRegions = CityData.geographicRegion().split("\\.");
            for (SearchContent tiles: originalData) {
                boolean isNeedRemove = isNeedRemoveItem(isCity, geographicRegions, tiles.category(),tiles.subCategory(), tiles.geographicRegion());
                if (isNeedRemove) {
                    toBeRemoved.add(tiles);
                }//do nothing, show this item => continue
            }
            originalData.removeAll(toBeRemoved);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static final String VALUE_CATEGORY_GLOBAL_PARTNERS = "Global Partners";
    private static final String VALUE_CATEGORY_HOTELS = "Hotels";
    private static final String VALUE_CATEGORY_VACATION_PACKAGES = "Vacation Packages";
    private static final String VALUE_CATEGORY_CRUISES = "Cruises";

    /*
     * check with subCategory and geographic
     * @return true will remove item, otherwise false will hold item
     */
    private static boolean isNeedRemoveItem(boolean isCity, String[] geographicRegion,
                                            String categoryTypeRemote, String subCatRemote, String geoRegionRemote) {

        boolean isRemoveItem;
        geoRegionRemote = geoRegionRemote.trim();

        if (VALUE_CATEGORY_HOTELS.equalsIgnoreCase(categoryTypeRemote) ||
                VALUE_CATEGORY_VACATION_PACKAGES.equalsIgnoreCase(categoryTypeRemote) ||
                VALUE_CATEGORY_CRUISES.equalsIgnoreCase(categoryTypeRemote)) {

            //<editor-fold desc="case Category Hotels, Cruises, Vacation Packages">
            if (TextUtils.isEmpty(subCatRemote) || VALUE_CATEGORY_GLOBAL_PARTNERS.equals(subCatRemote)) {
                isRemoveItem = false;
            } else {
                isRemoveItem = shouldIgnore(isCity, geographicRegion, geoRegionRemote, subCatRemote);
            }
            //</editor-fold>
        } else {
            //-- will don't filter subCateRemote, only check filter geoRemote, subCateLocal, geoLocal
            //- don't check case geoLocal get All
            //<editor-fold desc="case Category: Dining, Entertainment, Shopping, Flower, Transportation, Travel, Tour, Sport">
            isRemoveItem = isNeedRemoveCategoryTypeOther(geographicRegion, geoRegionRemote);
            //</editor-fold>
        }
        return isRemoveItem;
    }


    /**  will don't filter subCategory if category is not Hotels, Vacation Packages, Cruises
     * category like: Tour */
    private static boolean isNeedRemoveCategoryTypeOther(String[] geographicRegions, String geographicRegion) {
        if (TextUtils.isEmpty(geographicRegion)) return false;
        if(geographicRegions.length == 0 || geographicRegions[0].equals("")) return true;
        if (geographicRegions.length > 0) {
            for (String region : geographicRegions) {
                if (geographicRegion.contains(region)
                        || CityData.VALUE_GEOGRAPHIC_REGION_ALL.equalsIgnoreCase(region)) {
                    return false;
                }
            }
        }
        return true;
    }

    private static boolean shouldIgnore(boolean isCity,
                                 String[] geographicRegions,
                                 String geographicRegion,
                                 String subCategory) {

        if(geographicRegions.length == 0 || geographicRegions[0].equals("")) return true;
        if(!subCategory.contains(geographicRegions[0])) return true;
        if(!isCity && geographicRegions.length == 1) return false;
        for (String region : geographicRegions) {
            if (geographicRegion.contains(region)
                    || CityData.VALUE_GEOGRAPHIC_REGION_ALL.equalsIgnoreCase(region)) {
                return false;
            }
        }
        return true;
    }
}
