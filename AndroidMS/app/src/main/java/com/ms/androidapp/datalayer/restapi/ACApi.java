package com.ms.androidapp.datalayer.restapi;

import com.ms.androidapp.datalayer.entity.askconcierge.ACRequest;
import com.ms.androidapp.datalayer.entity.askconcierge.ACResponse;
import com.ms.androidapp.datalayer.entity.askconcierge.GetACRequest;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by vinh.trinh on 5/16/2017.
 */

public interface ACApi {
    @POST("Requests/CreateNewConciergeCase")
    Call<ACResponse> createNewConciergeCase(@Body ACRequest request);

    @POST("Requests/CreateNewConciergeCase")
    Call<ACResponse> updateConciergeCase(@Body ACRequest request);

    @POST("ManageRequest/GetRecentRequestList")
    Call<ResponseBody> getRequestList(@Body GetACRequest request);

    @POST("ManageRequest/GetConciergeRequestDetails")
    Call<ResponseBody> getRequestDetail(@Body GetACRequest request);
}
