package com.ms.androidapp.presentation.profile;

import com.api.aspire.common.constant.ErrCode;
import com.ms.androidapp.domain.model.Profile;
import com.ms.androidapp.presentation.base.BasePresenter;

/**
 * Created by vinh.trinh on 4/27/2017.
 */

public interface CreateProfile {

    interface View {
        void showProfileCreatedDialog();
        void showErrorDialog(ErrCode errCode, String extraMsg);
        void showProgressDialog();
        void dismissProgressDialog();
    }

    interface Presenter extends BasePresenter<View> {
        void createProfile(Profile profile, String password);
        void abort();
    }

}
