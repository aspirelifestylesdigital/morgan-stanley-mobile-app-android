package com.ms.androidapp.presentation.profile.signUpV2;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatEditText;
import android.text.InputFilter;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.api.aspire.common.constant.ErrCode;
import com.api.aspire.domain.model.SecurityQuestion;
import com.api.aspire.domain.usecases.ChangeSecurityQuestion;
import com.ms.androidapp.App;
import com.ms.androidapp.R;
import com.ms.androidapp.common.logic.MaskerHelper;
import com.ms.androidapp.common.logic.Validator;
import com.ms.androidapp.domain.mapper.profile.MapDataApi;
import com.ms.androidapp.domain.model.Profile;
import com.ms.androidapp.presentation.base.BaseSwipeBackFragment;
import com.ms.androidapp.presentation.challengequestion.ListChallengeQuestion;
import com.ms.androidapp.presentation.challengequestion.ListChallengeQuestionPresenter;
import com.ms.androidapp.presentation.profile.forgotPwdV2.ForgotPasswordComparator;
import com.ms.androidapp.presentation.widget.CustomSpinner;
import com.ms.androidapp.presentation.widget.DialogHelper;
import com.ms.androidapp.presentation.widget.DropdownAdapter;
import com.ms.androidapp.presentation.widget.PasswordInputFilter;
import com.ms.androidapp.presentation.widget.ViewKeyboardListener;
import com.ms.androidapp.presentation.widget.ViewUtils;
import com.support.mylibrary.widget.ClickGuard;
import com.support.mylibrary.widget.ErrorIndicatorEditText;
import com.support.mylibrary.widget.LetterSpacingTextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class PasswordFragment extends BaseSwipeBackFragment implements
        ListChallengeQuestion.View {

    private static final String ARG_PROFILE = "profile";

    @BindView(R.id.edt_email)
    ErrorIndicatorEditText edtEmail;
    @BindView(R.id.edt_answer_question)
    ErrorIndicatorEditText edtAnswerQuestion;
    @BindView(R.id.edt_forgot_pwd)
    ErrorIndicatorEditText edtForgotPwd;
    @BindView(R.id.edt_retype_pwd)
    ErrorIndicatorEditText edtForgotRetypePwd;
    @BindView(R.id.content_wrapper)
    RelativeLayout contentWrapper;
    @BindView(R.id.scroll_view)
    ScrollView scrollView;
    @BindView(R.id.btn_submit)
    Button btnSubmit;
    @BindView(R.id.tvChallengeQuestion)
    LetterSpacingTextView tvChallengeQuestion;
    @BindView(R.id.vLineChallengeQuestion)
    View vLineChallengeQuestion;
    @BindView(R.id.contain_view)
    View linearContainView;
    @BindView(R.id.password_parent_view)
    View rootView;
    @BindView(R.id.flEmailMask)
    View flEmailMask;

    private FocusChangeListener emailMasker = new FocusChangeListener(MaskerHelper.INPUT.EMAIL);
    private FocusChangeListener answerMasker = new FocusChangeListener(MaskerHelper.INPUT.FREE);
    private FocusChangeListener pwdMasker = new FocusChangeListener(MaskerHelper.INPUT.FREE);
    private FocusChangeListener retryPwdMasker = new FocusChangeListener(MaskerHelper.INPUT.FREE);

    private PasswordEventsListener listener;

    private ListChallengeQuestionPresenter listChallengeQuestionPresenter;

    private Validator validator;
    private CreatePassObject createPassObject;
    private ErrorIndicatorEditText edtCurrentError = null;
    private CustomSpinner mSpinnerChallengeQuestion;
    private ForgotPasswordComparator forgotPasswordComparator;
    private DialogHelper dialogHelper;
    private ViewKeyboardListener keyboardListener;
    private Profile profile;


    public static PasswordFragment newInstance(Profile profile) {
        Bundle args = new Bundle();
        args.putParcelable(ARG_PROFILE, profile);
        PasswordFragment fragment = new PasswordFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof SignUpV2Activity) {
            listener = (PasswordEventsListener) context;
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_pass, container, false);
        return attachToSwipeBack(view);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setTitle(getString(R.string.register).toUpperCase());
        listChallengeQuestionPresenter = new ListChallengeQuestionPresenter();
        listChallengeQuestionPresenter.attach(this);
        listChallengeQuestionPresenter.getChallengeQuestions();
        dialogHelper = new DialogHelper(getActivity());
        profile = getArguments().getParcelable(ARG_PROFILE);
        if (profile == null) {
            profile = new Profile();
        }

        edtEmail.setOnFocusChangeListener(emailMasker);
        edtAnswerQuestion.setOnFocusChangeListener(answerMasker);
        edtForgotPwd.setOnFocusChangeListener(pwdMasker);
        edtForgotRetypePwd.setOnFocusChangeListener(retryPwdMasker);

        setUpView();
    }


    private void setUpView() {
        storeUp();

        validator = new Validator();
        ClickGuard.guard(btnSubmit);
        btnSubmit.post(() -> {
            btnSubmit.setEnabled(false);
            btnSubmit.setClickable(false);
        });

        InputFilter[] filtersPassword = new InputFilter[]{new PasswordInputFilter(), new InputFilter.LengthFilter(getResources().getInteger(R.integer.password_max_length_characters))};
        edtForgotPwd.setFilters(filtersPassword);
        edtForgotRetypePwd.setFilters(filtersPassword);

        keyboardInteractListener();

        forgotPasswordComparator = new ForgotPasswordComparator(
                edtEmail,
                edtAnswerQuestion,
                edtForgotPwd,
                edtForgotRetypePwd,
                btnSubmit);
        if (profile != null) {
            edtEmail.setText(MaskerHelper.mask(MaskerHelper.INPUT.EMAIL, createPassObject.email));
            emailMasker.original = createPassObject.email;
        }

        edtEmail.setEnabled(false);
        edtEmail.setClickable(false);
        edtEmail.setCursorVisible(false);
        edtEmail.setTextColor(ContextCompat.getColor(App.getInstance(), R.color.heading_gray));

        edtAnswerQuestion.setOnFocusChangeListener(answerMasker);
        edtForgotPwd.setOnFocusChangeListener(pwdMasker);
        edtForgotRetypePwd.setOnFocusChangeListener(retryPwdMasker);
    }

    /**
     * handle click on back hide keyboard close cursor
     */
    private void keyboardInteractListener() {
        ViewKeyboardListener.KeyboardEvent event = new ViewKeyboardListener.KeyboardEvent() {
            @Override
            public void showKeyboard() {
                //dummy do nothing
                scrollView.smoothScrollTo(0, (int) contentWrapper.getTop());
            }

            @Override
            public void hideKeyboard() {
                //dummy do nothing
            }

            @Override
            public View getCurrentFocus() {
                if (getActivity() == null) return null;
                return getActivity().getCurrentFocus();
            }
        };
        //-- add listen keyboard
        this.keyboardListener = new ViewKeyboardListener(rootView, event);
        this.keyboardListener.setViewFocusChangeListener(linearContainView);

    }

    @Override
    public void onStop() {
        ViewUtils.hideSoftKey(getView());
        keyboardListener.removeListener();
        super.onStop();
    }

    @OnClick({R.id.password_parent_view,
            R.id.contain_view,
            R.id.scroll_view, R.id.flEmailMask})
    public void onClickOutside(View view) {
        ViewUtils.hideSoftKey(view);
        if (getActivity().getCurrentFocus() != null) {
            getActivity().getCurrentFocus().clearFocus();
        }

        if (view.getId() == R.id.flEmailMask) {
            edtEmail.setText(createPassObject.email);
        }
    }


    //<editor-fold desc="Custom Spinner">
    private CustomSpinner setUpCustomSpinner(TextView anchor,
                                             List<SecurityQuestion> data) {
        CustomSpinner customSpinner = new CustomSpinner(getActivity(), anchor);

        List<String> questionChallenges = new ArrayList<>();
        for (SecurityQuestion item : data) {
            questionChallenges.add(item.getQuestionText());
        }

        DropdownAdapter dropdownAdapter = new DropdownAdapter(getActivity(),
                R.layout.item_challenge_question_dropdown,
                questionChallenges, 52);
        customSpinner.setDropdownAdapter(dropdownAdapter);
        customSpinner.setmSpinnerListener(spinnerListener);
        customSpinner.setPopupHeightListQuestion();
        return customSpinner;
    }

    CustomSpinner.SpinnerListener spinnerListener = new CustomSpinner.SpinnerListener() {
        @Override
        public void onArchorViewClick() {
            ViewUtils.hideSoftKey(getActivity().getCurrentFocus());
            linearContainView.clearFocus();
        }

        @Override
        public void onItemSelected(int index) {
            if (forgotPasswordComparator == null || mSpinnerChallengeQuestion == null)
                return;
            forgotPasswordComparator.handleChallengeQuestion(
                    mSpinnerChallengeQuestion.getDropdownAdapter().getItem(index));
        }
    };
    //</editor-fold>


    @OnClick(R.id.btn_submit)
    void onSubmit(View view) {
        hideKeyboardOnScreen();
        if (formValidation()) {
            ChangeSecurityQuestion.Param param = new ChangeSecurityQuestion.Param(
                    createPassObject.email,
                    createPassObject.pwdSecret,
                    createPassObject.challengeQuestion,
                    createPassObject.answer
            );
            listener.onProfileSubmit(profile, param);
        }
    }

    @Override
    public void showProgressDialog() {
        dialogHelper.showProgress();
    }

    @Override
    public void dismissProgressDialog() {
        dialogHelper.dismissProgress();
    }

    @Override
    public void showErrorMessage(ErrCode errCode, String extraMsg) {

    }

    @Override
    public void showSuccessMessage() {

    }

    @Override
    public void getChallengeQuestions(List<SecurityQuestion> questions) {
        mSpinnerChallengeQuestion = setUpCustomSpinner(tvChallengeQuestion, questions);
        mSpinnerChallengeQuestion.setNone(false);
    }

    private void storeUp() {
        if (createPassObject == null) {
            createPassObject = new CreatePassObject();
        }

        if (listChallengeQuestionPresenter != null && mSpinnerChallengeQuestion != null) {
            String questionContent = listChallengeQuestionPresenter.
                    getQuestionContent(mSpinnerChallengeQuestion.getSelectionPosition());
            createPassObject.setChallengeQuestion(questionContent);
        }


        createPassObject.setEmail(profile.getEmail());
        createPassObject.setAnswer(edtAnswerQuestion.getText().toString().trim());
        createPassObject.setPwdSecret(edtForgotPwd.getText().toString());
        createPassObject.setRetypePwdSecret(edtForgotRetypePwd.getText().toString());
    }

    private boolean formValidation() {
        hideAllErrorColorLineEditText();
        storeUp();

        edtCurrentError = null;
        List<String> listErrorContent = new ArrayList<>();

//        //-- email
//        if (!validator.email(forgotViewObject.email)) {
//            edtCurrentError = edtEmail;
//            edtEmail.showErrorColorLine();
//            if (!TextUtils.isEmpty(forgotViewObject.email)) {
//                listErrorContent.add(getString(R.string.input_err_invalid_email));
//            }
//        }//- else do nothing

        //-- Security question
        boolean selectedChallengeQuestion = true;
        if (App.getInstance().getString(R.string.forgot_password_hint_challenge_question).
                equalsIgnoreCase(createPassObject.challengeQuestion)) {
            selectedChallengeQuestion = false;
            vLineChallengeQuestion.setBackgroundColor(ContextCompat.getColor(getActivity(),
                    android.R.color.holo_red_light));
        }//- else do nothing

        //-- answer question
        if (TextUtils.isEmpty(createPassObject.answer) ||
                !validator.answerSecurity(createPassObject.answer)) {
            if (edtCurrentError == null) edtCurrentError = edtAnswerQuestion;

            listErrorContent.add(App.getInstance().getString(R.string.invalid_answer));
            edtAnswerQuestion.showErrorColorLine();
        }

        //password ( week password )
        boolean invalidNewPwd = false;

        String errorSecretRequire = getString(R.string.invalid_create_pwd_2);



        if (!validator.secretValidator(createPassObject.pwdSecret)) {
            if (edtCurrentError == null) {
                edtCurrentError = edtForgotPwd;
            } else {
                invalidNewPwd = true;
            }
            edtForgotPwd.showErrorColorLine();
            if (!TextUtils.isEmpty(createPassObject.pwdSecret)) {
                listErrorContent.add(errorSecretRequire);
            }
        }
        else if (!TextUtils.isEmpty(createPassObject.pwdSecret)
                && validator.secretRuleContain(createPassObject.pwdSecret, profile.getEmail(), profile.getFirstName(), profile.getLastName())) {
            //-- rule secret have contain text first name, last name, username
            if (edtCurrentError == null) {
                edtCurrentError = edtForgotPwd;
            } else {
                invalidNewPwd = true;
            }
            edtForgotPwd.showErrorColorLine();
            if (!listErrorContent.contains(errorSecretRequire)) {
                listErrorContent.add(errorSecretRequire);
            }
        }
        //-- reTypePassword
        if (!TextUtils.isEmpty(createPassObject.retypePwdSecret)) {
            boolean validRetypePwdCompare = validator.password(createPassObject.pwdSecret,
                    createPassObject.retypePwdSecret);
            if (!validRetypePwdCompare) {
                if (edtCurrentError == null) edtCurrentError = edtForgotRetypePwd;
                edtForgotRetypePwd.showErrorColorLine();
                listErrorContent.add(getString(R.string.input_err_pwd_confirmation_failed));
            }//-- else do nothing
        }

        //priority focus error
        if (edtCurrentError != null) {
            //case: retypePwd empty but newPwd invalidate -> focus newPwd (no focus retypePwd)
            if (invalidNewPwd && edtCurrentError.getId() == edtForgotRetypePwd.getId()) {
                edtCurrentError = edtForgotPwd;
            }
        }

        //-- message error
        StringBuilder stringBuilder = new StringBuilder();
        for (String contentError : listErrorContent) {
            stringBuilder.append(contentError).append("\n");
        }
        String errMessage = stringBuilder.toString().trim(); // remove last "\n"

        //-- handle validation
        if (edtCurrentError != null) {
            edtCurrentError.requestFocus();
            edtCurrentError.showErrorColorLine();
            edtCurrentError.setSelection(edtCurrentError.getText().length());
            showDialogInputError(errMessage);
            return false;

        } else if (!selectedChallengeQuestion) {
            return false;
        }
        return true;
    }

    private void showDialogInputError(String errMessage) {
        dialogHelper.profileDialog(errMessage, dialog -> {
            if (btnSubmit != null) {
                btnSubmit.postDelayed(this::showKeyboardFocusError, 100);
            }
        });
    }

    void showKeyboardFocusError() {
        if (edtCurrentError != null) {
            edtCurrentError.requestFocus();
            edtCurrentError.setError("");
            edtCurrentError.setCursorVisible(true);
            edtCurrentError.setSelection(edtCurrentError.getText().toString().length());
            ViewUtils.showSoftKey(edtCurrentError);
        }
    }

    private void hideAllErrorColorLineEditText() {
        edtEmail.hideErrorColorLine();
        vLineChallengeQuestion.setBackgroundColor(ContextCompat.getColor(getActivity(),
                R.color.color_separator));
        edtAnswerQuestion.hideErrorColorLine();
        edtForgotPwd.hideErrorColorLine();
        edtForgotRetypePwd.hideErrorColorLine();
    }

    private void hideKeyboardOnScreen() {
        ViewUtils.hideSoftKey(contentWrapper);
    }

    public final class CreatePassObject {

        String email;
        String challengeQuestion;
        String answer;
        String pwdSecret;
        String retypePwdSecret;

        public void setEmail(String email) {
            this.email = email;
        }

        public void setChallengeQuestion(String challengeQuestion) {
            this.challengeQuestion = challengeQuestion;
        }

        public void setAnswer(String answer) {
            this.answer = answer;
        }

        public void setPwdSecret(String pwdSecret) {
            this.pwdSecret = pwdSecret;
        }

        public void setRetypePwdSecret(String retypePwdSecret) {
            this.retypePwdSecret = retypePwdSecret;
        }
    }

    public interface PasswordEventsListener {
        void onProfileSubmit(Profile message, ChangeSecurityQuestion.Param param);

    }

    private class FocusChangeListener implements View.OnFocusChangeListener {

        MaskerHelper.INPUT input;
        private String original = "";

        FocusChangeListener(MaskerHelper.INPUT type) {
            input = type;
        }

        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            AppCompatEditText view = (AppCompatEditText) v;
            if (hasFocus) {
                view.setText(original);
                edtEmail.setText(MaskerHelper.mask(MaskerHelper.INPUT.EMAIL, emailMasker.original));
            } else {
                String text = view.getText().toString().trim();
                original = text;
                view.setText(MaskerHelper.mask(input, original));
            }
            view.setCursorVisible(hasFocus);

        }
    }
}
