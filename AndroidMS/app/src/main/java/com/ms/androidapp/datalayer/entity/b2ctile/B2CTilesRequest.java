package com.ms.androidapp.datalayer.entity.b2ctile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ms.androidapp.BuildConfig;

import java.util.Arrays;
import java.util.List;

/**
 * Created by vinh.trinh on 6/6/2017.
 */

public class B2CTilesRequest {
    @SerializedName("subDomain")
    @Expose
    public final String subDomain;
    @SerializedName("password")
    @Expose
    public final String secretKey;
    @SerializedName("categories")
    @Expose
    public final List<String> categories;

    public B2CTilesRequest(String... categories) {
        subDomain = BuildConfig.WS_B2C_SUBDOMAIN;
        secretKey = BuildConfig.WS_B2C_SECRET;
        this.categories = Arrays.asList(categories);
    }

}
