package com.ms.androidapp.datalayer.entity.askconcierge;

import com.google.gson.annotations.SerializedName;

/**
 * Created by vinh.trinh on 8/29/2017.
 */

public class ACRequestDetail {
    @SerializedName("EPCCaseID")
    private String EPCCaseID;
    @SerializedName("TransactionID")
    private String transactionID;
    @SerializedName("ConsumerKey")
    private String consumerKey;
    @SerializedName("EPCClientCode")
    private String EPCClientCode;
    @SerializedName("EPCProgramCode")
    private String EPCProgramCode;
    @SerializedName("OnlineMemberId")
    private String onlineMemberId;
    @SerializedName("Salutation")
    private String salutation;
    @SerializedName("FirstName")
    private String firstName;
    @SerializedName("LastName")
    private String lastName;
    @SerializedName("EditType")
    private String type;
    @SerializedName("Source")
    private String source;
    @SerializedName("Functionality")
    private String functionality;
    @SerializedName("RequestType")
    private String requestType;
    @SerializedName("PrefResponse")
    private String prefResponse;
    @SerializedName("PhoneNumber")
    private String phone;
    @SerializedName("EmailAddress1")
    private String email;
    @SerializedName("PickupDate")
    private String pickupDate;
    @SerializedName("NumberOfAdults")
    private Integer numberOfAdults;
    @SerializedName("RequestDetails")
    private String requestDetails;
}
