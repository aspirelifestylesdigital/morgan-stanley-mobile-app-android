package com.ms.androidapp.datalayer.entity;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ThuNguyen on 6/5/2017.
 */

public class GetContentFullResult {
    @SerializedName("ContentFull")
    private ContentFulls content;
    @SerializedName("Status")
    private String status;
    @SerializedName("Success")
    private Boolean success;

    public ContentFulls getContent() {
        return content;
    }

    public void setContent(ContentFulls content) {
        this.content = content;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }
}
