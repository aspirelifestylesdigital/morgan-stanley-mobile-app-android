package com.ms.androidapp.presentation.requestdetail;

import com.ms.androidapp.presentation.base.BasePresenter;

/**
 * Created by vinh.trinh on 9/13/2017.
 */

public interface RequestDetail {
    interface View {
        void showProgressDialog();
        void dismissProgressDialog();
        void showData(DetailViewData data);
        void showErrorMessage(String message);
        void onCancelDone();
    }
    interface Presenter extends BasePresenter<View> {
        void getRequestDetail();
        String content(boolean create);
        void dismissRequest();
    }
}
