package com.ms.androidapp.common.logic;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.os.Build;
import android.text.Editable;
import android.text.Html;
import android.text.Layout;
import android.text.Spanned;
import android.text.style.BulletSpan;
import android.text.style.LeadingMarginSpan;
import android.util.Log;
import android.util.TypedValue;

import com.ms.androidapp.App;

import org.xml.sax.XMLReader;

import java.util.Stack;


/**
 * Implements support for ordered ({@code <ol>}) and unordered ({@code <ul>}) lists in to Android TextView.
 * <p>
 * This can be used as follows:<br/>
 * {@code textView.setText(Html.fromHtml("<ul><li>item 1</li><li>item 2</li></ul>", null, new HtmlListTagHandler()));}</p>
 * <p>
 * Implementation based on code by Juha Kuitunen (https://bitbucket.org/Kuitsi/android-textview-html-list),
 * released under Apache License v2.0. Refactored & improved by Matthias Stevens (InThePocket.mobi).</p>
 * <p>
 * <b>Known issues:</b><ul>
 *     <li>The indentation on nested {@code <ul>}s isn't quite right (TODO fix this)</li>
 *     <li>the {@code start} attribute of {@code <ol>} is not supported. Doing so is tricky because
 *     {@link Html.TagHandler#handleTag(boolean, String, Editable, XMLReader)} does not expose tag attributes.
 *     The only way to do it would be to use reflection to access the attribute information kept by the XMLReader
 *     (see: http://stackoverflow.com/a/24534689/1084488).</li>
 * </ul></p>
 */
public class HtmlTagHandler implements Html.TagHandler
{
    public static final String OL_TAG = Build.VERSION.SDK_INT >= Build.VERSION_CODES.N ? "HTML_TEXTVIEW_ESCAPED_OL_TAG" : "ol";
    public static final String UL_TAG = Build.VERSION.SDK_INT >= Build.VERSION_CODES.N ? "HTML_TEXTVIEW_ESCAPED_UL_TAG" : "ul";
    public static final String LI_TAG = Build.VERSION.SDK_INT >= Build.VERSION_CODES.N ? "HTML_TEXTVIEW_ESCAPED_LI_TAG" : "li";

    /**
     * List indentation in pixels. Nested lists use multiple of this.
     */
    private static int INDENT_PX;
    private static int LIST_ITEM_INDENT_PX = INDENT_PX * 2;
    private static BulletSpan BULLET_SPAN;
    private static int BULLET_RADIUS;

    /**
     * Keeps track of lists (ol, ul). On bottom of Stack is the outermost list
     * and on top of Stack is the most nested list
     */
    private final Stack<ListTag> lists = new Stack<ListTag>();
    public HtmlTagHandler(){
        INDENT_PX = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10, App.getInstance().getResources().getDisplayMetrics());
        LIST_ITEM_INDENT_PX = INDENT_PX * 2;
        BULLET_SPAN = new BulletSpan(INDENT_PX);
        BULLET_RADIUS = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, App.getInstance().getResources().getDisplayMetrics());
    }
    /**
     * @see android.text.Html.TagHandler#handleTag(boolean, String, Editable, XMLReader)
     */
    @Override
    public void handleTag(final boolean opening, final String tag, final Editable output, final XMLReader xmlReader)
    {
        if (UL_TAG.equalsIgnoreCase(tag))
        {
            if (opening)
            {   // handle <ul>
                lists.push(new Ul());
            }
            else
            {   // handle </ul>
                lists.pop();
            }
        }
        else if (OL_TAG.equalsIgnoreCase(tag))
        {
            if (opening)
            {   // handle <ol>
                lists.push(new Ol()); // use default start index of 1
            }
            else
            {   // handle </ol>
                lists.pop();
            }
        }
        else if (LI_TAG.equalsIgnoreCase(tag))
        {
            if (opening)
            {   // handle <li>
                lists.peek().openItem(output);
            }
            else
            {   // handle </li>
                lists.peek().closeItem(output, lists.size());
            }
        }
        else
        {
            Log.d("TagHandler", "Found an unsupported tag " + tag);
        }
        if(tag.equalsIgnoreCase("html") && !opening){
            CharSequence allText = output.subSequence(0, output.length());
            output.clear();
            output.append(trimTrailingWhitespace(trimHeadingWhitespaceAndSpecialChar(allText)));
        }
    }

    /**
     * Abstract super class for {@link Ul} and {@link Ol}.
     */
    private abstract static class ListTag
    {
        /**
         * Opens a new list item.
         *
         * @param text
         */
        public void openItem(final Editable text)
        {
            if (text.length() > 0 && text.charAt(text.length() - 1) != '\n')
            {
                text.append("\n");
            }
            final int len = text.length();
            text.setSpan(this, len, len, Spanned.SPAN_MARK_MARK);
        }

        /**
         * Closes a list item.
         *
         * @param text
         * @param indentation
         */
        public final void closeItem(final Editable text, final int indentation)
        {
            if (text.length() > 0 && text.charAt(text.length() - 1) != '\n')
            {
                text.append("\n");
            }
            final Object[] replaces = getReplaces(text, indentation);
            final int len = text.length();
            final ListTag listTag = getLast(text);
            final int where = text.getSpanStart(listTag);
            text.removeSpan(listTag);
            if (where != len)
            {
                for (Object replace : replaces)
                {
                    text.setSpan(replace, where, len, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                }
            }
        }

        protected abstract Object[] getReplaces(final Editable text, final int indentation);

        /**
         * Note: This knows that the last returned object from getSpans() will be the most recently added.
         *
         * @see Html
         */
        private ListTag getLast(final Spanned text)
        {
            final ListTag[] listTags = text.getSpans(0, text.length(), ListTag.class);
            if (listTags.length == 0)
            {
                return null;
            }
            return listTags[listTags.length - 1];
        }
    }

    /**
     * Class representing the unordered list ({@code <ul>}) HTML tag.
     */
    private static class Ul extends ListTag
    {

        @Override
        protected Object[] getReplaces(final Editable text, final int indentation)
        {
            // Nested BulletSpans increases distance between BULLET_SPAN and text, so we must prevent it.
            int bulletMargin = INDENT_PX;
            if (indentation > 1)
            {
                bulletMargin = INDENT_PX - BULLET_SPAN.getLeadingMargin(true);
                if (indentation > 2)
                {
                    // This get's more complicated when we add a LeadingMarginSpan into the same line:
                    // we have also counter it's effect to BulletSpan
                    bulletMargin -= (indentation - 2) * LIST_ITEM_INDENT_PX;
                }
            }
            return new Object[] {
                    new LeadingMarginSpan.Standard(LIST_ITEM_INDENT_PX * (indentation - 1)),
                    new BulletSpanWithRadius(BULLET_RADIUS, bulletMargin)
            };
        }
    }

    /**
     * Class representing the ordered list ({@code <ol>}) HTML tag.
     */
    private static class Ol extends ListTag
    {
        private int nextIdx;

        /**
         * Creates a new {@code <ul>} with start index of 1.
         */
        public Ol()
        {
            this(1); // default start index
        }

        /**
         * Creates a new {@code <ul>} with given start index.
         *
         * @param startIdx
         */
        public Ol(final int startIdx)
        {
            this.nextIdx = startIdx;
        }

        @Override
        public void openItem(final Editable text)
        {
            super.openItem(text);
            text.append(Integer.toString(nextIdx++)).append("   \u2022 ");
        }

        @Override
        protected Object[] getReplaces(final Editable text, final int indentation)
        {
            int numberMargin = LIST_ITEM_INDENT_PX * (indentation - 1);
            if (indentation > 2)
            {
                // Same as in ordered lists: counter the effect of nested Spans
                numberMargin -= (indentation - 2) * LIST_ITEM_INDENT_PX;
            }
            return new Object[] { new LeadingMarginSpan.Standard(numberMargin) };
        }
    }
    private static class BulletSpanWithRadius implements LeadingMarginSpan {
        private final int mGapWidth;
        private final int mBulletRadius;
        private final boolean mWantColor;
        private final int mColor;

        private static Path sBulletPath = null;
        public static final int STANDARD_GAP_WIDTH = 2;
        public static final int STANDARD_BULLET_RADIUS = 4;

        public BulletSpanWithRadius() {
            mGapWidth = STANDARD_GAP_WIDTH;
            mBulletRadius = STANDARD_BULLET_RADIUS;
            mWantColor = false;
            mColor = 0;
        }

        public BulletSpanWithRadius(int gapWidth) {
            mGapWidth = gapWidth;
            mBulletRadius = STANDARD_BULLET_RADIUS;
            mWantColor = false;
            mColor = 0;
        }

        public BulletSpanWithRadius(int bulletRadius, int gapWidth) {
            mGapWidth = gapWidth;
            mBulletRadius = bulletRadius;
            mWantColor = false;
            mColor = 0;
        }

        public BulletSpanWithRadius(int bulletRadius, int gapWidth, int color) {
            mGapWidth = gapWidth;
            mBulletRadius = bulletRadius;
            mWantColor = true;
            mColor = color;
        }

        public int getLeadingMargin(boolean first) {
            return 2 * mBulletRadius + mGapWidth;
        }

        //@SuppressLint("NewApi")
        public void drawLeadingMargin(Canvas c, Paint p, int x, int dir, int top, int baseline, int bottom,
                                      CharSequence text, int start, int end, boolean first, Layout l) {
            if (((Spanned) text).getSpanStart(this) == start) {
                Paint.Style style = p.getStyle();
                int oldcolor = 0;

                if (mWantColor) {
                    oldcolor = p.getColor();
                    p.setColor(mColor);
                }

                p.setStyle(Paint.Style.FILL);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB && c.isHardwareAccelerated()) {
                    if (sBulletPath == null) {
                        sBulletPath = new Path();
                        // Bullet is slightly better to avoid aliasing artifacts on mdpi devices.
                        sBulletPath.addCircle(0.0f, 0.0f, mBulletRadius, Path.Direction.CW);
                    }

                    c.save();
                    c.translate(x + dir * (mBulletRadius + 1), (top + bottom) / 2.0f);
                    c.drawPath(sBulletPath, p);
                    c.restore();
                } else {
                    c.drawCircle(x + dir * (mBulletRadius + 1), (top + bottom) / 2.0f, mBulletRadius, p);
                }

                if (mWantColor) {
                    p.setColor(oldcolor);
                }
                p.setStyle(style);
            }
        }

    }
    public static CharSequence trimTrailingWhitespace(CharSequence source) {

        if(source == null)
            return "";

        int i = source.length();

        // loop back to the first non-whitespace character
        while(--i >= 0 && Character.isWhitespace(source.charAt(i))) {
        }

        return source.subSequence(0, i+1);
    }
    public static CharSequence trimHeadingWhitespaceAndSpecialChar(CharSequence source) {

        if(source == null)
            return "";

        int i = 0;

        // loop back to the first non-whitespace character
        while(i < source.length() && (Character.isWhitespace(source.charAt(i)) || !Character.isLetterOrDigit(source.charAt(i)))) {
            i++;
        }

        return source.subSequence(i, source.length());
    }

}
