package com.ms.androidapp.presentation.profile.forgotPwdV2;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.api.aspire.common.constant.ErrCode;
import com.api.aspire.domain.model.SecurityQuestion;
import com.ms.androidapp.App;
import com.ms.androidapp.R;
import com.ms.androidapp.common.constant.AppConstant;
import com.ms.androidapp.common.constant.IntentConstant;
import com.ms.androidapp.common.logic.Validator;
import com.ms.androidapp.presentation.base.CommonActivity;
import com.ms.androidapp.presentation.challengequestion.ListChallengeQuestion;
import com.ms.androidapp.presentation.challengequestion.ListChallengeQuestionPresenter;
import com.ms.androidapp.presentation.widget.CustomSpinner;
import com.ms.androidapp.presentation.widget.DialogHelper;
import com.ms.androidapp.presentation.widget.DropdownAdapter;
import com.ms.androidapp.presentation.widget.PasswordInputFilter;
import com.ms.androidapp.presentation.widget.ViewKeyboardListener;
import com.ms.androidapp.presentation.widget.ViewUtils;
import com.support.mylibrary.widget.ClickGuard;
import com.support.mylibrary.widget.ErrorIndicatorEditText;
import com.support.mylibrary.widget.LetterSpacingTextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ForgotPasswordV2Activity extends CommonActivity implements ForgotPasswordV2.View,
        ListChallengeQuestion.View {

    @BindView(R.id.edt_email)
    ErrorIndicatorEditText edtEmail;
    @BindView(R.id.edt_answer_question)
    ErrorIndicatorEditText edtAnswerQuestion;
    @BindView(R.id.edt_forgot_pwd)
    ErrorIndicatorEditText edtForgotPwd;
    @BindView(R.id.edt_retype_pwd)
    ErrorIndicatorEditText edtForgotRetypePwd;
    @BindView(R.id.content_wrapper)
    LinearLayout contentWrapper;
    @BindView(R.id.scroll_view)
    ScrollView scrollView;
    @BindView(R.id.btn_submit)
    Button btnSubmit;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tvChallengeQuestion)
    LetterSpacingTextView tvChallengeQuestion;
    @BindView(R.id.vLineChallengeQuestion)
    View vLineChallengeQuestion;
    @BindView(R.id.contain_view)
    View linearContainView;
    @BindView(R.id.forgot_pwd_parent_view)
    View rootView;

    private ForgotPasswordV2Presenter presenter;
    private ListChallengeQuestionPresenter listChallengeQuestionPresenter;

    private DialogHelper dialogHelper;
    private Validator validator;
    private ForgotPwdViewObject forgotViewObject = new ForgotPwdViewObject();
    private ErrorIndicatorEditText edtCurrentError = null;
    private CustomSpinner mSpinnerChallengeQuestion;
    private ForgotPasswordComparator forgotPasswordComparator;

    private ForgotPasswordV2Presenter buildPresenter() {
        return new ForgotPasswordV2Presenter();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        setContentView(R.layout.activity_forgot_password_v2);
        ButterKnife.bind(this);
        setTitle(R.string.forgot_password_screen);
        presenter = buildPresenter();
        presenter.attach(this);
        listChallengeQuestionPresenter = new ListChallengeQuestionPresenter();
        listChallengeQuestionPresenter.attach(this);
        listChallengeQuestionPresenter.getChallengeQuestions();
        dialogHelper = new DialogHelper(this);
        setUpView();

        if (savedInstanceState == null) {
            // Track GA
            App.getInstance().track(AppConstant.GA_TRACKING_SCREEN_NAME.FORGOT_PASSWORD.getValue());
        }

    }

    private void setUpView() {
        validator = new Validator();
        toolbar.setClickable(true);
        ClickGuard.guard(btnSubmit);
        btnSubmit.post(() -> {
            btnSubmit.setEnabled(false);
            btnSubmit.setClickable(false);
        });

        InputFilter[] filtersPassword = new InputFilter[]{new PasswordInputFilter(), new InputFilter.LengthFilter(getResources().getInteger(R.integer.password_max_length_characters))};
        edtForgotPwd.setFilters(filtersPassword);
        edtForgotRetypePwd.setFilters(filtersPassword);

        keyboardInteractListener();

        forgotPasswordComparator = new ForgotPasswordComparator(
                edtEmail,
                edtAnswerQuestion,
                edtForgotPwd,
                edtForgotRetypePwd,
                btnSubmit);

        hideToolbarElevation();
        scrollView.postDelayed(() -> scrollView.smoothScrollTo(0, (int) linearContainView.getTop()), 150);
    }

    /** handle click on back hide keyboard close cursor */
    private void keyboardInteractListener() {
        ViewKeyboardListener.KeyboardEvent event = new ViewKeyboardListener.KeyboardEvent() {
            @Override
            public void showKeyboard() {
                //dummy do nothing
                scrollView.smoothScrollTo(0, (int) contentWrapper.getTop());
            }

            @Override
            public void hideKeyboard() {
                //dummy do nothing
                scrollView.smoothScrollTo(0, (int) linearContainView.getTop());
            }

            @Override
            public View getCurrentFocus() {
                return ForgotPasswordV2Activity.this.getCurrentFocus();
            }
        };
        //-- add listen keyboard
        ViewKeyboardListener keyboardListener = new ViewKeyboardListener(rootView, event);
        keyboardListener.setViewFocusChangeListener(linearContainView);

    }

    @OnClick({R.id.forgot_pwd_parent_view, R.id.contain_view, R.id.scroll_view, R.id.toolbar, R.id.forgot_pwd_rootview, R.id.linearBottomContainer})
    public void onClickOutside(View view) {
        ViewUtils.hideSoftKey(view);
        if(getCurrentFocus() != null){
            getCurrentFocus().clearFocus();
        }
    }

    @Override
    protected void onDestroy() {
        presenter.detach();
        listChallengeQuestionPresenter.detach();
        super.onDestroy();
    }

    //<editor-fold desc="Custom Spinner">
    private CustomSpinner setUpCustomSpinner(TextView anchor,
                                             List<SecurityQuestion> data) {
        CustomSpinner customSpinner = new CustomSpinner(this, anchor);

        List<String> questionChallenges = new ArrayList<>();
        for (SecurityQuestion item : data) {
            questionChallenges.add(item.getQuestionText());
        }

        DropdownAdapter dropdownAdapter = new DropdownAdapter(this,
                R.layout.item_dropdown,
                questionChallenges,52);
        customSpinner.setDropdownAdapter(dropdownAdapter);
        customSpinner.setmSpinnerListener(spinnerListener);
        customSpinner.setPopupHeightListQuestion();
        return customSpinner;
    }

    CustomSpinner.SpinnerListener spinnerListener = new CustomSpinner.SpinnerListener() {
        @Override
        public void onArchorViewClick() {
            hideKeyboardOnScreen();
            linearContainView.clearFocus();
        }

        @Override
        public void onItemSelected(int index) {
            if (forgotPasswordComparator == null || mSpinnerChallengeQuestion == null)
                return;
            forgotPasswordComparator.handleChallengeQuestion(mSpinnerChallengeQuestion.getDropdownAdapter().getItem(index));
        }
    };
    //</editor-fold>

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            hideKeyboardOnScreen();
            super.onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.btn_submit)
    void onSubmit(View view) {
        hideKeyboardOnScreen();
        if (formValidation()) {
            showProgressDialog();
            presenter.retrievePassword(forgotViewObject.email,
                    forgotViewObject.challengeQuestion,
                    forgotViewObject.answer,
                    forgotViewObject.pwdSecret
            );
        }// else do nothing
    }

    @Override
    public void showProgressDialog() {
        dialogHelper.showProgress();
    }

    @Override
    public void dismissProgressDialog() {
        dialogHelper.dismissProgress();
    }

    @Override
    public void showErrorMessage(ErrCode errCode, String extraMsg) {
        if (dialogHelper.networkUnavailability(errCode, extraMsg)) return;
        /*
        //-- case
        else if (ErrCode.FORGOT_PASSWORD_NOT_ALLOWED.name().equalsIgnoreCase(extraMsg)) {
            dialogHelper.alert(getString(R.string.errorTitle),
                    getString(R.string.forgot_password_error_not_allowed_user));
        }
        */
        if(ErrCode.PROFILE_OKTA_LOCKED_OUT.name().equals(extraMsg)
                || ErrCode.FORGOT_PASSWORD_NOT_ALLOWED.name().equalsIgnoreCase(extraMsg)){
            dialogHelper.alert(getString(R.string.errorTitle),
                    App.getInstance().getString(R.string.valid_login_okta_profile_error));
        } else if (ErrCode.USER_PROFILE_OKTA_NOT_EXIST.name().equalsIgnoreCase(extraMsg)) {
            //-i.
            showDialogErrorAllField(getString(R.string.forgot_password_error_email));

        } else if (ErrCode.FORGOT_PASSWORD_WRONG_ANSWER.name().equalsIgnoreCase(extraMsg)) {

            //-v/ vi
            showDialogErrorRetainEmailField(getString(R.string.forgot_password_error_answer));

        } else if (ErrCode.FORGOT_PASSWORD_PASSWORD_NOT_MEET_RULE.name().equalsIgnoreCase(extraMsg)
                || ErrCode.FORGOT_PASSWORD_CHANGE_RECENTLY.name().equalsIgnoreCase(extraMsg)
                || ErrCode.FORGOT_PASSWORD_CANNOT_CURRENT.name().equalsIgnoreCase(extraMsg)) {
            //-vii.
            dialogHelper.alert(getString(R.string.invalid_create_pwd_title), getString(R.string.invalid_create_pwd),
                    dialog -> {
                        if (edtForgotPwd != null) {
                            edtForgotPwd.postDelayed(() -> {
                                edtForgotPwd.requestFocus();
                                edtForgotPwd.showErrorColorLine();
                                edtForgotPwd.setSelection(edtForgotPwd.getText().length());
                                ViewUtils.showSoftKey(edtForgotPwd);
                            }, 150);
                        }
                    });

        } else {
            //-v/ vi // ErrCode.FORGOT_PASSWORD_WRONG_QUESTION
            showDialogErrorRetainEmailField(getString(R.string.forgot_password_error_answer));
        }
    }

    private void showDialogTitleErrorAllField(String title, String message) {
        dialogHelper.action(title, message,
                getString(R.string.text_yes),
                getString(R.string.text_no),
                (dialog, which) -> {
                    dialog.dismiss();
                    handleErrorNoIssuesOnView();
                }, (dialog, which) -> onBackPressed()
        );
    }

    private void showDialogErrorRetainEmailField(String message) {
        dialogHelper.action(getString(R.string.errorTitle), message,
                getString(R.string.text_yes),
                getString(R.string.text_no),
                (dialog, which) -> {
                    dialog.dismiss();
                    handleErrorAnswerOnView();
                }, (dialog, which) -> onBackPressed()
        );
    }

    private void showDialogErrorAllField(String message) {
        showDialogTitleErrorAllField(getString(R.string.errorTitle), message);
    }

    /** remove all field */
    private void handleErrorNoIssuesOnView(){
        hideAllErrorColorLineEditText();
        edtEmail.setText("");
        mSpinnerChallengeQuestion.setDefaultUnSelect(getString(R.string.forgot_password_hint_challenge_question));
        mSpinnerChallengeQuestion.setCurrentSelected();
        edtAnswerQuestion.setText("");
        edtForgotPwd.setText("");
        edtForgotRetypePwd.setText("");
        forgotPasswordComparator.resetChallengeQuestion();
    }

    /**  The email address is retained, but all other fields are removed. */
    private void handleErrorAnswerOnView(){
        hideAllErrorColorLineEditText();
        mSpinnerChallengeQuestion.setDefaultUnSelect(getString(R.string.forgot_password_hint_challenge_question));
        mSpinnerChallengeQuestion.setCurrentSelected();
        edtAnswerQuestion.setText("");
        edtForgotPwd.setText("");
        edtForgotRetypePwd.setText("");
        forgotPasswordComparator.resetChallengeQuestion();
    }

    @Override
    public void showSuccessMessage() {
        dialogHelper.alert(null, getString(R.string.forgot_password_success), dialog -> returnSignInScreen());
    }

    public void returnSignInScreen(){
        Intent intent = new Intent();
        intent.putExtra(IntentConstant.EMAIL_VALUES_FORGOT,edtEmail.getText().toString());
        setResult(RESULT_OK,intent);
        onBackPressed();
    }

    @Override
    public void onViewPositionChanged(float fractionAnchor, float fractionScreen) {
        if (fractionScreen == 1.0) hideKeyboardOnScreen();
        super.onViewPositionChanged(fractionAnchor, fractionScreen);
    }

    @Override
    public void getChallengeQuestions(List<SecurityQuestion> questions) {
        mSpinnerChallengeQuestion = setUpCustomSpinner(tvChallengeQuestion, questions);
        mSpinnerChallengeQuestion.setNone(false);
    }

    private void storeUp() {
        if (forgotViewObject == null) {
            forgotViewObject = new ForgotPwdViewObject();
        }

        String questionContent = listChallengeQuestionPresenter.getQuestionContent(mSpinnerChallengeQuestion.getSelectionPosition());

        forgotViewObject.setEmail(edtEmail.getText().toString().trim());
        forgotViewObject.setChallengeQuestion(questionContent);
        forgotViewObject.setAnswer(edtAnswerQuestion.getText().toString().trim());
        forgotViewObject.setPwdSecret(edtForgotPwd.getText().toString());
        forgotViewObject.setRetypePwdSecret(edtForgotRetypePwd.getText().toString());
    }

    private boolean formValidation() {
        hideAllErrorColorLineEditText();
        storeUp();

        edtCurrentError = null;
        List<String> listErrorContent = new ArrayList<>();

        //-- email
        if (!validator.email(forgotViewObject.email)) {
            edtCurrentError = edtEmail;
            edtEmail.showErrorColorLine();
            if (!TextUtils.isEmpty(forgotViewObject.email)) {
                listErrorContent.add(getString(R.string.input_err_invalid_email));
            }
        }//- else do nothing

        //-- Security question
        boolean selectedChallengeQuestion = true;
        if (getString(R.string.forgot_password_hint_challenge_question).equalsIgnoreCase(forgotViewObject.challengeQuestion)) {
            selectedChallengeQuestion = false;
            vLineChallengeQuestion.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), android.R.color.holo_red_light));
        }//- else do nothing

        //-- answer question
        if (TextUtils.isEmpty(forgotViewObject.answer)
                || !validator.answerSecurity(forgotViewObject.answer)) {
            if (edtCurrentError == null) edtCurrentError = edtAnswerQuestion;
            listErrorContent.add(getString(R.string.invalid_answer));
            edtAnswerQuestion.showErrorColorLine();
        }

        //password ( week password )
        boolean invalidNewPwd = false;

        if (!validator.secretValidatorWeekPassword(forgotViewObject.pwdSecret)) {
            if (edtCurrentError == null) {
                edtCurrentError = edtForgotPwd;
            } else {
                invalidNewPwd = true;
            }
            edtForgotPwd.showErrorColorLine();
            if (!TextUtils.isEmpty(forgotViewObject.pwdSecret)) {
                listErrorContent.add(getString(R.string.invalid_create_pwd_2));
            }
        }
        //-- reTypePassword
        if (!TextUtils.isEmpty(forgotViewObject.retypePwdSecret)) {
            boolean validRetypePwdCompare = validator.password(forgotViewObject.pwdSecret, forgotViewObject.retypePwdSecret);
            if (!validRetypePwdCompare) {
                if (edtCurrentError == null) edtCurrentError = edtForgotRetypePwd;
                edtForgotRetypePwd.showErrorColorLine();
                listErrorContent.add(getString(R.string.input_err_pwd_confirmation_failed));
            }//-- else do nothing
        }

        //priority focus error
        if (edtCurrentError != null) {
            //case: retypePwd empty but newPwd invalidate -> focus newPwd (no focus retypePwd)
            if (invalidNewPwd && edtCurrentError.getId() == edtForgotRetypePwd.getId()) {
                edtCurrentError = edtForgotPwd;
            }
        }

        //-- message error
        StringBuilder stringBuilder = new StringBuilder();
        for (String contentError : listErrorContent) {
            stringBuilder.append(contentError).append("\n");
        }
        String errMessage = stringBuilder.toString().trim(); // remove last "\n"

        //-- handle validation
        if (edtCurrentError != null) {
            edtCurrentError.requestFocus();
            edtCurrentError.showErrorColorLine();
            edtCurrentError.setSelection(edtCurrentError.getText().length());
            showDialogInputError(errMessage);
            return false;

        } else if (!selectedChallengeQuestion) {
            return false;
        }
        return true;
    }

    private void showDialogInputError(String errMessage) {
        dialogHelper.profileDialog(errMessage, dialog -> {
            if (btnSubmit != null) {
                btnSubmit.postDelayed(this::showKeyboardFocusError, 100);
            }
        });
    }

    void showKeyboardFocusError() {
        if (edtCurrentError != null) {
            edtCurrentError.requestFocus();
            edtCurrentError.setError("");
            edtCurrentError.setCursorVisible(true);
            edtCurrentError.setSelection(edtCurrentError.getText().toString().length());
            ViewUtils.showSoftKey(edtCurrentError);
        }
    }

    private void hideAllErrorColorLineEditText() {
        edtEmail.hideErrorColorLine();
        vLineChallengeQuestion.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.color_separator));
        edtAnswerQuestion.hideErrorColorLine();
        edtForgotPwd.hideErrorColorLine();
        edtForgotRetypePwd.hideErrorColorLine();
    }

    private void hideKeyboardOnScreen() {
        ViewUtils.hideSoftKey(contentWrapper);
    }

    private final class ForgotPwdViewObject {

        String email;
        String challengeQuestion;
        String answer;
        String pwdSecret;
        String retypePwdSecret;

        public void setEmail(String email) {
            this.email = email;
        }

        public void setChallengeQuestion(String challengeQuestion) {
            this.challengeQuestion = challengeQuestion;
        }

        public void setAnswer(String answer) {
            this.answer = answer;
        }

        public void setPwdSecret(String pwdSecret) {
            this.pwdSecret = pwdSecret;
        }

        public void setRetypePwdSecret(String retypePwdSecret) {
            this.retypePwdSecret = retypePwdSecret;
        }
    }
}
