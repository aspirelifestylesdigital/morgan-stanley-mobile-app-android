package com.ms.androidapp.presentation.checkout;


import com.api.aspire.common.constant.ErrCode;
import com.api.aspire.domain.model.ProfileAspire;
import com.ms.androidapp.presentation.base.BasePresenter;

public interface PassCodeCheckout {

    interface View {
        void showErrorDialog(ErrCode errCode, String extraMsg);
        void showProgressDialog();
        void dismissProgressDialog();
        void onCheckPassCodeSuccessfully();
        void onCheckPassCodeFailure();
        void onCheckPassCodeNone();
        void updatePassCodeApiCompleted();

        void onErrorGetToken();
    }

    interface Presenter extends BasePresenter<View> {
        void checkoutPassCode(String input);
        void handleSplashCheckPassCode();
        void handleSaveUserPrefPassCode(String newPassCode);

        void handleUpdateBinCodeSignIn(String passCode, ProfileAspire profileSignIn);
    }

}
