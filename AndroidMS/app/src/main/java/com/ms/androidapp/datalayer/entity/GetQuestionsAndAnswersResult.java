package com.ms.androidapp.datalayer.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by tung.phan on 6/1/2017.
 */

public class GetQuestionsAndAnswersResult {

    @SerializedName("QuestionsAndAnswers")
    @Expose
    private List<QuestionsAndAnswer> questionsAndAnswers = null;
    @SerializedName("Status")
    @Expose
    private String status;
    @SerializedName("Success")
    @Expose
    private Boolean success;

    public List<QuestionsAndAnswer> getQuestionsAndAnswers() {
        return questionsAndAnswers;
    }

    public void setQuestionsAndAnswers(List<QuestionsAndAnswer> questionsAndAnswers) {
        this.questionsAndAnswers = questionsAndAnswers;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

}
