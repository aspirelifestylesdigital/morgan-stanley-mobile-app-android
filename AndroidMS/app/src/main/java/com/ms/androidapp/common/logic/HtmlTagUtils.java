package com.ms.androidapp.common.logic;

/**
 * Created by ThuNguyen on 7/12/2017.
 */

public class HtmlTagUtils {
    public static String overrideTags(String html){

        if (html == null) return null;

        html = html.replace("<ul", /*(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N ? "<br/>" : "") +*/ "<" + HtmlTagHandler.UL_TAG);
        html = html.replace("</ul>", "</" + HtmlTagHandler.UL_TAG + ">");
        html = html.replace("<ol", /*(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N ? "<br/>" : "") +*/ "<" + HtmlTagHandler.OL_TAG);
        html = html.replace("</ol>", "</" + HtmlTagHandler.OL_TAG + ">");
        html = html.replace("<li", "<" + HtmlTagHandler.LI_TAG);
        html = html.replace("</li>", "</" + HtmlTagHandler.LI_TAG + ">");

        return html;
    }
}
