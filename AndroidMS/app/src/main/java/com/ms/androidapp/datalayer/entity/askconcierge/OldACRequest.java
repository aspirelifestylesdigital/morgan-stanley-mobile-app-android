package com.ms.androidapp.datalayer.entity.askconcierge;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;
import com.ms.androidapp.domain.model.Profile;

/**
 * Created by vinh.trinh on 8/31/2017.
 */

public class OldACRequest {
    @SerializedName("newCaseRequest")
    NewCaseRequest body;

    public OldACRequest(Profile profile, String memberID, String requestDetails, String city, String type) {
        String salutation = TextUtils.isEmpty(profile.getSalutation()) ? "N/A" : profile.getSalutation();
        String firstName = profile.getFirstName();
        String lastName = profile.getLastName();
        String emailAddress = profile.getEmail();
        String phoneNumber = profile.getPhone();
        body = new NewCaseRequest(memberID, salutation, firstName, lastName, emailAddress,
                phoneNumber, city, type, requestDetails);
    }

    class NewCaseRequest {
        @SerializedName("accessToken")
        AccessToken accessToken;
        @SerializedName("memberId")
        String memberID;
        @SerializedName("salutation")
        String salutation;
        @SerializedName("firstName")
        String firstName;
        @SerializedName("lastName")
        String lastName;
        @SerializedName("emailAddress")
        String emailAddress;
        @SerializedName("phoneNumber")
        String phoneNumber;
        @SerializedName("requestCity")
        String requestCity;
        @SerializedName("requestType")
        String requestType;
        @SerializedName("requestDetails")
        String requestDetails;

        NewCaseRequest(String memberID, String salutation, String firstName, String lastName,
                       String emailAddress, String phoneNumber, String requestCity,
                       String requestType, String requestDetails) {
            this.memberID = memberID;
            this.salutation = salutation;
            this.firstName = firstName;
            this.lastName = lastName;
            this.emailAddress = emailAddress;
            this.phoneNumber = phoneNumber;
            this.requestCity = "N/A";
            this.requestType = "N/A";
            this.requestDetails = requestDetails;
            this.accessToken = new AccessToken();
        }
    }

    class AccessToken {
        @SerializedName("applicationName")
        String appName;
        @SerializedName("programPassword")
        String secretKey;
        @SerializedName("licenseKey")
        LicenseKey licenseKey;
        AccessToken() {
            appName = "demo.vipdesk.com";
            secretKey = "B0D45CDA-E3D9-490C-A2F1-975D5ACA0519";
            licenseKey = new LicenseKey("D8FE24E1-E0D6-4EED-BCE1-231D6CB7F183");
        }
    }

    class LicenseKey {
        @SerializedName("key")
        String key;
        LicenseKey(String key) {
            this.key = key;
        }
    }
}
