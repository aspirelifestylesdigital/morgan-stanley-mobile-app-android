package com.ms.androidapp.common.constant;

/**
 * Created by tung.phan on 6/1/2017.
 */

public interface ResultCode {

    int RESULT_OK = -1;
    int RESULT_CANCELED = 0;
    int RESULT_OK_WITH_ID = 1;

}
