package com.ms.androidapp.presentation.selectcity;

        import com.ms.androidapp.presentation.base.BasePresenter;

/**
 * Created by tung.phan on 5/8/2017.
 */

public interface SelectCity {
    interface View {

    }

    interface Presenter extends BasePresenter<View> {
        void saveSelectCity(String cityName);
    }
}
