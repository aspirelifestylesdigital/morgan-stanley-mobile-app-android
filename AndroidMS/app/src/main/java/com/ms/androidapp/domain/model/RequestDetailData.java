package com.ms.androidapp.domain.model;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by vinh.trinh on 9/13/2017.
 */

public class RequestDetailData implements Parcelable{
    public final String transactionID;
    public final String date;
    public final String requestType;
    public final String functionality;
    public final String eventDate;
    public final String pickupDate;
    public final String startDate;
    public final String endDate;
    public String city;
    public String state;
    public String country;
    public final String detail;
    public final int numberOfAdults;
    public final int numberOfChildren;
    public final String responseMode;
    public final boolean open;
    private JSONObject jsonDetail;

    public RequestDetailData(String transactionID, String requestType, String functionality, String date,
                             String eventDate, String pickupDate, String startDate, String endDate,
                             Integer numberOfAdults, Integer numberOfChildren, String detail,
                             String responseMode, boolean open) {
        this.transactionID = transactionID;
        this.requestType = requestType;
        this.functionality = functionality;
        this.date = date;
        this.eventDate = eventDate;
        this.pickupDate = pickupDate;
        this.startDate = startDate;
        this.endDate = endDate;
        this.numberOfAdults = numberOfAdults;
        this.numberOfChildren = numberOfChildren;
        this.detail = detail;
        this.responseMode = responseMode;
        this.open = open;
    }
    public RequestDetailData setCity(String city){
        this.city = city;
        return this;
    }
    public RequestDetailData setState(String state){
        this.state = state;
        return this;
    }
    public RequestDetailData setCountry(String country){
        this.country = country;
        return this;
    }
    public JSONObject detail() {
        return jsonDetail;
    }

    public void buildJsonDetail() throws JSONException {
        jsonDetail = new JSONObject();
        String[] data = detail.split("\\|");
        for (String d : data) {
            String datum[] = d.split(":", 2);
            if (datum.length == 2) {
                jsonDetail.put(datum[0].toLowerCase().trim(), datum[1]);
            }
        }
    }

    protected RequestDetailData(Parcel in) {
        transactionID = in.readString();
        requestType = in.readString();
        functionality = in.readString();
        date = in.readString();
        eventDate = in.readString();
        pickupDate = in.readString();
        startDate = in.readString();
        endDate = in.readString();
        city = in.readString();
        state = in.readString();
        country = in.readString();
        numberOfAdults = in.readInt();
        numberOfChildren = in.readInt();
        detail = in.readString();
        responseMode = in.readString();
        open = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(transactionID);
        dest.writeString(requestType);
        dest.writeString(functionality);
        dest.writeString(date);
        dest.writeString(eventDate);
        dest.writeString(pickupDate);
        dest.writeString(startDate);
        dest.writeString(endDate);
        dest.writeString(city);
        dest.writeString(state);
        dest.writeString(country);
        dest.writeInt(numberOfAdults);
        dest.writeInt(numberOfChildren);
        dest.writeString(detail);
        dest.writeString(responseMode);
        dest.writeByte((byte) (open ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<RequestDetailData> CREATOR = new Creator<RequestDetailData>() {
        @Override
        public RequestDetailData createFromParcel(Parcel in) {
            return new RequestDetailData(in);
        }

        @Override
        public RequestDetailData[] newArray(int size) {
            return new RequestDetailData[size];
        }
    };
}