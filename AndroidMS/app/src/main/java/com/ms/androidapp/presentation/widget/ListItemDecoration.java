package com.ms.androidapp.presentation.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.ms.androidapp.R;

/**
 * Created by vinh.trinh on 6/7/2017.
 */

public class ListItemDecoration extends RecyclerView.ItemDecoration {

    private Drawable divider;
    private int adapterItemCount = -1;

    public ListItemDecoration(Context context) {
//        divider = ContextCompat.getDrawable(context, R.drawable.divider_drawable);
        divider = ContextCompat.getDrawable(context, R.drawable.line_divider);
    }

    @Override
    public void onDrawOver(Canvas c, RecyclerView parent, RecyclerView.State state) {
        int left = parent.getPaddingLeft();
        int right = parent.getWidth() - parent.getPaddingRight();

        int childCount = parent.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View child = parent.getChildAt(i);
            int adapterPosition = parent.getChildAdapterPosition(child);
            if(adapterPosition != 0 && adapterPosition == adapterItemCount -1) {
                continue;
            }

            RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();

            int top = child.getBottom() + params.bottomMargin;
            int bottom = top + divider.getIntrinsicHeight();

            divider.setBounds(left, top, right, bottom);
            divider.draw(c);
        }
    }

    public void setAdapterItemCount(int count) {
        this.adapterItemCount = count;
    }
}
