package com.ms.androidapp.presentation.explore;

import android.location.Location;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.ms.androidapp.datalayer.datasource.AppGeoCoder;
import com.ms.androidapp.domain.model.LatLng;
import com.ms.androidapp.domain.model.explore.ExploreRViewItem;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Admin on 7/30/2017.
 */

class DiningSorting {
    static final float TEN_MILES_IN_METERS = 16093.4f;
    static final float FIFTY_MILES_IN_METERS = 5* TEN_MILES_IN_METERS;

    static List<ExploreRViewItem> sort(List<ExploreRViewItem> data, DiningSortingCriteria criteria) {
        String userFullAddress = null;
        if(criteria.location != null) {
            AppGeoCoder appGeoCoder = new AppGeoCoder();
            userFullAddress = appGeoCoder.getFormattedAddress(criteria.location);
        }
        String finalUserFullAddress = userFullAddress;
        Collections.sort(data, new Comparator<ExploreRViewItem>() {
            @Override
            public int compare(ExploreRViewItem exploreRViewItem, ExploreRViewItem t1) {
                // Criteria priority: Cuisine > Nearby > Alphabet
                boolean isPreferredCuisineForLeft = isPreferredCuisine(exploreRViewItem, criteria);
                boolean isPreferredCuisineForRight = isPreferredCuisine(t1, criteria);
                if(isPreferredCuisineForLeft == isPreferredCuisineForRight){
                    boolean isNearbyForLeft = isNearbyRestaurant(exploreRViewItem, finalUserFullAddress, criteria);
                    boolean isNearbyForRight = isNearbyRestaurant(t1, finalUserFullAddress, criteria);
                    if(isNearbyForLeft == isNearbyForRight){
                        // Sort based on alphabet so do nothing
                    }else if(isNearbyForLeft){
                        return -1; // Left on top
                    }else{
                        return 1; // Right on top
                    }
                }else if(isPreferredCuisineForLeft){
                    return -1; // Left on top
                }else{
                    return 1; // Right on top
                }
                return exploreRViewItem.compareTo(t1); // Default for alphabet sort
            }
        });
        return data;
        /*sortByAlphabe(data);
        List<ExploreRViewItem> dataWithCuisine = new ArrayList<>();
        if(!TextUtils.isEmpty(criteria.cuisine)) {
            filterCuisineList(dataWithCuisine, data, criteria.cuisine.toLowerCase());
            data.removeAll(dataWithCuisine);
        }
        if(dataWithCuisine.isEmpty()) {
            dataWithCuisine.addAll(data);
            data.removeAll(dataWithCuisine);
        }
        if(criteria.location != null) {
            //data = sortByDistance(data, criteria.location);
            List<ExploreRViewItem> sortedDataWithCuisine = sortByDistance(dataWithCuisine, criteria.location, criteria.isRegion);
            dataWithCuisine.removeAll(sortedDataWithCuisine);
            dataWithCuisine.addAll(0, sortedDataWithCuisine);
            List<ExploreRViewItem> sortedData = sortByDistance(data, criteria.location, criteria.isRegion);
            data.removeAll(sortedData);
            data.addAll(0, sortedData);
        }
        dataWithCuisine.addAll(data);
        return dataWithCuisine;*/
    }
    private static boolean isNearbyRestaurant(ExploreRViewItem item, String userFullAddress, DiningSortingCriteria criteria){
        if(criteria.location != null && !TextUtils.isEmpty(userFullAddress)
                && item.getSortingCriteria() != null && item.getSortingCriteria().location != null){
            float[] result = new float[1];
            Location.distanceBetween(item.getSortingCriteria().location.latitude, item.getSortingCriteria().location.longitude, criteria.location.latitude, criteria.location.longitude, result);
            boolean isSameCity = item.getSortingCriteria().isSameCity(userFullAddress);
            if(criteria.isRegion){
                return result[0] <= FIFTY_MILES_IN_METERS;
            }
            if(isSameCity){
                return result[0] <= TEN_MILES_IN_METERS;
            }
            return result[0] <= FIFTY_MILES_IN_METERS;
        }
        return false;
    }
    private static boolean isPreferredCuisine(ExploreRViewItem item, DiningSortingCriteria criteria){
        if(!TextUtils.isEmpty(criteria.cuisine)){
            DiningSortingCriteria itemCriteria = item.getSortingCriteria();
            if(itemCriteria != null && itemCriteria.cuisine != null &&
                    itemCriteria.cuisine.toLowerCase().contains(criteria.cuisine.toLowerCase())){
                return true;
            }
        }
        return false;
    }
    private static void filterCuisineList(List<ExploreRViewItem> desireCuisineList, List<ExploreRViewItem> data, String cuisine) {
        for (int i = 0; i < data.size(); i++) {
            DiningSortingCriteria dsc = data.get(i).getSortingCriteria();
            if(dsc == null) continue;
            String itemCuisine = dsc.cuisine;
            if(itemCuisine != null && itemCuisine.toLowerCase().contains(cuisine)) {
                desireCuisineList.add(data.get(i));
            }
        }
    }

    private static List<ExploreRViewItem> sortByDistance(List<ExploreRViewItem> data, LatLng point, boolean isRegion) {
        float[] result = new float[1];
        int size = data.size();
        List<DistanceCriteria> sorted = new ArrayList<>(size);
        for (int i = 0; i < size; i++) {
            DiningSortingCriteria dsc = data.get(i).getSortingCriteria();
            if(dsc == null) continue;
            LatLng target = dsc.location;
            Location.distanceBetween(target.latitude, target.longitude, point.latitude, point.longitude, result);
//            dsc.distance = result[0] / 1609.34f;
            sorted.add(new DistanceCriteria(result[0], i));
        }
        Collections.sort(sorted);
        final float allowedDistance = isRegion ? 1609.34f*5 : 16093.4f;
        size = sorted.size();
        for (int i = size-1; i >= 0; i--) {
            DistanceCriteria dc = sorted.get(i);
            if (dc.distance > allowedDistance) {
                sorted.remove(i);
            }
        }
        List<ExploreRViewItem> sortedData = new ArrayList<>();
        for (DistanceCriteria d : sorted) {
            sortedData.add(data.get(d.index));
        }
        return sortedData;
    }

    private static void sortByAlphabe(List<ExploreRViewItem> data) {
        Collections.sort(data);
    }

    private static class DistanceCriteria implements Comparable<DistanceCriteria> {
        Float distance;
        int index;

        DistanceCriteria(Float distance, int index) {
            this.distance = distance;
            this.index = index;
        }

        @Override
        public int compareTo(@NonNull DistanceCriteria another) {
            return distance.compareTo(another.distance);
        }
    }
}