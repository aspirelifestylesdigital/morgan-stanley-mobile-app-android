package com.ms.androidapp.domain.usecases;

import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;

import com.ms.androidapp.App;
import com.ms.androidapp.R;
import com.ms.androidapp.common.logic.ColorCodeGetter;
import com.ms.androidapp.datalayer.entity.Tiles;
import com.ms.androidapp.domain.model.GalleryViewPagerItem;
import com.ms.androidapp.domain.repository.B2CRepository;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;

/**
 * Created by vinh.trinh on 6/22/2017.
 */

public class GetGalleries extends UseCase<List<GalleryViewPagerItem>, Void> {

    private B2CRepository b2CRepository;

    public GetGalleries(B2CRepository b2CRepository) {
        this.b2CRepository = b2CRepository;
    }

    @Override
    Observable<List<GalleryViewPagerItem>> buildUseCaseObservable(Void aVoid) {
        return null;
    }

    @Override
    Single<List<GalleryViewPagerItem>> buildUseCaseSingle(Void aVoid) {
        return b2CRepository.getGalleries().map(this::transformGalleries);
    }

    @Override
    Completable buildUseCaseCompletable(Void aVoid) {
        return null;
    }

    private List<GalleryViewPagerItem> transformGalleries(List<Tiles> dataList) {
        List<GalleryViewPagerItem> galleryItems = new ArrayList<>();
        int length = dataList.size();
        for (int i = 0; i < length; i++) {
            Tiles tiles = dataList.get(i);
            galleryItems.add(transformGallery(tiles));
        }
        return galleryItems;
    }

    private GalleryViewPagerItem transformGallery(Tiles tiles) {
        return new GalleryViewPagerItem(
                tiles.image(),
                tiles.title(),
                tiles.text(),
                getColor(tiles.shortDescription())
        );
    }

    private int getColor(String shortDescription) {
        String colorCode = new ColorCodeGetter().getColorCode(shortDescription);
        if(TextUtils.isEmpty(colorCode)) {
            /*int[] colors = new int[]{Color.BLUE, Color.GREEN, Color.RED};
            return colors[new Random().nextInt(colors.length)];*/
            return ContextCompat.getColor(App.getInstance(), R.color.colorPrimaryDark);
        }
        int color;
        try {
            color = Color.parseColor(colorCode);
        } catch (Exception e){
            color = ContextCompat.getColor(App.getInstance(), R.color.colorPrimaryDark);
        }
        return color;
    }
}
