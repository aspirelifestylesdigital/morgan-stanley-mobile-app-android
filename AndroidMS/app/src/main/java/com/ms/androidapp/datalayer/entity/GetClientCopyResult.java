package com.ms.androidapp.datalayer.entity;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ThuNguyen on 6/5/2017.
 */

public class GetClientCopyResult {
    @SerializedName("CurrentVersion")
    private String currentVersion;
    @SerializedName("Status")
    private String status;
    @SerializedName("Success")
    private Boolean success;
    @SerializedName("Text")
    private String text;

    public String getCurrentVersion() {
        return currentVersion;
    }

    public String getStatus() {
        return status;
    }

    public Boolean getSuccess() {
        return success;
    }

    public String getText() {
        return text;
    }
}
