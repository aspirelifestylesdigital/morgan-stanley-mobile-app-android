package com.ms.androidapp.presentation.inspiregallery;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ms.androidapp.R;
import com.ms.androidapp.common.glide.GlideHelper;
import com.ms.androidapp.domain.model.GalleryViewPagerItem;
import com.support.mylibrary.widget.RegularText;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;

public class InspireGalleryPagerAdapter extends PagerAdapter {

    private LayoutInflater layoutInflater;
    private List<GalleryViewPagerItem> pagerItems = new ArrayList<>();
    private Context context;

    public InspireGalleryPagerAdapter(Context context, List<GalleryViewPagerItem> pagerItems) {
        this.context = context;
        this.pagerItems = pagerItems;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return pagerItems.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(ViewGroup view, int position, Object object) {
        view.removeView((View) object);
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        View itemView = layoutInflater.inflate(R.layout.inspiration_gallery_pager_item, view, false);
        ImageView galleryImage = ButterKnife.findById(itemView, R.id.gallery_image);
        TextView galleryTitle = ButterKnife.findById(itemView, R.id.gallery_title);
        RegularText galleryDescription = ButterKnife.findById(itemView, R.id.gallery_description);
        galleryDescription.setSpacing((float)4);
        galleryDescription.setLineSpacing((float)1.3, (float)1.3);
        GalleryViewPagerItem item = pagerItems.get(position);
//        Picasso.with(context)
//                .load(item.imageURL)
//                .fit()
//                .centerCrop()
//                .into(galleryImage);
        GlideHelper.getInstance().loadImage(item.imageURL,
                0, galleryImage, 0);
        galleryTitle.setText(item.title);
        galleryDescription.setText(item.summary);
        view.addView(itemView);
        return itemView;
    }
}