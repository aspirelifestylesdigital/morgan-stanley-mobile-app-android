package com.ms.androidapp.presentation.profile;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.Button;
import android.widget.EditText;

public class QuestionComparator {
    private EditText edtAnswer;
    private boolean answer;
    private Button btnSubmit, btnCancel;
    boolean changed;

    public QuestionComparator(EditText edtAnswer, Button btnSubmit, Button btnCancel) {
        this.edtAnswer = edtAnswer;
        this.btnSubmit = btnSubmit;
        this.btnCancel = btnCancel;
        setUp();
    }

    private void setUp() {
        edtAnswer.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                String string = s.toString();
                answer = string.trim().length() > 0;
                onChanged();
            }
        });
    }

    private void onChanged() {
        changed = answer;
        btnSubmit.setEnabled(changed);
        btnSubmit.setClickable(changed);
        btnCancel.setEnabled(changed);
        btnCancel.setClickable(changed);
    }
}
