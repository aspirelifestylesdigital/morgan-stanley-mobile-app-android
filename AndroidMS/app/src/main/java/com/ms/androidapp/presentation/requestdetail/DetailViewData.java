package com.ms.androidapp.presentation.requestdetail;

import com.ms.androidapp.domain.model.RequestDetailData;

import org.json.JSONException;

/**
 * Created by vinh.trinh on 9/19/2017.
 */

class DetailViewData {
    final String title;
    final String date;
    final boolean isOpening;
    String details;

    DetailViewData(RequestDetailData data) throws JSONException {
        TextDetailBuilder textDetailBuilder = new TextDetailBuilder();
        this.isOpening = data.open;
        this.title = textDetailBuilder.nameMapping(data.requestType);
        this.date = data.date;
    }
}
