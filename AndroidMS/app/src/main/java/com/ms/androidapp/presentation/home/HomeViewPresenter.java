package com.ms.androidapp.presentation.home;

import android.content.Intent;
import android.location.Location;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.util.Log;

import com.api.aspire.data.entity.preference.PreferenceData;
import com.api.aspire.data.preference.PreferencesStorageAspire;
import com.api.aspire.data.repository.ProfileDataAspireRepository;
import com.api.aspire.domain.usecases.GetToken;
import com.api.aspire.domain.usecases.LoadProfile;
import com.api.aspire.domain.usecases.SignOut;
import com.ms.androidapp.R;
import com.ms.androidapp.common.constant.AppConstant;
import com.ms.androidapp.common.constant.CityData;
import com.ms.androidapp.common.constant.IntentConstant;
import com.ms.androidapp.common.constant.RequestCode;
import com.ms.androidapp.datalayer.datasource.AppGeoCoder;
import com.ms.androidapp.domain.model.LatLng;
import com.ms.androidapp.domain.usecases.MapProfileApp;
import com.ms.androidapp.presentation.changepass.ChangePasswordActivity;
import com.ms.androidapp.presentation.checkout.SignInActivity;
import com.ms.androidapp.presentation.explore.ExploreFragment;
import com.ms.androidapp.presentation.info.MasterCardUtilityActivity;
import com.ms.androidapp.presentation.preferences.UserPreferencesActivity;
import com.ms.androidapp.presentation.profile.MyProfileActivity;
import com.ms.androidapp.presentation.request.AskConciergeActivity;
import com.ms.androidapp.presentation.requestlist.RequestListActivity;

import java.util.List;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;


/**
 * Created by vinh.trinh on 7/28/2017.
 */

public class HomeViewPresenter {

    private HomeActivity homeActivity;
    private LoadProfile loadProfile;
    private SignOut signOutCase;
    private GetToken getTokenCase;
    private AppGeoCoder geoCoder;
    private Single<LatLng> getLocation;

    LatLng location = null;
    String cuisine = null;

    HomeViewPresenter(HomeActivity homeActivity) {
        this.homeActivity = homeActivity;
        MapProfileApp mapLogic = new MapProfileApp();
        PreferencesStorageAspire preferencesStorageAspire = new PreferencesStorageAspire(homeActivity.getApplicationContext());
        this.signOutCase = new SignOut(preferencesStorageAspire);
        this.getTokenCase = new GetToken(preferencesStorageAspire, mapLogic);
        this.loadProfile = new LoadProfile(new ProfileDataAspireRepository(preferencesStorageAspire, mapLogic));
        this.geoCoder = new AppGeoCoder();
//        provideMockLocations();
    }

    void navigateToAskConcierge() {
        Intent intent = new Intent(homeActivity, AskConciergeActivity.class);
        homeActivity.startActivityForResult(intent, HomeActivity.REQUEST_CODE_ASK_CONCIERGE);
    }

    void navigateToMyProfile() {
        Intent intent = new Intent(homeActivity, MyProfileActivity.class);
        homeActivity.startActivityForResult(intent, RequestCode.PROFILE);
    }

    void navigateToChangePassword() {
        Intent intent = new Intent(homeActivity, ChangePasswordActivity.class);
        homeActivity.startActivityForResult(intent, RequestCode.CHANGE_PASSWORD);
    }

    void navigateToPreferences() {
        Intent intent = new Intent(homeActivity, UserPreferencesActivity.class);
        homeActivity.startActivityForResult(intent, RequestCode.USER_PREFERENCES);
    }

    void navigateToMasterCardUtility(AppConstant.MASTERCARD_COPY_UTILITY utilityType) {
        Intent intent = new Intent(homeActivity, MasterCardUtilityActivity.class);
        intent.putExtra(IntentConstant.MASTERCARD_COPY_UTILITY, utilityType);
        homeActivity.startActivity(intent);
    }

    void navigateToRequestList() {
        Intent intent = new Intent(homeActivity, RequestListActivity.class);
        homeActivity.startActivity(intent);
    }

    void signOut() {

        // revoke token
        String token = getTokenCase.getAuthTokenCurrent();
        if (!TextUtils.isEmpty(token)) {
            getTokenCase.revokeToken(token)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe();
        }

        signOutCase.setSignOutProfile(() -> {
            //remove select city data
            CityData.reset();
        });
        Intent intent = new Intent(homeActivity, SignInActivity.class);
        homeActivity.startActivity(intent);
        homeActivity.overridePendingTransition(R.anim.fadein,R.anim.fadeout);
        homeActivity.finish();
    }

    void addFragment(Fragment fragment) {
        FragmentManager fm = homeActivity.getSupportFragmentManager();
        //if(fm.getBackStackEntryCount()>1) fm.popBackStack();
        Fragment curFragment = fm.findFragmentById(R.id.fragment_place_holder);
        Fragment fragmentPopped = fm.findFragmentByTag(fragment.getClass().getSimpleName());
        FragmentTransaction ft = fm.beginTransaction();
        if(curFragment != null &&
                curFragment.getClass().getSimpleName().equalsIgnoreCase(fragment.getClass().getSimpleName())){
            //ft.show(fragment); // Do nothing
            return;
        }else if(fragment instanceof HomeFragment){
            clearAllFragments();
            // Add fragment to backstack
            ft.add(R.id.fragment_place_holder, fragment,
                    fragment.getClass().getSimpleName());
            ft.addToBackStack(fragment.getClass().getSimpleName())
                    .commit();
        }else{
            if(fragmentPopped == null){
                addFragmentToBackstack(fragment, ft);
            }else{
                if(fragment instanceof ExploreFragment){
                    clearAllFragmentExceptFragment(fragment);
                }else{
                    addFragmentToBackstack(fragment, ft);
                }
            }
        }
    }
    void addSingleInstanceFragment(Fragment fragment){
        FragmentTransaction ft = homeActivity.getSupportFragmentManager().beginTransaction();
        addFragmentToBackstack(fragment, ft);
    }
    void addFragmentToBackstack(Fragment fragment, FragmentTransaction ft){
        // Add fragment to backstack
        ft.add(R.id.fragment_place_holder, fragment,
                fragment.getClass().getSimpleName());
        ft.addToBackStack(fragment.getClass().getSimpleName())
                .commit();
    }
    public void clearAllFragments() {
        FragmentManager fm = homeActivity.getSupportFragmentManager();
        if (fm.getBackStackEntryCount() > 0) {
            fm.popBackStackImmediate(null,
                    FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }
    void clearAllFragmentExceptFragment(Fragment notRemovedFragment){
        FragmentManager fm = homeActivity.getSupportFragmentManager();
        List<Fragment> fragmentList = fm.getFragments();
        if(fragmentList != null && fragmentList.size() > 0){
            FragmentTransaction ft = fm.beginTransaction();
            for(int index = 0; index < fragmentList.size(); index++){
                if(!fragmentList.get(index).getClass().getSimpleName().equalsIgnoreCase(notRemovedFragment.getClass().getSimpleName())){
                    ft.remove(fragmentList.get(index));
                }
            }
            ft.commit();
        }
    }
    void addFragmentAfterClearAll(Fragment fragment){
        clearAllFragments();
        addFragment(fragment);
    }

    void loadPreferences(Location lastKnownLocation) {
        /*LatLng mockLocation = mockLocationProvider.get(CityData.cityName());
        if(mockLocation == null) {
            getLocation = Single.just(lastKnownLocation == null ? LatLng.INVALID :
                    new LatLng(lastKnownLocation.getLatitude(), lastKnownLocation.getLongitude()));
        } else {
            getLocation = Single.just(mockLocation);
        }*/
        getLocation = Single.just(lastKnownLocation == null ? LatLng.INVALID :
                new LatLng(lastKnownLocation.getLatitude(), lastKnownLocation.getLongitude()));
        Single.zip(getLocation(), getCuisinePreference(),
                (location, cuisine) -> new Result(
                        hasLocation(location),
                        hasCuisine(cuisine)
                ))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<Result>() {
                    @Override
                    public void onSuccess(Result result) {
                        location = result.location;
                        cuisine = result.cuisine;
                        dispatchChanged();
                    }

                    @Override
                    public void onError(Throwable e) {
                    }
                });
    }

    void loadLocationCriteria() {
        getLocation()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<LatLng>() {
                    @Override
                    public void onSuccess(LatLng latLng) {
                        location = hasLocation(latLng);
                        dispatchChanged();
                    }

                    @Override
                    public void onError(Throwable e) {
                    }
                });
    }

    void loadCuisineCriteria() {
        getCuisinePreference()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<String>() {
                    @Override
                    public void onSuccess(String cuisineLocal) {
                        cuisine = hasCuisine(cuisineLocal);
                        dispatchChanged();
                    }

                    @Override
                    public void onError(Throwable e) {

                    }
                });
    }

    private Single<LatLng> getLocation() {
        if (getLocation == null) return Single.just(LatLng.INVALID);
        return Single.zip(loadProfile.loadStorage(), getLocation,
                (profile, location) -> {
                    if (!location.isValid() || !profile.isLocationOn() || !CityData.citySelected())
                        return LatLng.INVALID;
                    String cityName = CityData.cityName();
                    if (CityData.regions.contains(cityName)) {
                        return location;
                    }
                    String currentAddress = geoCoder.getAddress(location);
                    return currentAddress.contains(CityData.cityName()) ? location : LatLng.INVALID;
                });

    }

    private Single<String> getCuisinePreference() {
        return loadProfile.loadStorage()
                .flatMap(profile -> {
                    String cuisine = PreferenceData.NA_VALUE;
                    if (profile.getPreferenceData() != null) {
                        cuisine = profile.getPreferenceData().getCuisine();
                    }
                    return Single.just(cuisine);
                });
    }

    private LatLng hasLocation(LatLng location) {
        return location.isValid() ? location : null;
    }

    private String hasCuisine(String cuisine) {
        return cuisine.equals(PreferenceData.NA_VALUE) ? null : cuisine;
    }

    private void dispatchChanged() {
        ExploreFragment exploreFragment = ((ExploreFragment) homeActivity.getSupportFragmentManager()
                .findFragmentByTag(ExploreFragment.class.getSimpleName()));
        if (exploreFragment != null) exploreFragment.preferencesChange(location, cuisine);
    }

    private class Result {
        LatLng location;
        String cuisine;

        Result(LatLng location, String cuisine) {
            this.location = location;
            this.cuisine = cuisine;
        }
    }

    /*private Map<String, LatLng> mockLocationProvider;
    private void provideMockLocations() {
        mockLocationProvider = new HashMap<>();
        mockLocationProvider.put("London", new LatLng(51.511981f,-0.0693553f));
        mockLocationProvider.put("Boston", new LatLng(42.3020856f,-71.0554553f));
        mockLocationProvider.put("Los Angeles", new LatLng(34.1442319f,-118.3975795f));
        mockLocationProvider.put("Vancouver", new LatLng(49.261055f,-123.1289526f));
    }*/
}
