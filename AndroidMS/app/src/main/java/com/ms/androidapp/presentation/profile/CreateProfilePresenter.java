package com.ms.androidapp.presentation.profile;

import android.content.Context;

import com.api.aspire.domain.usecases.CreateProfileCase;
import com.ms.androidapp.App;
import com.ms.androidapp.R;
import com.api.aspire.common.exception.BackendException;
import com.api.aspire.common.constant.ErrCode;
import com.ms.androidapp.domain.mapper.profile.MapProfile;
import com.ms.androidapp.domain.model.Profile;
import com.ms.androidapp.domain.usecases.MapProfileApp;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableCompletableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by vinh.trinh on 4/28/2017.
 */

public class CreateProfilePresenter implements CreateProfile.Presenter {

    private CompositeDisposable disposables;
    private CreateProfile.View view;
    // TODO: 10/1/2018 remove flow use create profileV2
//    private CreateProfileCase createProfileCase;

    CreateProfilePresenter(Context context) {
        disposables = new CompositeDisposable();
//        createProfileCase = new CreateProfileCase(context, new MapProfileApp());
    }

    @Override
    public void attach(CreateProfile.View view) {
        this.view = view;
    }

    @Override
    public void detach() {
        disposables.dispose();
        view = null;
    }

    @Override
    public void createProfile(Profile profile, String password) {
        if (!App.getInstance().hasNetworkConnection()) {
            view.showErrorDialog(ErrCode.CONNECTIVITY_PROBLEM, null);
            return;
        }
        view.showProgressDialog();
        // TODO: 10/1/2018 check case sign up
//        CreateProfileCase.Params params = new MapProfile().createProfile(profile, password);
//        disposables.add(
//                createProfileCase.buildUseCaseCompletable(params)
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribeWith(new SaveProfileObserver()));
    }

    @Override
    public void abort() {
        disposables.clear();
    }

    private final class SaveProfileObserver extends DisposableCompletableObserver {

        @Override
        public void onComplete() {
            if(view != null) {
                view.dismissProgressDialog();
                view.showProfileCreatedDialog();
            }
            dispose();
        }

        @Override
        public void onError(Throwable e) {
            if(view != null) {
                view.dismissProgressDialog();
                view.showErrorDialog(ErrCode.UNKNOWN_ERROR, e.getMessage());
            }
            dispose();
        }
    }

}
