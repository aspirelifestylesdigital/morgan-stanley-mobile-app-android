package com.ms.androidapp.datalayer.repository;

import com.api.aspire.common.exception.BackendException;
import com.ms.androidapp.datalayer.entity.askconcierge.ACRequest;
import com.ms.androidapp.datalayer.entity.askconcierge.ACRequestDetail;
import com.ms.androidapp.datalayer.entity.askconcierge.ACRequestItem;
import com.ms.androidapp.datalayer.entity.askconcierge.ACResponse;
import com.ms.androidapp.datalayer.entity.askconcierge.GetACRequest;
import com.ms.androidapp.datalayer.restapi.ACApi;
import com.ms.androidapp.datalayer.retro2client.AppHttpClient;
import com.ms.androidapp.domain.repository.ACRepository;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by vinh.trinh on 5/17/2017.
 */

public class RequestRepository implements ACRepository {
    
    @Override
    public Call<ACResponse> createNewConciergeCase(ACRequest requestBody) {
        return AppHttpClient.getInstance().getAcApi()
                .createNewConciergeCase(requestBody);
    }

    @Override
    public Call<ACResponse> updateConciergeCase(ACRequest requestBody) {
        return AppHttpClient.getInstance().getAcApi().updateConciergeCase(requestBody);
    }

    @Override
    public List<ACRequestItem> getRequestList(GetACRequest requestBody) throws IOException, BackendException,
            JSONException {
        ACApi acApi = AppHttpClient.getInstance().getAcApi();
        Call<ResponseBody> request = acApi.getRequestList(requestBody);
        Response<ResponseBody> response = request.execute();
        if(!response.isSuccessful()) throw new IOException(response.errorBody().string());
        String jsonString = response.body().string();
        JSONArray result;
        try {
            result = new JSONArray(jsonString);
        } catch (JSONException ignored) {
            JSONObject e = new JSONObject(jsonString);
            throw new BackendException(e.getJSONObject("message").getString("message"));
        }
        int length = result.length();
        List<ACRequestItem> list = new ArrayList<>(length);
        for (int i = 0; i < length; i++) {
            list.add(new ACRequestItem(result.getJSONObject(i)));
        }
        return list;
    }

    @Override
    public ACRequestDetail getRequestDetail(GetACRequest requestBody) {
        return null;
    }
}