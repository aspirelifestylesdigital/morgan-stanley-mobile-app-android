package com.ms.androidapp.presentation.selectcity;

import android.content.Intent;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;

import com.ms.androidapp.App;
import com.ms.androidapp.R;
import com.ms.androidapp.common.constant.AppConstant;
import com.ms.androidapp.common.constant.CityData;
import com.ms.androidapp.common.constant.IntentConstant;
import com.ms.androidapp.common.constant.ResultCode;
import com.ms.androidapp.domain.model.CityRViewItem;
import com.ms.androidapp.presentation.base.CommonActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * Created by tung.phan on 5/9/2017.
 */

public class SelectCityActivity extends CommonActivity implements SelectCity.View,
        CityRViewAdapter.CityRViewAdapterListener {

    @BindView(R.id.selection_recycle_view)
    RecyclerView selectCityRView;
    private CityRViewAdapter cityRViewAdapter;

    private SelectCityPresenter presenter;
    /**
     * var know from ask activity to select city screen
     */
    private boolean mBackAskScreen = false;

    private SelectCityPresenter buildPresenter() {
        return new SelectCityPresenter(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.common_recyclerview_activity_layout);
        setTitle(R.string.title_choose_city);
        setToolbarColor(R.color.ms_colorPrimary);
        fakeCityData();
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        selectCityRView.setBackgroundColor(Color.WHITE);
        selectCityRView.setLayoutManager(layoutManager);
        selectCityRView.setAdapter(cityRViewAdapter);
        selectCityRView.addItemDecoration(new DividerItemDecoration(this));
        layoutManager.scrollToPosition(CityData.selectedCity());

        presenter = buildPresenter();
        presenter.attach(this);

        if (savedInstanceState == null) {
            // Track GA
            App.getInstance().track(AppConstant.GA_TRACKING_SCREEN_NAME.CITY_LIST.getValue());
        }

        if (getIntent() != null) {
            mBackAskScreen = getIntent().getBooleanExtra(IntentConstant.ASK_SCREEN_TO_SELECT_CITY, false);
        }
    }

    @Override
    protected void onDestroy() {
        presenter.detach();
        super.onDestroy();
    }

    private void fakeCityData() {
        Resources res = getResources();
        TypedArray imagesResources = res.obtainTypedArray(R.array.city_images);
        String[] cities = res.getStringArray(R.array.city_list);
        String[] citiesDescriptions = res.getStringArray(R.array.city_locate_list);
        final List<CityRViewItem> cityRViewItems = new ArrayList<>();
        for (int i = 0; i < cities.length; i++) {
            cityRViewItems.add(new CityRViewItem(
                    imagesResources.getResourceId(i, R.drawable.img_placeholder), cities[i], citiesDescriptions[i]));
        }
        cityRViewAdapter = new CityRViewAdapter(cityRViewItems, this);
    }

    @Override
    public void onItemClick(int pos) {

        if (cityRViewAdapter == null || cityRViewAdapter.getItem(pos) == null
                || TextUtils.isEmpty(cityRViewAdapter.getItem(pos).getCity())) {
            return;
        }

        String cityNameSelected = cityRViewAdapter.getItem(pos).getCity();
        presenter.saveSelectCity(cityNameSelected);

        // Track "select city"
        App.getInstance().track(AppConstant.GA_TRACKING_CATEGORY.CITY_SELECTION.getValue(),
                AppConstant.GA_TRACKING_ACTION.SELECT.getValue(),
                CityData.cityName());

        Intent intent = getIntent();
        if (mBackAskScreen) {
            intent.putExtra(IntentConstant.ASK_SCREEN_TO_SELECT_CITY, true);
        }// else do nothing

        setResult(ResultCode.RESULT_OK, getIntent());
        super.onBackPressed();
    }

}
