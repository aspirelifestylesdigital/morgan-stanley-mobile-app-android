package com.ms.androidapp.common.logic;

import android.graphics.PointF;
import android.os.Build;

/**
 * Created by vinh.trinh on 5/15/2017.
 */

public final class Utils {

    private Utils() {

    }

    public static boolean isHigherThanM() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.M;
    }

    public static boolean isHigherThanLolipop() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP;
    }
    public static PointF calculateScaledCoordinate(float oldX, float oldY, float centerX, float centerY, float scale){
        PointF newPoint = new PointF();
        float newX = centerX * (1 - scale) + scale * oldX;
        float newY = centerY * (1 - scale) + scale * oldY;
        newPoint.set(newX, newY);

        return newPoint;
    }
}
