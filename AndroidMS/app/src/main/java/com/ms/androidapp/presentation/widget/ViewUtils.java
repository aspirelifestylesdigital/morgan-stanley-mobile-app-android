package com.ms.androidapp.presentation.widget;

import android.app.Activity;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.util.TypedValue;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.ms.androidapp.R;

/**
 * Created by vinh.trinh on 5/8/2017.
 */

public class ViewUtils {

    //click range threshold
    private final static int CLICK_ACTION_THRESHHOLD = 5;

    public static void menuTintColors(Context context, Drawable... drawables) {
        ColorStateList colorStateList = ContextCompat.getColorStateList(context, R.color.ic_menu_tint_color);
        for (Drawable icMenu : drawables) {
            DrawableCompat.setTintList(icMenu, colorStateList);
        }
    }
    public static void hyperlinkTintColors(Context context, TextView tv) {
        ColorStateList colorStateList = ContextCompat.getColorStateList(context, R.color.hyperlink_text_color);
        tv.setTextColor(colorStateList);
    }
    public static void drawableTop(TextView view, int id) {
        Drawable drawableTop = ContextCompat.getDrawable(view.getContext(), id);
        view.setCompoundDrawablesWithIntrinsicBounds(null,drawableTop,null,null);
    }

    public static void drawableEnd(TextView view, int id) {
        Drawable drawableEnd = ContextCompat.getDrawable(view.getContext(), id);
        view.setCompoundDrawablesWithIntrinsicBounds(null,null,drawableEnd,null);
    }

    public static void drawableStart(TextView view, int id) {
        Drawable drawableStart = ContextCompat.getDrawable(view.getContext(), id);
        view.setCompoundDrawablesWithIntrinsicBounds(drawableStart,null,null,null);
    }

    public static void emptyDrawable(TextView view) {
        view.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
    }

    /**
     *
     * @param startX start X of the motion event
     * @param endX end X of motion event
     * @param startY start Y of the motion event
     * @param endY end Y of the motion event
     * @return true if the event is click event
     */
    public static boolean isAClick(float startX, float endX, float startY, float endY) {
        float differenceX = Math.abs(startX - endX);
        float differenceY = Math.abs(startY - endY);
        return !(differenceX > CLICK_ACTION_THRESHHOLD || differenceY > CLICK_ACTION_THRESHHOLD);
    }

    public static Rect getViewRect(View view) {
        int[] location = new int[2];
        view.getLocationOnScreen(location);
        return new Rect(location[0], location[1],
                location[0] + view.getWidth(),
                location[1] + view.getHeight());
    }

    public static void hideSoftKey(View view) {
        if (view == null) {
            return;
        }
        InputMethodManager im = (InputMethodManager) view.getContext().getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (im != null) {
            im.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static void showSoftKey(View view) {
        if (view == null) {
            return;
        }
        InputMethodManager im = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (im != null) {
            im.toggleSoftInputFromWindow(view.getWindowToken(), InputMethodManager.SHOW_IMPLICIT, 0);
        }
    }

    public static int convertDpToPx(int dp, Context context) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                dp,
                context.getResources()
                        .getDisplayMetrics());
    }
}
