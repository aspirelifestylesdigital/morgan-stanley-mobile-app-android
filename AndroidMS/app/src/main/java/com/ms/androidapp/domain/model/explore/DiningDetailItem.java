package com.ms.androidapp.domain.model.explore;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.ms.androidapp.common.constant.AppConstant;

import java.util.Arrays;

/**
 * Created by vinh.trinh on 6/8/2017.
 */

public class DiningDetailItem extends ExploreRView implements Parcelable {

    public static final Creator<DiningDetailItem> CREATOR = new Creator<DiningDetailItem>() {
        @Override
        public DiningDetailItem createFromParcel(Parcel in) {
            return new DiningDetailItem(in);
        }

        @Override
        public DiningDetailItem[] newArray(int size) {
            return new DiningDetailItem[size];
        }
    };
    public final String title;
    public final String address;
    public final String city;
    public final String state;
    public final String country;
    public final String postalCode;
    public final String address2;
    public final String address3;
    public final String url;
    public final String insiderTips;
    public final String benefits;
    public final String priceRange;
    public final String cuisine;
    public final String description;
    public final String hoursOfOperation;
    public final String imageURL;
    public final double latitude;
    public final double longitude;

    private DiningDetailItem(String title, String address, String city, String state, String country, String postalCode,
                             String address2, String address3, String url, String insiderTips, String benefits,
                             String priceRange, String cuisine, String description, String hoursOfOperation,
                             String imageURL, double lat, double lng) {
        this.title = title;
        this.address = address;
        this.city = city;
        this.state = state;
        this.country = country;
        this.postalCode = postalCode;
        this.address2 = address2;
        this.address3 = address3;
        this.url = url;
        this.insiderTips = insiderTips;
        this.benefits = benefits;
        this.priceRange = priceRange;
        this.cuisine = cuisine;
        this.description = description;
        this.hoursOfOperation = hoursOfOperation;
        this.imageURL = imageURL;
        this.latitude = lat;
        this.longitude = lng;
        itemType = ItemType.DINING;
    }

    protected DiningDetailItem(Parcel in) {
        title = in.readString();
        address = in.readString();
        city = in.readString();
        state = in.readString();
        country = in.readString();
        postalCode = in.readString();
        address2 = in.readString();
        address3 = in.readString();
        url = in.readString();
        insiderTips = in.readString();
        benefits = in.readString();
        priceRange = in.readString();
        cuisine = in.readString();
        description = in.readString();
        hoursOfOperation = in.readString();
        imageURL = in.readString();
        latitude = in.readDouble();
        longitude = in.readDouble();
        itemType = ItemType.NORMAL;
    }

    @Override
    public void setItemType(ItemType itemType) {}

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public String getImageUrl() {
        return imageURL;
    }

    @Override
    public Integer getId() {
        return 0;
    }

    @Override
    public String getSuggestedToAC() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(AppConstant.PREFILL_AC_NAME + title);
        stringBuilder.append("\n" + AppConstant.PREFILL_AC_DATE_TIME);
        stringBuilder.append("\n" + AppConstant.PREFILL_AC_NO_OF_ADULTS);
        stringBuilder.append("\n" + AppConstant.PREFILL_AC_NO_OF_CHILDREN);
        stringBuilder.append("\n" + AppConstant.PREFILL_AC_CITY + ((city != null) ? city : ""));
        stringBuilder.append("\n" + AppConstant.PREFILL_AC_STATE + ((state != null) ? state : ""));
        stringBuilder.append("\n" + AppConstant.PREFILL_AC_COUNTRY + ((country != null) ? country : ""));
        stringBuilder.append("\n" + AppConstant.PREFILL_AC_OTHER_COMMENTS);
        return stringBuilder.toString();
    }

    public String getDisplayAddress(){
        String result = "";
        if(!TextUtils.isEmpty(address)){
            result += address;
        }
        if(!TextUtils.isEmpty(city)){
            if(!TextUtils.isEmpty(result)){
                result += ", ";
            }
            result += city;
        }
        if(!TextUtils.isEmpty(state)){
            if(!TextUtils.isEmpty(result)){
                result += ", ";
            }
            result += state;
        }
        if(!TextUtils.isEmpty(postalCode)){
            if(!TextUtils.isEmpty(result)){
                result += ", ";
            }
            result += postalCode;
            if(!TextUtils.isEmpty(address3)){
                result += " " + address3;
            }
        }else{
            if(!TextUtils.isEmpty(result)){
                result += ", ";
            }
            if(!TextUtils.isEmpty(address3)){
                result += address3;
            }
        }
        return result;
    }

    public String getQuerySearchOnGoogleMap(){
        String result = "";
        if(!TextUtils.isEmpty(title)){
            result += title;
        }
        if(!TextUtils.isEmpty(address)){
            if(!TextUtils.isEmpty(result)){
                result += ", ";
            }
            result += address;
        }
//        if(!TextUtils.isEmpty(address2)){
//            if(!TextUtils.isEmpty(result)){
//                result += ", ";
//            }
//            result += address2;
//        }
//        if(!TextUtils.isEmpty(address3)){
//            if(!TextUtils.isEmpty(result)){
//                result += ", ";
//            }
//            result += address3;
//        }
        if(!TextUtils.isEmpty(state)){
            if(!TextUtils.isEmpty(result)){
                result += ", ";
            }
            result += state;
        }
        if(!TextUtils.isEmpty(city)){
            if(!TextUtils.isEmpty(result)){
                result += ", ";
            }
            result += city;
        }
        if(!TextUtils.isEmpty(postalCode)){
            if(!TextUtils.isEmpty(result)){
                result += ", ";
            }
            result += postalCode;
        }


        return result;
    }

    public String getQueryBeiJingSearchOnGoogleMap(){
        String result = "";
        if(!TextUtils.isEmpty(title)){
            result += title;
        }
        if(!TextUtils.isEmpty(address)){
            if(!TextUtils.isEmpty(result)){
                result += ", ";
            }
            result += address;
        }
        if(!TextUtils.isEmpty(city)){
            if(!TextUtils.isEmpty(result)){
                result += ", ";
            }
            result += city;
        }

        return result;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(address);
        dest.writeString(city);
        dest.writeString(state);
        dest.writeString(country);
        dest.writeString(postalCode);
        dest.writeString(address2);
        dest.writeString(address3);
        dest.writeString(url);
        dest.writeString(insiderTips);
        dest.writeString(benefits);
        dest.writeString(priceRange);
        dest.writeString(cuisine);
        dest.writeString(description);
        dest.writeString(hoursOfOperation);
        dest.writeString(imageURL);
        dest.writeDouble(latitude);
        dest.writeDouble(longitude);
    }

    public static class Builder {

        private String title;
        private String address;
        private String city;
        private String state;
        private String country;
        private String postalCode;
        private String address2;
        private String address3;
        private String url;
        private String benefits;
        private String priceRange;
        private String cuisine;
        private String description;
        private String hoursOfOperation;
        private String imageURL;
        private double latitude;
        private double longitude;

        public Builder(String title, String address, String city, String postalCode, int priceRange,
                       String cuisine, String description, String hoursOfOperation, String imageURL) {
            this.title = title;
            this.address = address;
            this.city = city;
            this.postalCode = postalCode;
            this.priceRange = priceRange(priceRange);
            this.cuisine = cuisine;
            this.description = description;
            this.hoursOfOperation = hoursOfOperation;
            this.imageURL = imageURL;
        }

        public Builder state(String val) {
            this.state = inspect(val);
            return this;
        }
        public Builder country(String val){
            this.country = inspect(val);
            return this;
        }
        public Builder address2(String val){
            this.address2 = inspect(val);
            return this;
        }
        public Builder address3(String val) {
            this.address3 = inspect(val);
            return this;
        }

        public Builder url(String val) {
            this.url = inspect(val);
            return this;
        }

        public Builder benefits(String val) {
            this.benefits = inspect(val);
            return this;
        }

        public Builder coordination(double lat, double lng) {
            this.latitude = lat;
            this.longitude = lng;
            return this;
        }

        public DiningDetailItem build() {
            StringBuilder descriptBuilder = new StringBuilder(description);
            String insiderTips = insiderTip(descriptBuilder);
            return new DiningDetailItem(title, address, city, state, country, postalCode, address2, address3, url,
                    insiderTips, benefits, priceRange, cuisine, descriptBuilder.toString(), hoursOfOperation,
                    imageURL, latitude, longitude);
        }

        private String inspect(String val) {
            if(val == null) return null;
            if(TextUtils.isEmpty(val.trim())) return null;
            return val;
        }
        private String insiderTip(StringBuilder description) {
            String insiderTipSeg1 = "<b>Insider tip</b>:";
            String insiderTipSeg2 = "<b>Insider tip:</b>";
            int insiderTipInd = -1;
            if(description.toString().contains(insiderTipSeg1)){
                insiderTipInd = description.toString().indexOf(insiderTipSeg1);
            }else{
                insiderTipInd = description.toString().indexOf(insiderTipSeg2);
            }
            if(insiderTipInd > -1){
                String insiderTip = description.substring(insiderTipInd + insiderTipSeg1.length()).trim().replace("\n", "").replace("\r", "");
                String newDescription = description.substring(0, insiderTipInd).trim().replace("\n", "").replace("\r", "");
                description.delete(0, description.length());
                description.append(newDescription);
                return insiderTip;
            }
            return "";
        }
        private String priceRange(int range) {
            if(range <= 0) return null;
            char[] charArray = new char[range];
            Arrays.fill(charArray, '$');
            return new String(charArray);
        }
    }
}
