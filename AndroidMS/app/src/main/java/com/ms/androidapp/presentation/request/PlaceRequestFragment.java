package com.ms.androidapp.presentation.request;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatEditText;
import android.text.Editable;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageButton;

import com.ms.androidapp.R;
import com.ms.androidapp.common.constant.IntentConstant;
import com.ms.androidapp.presentation.base.BaseFragment;
import com.ms.androidapp.presentation.widget.DialogHelper;
import com.ms.androidapp.presentation.widget.ViewKeyboardListener;
import com.ms.androidapp.presentation.widget.ViewUtils;
import com.support.mylibrary.widget.ClickGuard;

import butterknife.BindDrawable;
import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by vinh.trinh on 5/24/2017.
 */

public class PlaceRequestFragment extends BaseFragment implements View.OnTouchListener, ViewTreeObserver.OnGlobalLayoutListener {

    private static final String STATE_RECORD_TEXT = "text";

    @BindView(R.id.edt_ask_content)
    AppCompatEditText edtAskContent;
    @BindView(R.id.btn_record)
    ImageButton btnRecord;
    @BindView(R.id.layout_container)
    View layoutContainer;
    @BindDrawable(R.drawable.ic_microphone)
    Drawable icRecord;
    @BindDrawable(R.drawable.ic_send)
    Drawable icSend;
    @BindView(R.id.chb_reply_phone)
    AppCompatCheckBox checkBoxPhone;
    @BindView(R.id.chb_reply_email)
    AppCompatCheckBox checkBoxEmail;
    @BindView(R.id.reply_mode_container)
    ViewGroup replyModeContainer;
    AnimatorSet slideUp, slideDown;
    boolean checkboxShowing;
    private Spanned hintText;
    private boolean hasFocus;
    ViewEventsListener listener;
    DialogHelper dialogHelper;

    public static PlaceRequestFragment newInstance() {
        Bundle args = new Bundle();
        PlaceRequestFragment fragment = new PlaceRequestFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static PlaceRequestFragment newInstance(Bundle bundle) {
        PlaceRequestFragment fragment = new PlaceRequestFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ViewEventsListener) {
            listener = (ViewEventsListener) context;
        } else {
            throw new ClassCastException(context.toString()
                    + " must implement PlaceRequestFragment.ViewEventsListener.");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_ask_concierge, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        dialogHelper = new DialogHelper(getActivity());
        icRecord = ContextCompat.getDrawable(getContext(), R.drawable.ic_microphone);
        icSend = ContextCompat.getDrawable(getContext(), R.drawable.ic_send);
        hintText = Html.fromHtml(getString(R.string.ask_hint));
        edtAskContent.setHint(hintText);
        if (edtAskContent.getText().length() > 0)
            replyModeContainer.setVisibility(View.VISIBLE);
        edtAskContent.setOnTouchListener((view1, motionEvent) -> {
            view1.performClick();
            handleEditTextFocus(true);
            return false;
        });
        edtAskContent.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void afterTextChanged(Editable editable) {
                String s = edtAskContent.getText().toString().trim();
                if (s.length() == 0) {
                    btnRecord.setVisibility(View.INVISIBLE);
                    replyModeContainer.setVisibility(View.INVISIBLE);

                } else if (s.length() > 0) {
                    btnRecord.setVisibility(View.VISIBLE);
                    btnRecord.setImageDrawable(icSend);
                    replyModeContainer.setVisibility(View.VISIBLE);
                }

            }
        });
        layoutContainer.setOnTouchListener(this);

        if(getArguments() != null){
            String suggestedConcierge = getArguments().getString(IntentConstant.AC_SUGGESTED_CONCIERGE);
            if (suggestedConcierge!=null) {
                suggestedConcierge = suggestedConcierge.replace("\n", "<br/>");
                if (!TextUtils.isEmpty(suggestedConcierge + "<br/>")) {
                    edtAskContent.setText(Html.fromHtml(suggestedConcierge));
                }
            }
            String prefResponse = getArguments().getString(IntentConstant.AC_PREF_RESPONSE);
            if(!TextUtils.isEmpty(prefResponse)){
                if(prefResponse.toLowerCase().contains("email")){
                    checkBoxEmail.setChecked(true);
                }
                if(prefResponse.toLowerCase().contains("mobile")){
                    checkBoxPhone.setChecked(true);
                }
            }
            boolean invokeKeyboard = getArguments().getBoolean(IntentConstant.INVOKE_KEYBOARD, false);
            if(invokeKeyboard) {
                edtAskContent.post(() -> {
                    edtAskContent.requestFocus();
                    edtAskContent.setHint("");
                    edtAskContent.setCursorVisible(true);
                    ViewUtils.showSoftKey(edtAskContent);
                });
            }
        }
        setupCheckboxAnimation();
        ClickGuard.guard(btnRecord);

        keyboardInteractListener();
    }

    /** handle click on back hide keyboard close cursor */
    private void keyboardInteractListener() {
        ViewKeyboardListener.KeyboardEvent event = new ViewKeyboardListener.KeyboardEvent() {
            @Override
            public void showKeyboard() {
                //dummy do nothing
            }

            @Override
            public void hideKeyboard() {
                //dummy do nothing
                hideCursorFocus();
            }

            @Override
            public View getCurrentFocus() {
                return getActivity() != null ? getActivity().getCurrentFocus() : null;
            }
        };
        new ViewKeyboardListener(layoutContainer, event);
    }

    @Override
    public void onResume() {
        handleEditTextFocus(false);
        super.onResume();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString(STATE_RECORD_TEXT, edtAskContent.getText().toString());
        super.onSaveInstanceState(outState);
    }

    @OnClick(R.id.btn_record)
    public void recordOrSend(View view) {
        String text = edtAskContent.getText().toString().trim();
        if(text.length() > 0)
            if(!checkBoxPhone.isChecked() && !checkBoxEmail.isChecked()){
                dialogHelper.alert(null,  getString(R.string.choose_at_lease) );
            }
        if(checkBoxPhone.isChecked() || checkBoxEmail.isChecked()){
            listener.sendRequest(edtAskContent.getText().toString(), checkBoxEmail.isChecked(),
                    checkBoxPhone.isChecked());
            ViewUtils.hideSoftKey(edtAskContent);
        }

        if(text.length()  == 0 && !hasFocus) {
            listener.record();
        }
    }

    @OnClick(R.id.btn_call)
    public void callTheConcierge(View view) {
        listener.makePhoneCall(getString(R.string.phone_number_display_on_call));
    }

    @OnClick(R.id.tv_call_phone)
    public void internationalCall(View view) {
        listener.makePhoneCall(getString(R.string.call_international_number_display_on_call));
    }

    void speechToTextResult(String text) {
        edtAskContent.setText(text);
    }

    @Override
    public boolean onTouch(View view, MotionEvent event) {
        view.performClick();
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            Rect outRect = new Rect();
            edtAskContent.getGlobalVisibleRect(outRect);
            if (!outRect.contains((int)event.getRawX(), (int)event.getRawY())) {
                handleEditTextFocus(false);
            }
        }
        return false;
    }

    private void handleEditTextFocus(boolean hasFocus) {
        if(hasFocus) {
            if(edtAskContent.getText().toString().trim().length() > 0) {
                btnRecord.setVisibility(View.VISIBLE);
                btnRecord.setImageDrawable(icSend);
            } else {
                btnRecord.setVisibility(View.INVISIBLE);
            }
            showCursorFocus();
        } else {
            if(edtAskContent.getText().toString().trim().length() == 0) {
                if(edtAskContent.getText().length() == 0){
                    btnRecord.setVisibility(View.VISIBLE);
                    btnRecord.setImageDrawable(icRecord);
                }else{
                    btnRecord.setVisibility(View.INVISIBLE);
                }

            }else{
                btnRecord.setVisibility(View.VISIBLE);
                btnRecord.setImageDrawable(icSend);
            }
            hideCursorFocus();
            ViewUtils.hideSoftKey(edtAskContent);
        }
        this.hasFocus = hasFocus;
    }

    private void showCursorFocus() {
        edtAskContent.setHint("");
        edtAskContent.setCursorVisible(true);
        edtAskContent.requestFocus();
    }

    private void hideCursorFocus() {
        edtAskContent.setHint(hintText);
        edtAskContent.setCursorVisible(false);
    }

    private void setupCheckboxAnimation() {
        replyModeContainer.getViewTreeObserver().addOnGlobalLayoutListener(this);
        edtAskContent.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                boolean hasText = s.length() > 0;
                btnRecord.setImageDrawable(hasText ? icSend : icRecord);
                if (slideDown != null && hasText && !checkboxShowing) {
                    slideDown.start();
                    checkboxShowing = true;
                    return;
                }
                if (slideUp != null && !hasText && checkboxShowing) {
                    slideUp.start();
                    checkboxShowing = false;
                }
            }
        });
    }

    @Override
    public void onGlobalLayout() {
        slideUp = checkBoxAnimation(true);
        slideDown = checkBoxAnimation(false);
        checkboxShowing = edtAskContent.getText().length() > 0;
        if (!checkboxShowing) {
            slideUp.start();
        }
        replyModeContainer.getViewTreeObserver().removeOnGlobalLayoutListener(this);
    }

    private AnimatorSet checkBoxAnimation(boolean up) {
        AnimatorSet set = new AnimatorSet();
        ObjectAnimator move = ObjectAnimator
                .ofFloat(replyModeContainer, "translationY", up ? -replyModeContainer.getHeight() : 0);
        if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            set.play(move);
        } else {
            ObjectAnimator fade = ObjectAnimator
                    .ofFloat(replyModeContainer, "alpha", up ? 0 : 1);
            set.playTogether(move, fade);
        }
        set.setDuration(300);
        return set;
    }

    interface ViewEventsListener {
        void record();

        void sendRequest(String content, boolean email, boolean phone);

        void makePhoneCall(String number);
    }
}
