package com.ms.androidapp.presentation.base;

/**
 * Created by vinh.trinh on 4/26/2017.
 */

public interface BasePresenter<View> {
    void attach(View view);
    void detach();
}