package com.ms.androidapp.datalayer.datasource;

import com.ms.androidapp.datalayer.entity.geocoder.GeoCoderResponse;
import com.ms.androidapp.datalayer.restapi.GeoCoderApi;
import com.ms.androidapp.datalayer.retro2client.AppHttpClient;
import com.ms.androidapp.domain.model.LatLng;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by vinh.trinh on 7/31/2017.
 */

public class AppGeoCoder {

    public String getAddress(LatLng location) {
        GeoCoderApi geoCoderApi = AppHttpClient.getInstance().geoCoderApi();
        Call<GeoCoderResponse> request = geoCoderApi.getAddress(location.toString());
        try {
            Response<GeoCoderResponse> response = request.execute();
            GeoCoderResponse responseBody = response.body();
            if(response.isSuccessful() && responseBody.success()) {
                GeoCoderResponse.AddressComponent[] addressComponents = responseBody.addressComponents();
                StringBuilder stringBuilder = new StringBuilder(addressComponents[0].name());
                for (int i = 1; i < addressComponents.length; i++) {
                    stringBuilder.append(", ").append(addressComponents[i].name());
                }
                return stringBuilder.toString();
            } else {
                return "";
            }
        } catch (Exception e) {
            return "";
        }
    }
    public String getFormattedAddress(LatLng location) {
        GeoCoderApi geoCoderApi = AppHttpClient.getInstance().geoCoderApi();
        Call<GeoCoderResponse> request = geoCoderApi.getAddress(location.toString());
        try {
            Response<GeoCoderResponse> response = request.execute();
            GeoCoderResponse responseBody = response.body();
            if(response.isSuccessful() && responseBody.success()) {
                return responseBody.getFormattedAddress();
            } else {
                return "";
            }
        } catch (Exception e) {
            return "";
        }
    }
    public LatLng getLocation(String strAddress) {
        GeoCoderApi geoCoderApi = AppHttpClient.getInstance().geoCoderApi();
        Call<GeoCoderResponse> request = geoCoderApi.getLatLng(strAddress);
        try {
            Response<GeoCoderResponse> response = request.execute();
            GeoCoderResponse responseBody = response.body();
            if(response.isSuccessful() && responseBody.success()) {
                return new LatLng(responseBody.location().getLat(), responseBody.location().getLng());
            } else {
                return LatLng.INVALID;
            }
        } catch (Exception e) {
            return LatLng.INVALID;
        }
    }
    public GeoCoderResponse getFullGeoCoder(String strAddress){
        GeoCoderApi geoCoderApi = AppHttpClient.getInstance().geoCoderApi();
        Call<GeoCoderResponse> request = geoCoderApi.getLatLng(strAddress);
        try {
            Response<GeoCoderResponse> response = request.execute();
            GeoCoderResponse responseBody = response.body();
            if(response.isSuccessful() && responseBody.success()) {
                return responseBody;
            }
        } catch (Exception e) {
            return null;
        }
        return null;
    }
}
