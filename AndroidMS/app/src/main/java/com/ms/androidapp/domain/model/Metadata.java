package com.ms.androidapp.domain.model;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by vinh.trinh on 6/20/2017.
 */

public class Metadata {
    public final String onlineMemberID;
    public final String onlineMemberDetailID;
    public final String uuid;

    public Metadata(String jsonString) throws JSONException {
        JSONObject json = new JSONObject(jsonString);
        onlineMemberID = json.getString("id");
        onlineMemberDetailID = json.getString("detail");
        uuid = json.getString("uuid");
    }

    public Metadata(String memberID, String detailID, String uuid) {
        this.onlineMemberID = memberID;
        this.onlineMemberDetailID = detailID;
        this.uuid = uuid;
    }

    public JSONObject toJSON() throws JSONException {
        JSONObject json = new JSONObject();
        json.put("id", onlineMemberID);
        json.put("detail", onlineMemberDetailID);
        json.put("uuid", uuid);
        return json;
    }
}
