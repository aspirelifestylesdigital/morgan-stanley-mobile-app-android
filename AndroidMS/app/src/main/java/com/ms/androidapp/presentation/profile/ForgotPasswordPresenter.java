package com.ms.androidapp.presentation.profile;

import com.api.aspire.common.exception.BackendException;
import com.api.aspire.domain.usecases.RetrievePassword;
import com.ms.androidapp.App;
import com.api.aspire.common.constant.ErrCode;
import com.ms.androidapp.domain.usecases.MapProfileApp;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableCompletableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by vinh.trinh on 7/24/2017.
 */

public class ForgotPasswordPresenter implements ForgotPassword.Presenter {
    private CompositeDisposable disposables;
    private RetrievePassword retrievePassword;
    private ForgotPassword.View view;

    ForgotPasswordPresenter() {
        disposables = new CompositeDisposable();
        // TODO: 10/1/2018 remove file
//        this.retrievePassword = new RetrievePassword(new MapProfileApp());
    }


    @Override
    public void attach(ForgotPassword.View view) {
        this.view = view;
    }

    @Override
    public void detach() {
        view = null;
        disposables.dispose();
    }

    @Override
    public void retrievePassword(String email) {
        /*if(!App.getInstance().hasNetworkConnection()) {
            view.dismissProgressDialog();
            view.showErrorMessage(ErrCode.CONNECTIVITY_PROBLEM, null);
            return;
        }
        disposables.add(
        retrievePassword.param(email)
                .on(Schedulers.io(), AndroidSchedulers.mainThread())
                .execute(new DisposableCompletableObserver() {
                    @Override
                    public void onComplete() {
                        view.dismissProgressDialog();
                        view.showSuccessMessage();
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.dismissProgressDialog();
                        if(e instanceof BackendException) {
                            view.showErrorMessage(ErrCode.API_ERROR, e.getMessage());
                        } else {
                            view.showErrorMessage(ErrCode.UNKNOWN_ERROR, e.getMessage());
                        }
                    }
                })
        );*/
    }
}
