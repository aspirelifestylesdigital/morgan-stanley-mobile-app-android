package com.ms.androidapp.presentation.info;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.ms.androidapp.R;
import com.ms.androidapp.common.constant.AppConstant;
import com.ms.androidapp.common.constant.IntentConstant;
import com.ms.androidapp.presentation.base.BaseFragment;
import com.ms.androidapp.presentation.widget.DialogHelper;
import com.support.mylibrary.widget.JustifiedTextView;

import butterknife.BindView;

/**
 * Created by ThuNguyen on 6/20/2017.
 */

public class MasterCardUtilityFragment extends BaseFragment {

    @BindView(R.id.pbLoading)
    ProgressBar pbLoading;
    @BindView(R.id.content)
    JustifiedTextView tvContent;
    JustifiedTextView.IJustifyTextViewListener justifyTextViewListener;
    public MasterCardUtilityFragment(){}

    public static MasterCardUtilityFragment newInstance(AppConstant.MASTERCARD_COPY_UTILITY utilityType){
        Bundle args = new Bundle();
        args.putSerializable(IntentConstant.MASTERCARD_COPY_UTILITY, utilityType);
        MasterCardUtilityFragment fragment = new MasterCardUtilityFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public void setJustifyTextViewListener(JustifiedTextView.IJustifyTextViewListener contentScrollListener) {
        this.justifyTextViewListener = contentScrollListener;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_mastercard_utility, container, false);
}


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tvContent.getSettings();
        tvContent.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.ms_colorPrimary));
        ((MasterCardUtilityActivity)getActivity()).onFragmentCreated();
        if(justifyTextViewListener != null) {
            tvContent.setJustifyTextViewListener(justifyTextViewListener);
        }else{
            tvContent.setJustifyTextViewListener(defaultJustifyTextViewListener);
        }
    }

    public void renderContent(String content) {
        pbLoading.setVisibility(View.GONE);
//        content = "MAKING EACH EXPERIENCE A PRICELESS ONE, NO MATTER WHERE YOU ARE.<br/><br/>With the Mastercard Concierge app, we’ll help you book and arrange those memorable experiences that you won’t get anywhere else.  Whether you’re interested in travel planning, shopping services, dining reservations, or access to a once-in-a-lifetime event, Mastercard Concierge’s dedicated team will work diligently to fulfill your every request.<br/><br/>We look forward to making your experiences priceless, 24\\/7\\/365, everywhere and anywhere.  Just call us toll free at 1-877-288-6503 or email us at MastercardAppConcierge@vipdeskservice.com to book your experience now.  This is a free service for all Mastercard World or World Elite card holders.";
        if(!TextUtils.isEmpty(content)){
//            tvContent.setText(Html.fromHtml(HtmlUtils.adjustSomeHtmlTag(content), new URLImageParser(tvContent, getContext(), true, null),
//                    new HtmlTagHandler(tvContent.getFont())));
            tvContent.setText(content);
        }
    }
    JustifiedTextView.IJustifyTextViewListener defaultJustifyTextViewListener = new JustifiedTextView.IJustifyTextViewListener() {
        @Override
        public void onContentScroll(boolean hitToBottom) {
        }

        @Override
        public void onHyperLinkClicked(String url) {
            new DialogHelper(getActivity()).showLeavingAlert(url);
        }
    };
}
