package com.ms.androidapp.common.logic;

/**
 * Created by Thu Nguyen on 6/30/2017.
 */

public class StringUtils {
    private static final String CONTACT_US_1 = "Contact us for more information.";
    private static final String CONTACT_US_2 = "Contact us for more information";
    private static final String BROWSE_AND_BOOK_1 = "Browse and Book Sightseeing & Tours Worldwide.";
    private static final String BROWSE_AND_BOOK_2 = "Browse and Book Sightseeing & Tours Worldwide";

    public static String removeRedundantToken(String text){
        return text.replace(CONTACT_US_1, "").replace(CONTACT_US_2, "")
                .replace(BROWSE_AND_BOOK_1, "").replace(BROWSE_AND_BOOK_2, "");
    }
}
