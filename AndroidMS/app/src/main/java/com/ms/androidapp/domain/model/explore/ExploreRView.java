package com.ms.androidapp.domain.model.explore;

import android.os.Parcelable;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by vinh.trinh on 6/8/2017.
 */

public abstract class ExploreRView implements Parcelable {

    protected ItemType itemType;

    public ItemType getItemType() {
        return this.itemType;
    }

    public void setItemType(ItemType itemType) {
        this.itemType = itemType;
    }

    public abstract String getTitle();

    public abstract String getDescription();

    public abstract String getImageUrl();

    public abstract Integer getId();

    public abstract String getSuggestedToAC();
    public enum ItemType {
        NORMAL("Normal"),
        DINING("Dining"),
        CITY_GUIDE("City guide"),
        SEARCH("Search");
        private static final Map<String, ItemType> enumsByValue = new HashMap<String, ItemType>();
        static {
            for (ItemType type : ItemType.values()) {
                enumsByValue.put(type.value, type);
            }
        }

        private final String value;
        private ItemType(String value) {
            this.value = value;
        }

        public static ItemType getEnum(String value) {
            return enumsByValue.get(value);
        }

        public String getValue() {
            return value;
        }
    }
}
