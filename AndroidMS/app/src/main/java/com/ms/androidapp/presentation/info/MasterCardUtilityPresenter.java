package com.ms.androidapp.presentation.info;

import com.ms.androidapp.App;
import com.api.aspire.common.constant.ErrCode;
import com.ms.androidapp.datalayer.entity.GetClientCopyResult;
import com.ms.androidapp.domain.usecases.GetMasterCardCopy;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Thu Nguyen on 6/13/2017.
 */

public class MasterCardUtilityPresenter implements MasterCardUtility.Presenter {

    private GetMasterCardCopy getMasterCardCopy;
    private MasterCardUtility.View view;
    private CompositeDisposable disposables;

    MasterCardUtilityPresenter(GetMasterCardCopy getMasterCardCopy) {
        disposables = new CompositeDisposable();
        this.getMasterCardCopy = getMasterCardCopy;
    }

    @Override
    public void attach(MasterCardUtility.View view) {
        this.view = view;
    }

    @Override
    public void detach() {
        disposables.dispose();
        this.view = null;
    }

    @Override
    public void getMasterCardCopy(String type) {
        if(!App.getInstance().hasNetworkConnection()) {
            view.loadEmptyContent();
            view.showErrorMessage(ErrCode.CONNECTIVITY_PROBLEM);
            return;
        }
        disposables.add(getMasterCardCopy.param(type)
                .on(Schedulers.io(), AndroidSchedulers.mainThread())
                .execute(new MasterCardUtilityPresenter.GetContentFullObserver()));
    }

    private final class GetContentFullObserver extends DisposableSingleObserver<GetClientCopyResult> {

        @Override
        public void onSuccess(GetClientCopyResult result) {
            view.onGetMasterCardCopy(result);
            dispose();
        }

        @Override
        public void onError(Throwable e) {
            view.loadEmptyContent();
            view.showErrorMessage(ErrCode.UNKNOWN_ERROR);
            dispose();
        }
    }
}
