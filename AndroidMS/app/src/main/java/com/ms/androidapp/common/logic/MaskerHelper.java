package com.ms.androidapp.common.logic;

import java.util.Arrays;

/**
 * Created by vinh.trinh on 4/28/2017.
 */

public class MaskerHelper {

    public enum INPUT {NAME, EMAIL, PHONE, ZIPCODE, FREE, ANSWER}

    public static String mask(INPUT which, String input) {
        switch (which) {
            case NAME:
                return nameMasking(input);
            case EMAIL:
                return emailMasking(input);
            case PHONE:
                return phoneMasking(input);
            case ZIPCODE:
                return zipCodeMasking(input);
            case ANSWER:
                return answerMasking(input);
        }
        return input;
    }


    /**
     * display last 3 characters before the @ sign. Example, For Multiple characters: *****xyz@gmail.com,
     * For 2 and 3 characters: *x@gmail.com, **x@gmail.com, For 1 Character: *@gmail.com
     */
    private static String emailMasking(String email) {
        int index = email.indexOf('@');
        if (index <= 0) return email;
        if (index == 1) return "*" + email.substring(index);
        if (index <= 3) return masking(index - 1) + email.substring(index - 1);
        return masking(index - 3) + email.substring(index - 3);
    }

    /**
     * masking behaviour for phone
     */
    private static String phoneMasking(String phone) {
        int length = phone.length();
        if (length <= 3) return phone;
        char[] digits = phone.toCharArray();
        for (int i = 0; i < digits.length - 3; i++) {
            /*if(digits[i] == '-') continue;
            digits[i] = '*';*/
//            if(i != 1)
            digits[i] = '*';
        }
        return new String(digits);
    }

    /**
     * turn all chars into asterisk(*) until last 3 characters
     * ignore if less than 3 chars
     */
    private static String nameMasking(String input) {
        int length = input.length();
        if (length <= 0) return input;
        if (length == 1) return "*";
        if (length <= 3) return masking(length - 1) + input.substring(length - 1);
        return masking(length - 3) + input.substring(length - 3);
    }

    private static String zipCodeMasking(String input) {
        /*int length = input.length();
        if (length <= 0) return input;
        if (length == 1) return "*";
        if (length <= 3) return masking(length - 1) + input.substring(length - 1);*/
        return input;
    }

    private static String masking(int length) {
        char[] charArray = new char[length];
        Arrays.fill(charArray, '*');
        return new String(charArray);
    }

    private static String answerMasking(String input) {
        int length = input.length();
        if (length <= 0) return "*******";
        else return masking(length );
    }

    public static boolean answerMaskValue(String answerView){
        return "*******".equals(answerView);
    }
}
