package com.ms.androidapp.common.constant;

/**
 * Created by tung.phan on 5/22/2017.
 */

public interface DataTypeConstant {
    String TYPE_HOTEL = "Hotel";
    String TYPE_EVENT = "Event";
}
