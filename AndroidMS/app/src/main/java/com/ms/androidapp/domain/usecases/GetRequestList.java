package com.ms.androidapp.domain.usecases;

import com.api.aspire.common.exception.BackendException;
import com.ms.androidapp.datalayer.entity.askconcierge.ACRequestItem;
import com.ms.androidapp.datalayer.entity.askconcierge.GetACRequest;
import com.ms.androidapp.domain.model.Metadata;
import com.ms.androidapp.domain.model.RequestDetailData;
import com.ms.androidapp.domain.model.RequestItemView;
import com.ms.androidapp.domain.repository.ACRepository;
import com.ms.androidapp.presentation.requestdetail.TextDetailBuilder;

import org.json.JSONException;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;

/**
 * Created by vinh.trinh on 8/30/2017.
 */

public class GetRequestList extends UseCase<List<RequestItemView>, GetRequestList.Params> {

    private ACRepository acRepository;
    private List<ACRequestItem> data;
    private SimpleDateFormat dateFormat;
//    private SimpleDateFormat compareFormat;
    private SimpleDateFormat datePrinter;
    private TextDetailBuilder textDetailBuilder;
    private final long fiveMinutesMs;

    public GetRequestList(ACRepository acRepository) {
        this.acRepository = acRepository;
        data = new ArrayList<>();
        dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.US);
        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT+0"));
//        compareFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.US);
//        compareFormat.setTimeZone(TimeZone.getTimeZone("GMT+0"));
        datePrinter = new SimpleDateFormat("MM/dd/yyyy | h:mm a", Locale.US);
        fiveMinutesMs = (60 * 5)*1000;
        textDetailBuilder = new TextDetailBuilder();
    }

    @Override
    Observable<List<RequestItemView>> buildUseCaseObservable(Params params) {
        return null;
    }

    @Override
    public Single<List<RequestItemView>> buildUseCaseSingle(Params params) {
        return Single.create(e -> {
            GetACRequest requestBody = new GetACRequest(params.accessToken, params.metadata.onlineMemberID);
            requestBody.list(params.rowStart, params.rowEnd);
            try {
                int startIndex = this.data.size();
                List<ACRequestItem> data = acRepository.getRequestList(requestBody);
                this.data.addAll(data);
                List<RequestItemView> viewData = new ArrayList<>(data.size());

                Calendar current = Calendar.getInstance(Locale.US);
                for (int i = 0; i < data.size(); i++) {
                    viewData.add(bindView(data.get(i), startIndex + i, current.getTimeInMillis()));
                }
                e.onSuccess(viewData);
            } catch (BackendException | IOException | JSONException exception) {
                e.onError(exception);
            }
        });
    }

    @Override
    Completable buildUseCaseCompletable(Params params) {
        return null;
    }

    private RequestItemView bindView(ACRequestItem item, int index, long currentTime) throws
            ParseException, JSONException {
        Date date = dateFormat.parse(item.createdDate());
//        Date compareDate = compareFormat.parse(item.createdDate());
        return new RequestItemView(
                textDetailBuilder.getName(item.type(), item.details()),
                formatDateView(date),
                index,
                viewStatus("Open".equals(item.status()), item.mode(), date, currentTime));
    }


    private String formatDateView(Date date) {
        return datePrinter.format(date);
    }

    private RequestItemView.STATUS viewStatus(boolean open, String requestMode, Date date, long currentTime) {
        if("CANCEL".equals(requestMode) || !open) return RequestItemView.STATUS.CLOSED;
        return currentTime - date.getTime() > fiveMinutesMs ? RequestItemView.STATUS.OPEN :
                RequestItemView.STATUS.PENDING;
    }

    public RequestDetailData getDatum(int index) throws ParseException {
        ACRequestItem item = data.get(index);
        return new RequestDetailData(
                item.transactionID(),
                item.type(),
                item.functionality(),
                datePrinter.format(dateFormat.parse(item.createdDate())),
                item.eventDate(),
                item.pickupDate(),
                item.startDate(),
                item.endDate(),
                item.numberOfAdults(),
                item.numberOfChildren(),
                item.details(),
                item.prefResponse(),
                "Open".equals(item.status()) && !"CANCEL".equals(item.mode())
        ).setCity(item.city()).setState(item.state()).setCountry(item.country());
    }

    public void reset() {
        data.clear();
    }

    public static class Params {
        private String accessToken;
        private Metadata metadata;
        private int rowStart;
        private int rowEnd;

        public Params(String accessToken, Metadata metadata, int rowStart, int rowEnd) {
            this.accessToken = accessToken;
            this.metadata = metadata;
            this.rowStart = rowStart;
            this.rowEnd = rowEnd;
        }
    }
}
