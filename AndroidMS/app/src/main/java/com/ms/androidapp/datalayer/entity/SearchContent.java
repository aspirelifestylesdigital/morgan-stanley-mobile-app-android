package com.ms.androidapp.datalayer.entity;

import com.google.gson.annotations.SerializedName;

/**
 * Created by vinh.trinh on 6/22/2017.
 */

public class SearchContent {
    @SerializedName("ID")
    private Integer ID;
    @SerializedName("SecondaryID")
    private Integer secondaryID;
    @SerializedName("Description")
    private String description;
    @SerializedName("Product")
    private String product;
    @SerializedName("Title")
    private String title;
    @SerializedName("HasOffer")
    private Boolean hasOffer;
    @SerializedName("Address1")
    private String address1;
    @SerializedName("Address2")
    private String address2;
    @SerializedName("Address3")
    private String address3;
    @SerializedName("AnswerUserDefined1")
    private String cuisine;
    @SerializedName("GeographicRegion")
    private String geographicRegion;
    @SerializedName("SubCategory")
    private String subCategory;
    @SerializedName("Category")
    private String category;

    public Integer ID() {
        return ID;
    }

    public Integer secondaryID() {
        return secondaryID;
    }

    public String description() {
        return description;
    }

    public String product() {
        return product;
    }

    public String title() {
        return title;
    }

    public Boolean hasOffer() {
        return hasOffer;
    }

    public String address() {
        return address1 + ", " + address3;
    }

    public String cuisine() {
        return cuisine;
    }

    public String geographicRegion() {
        return geographicRegion;
    }

    public String subCategory() {
        return subCategory;
    }

    public String address3() {
        return address3;
    }

    public String category() {
        return category;
    }
}
