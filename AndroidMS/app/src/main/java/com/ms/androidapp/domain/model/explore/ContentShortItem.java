package com.ms.androidapp.domain.model.explore;

import android.annotation.SuppressLint;

/**
 * Created by ThuNguyen on 6/20/2017.
 */
@SuppressLint("ParcelCreator")
public class ContentShortItem extends ExploreRViewItem {

    public ContentShortItem(int id, String title, String description, String imageURL, boolean hasStar, String summary, int dataIndex) {
        super(id, title, description, imageURL, hasStar, summary, dataIndex);
    }
}
