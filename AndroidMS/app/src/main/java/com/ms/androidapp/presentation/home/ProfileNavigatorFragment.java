package com.ms.androidapp.presentation.home;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ms.androidapp.R;
import com.ms.androidapp.presentation.base.BaseFragment;

import butterknife.OnClick;

/**
 * Created by vinh.trinh on 9/7/2017.
 */

public class ProfileNavigatorFragment extends BaseFragment {

    private EventsListener eventsListener;


    public static ProfileNavigatorFragment newInstance() {
        Bundle args = new Bundle();
        ProfileNavigatorFragment fragment = new ProfileNavigatorFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof EventsListener) {
            eventsListener = (EventsListener) context;
        } else {
            throw new ClassCastException(context.toString() + " must implement ProfileNavigatorFragment.EventsListener.");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_profile_navigator, container, false);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        applyDrawable(view.findViewById(R.id.profile_nav_contact));
        applyDrawable(view.findViewById(R.id.profile_nav_preferences));
        applyDrawable(view.findViewById(R.id.profile_nav_change_pwd));
    }

    void applyDrawable(TextView view) {
        Drawable drawableEnd = ContextCompat.getDrawable(view.getContext(), R.drawable.ic_arrow_right);
        ColorStateList colorStateList = ContextCompat.getColorStateList(view.getContext(), R.color.button_tint_color);
        DrawableCompat.setTintList(drawableEnd, colorStateList);
        view.setCompoundDrawablesWithIntrinsicBounds(null,null,drawableEnd,null);
    }

    @OnClick(R.id.profile_nav_contact)
    public void openProfile(View view) {

        eventsListener.openProfile();
    }

    @OnClick(R.id.profile_nav_preferences)
    public void openPreferences(View view) {
        eventsListener.openPreferences();
    }

    @OnClick(R.id.profile_nav_change_pwd)
    public void openChangePassword(View view) {
        eventsListener.openChangePassword();
    }

    public interface EventsListener {
        void openProfile();
        void openPreferences();
        void openChangePassword();
    }
}
