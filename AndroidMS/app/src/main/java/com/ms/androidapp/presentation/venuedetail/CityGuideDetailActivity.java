package com.ms.androidapp.presentation.venuedetail;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.BaseTarget;
import com.bumptech.glide.request.target.SizeReadyCallback;
import com.bumptech.glide.request.target.Target;
import com.ms.androidapp.App;
import com.ms.androidapp.R;
import com.ms.androidapp.common.constant.AppConstant;
import com.api.aspire.common.constant.ErrCode;
import com.ms.androidapp.common.constant.IntentConstant;
import com.ms.androidapp.common.glide.GlideHelper;
import com.ms.androidapp.common.logic.HtmlTagHandler;
import com.ms.androidapp.common.logic.HtmlUtils;
import com.ms.androidapp.common.logic.PermissionUtils;
import com.ms.androidapp.common.logic.ShareHelper;
import com.ms.androidapp.common.logic.TextViewLinkHandler;
import com.ms.androidapp.common.logic.urlimage.URLImageParser;
import com.ms.androidapp.datalayer.repository.B2CDataRepository;
import com.ms.androidapp.domain.model.explore.CityGuideDetailItem;
import com.ms.androidapp.domain.model.explore.ExploreRView;
import com.ms.androidapp.domain.model.explore.SearchDetailItem;
import com.ms.androidapp.domain.usecases.GetCityGuideDetail;
import com.ms.androidapp.presentation.base.CommonActivity;
import com.ms.androidapp.presentation.request.AskConciergeActivity;
import com.ms.androidapp.presentation.venuedetail.mapview.VenueDetailMapActivity;
import com.ms.androidapp.presentation.widget.DialogHelper;
import com.support.mylibrary.widget.LetterSpacingTextView;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by ThuNguyen on 6/8/2017.
 */

public class CityGuideDetailActivity extends CommonActivity implements CityGuideDetail.View{
    @BindView(R.id.city_guide_detail_layout)
    View cityGuideDetailLayout;
    @BindView(R.id.loading)
    ProgressBar pbLoading;
    @BindView(R.id.explore_detail_image)
    ImageView ivDetailImage;
    @BindView(R.id.explore_action_book)
    ImageButton ivActionBook;
    @BindView(R.id.explore_action_share)
    ImageButton ivActionShare;
    @BindView(R.id.explore_name)
    TextView tvCityGuideName;
    @BindView(R.id.explore_address)
    TextView tvCityGuideAddress;
    @BindView(R.id.explore_see_map)
    TextView tvCityGuideSeeMap;
    @BindView(R.id.explore_website)
    TextView tvCityGuideWebsite;
    @BindView(R.id.explore_insider_tip_root_layout)
    View insiderTipLayout;
    @BindView(R.id.insider_tip_content)
    LetterSpacingTextView tvInsiderTip;

    @BindView(R.id.explore_description_layout)
    View cityGuideDescriptionLayout;
    @BindView(R.id.explore_description)
    LetterSpacingTextView tvCityGuideDescription;
    DialogHelper dialogHelper;
    Bitmap detailBitmap;
    private CityGuideDetailItem cityGuideDetailItem;
    private SearchDetailItem searchDetailItem;
    private CityGuideDetailPresenter presenter;
    private String categoryName;
    private int imgIndex;
    private TextViewLinkHandler textViewLinkHandler = new TextViewLinkHandler() {
        @Override
        public void onLinkClick(String url) {
            new DialogHelper(CityGuideDetailActivity.this).showLeavingAlert(url);
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city_guide_detail);
        setToolbarColor(R.color.ms_colorPrimary);
        // Make hyperlink in textview clickable
        tvInsiderTip.setMovementMethod(textViewLinkHandler);
        tvCityGuideDescription.setMovementMethod(textViewLinkHandler);

        tvCityGuideSeeMap.setPaintFlags(tvCityGuideSeeMap.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        tvCityGuideWebsite.setPaintFlags(tvCityGuideWebsite.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        categoryName = getIntent().getStringExtra(IntentConstant.CATEGORY_NAME);
        // Suppress the letter spacing
        ((LetterSpacingTextView)title).setLetterSpacing_(0f);
        title.setAllCaps(true);

        // Get data
        ExploreRView exploreRView = getIntent().getParcelableExtra(IntentConstant.EXPLORE_DETAIL);
        if(exploreRView instanceof CityGuideDetailItem){
            cityGuideDetailItem = (CityGuideDetailItem) exploreRView;
            renderUI();
        }else{
            searchDetailItem = (SearchDetailItem) exploreRView;
            setTitle(Html.fromHtml(searchDetailItem.title));
            cityGuideDetailLayout.setVisibility(View.INVISIBLE);
            pbLoading.setVisibility(View.VISIBLE);
            imgIndex = searchDetailItem.cityGuideSubCategoryIndex;

            // Call API
            presenter = new CityGuideDetailPresenter(new GetCityGuideDetail(new B2CDataRepository()));
            presenter.attach(this);
            presenter.getCityGuideDetail(searchDetailItem.secondaryID, searchDetailItem.ID);
        }
        if(savedInstanceState == null){
            // Track GA
            App.getInstance().track(AppConstant.GA_TRACKING_SCREEN_NAME.VENUE_DETAIL.getValue());
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(presenter != null){
            presenter.detach();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PermissionUtils.EXTERNAL_STORAGE_REQUEST_CODE:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Call share again
                    ShareHelper.getInstance().share(this, cityGuideDetailItem, detailBitmap);
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void renderUI(){
        pbLoading.setVisibility(View.GONE);
        if(cityGuideDetailItem != null){
            cityGuideDetailLayout.setVisibility(View.VISIBLE);
            Target<Bitmap> target = new BaseTarget<Bitmap>() {
                @Override
                public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                    detailBitmap = resource;
                }

                @Override
                public void getSize(SizeReadyCallback cb) {

                }
            };
            if(!TextUtils.isEmpty(cityGuideDetailItem.getImageUrl())){
                GlideHelper.getInstance().loadImage(cityGuideDetailItem.getImageUrl(),
                        0, ivDetailImage, 0, target);
            }else if(cityGuideDetailItem.imageResId != 0){
                GlideHelper.getInstance().loadImage(mapImageResIdForLarger(cityGuideDetailItem.imageResId),
                        0, ivDetailImage, 0, target);
            }
            // Dining name
            if(TextUtils.isEmpty(cityGuideDetailItem.title)){
                tvCityGuideName.setVisibility(View.GONE);
            }else{
                tvCityGuideName.setVisibility(View.VISIBLE);
                tvCityGuideName.setText(Html.fromHtml(cityGuideDetailItem.title));
                setTitle(Html.fromHtml(cityGuideDetailItem.title));
            }
            // Dining address
            String diningAddress = cityGuideDetailItem.getDisplayAddress();
            if(TextUtils.isEmpty(diningAddress)){
                tvCityGuideAddress.setVisibility(View.GONE);
            }else{
                tvCityGuideAddress.setVisibility(View.VISIBLE);
                tvCityGuideAddress.setText(diningAddress);
            }
            // See map --- later
            // Website
            if(TextUtils.isEmpty(cityGuideDetailItem.url) || !cityGuideDetailItem.url.contains("http")){
                tvCityGuideWebsite.setVisibility(View.GONE);
            }else{
                tvCityGuideWebsite.setVisibility(View.VISIBLE);
                tvCityGuideWebsite.setText(cityGuideDetailItem.url);
            }
            // Insider tip
            if(TextUtils.isEmpty(cityGuideDetailItem.insiderTips)){
                insiderTipLayout.setVisibility(View.GONE);
            }else{
                insiderTipLayout.setVisibility(View.VISIBLE);
                tvInsiderTip.setText(Html.fromHtml(HtmlUtils.adjustSomeHtmlTag(cityGuideDetailItem.insiderTips), null,
                        new HtmlTagHandler()));
            }
            // Description
            if(TextUtils.isEmpty(cityGuideDetailItem.description)){
                cityGuideDescriptionLayout.setVisibility(View.GONE);
            }else{
                cityGuideDescriptionLayout.setVisibility(View.VISIBLE);
                tvCityGuideDescription.setText(Html.fromHtml(HtmlUtils.adjustSomeHtmlTag(cityGuideDetailItem.description), new URLImageParser(tvCityGuideDescription, this, false, null),
                        new HtmlTagHandler()));
            }
        }
    }

    private int mapImageResIdForLarger(int resId){
        if(resId == R.drawable.sub_category_accomodation){
            return R.drawable.sub_category_accomodation_1;
        }else if(resId == R.drawable.sub_category_bar_club){
            return R.drawable.sub_category_bar_club_1;
        }else if(resId == R.drawable.sub_category_culture){
            return R.drawable.sub_category_culture_1;
        }else if(resId == R.drawable.sub_category_dining){
            return R.drawable.sub_category_dining_1;
        }else if(resId == R.drawable.sub_category_shopping){
            return R.drawable.sub_category_shopping_1;
        }
        else if(resId == R.drawable.sub_category_spas){
            return R.drawable.sub_category_spas_1;
        }
        return 0;
    }

    private int imgResource(int index) {
        switch (index) {
            case 0:
                return R.drawable.sub_category_accomodation;
            case 1:
                return R.drawable.sub_category_bar_club;
            case 2:
                return R.drawable.sub_category_culture;
            case 3:
                return R.drawable.sub_category_dining;
            case 4:
                return R.drawable.sub_category_shopping;
            default:
                return  R.drawable.sub_category_spas;
        }
    }

    @OnClick({R.id.explore_see_map, R.id.explore_website, R.id.explore_action_book, R.id.explore_action_share})
    public void onClick(View view){
        switch (view.getId()){
            case R.id.explore_see_map:
                //new DialogHelper(CityGuideDetailActivity.this)
//                        .showLeavingAlert((dialogInterface, i) -> MapSearchHelper.getInstance().searchNameAndShowOnMap(CityGuideDetailActivity.this, cityGuideDetailItem.getQuerySearchOnGoogleMap(), cityGuideDetailItem.getTitle()));
                //        .showLeavingAlert((dialogInterface, i) ->
                if(!App.getInstance().hasNetworkConnection()) {
                    dialogHelper = new DialogHelper(this);
                    dialogHelper.networkUnavailability(ErrCode.CONNECTIVITY_PROBLEM, null);
                }
                else
                                startActivity(new Intent(this, VenueDetailMapActivity.class).putExtra("cityGuide",cityGuideDetailItem));
                //        );
                break;
            case R.id.explore_website:
                new DialogHelper(CityGuideDetailActivity.this).showLeavingAlert(cityGuideDetailItem.url);
                break;
            case R.id.explore_action_book:
                Intent intent = new Intent(this, AskConciergeActivity.class);
                intent.putExtra(IntentConstant.SELECTED_CITY, cityGuideDetailItem.city);
                intent.putExtra(IntentConstant.SELECTED_CATEGORY, cityGuideDetailItem.subCategoryName);
                intent.putExtra(IntentConstant.AC_SUGGESTED_CONCIERGE, cityGuideDetailItem.title);
                startActivity(intent);
                break;
            case R.id.explore_action_share:
                ShareHelper.getInstance().share(this, cityGuideDetailItem, detailBitmap);
                break;
        }
    }

    @Override
    public void onGetCityGuideDetailFinished(CityGuideDetailItem cityGuideDetailItem) {
        this.cityGuideDetailItem = cityGuideDetailItem;
        this.cityGuideDetailItem.setImageResId(imgResource(imgIndex));
        renderUI();
    }

    @Override
    public void onUpdateFailed() {
        pbLoading.setVisibility(View.GONE);
    }
}
