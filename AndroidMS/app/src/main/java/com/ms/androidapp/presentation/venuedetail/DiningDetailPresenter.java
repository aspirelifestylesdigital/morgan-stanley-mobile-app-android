package com.ms.androidapp.presentation.venuedetail;

import com.ms.androidapp.domain.model.explore.DiningDetailItem;
import com.ms.androidapp.domain.usecases.GetDiningDetail;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by ThuNguyen on 6/13/2017.
 */

public class DiningDetailPresenter implements DiningDetail.Presenter {

    private GetDiningDetail getDiningDetail;
    private DiningDetail.View view;
    private CompositeDisposable disposables;

    DiningDetailPresenter(GetDiningDetail getDiningDetail) {
        disposables = new CompositeDisposable();
        this.getDiningDetail = getDiningDetail;
    }


    @Override
    public void attach(DiningDetail.View view) {
        this.view = view;
    }

    @Override
    public void detach() {
        disposables.dispose();
        this.view = null;
    }

    @Override
    public void getDiningDetail(Integer categoryId, Integer itemId) {
        disposables.add(getDiningDetail.param(new GetDiningDetail.Params(categoryId, itemId))
                .on(Schedulers.io(), AndroidSchedulers.mainThread())
                .execute(new DiningDetailPresenter.GetDiningDetailObserver()));
    }

    private final class GetDiningDetailObserver extends DisposableSingleObserver<DiningDetailItem> {

        @Override
        public void onSuccess(DiningDetailItem diningDetailItem) {
            view.onGetDiningDetailFinished(diningDetailItem);
            dispose();
        }

        @Override
        public void onError(Throwable e) {
            view.onUpdateFailed();
            dispose();
        }
    }
}
