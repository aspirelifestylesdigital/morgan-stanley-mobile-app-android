package com.ms.androidapp.common.constant;

/**
 * Created by tung.phan on 5/23/2017.
 */

public interface RequestCode {

    int SELECT_CITY = 100;
    int SELECT_CATEGORY = 101;
    int SELECT_SUB_CATEGORY = 104;
    int SEARCH_INPUT = 105;
    int PROFILE = 106;
    int USER_PREFERENCES = 107;
    int CHANGE_PASSWORD = 108;
    int SIGN_IN_SIGN_UP = 109;
    int SIGN_IN_FORGOT = 110;
}
