package com.ms.androidapp.datalayer.restapi;

import com.ms.androidapp.datalayer.entity.geocoder.GeoCoderResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by vinh.trinh on 8/14/2017.
 */

public interface GeoCoderApi {

    @GET("geocode/json?sensor=false")
    Call<GeoCoderResponse> getLatLng(@Query("address") String address);

    @GET("geocode/json?sensor=false")
    Call<GeoCoderResponse> getAddress(@Query("latlng") String location);
}
