package com.ms.androidapp.common.logic;

import android.text.TextUtils;

import com.ms.androidapp.common.constant.CountryCode;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by vinh.trinh on 4/28/2017.
 */

public final class Validator {
    public final int NAME_MIN_LENGTH = 2;
    public final int NAME_MAX_LENGTH = 25;
    public final int PHONE_MAX_LENGTH = 1;
    private final int PASSWORD_MIN_LENGTH = 1;
    public final int EMAIL_MAX_LENGTH = 100;
    public final int ZIP_CODE_LENGTH_1 = 1;
    public final int ZIP_CODE_LENGTH_10 = 10;// include "-"
    public final String BIN_CODE = "545212";
    public final String PASSCODE = "atyourservice";

    private final String EMAIL_REGEX = "(?:[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-zA-Z0-9-]*[a-zA-Z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";

    public boolean email(String email) {
        boolean isEmailValid = email.matches(EMAIL_REGEX);
        if(isEmailValid){
            if(email.contains(".")) {
                String passEmail = email.substring(email.lastIndexOf("."));
                if(passEmail.length()>4){
                    isEmailValid = false;
                }
            }
        }
        return isEmailValid ;
    }

    public boolean phone(String phone) {//length?
//        return phone.length() >= PHONE_MAX_LENGTH;
        Pattern pattern;
        Matcher matcher;
        if (phone != null) {
            final String SECRET_PATTERN = "^[0-9]*$";
            pattern = Pattern.compile(SECRET_PATTERN);
            matcher = pattern.matcher(phone);
            return matcher.matches() && phone.length() >= PHONE_MAX_LENGTH;
        }else {
            return false;
        }
    }

    public boolean name(String name) {
        return name.length() >= NAME_MIN_LENGTH;
    }

    public boolean password(String pwd, String confirmPwd) {
        return pwd.length() > 0 && confirmPwd.equals(pwd);
    }
    public boolean secretValidator(String secretWord) {
        Pattern pattern;
        Matcher matcher;
        if(secretWord!=null) {
            final String SECRET_PATTERN =
                    "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\\$%\\^&\\*]).{10,25}";
            pattern = Pattern.compile(SECRET_PATTERN);
            matcher = pattern.matcher(secretWord);
            return matcher.matches();
        } else {
          return false;
        }
//        return secretWord.length() > 0;
    }

    public boolean secretValidatorWeekPassword(String minPass) {
        return minPass.length() >= PASSWORD_MIN_LENGTH;
    }

    public boolean specialChars(String specialChars){
        Pattern pattern;
        Matcher matcher;
        if(specialChars!=null ) {
            final  String SECRET_PATTERN =
                    "^[‘’‛'a-zA-Z0-9-\\s]*$";
            pattern = Pattern.compile(SECRET_PATTERN);
            matcher = pattern.matcher(specialChars);
            return matcher.matches();
        } else {
            return false;
        }

    }

    public boolean phoneFormatValidator(String phoneNumber) {
        Pattern pattern;
        Matcher matcher;
        if(phoneNumber!=null && phoneNumber.trim().length()>0) {
            final String PHONE_PATTERN = "^\\+?\\d+";
            //"^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\\$%\\^&\\*\\[\\]\"\\';:_\\-<>\\., =\\+\\/\\\\]).{10,25}";
            pattern = Pattern.compile(PHONE_PATTERN);
            matcher = pattern.matcher(phoneNumber);
            return matcher.matches();
        } else {
            return false;
        }
    }

    public boolean zipCode(String zipCode) {//5 or 10
//        return zipCode.length() >= ZIP_CODE_LENGTH_1;
        Pattern pattern;
        Matcher matcher;
        if (zipCode != null) {
            final String SECRET_PATTERN = "^[a-zA-Z0-9]{1,10}$";
            pattern = Pattern.compile(SECRET_PATTERN);
            matcher = pattern.matcher(zipCode);
            return matcher.matches() ;
        }else {
            return false;
        }
    }

    public boolean binCode(String binCode) {return  binCode.equals(BIN_CODE) ;}

    public boolean passCode(String passCode) { return passCode.equalsIgnoreCase(PASSCODE);}

    public String findCountryCodeFromScratch(String val) {
        int length = val.length() < 4 ? val.length() : 4;
        for (int i = 1; i <= length; i++) {
            String countryCode = val.substring(0, i);
            if (CountryCode.contains(countryCode)) return countryCode;
        }
        return "";
    }
    public boolean answerSecurity(String answer) {
        return answer.trim().length() >= 4;
    }

    /**
     * rule secret have contain text first name, last name, username
     * */
    public boolean secretRuleContain(String secret, String email, String firstName, String lastName) {
        if(TextUtils.isEmpty(secret)){
            return false;
        }

        secret = secret.toLowerCase();
        email = email.toLowerCase();
        firstName = firstName.toLowerCase();
        lastName = lastName.toLowerCase();

        try {
            if(email.contains("@") && email.split("@").length > 1){
                email = email.split("@")[0];
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return secret.contains(email)
                || secret.contains(firstName)
                || secret.contains(lastName);
    }
}
