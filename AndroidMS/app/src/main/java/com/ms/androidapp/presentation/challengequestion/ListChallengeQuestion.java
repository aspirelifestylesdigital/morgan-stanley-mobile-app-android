package com.ms.androidapp.presentation.challengequestion;

import com.api.aspire.common.constant.ErrCode;
import com.api.aspire.domain.model.SecurityQuestion;
import com.ms.androidapp.presentation.base.BasePresenter;

import java.util.List;

public interface ListChallengeQuestion {
    interface View {
        void showProgressDialog();
        void dismissProgressDialog();
        void showErrorMessage(ErrCode errCode, String extraMsg);
        void showSuccessMessage();
        void getChallengeQuestions(List<SecurityQuestion> questions);
    }

    interface Presenter extends BasePresenter<View> {
        void getChallengeQuestions();

        String getQuestionContent(int position);
    }
}
