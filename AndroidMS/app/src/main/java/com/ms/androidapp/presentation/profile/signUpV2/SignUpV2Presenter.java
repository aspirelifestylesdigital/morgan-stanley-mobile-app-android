package com.ms.androidapp.presentation.profile.signUpV2;

import android.content.Context;
import android.text.TextUtils;

import com.api.aspire.common.constant.ErrCode;
import com.api.aspire.data.datasource.RemoteChangeRecoveryQuestionDataStore;
import com.api.aspire.data.datasource.RemoteUserProfileOKTADataStore;
import com.api.aspire.data.entity.userprofile.pma.ProfilePMAMapView;
import com.api.aspire.data.preference.PreferencesStorageAspire;
import com.api.aspire.data.repository.ChangeSecurityQuestionDataRepository;
import com.api.aspire.data.repository.ProfileDataAspireRepository;
import com.api.aspire.data.repository.ProfileDataPMARepository;
import com.api.aspire.data.repository.UserProfileOKTADataRepository;
import com.api.aspire.domain.usecases.ChangeSecurityQuestion;
import com.api.aspire.domain.usecases.CreateProfileCase;
import com.api.aspire.domain.usecases.GetProfilePMA;
import com.api.aspire.domain.usecases.GetToken;
import com.api.aspire.domain.usecases.SignInCase;
import com.api.aspire.domain.usecases.SignUpCase;
import com.ms.androidapp.App;
import com.ms.androidapp.domain.mapper.profile.MapProfile;
import com.ms.androidapp.domain.model.Profile;
import com.ms.androidapp.domain.usecases.MapProfileApp;

import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableCompletableObserver;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class SignUpV2Presenter implements SignUpV2.Presenter {

    private CompositeDisposable disposables;
    private SignUpV2.View view;
    private CreateProfileCase createProfileCase;

    private ProfilePMAMapView profilePMAMapView;
    private ChangeSecurityQuestion.Param securityQuestionParam;
    private PreferencesStorageAspire prefStorage;

    private MapProfile mapProfile;
    private SignUpCase signUpCase;

    SignUpV2Presenter(Context c) {
        disposables = new CompositeDisposable();
        this.prefStorage = new PreferencesStorageAspire(c);
        MapProfileApp mapLogic = new MapProfileApp();

        RemoteUserProfileOKTADataStore remoteProfileOKTA = new RemoteUserProfileOKTADataStore();
        UserProfileOKTADataRepository userProfileOKTARepository = new UserProfileOKTADataRepository(remoteProfileOKTA);
        ChangeSecurityQuestion useCaseSecurityQuestion = new ChangeSecurityQuestion(userProfileOKTARepository,
                new ChangeSecurityQuestionDataRepository(new RemoteChangeRecoveryQuestionDataStore()),
                mapLogic
        );
        ProfileDataAspireRepository profileRepository = new ProfileDataAspireRepository(prefStorage, mapLogic);
        GetToken getToken = new GetToken(prefStorage, mapLogic);

        SignInCase signInCase = new SignInCase(mapLogic,
                profileRepository,
                userProfileOKTARepository,getToken);

        this.createProfileCase = new CreateProfileCase(
                profileRepository,
                useCaseSecurityQuestion,
                signInCase,
                getToken
        );
        this.signUpCase = new SignUpCase(mapLogic, remoteProfileOKTA,
                new GetProfilePMA(mapLogic),
                new ProfileDataPMARepository(mapLogic));

        this.mapProfile = new MapProfile();
    }

    @Override
    public void attach(SignUpV2.View view) {
        this.view = view;
    }

    @Override
    public void detach() {
        disposables.dispose();
        view = null;
    }

    public void setProfilePMAMapView(ProfilePMAMapView profilePMAMapView) {
        this.profilePMAMapView = profilePMAMapView;
    }

    public void setSecurityQuestionParam(ChangeSecurityQuestion.Param securityQuestionParam) {
        this.securityQuestionParam = securityQuestionParam;
    }


    @Override
    public void createProfile(Profile profile) {
        if (!App.getInstance().hasNetworkConnection()) {
            view.showErrorDialog(ErrCode.CONNECTIVITY_PROBLEM, null);
            return;
        }
        view.showProgressDialog();
        disposables.add(
                checkParamRequestCase(profile)
                        .flatMap(params -> createProfileCase.buildUseCaseSingle(params))
                        .flatMapCompletable(profileAspire -> Completable.create(e->{
                            //--special case MS don't save binCode
//                            String binCodeValue = ProfileLogicCore.getAppUserPreference(profileAspire.getAppUserPreferences(),
//                                    ConstantAspireApi.APP_USER_PREFERENCES.BIN_CODE_KEY);
//
//                            prefStorage.editor().binCode(binCodeValue).build().save();
                            e.onComplete();
                        }))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new SignUpV2Presenter.SaveProfileObserver()));
    }

    @Override
    public void matchUpProfile(String email) {
        if (!App.getInstance().hasNetworkConnection()) {
            view.showErrorDialog(ErrCode.CONNECTIVITY_PROBLEM, null);
            return;
        }
        view.showProgressDialog();
        disposables.add(
                signUpCase.buildCaseCheckProfileOKTAAndPMA(email)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new MatchUpObserver(email))
        );
    }


    private Single<CreateProfileCase.Params> checkParamRequestCase(Profile profile) {
        return Single.create(e -> {
            if (securityQuestionParam == null) {
                e.onError(new Exception(ErrCode.UNKNOWN_ERROR.name()));
            } else {
                String partyId = (profilePMAMapView != null
                        && !TextUtils.isEmpty(profilePMAMapView.getPartyId()))
                        ? profilePMAMapView.getPartyId() : "";

                CreateProfileCase.Params params = mapProfile.createProfile(profile,
                        partyId,
                        securityQuestionParam);

                // clean reset preference data
                prefStorage.clear();

                e.onSuccess(params);
            }
        });
    }

    private final class MatchUpObserver extends DisposableSingleObserver<ProfilePMAMapView> {

        private String email;

        MatchUpObserver(String email) {
            this.email = email;
        }

        @Override
        public void onSuccess(ProfilePMAMapView profilePMAMapView) {
            if (view == null) return;
            view.dismissProgressDialog();
            view.showExistPMAAccountDialog(profilePMAMapView);
        }

        @Override
        public void onError(Throwable e) {
            if (view == null) return;
            view.dismissProgressDialog();
            String err = e.getMessage();
            if (!TextUtils.isEmpty(err)) {
                if (err.equalsIgnoreCase(ErrCode.SIGN_UP_PROFILE_OKTA_EXISTS_ERROR.name())) {
                    view.showMatchUpDialog();
                } else if (err.equalsIgnoreCase(ErrCode.SIGN_UP_PROFILE_OKTA_AND_PMA_NO_FOUND_ERROR.name())) {
                    view.showCreateNewAccountDialog(email);
                } else {
                    view.showErrorDialog(ErrCode.UNKNOWN_ERROR, err);
                }
            }
        }
    }

    private final class SaveProfileObserver extends DisposableCompletableObserver {

        @Override
        public void onComplete() {
            if(view == null) return;
            view.dismissProgressDialog();
            view.showProfileCreatedDialog();
            dispose();
        }

        @Override
        public void onError(Throwable e) {
            if(view == null) return;
            view.dismissProgressDialog();
            view.showErrorDialog(ErrCode.UNKNOWN_ERROR, e.getMessage());
            dispose();
        }
    }

}