package com.ms.androidapp.presentation.venuedetail;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.BaseTarget;
import com.bumptech.glide.request.target.SizeReadyCallback;
import com.ms.androidapp.App;
import com.ms.androidapp.R;
import com.ms.androidapp.common.constant.AppConstant;
import com.api.aspire.common.constant.ErrCode;
import com.ms.androidapp.common.constant.IntentConstant;
import com.ms.androidapp.common.glide.GlideHelper;
import com.ms.androidapp.common.logic.HtmlTagHandler;
import com.ms.androidapp.common.logic.HtmlUtils;
import com.ms.androidapp.common.logic.PermissionUtils;
import com.ms.androidapp.common.logic.ShareHelper;
import com.ms.androidapp.common.logic.TextViewLinkHandler;
import com.ms.androidapp.common.logic.urlimage.URLImageParser;
import com.ms.androidapp.datalayer.repository.B2CDataRepository;
import com.ms.androidapp.domain.model.explore.DiningDetailItem;
import com.ms.androidapp.domain.model.explore.ExploreRView;
import com.ms.androidapp.domain.model.explore.SearchDetailItem;
import com.ms.androidapp.domain.usecases.GetDiningDetail;
import com.ms.androidapp.presentation.base.CommonActivity;
import com.ms.androidapp.presentation.request.AskConciergeActivity;
import com.ms.androidapp.presentation.venuedetail.mapview.VenueDetailMapActivity;
import com.ms.androidapp.presentation.widget.DialogHelper;
import com.support.mylibrary.widget.LetterSpacingTextView;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by ThuNguyen on 6/8/2017.
 */

public class DiningDetailActivity extends CommonActivity implements DiningDetail.View{
    @BindView(R.id.dining_detail_layout)
    View diningDetailLayout;
    @BindView(R.id.loading)
    ProgressBar pbLoading;

    @BindView(R.id.explore_detail_image)
    ImageView ivDetailImage;
    @BindView(R.id.explore_action_book)
    ImageButton ivActionBook;
    @BindView(R.id.explore_action_share)
    ImageButton ivActionShare;
    @BindView(R.id.explore_name)
    TextView tvDiningName;
    @BindView(R.id.explore_address)
    TextView tvDiningAddress;
    @BindView(R.id.explore_see_map)
    TextView tvDiningSeeMap;
    @BindView(R.id.explore_website)
    TextView tvDiningWebsite;
    @BindView(R.id.explore_insider_tip_root_layout)
    View insiderTipLayout;
    @BindView(R.id.insider_tip_content)
    LetterSpacingTextView tvInsiderTip;
    @BindView(R.id.explore_your_benefit_root_layout)
    View benefitLayout;
    @BindView(R.id.your_benefit_content)
    LetterSpacingTextView tvBenefitContent;
    @BindView(R.id.explore_price_cuisine_layout)
    View priceCuisineLayout;
    @BindView(R.id.explore_price_range_layout)
    View priceLayout;
    @BindView(R.id.explore_price_range)
    TextView tvPriceRange;
    @BindView(R.id.explore_cuisine_layout)
    View cuisineLayout;
    @BindView(R.id.explore_cuisine)
    TextView tvCuisine;
    @BindView(R.id.explore_description_layout)
    View diningDescriptionLayout;
    @BindView(R.id.explore_description)
    LetterSpacingTextView tvDiningDescription;
    @BindView(R.id.explore_hour_of_operation_layout)
    View hourOfOperationLayout;
    @BindView(R.id.explore_hour_of_operation)
    LetterSpacingTextView tvHourOfOperation;
    @BindView(R.id.explore_terms_and_conditions_layout)
    View termsAndConditionsLayout;
    @BindView(R.id.explore_terms_and_conditions)
    LetterSpacingTextView tvTermsAndConditions;
    DialogHelper dialogHelper;
    private SearchDetailItem searchDetailItem;
    private DiningDetailItem diningDetailItem;
    private Bitmap detailBitmap;
    private DiningDetailPresenter presenter;
    private TextViewLinkHandler textViewLinkHandler = new TextViewLinkHandler() {
        @Override
        public void onLinkClick(String url) {
            new DialogHelper(DiningDetailActivity.this).showLeavingAlert(url);
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dining_detail);
        setToolbarColor(R.color.ms_colorPrimary);
        // Make hyperlink in textview clickable
        tvInsiderTip.setMovementMethod(textViewLinkHandler);
        tvBenefitContent.setMovementMethod(textViewLinkHandler);
        tvDiningDescription.setMovementMethod(textViewLinkHandler);
        tvHourOfOperation.setMovementMethod(textViewLinkHandler);
        tvTermsAndConditions.setMovementMethod(textViewLinkHandler);

        tvDiningSeeMap.setPaintFlags(tvDiningSeeMap.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        tvDiningWebsite.setPaintFlags(tvDiningWebsite.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        ExploreRView exploreRView = getIntent().getParcelableExtra(IntentConstant.EXPLORE_DETAIL);
        // Suppress the letter spacing
        ((LetterSpacingTextView)title).setLetterSpacing_(0f);
        title.setAllCaps(true);
        if(exploreRView instanceof DiningDetailItem) { // Load UI directly from bundle
            diningDetailItem = (DiningDetailItem) exploreRView;
            renderUI();
        }else{
            searchDetailItem = (SearchDetailItem) exploreRView;
            setTitle(Html.fromHtml(searchDetailItem.title));

            pbLoading.setVisibility(View.VISIBLE);
            diningDetailLayout.setVisibility(View.GONE);
            // Load from API
            presenter = new DiningDetailPresenter(new GetDiningDetail(new B2CDataRepository()));
            presenter.attach(this);
            presenter.getDiningDetail(searchDetailItem.secondaryID, searchDetailItem.ID);
        }
        if(savedInstanceState == null){
            // Track GA
            App.getInstance().track(AppConstant.GA_TRACKING_SCREEN_NAME.VENUE_DETAIL.getValue());
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(presenter != null){
            presenter.detach();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PermissionUtils.EXTERNAL_STORAGE_REQUEST_CODE:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Call share again
                    ShareHelper.getInstance().share(this, diningDetailItem, detailBitmap);
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void renderUI(){
        pbLoading.setVisibility(View.GONE);
        if(diningDetailItem != null){
            diningDetailLayout.setVisibility(View.VISIBLE);
            // Dining avatar
            if(!TextUtils.isEmpty(diningDetailItem.imageURL) && diningDetailItem.imageURL.contains("http")) {
                /*Picasso.with(this)
                        .load(diningDetailItem.imageURL)
                        .into(new Target() {
                            @Override
                            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                                detailBitmap = bitmap;
                                ivDetailImage.setImageBitmap(bitmap);
                            }

                            @Override
                            public void onBitmapFailed(Drawable errorDrawable) {

                            }

                            @Override
                            public void onPrepareLoad(Drawable placeHolderDrawable) {

                            }
                        });*/
                GlideHelper.getInstance().loadImage(diningDetailItem.imageURL,
                        0, ivDetailImage, 0, new BaseTarget<Bitmap>() {
                            @Override
                            public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                                detailBitmap = resource;
                            }

                            @Override
                            public void getSize(SizeReadyCallback cb) {

                            }
                        });
            }
            // Dining name
            if(TextUtils.isEmpty(diningDetailItem.title)){
                tvDiningName.setVisibility(View.GONE);
            }else{
                tvDiningName.setVisibility(View.VISIBLE);
                tvDiningName.setText(Html.fromHtml(diningDetailItem.title));
                setTitle(Html.fromHtml(diningDetailItem.title));
            }
            // Dining address
            String diningAddress = diningDetailItem.getDisplayAddress();
            if(TextUtils.isEmpty(diningAddress)){
                tvDiningAddress.setVisibility(View.GONE);
            }else{
                tvDiningAddress.setVisibility(View.VISIBLE);
                tvDiningAddress.setText(diningAddress);
            }
            // See map --- later
            // Website
            if(TextUtils.isEmpty(diningDetailItem.url) || !diningDetailItem.url.contains("http")){
                tvDiningWebsite.setVisibility(View.GONE);
            }else{
                tvDiningWebsite.setVisibility(View.VISIBLE);
                tvDiningWebsite.setText(diningDetailItem.url);
            }
            // Insider tip
            String insiderTip = diningDetailItem.insiderTips;
            if(TextUtils.isEmpty(insiderTip)){
                insiderTipLayout.setVisibility(View.GONE);
            }else{
                insiderTipLayout.setVisibility(View.VISIBLE);
                tvInsiderTip.setText(Html.fromHtml(HtmlUtils.adjustSomeHtmlTag(insiderTip), new URLImageParser(tvInsiderTip, this, false, null),
                        new HtmlTagHandler()));
            }
            // Benefit
            if(TextUtils.isEmpty(diningDetailItem.benefits)){
                benefitLayout.setVisibility(View.GONE);
            }else{
                benefitLayout.setVisibility(View.VISIBLE);
                tvBenefitContent.setText(Html.fromHtml(HtmlUtils.adjustSomeHtmlTag(diningDetailItem.benefits), new URLImageParser(tvBenefitContent, this, false, null),
                        new HtmlTagHandler()));
            }
            priceCuisineLayout.setVisibility(View.GONE);
            // Price range
            if(TextUtils.isEmpty(diningDetailItem.priceRange)){
                priceLayout.setVisibility(View.GONE);
            }else{
                priceCuisineLayout.setVisibility(View.VISIBLE);
                priceLayout.setVisibility(View.VISIBLE);
                tvPriceRange.setText(diningDetailItem.priceRange);
            }
            // Cuisine
            if(TextUtils.isEmpty(diningDetailItem.cuisine)){
                cuisineLayout.setVisibility(View.GONE);
            }else{
                priceCuisineLayout.setVisibility(View.VISIBLE);
                cuisineLayout.setVisibility(View.VISIBLE);
                tvCuisine.setText(diningDetailItem.cuisine);
            }
            // Description
            if(TextUtils.isEmpty(diningDetailItem.description)){
                diningDescriptionLayout.setVisibility(View.GONE);
            }else{
                diningDescriptionLayout.setVisibility(View.VISIBLE);
                tvDiningDescription.setText(Html.fromHtml(HtmlUtils.adjustSomeHtmlTag(diningDetailItem.description), new URLImageParser(tvDiningDescription, this, false, null),
                        new HtmlTagHandler()));
            }
            // Hour of operation
            if(TextUtils.isEmpty(diningDetailItem.hoursOfOperation)){
                hourOfOperationLayout.setVisibility(View.GONE);
            }else{
                hourOfOperationLayout.setVisibility(View.VISIBLE);
                tvHourOfOperation.setText(Html.fromHtml(HtmlUtils.adjustSomeHtmlTag(diningDetailItem.hoursOfOperation), new URLImageParser(tvHourOfOperation, this, false, null),
                        new HtmlTagHandler()));
            }
            // Terms of Condition
            termsAndConditionsLayout.setVisibility(TextUtils.isEmpty(diningDetailItem.benefits) ? View.GONE : View.VISIBLE);
        }
    }

    @OnClick({R.id.explore_see_map, R.id.explore_website, R.id.explore_action_book, R.id.explore_action_share})
    public void onClick(View view){
        switch (view.getId()){
            case R.id.explore_see_map:
                //new DialogHelper(DiningDetailActivity.this)
//                        .showLeavingAlert((dialogInterface, i) -> MapSearchHelper.getInstance().searchNameAndShowOnMap(DiningDetailActivity.this, diningDetailItem.getQuerySearchOnGoogleMap(), diningDetailItem.getTitle()));
                  //      .showLeavingAlert((dialogInterface, i) ->
                    if(!App.getInstance().hasNetworkConnection()) {
                        dialogHelper = new DialogHelper(this);
                        dialogHelper.networkUnavailability(ErrCode.CONNECTIVITY_PROBLEM, null);
                    }
                    else
                        startActivity(new Intent(this, VenueDetailMapActivity.class).putExtra("dining",diningDetailItem));
                //);
                break;
            case R.id.explore_website:
                new DialogHelper(DiningDetailActivity.this).showLeavingAlert(diningDetailItem.url);
                break;
            case R.id.explore_action_book:
                Intent intent = new Intent(this, AskConciergeActivity.class);
                intent.putExtra(IntentConstant.SELECTED_CATEGORY, "Dining");
                intent.putExtra(IntentConstant.AC_SUGGESTED_CONCIERGE, diningDetailItem.title);
                startActivity(intent);
                break;
            case R.id.explore_action_share:
                ShareHelper.getInstance().share(this, diningDetailItem, detailBitmap);
                break;
        }
    }

    @Override
    public void onGetDiningDetailFinished(DiningDetailItem diningDetailItem) {
        this.diningDetailItem = diningDetailItem;
        renderUI();
    }

    @Override
    public void onUpdateFailed() {
        pbLoading.setVisibility(View.GONE);
    }

}
