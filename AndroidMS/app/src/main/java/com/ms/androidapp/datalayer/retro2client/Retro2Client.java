package com.ms.androidapp.datalayer.retro2client;

import android.util.Log;

import com.api.aspire.common.SSLSolverOkHttpClientCore;
import com.ms.androidapp.App;
import com.ms.androidapp.BuildConfig;

import java.io.File;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.CacheControl;
import okhttp3.CipherSuite;
import okhttp3.ConnectionSpec;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.TlsVersion;
import okhttp3.logging.HttpLoggingInterceptor;

/**
 * Created by tung.phan on 5/17/2017.
 */

public class Retro2Client {

    private static final String TAG = Retro2Client.class.getSimpleName();
    private static final int OFFLINE_EXPIRE_TIME_DAY = 7;
    private static final String CACHE_CONTROL = "cache_control";
    private static final int CACHE_SIZE = 10 * 1024 * 1024;
    private static final String HTTP_CACHE = "mastercard_http_cache";

    protected OkHttpClient provideOkHttpClient(int value, TimeUnit timeUnit) {
        OkHttpClient.Builder okHttpBuilder = new OkHttpClient.Builder();
        if (BuildConfig.DEBUG) {
            okHttpBuilder.addInterceptor(provideHttpLoggingInterceptor());
        }
        okHttpBuilder
                .readTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .connectTimeout(30, TimeUnit.SECONDS)
                .addInterceptor(provideOfflineCacheInterceptor())
                .addNetworkInterceptor(provideCacheInterceptor(value, timeUnit))
                //.connectionSpecs(Collections.singletonList(connectionSpec()))
                .cache(provideCache());
        SSLSolverOkHttpClientCore.solve(okHttpBuilder);
        enableCertificatePinning(okHttpBuilder);
        return okHttpBuilder.build();
    }
    protected OkHttpClient provideOkHttpClientWithClearTextConnectionSpec(int value, TimeUnit timeUnit) {
        OkHttpClient.Builder okHttpBuilder = new OkHttpClient.Builder();
        if (BuildConfig.DEBUG) {
            okHttpBuilder.addInterceptor(provideHttpLoggingInterceptor());
        }
        okHttpBuilder
                .readTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .connectTimeout(30, TimeUnit.SECONDS)
                .addInterceptor(provideOfflineCacheInterceptor())
                .addNetworkInterceptor(provideCacheInterceptor(value, timeUnit))
                .connectionSpecs(Arrays.asList(ConnectionSpec.MODERN_TLS, ConnectionSpec.CLEARTEXT))
                .cache(provideCache());
        SSLSolverOkHttpClientCore.solve(okHttpBuilder);
        enableCertificatePinning(okHttpBuilder);
        return okHttpBuilder.build();
    }
    /**
     * provide OkHttpClient instance without using offline mode and caching
     * @return OkHttpClient instance,
     */
    protected OkHttpClient provideOkHttpClientWithoutCache() {
        OkHttpClient.Builder okHttpBuilder = new OkHttpClient.Builder();
        if (BuildConfig.DEBUG) {
            okHttpBuilder.addInterceptor(provideHttpLoggingInterceptor());
        }
        okHttpBuilder.readTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .connectTimeout(30, TimeUnit.SECONDS);
                //.connectionSpecs(Collections.singletonList(connectionSpec()));
        SSLSolverOkHttpClientCore.solve(okHttpBuilder);
        enableCertificatePinning(okHttpBuilder);
        return okHttpBuilder.build();
    }

    private HttpLoggingInterceptor provideHttpLoggingInterceptor() {
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        return loggingInterceptor;
    }

    private Interceptor provideOfflineCacheInterceptor() {
        return chain -> {
            Request request = chain.request();
            if (!App.getInstance().hasNetworkConnection()) {
                CacheControl cacheControl = new CacheControl.Builder()
                        .maxStale(OFFLINE_EXPIRE_TIME_DAY, TimeUnit.DAYS)
                        .build();
                request = request.newBuilder()
                        .cacheControl(cacheControl)
                        .build();
            }
            return chain.proceed(request);
        };
    }

    private Interceptor provideCacheInterceptor(int value, TimeUnit timeUnit) {
        return chain -> {
            Response response = chain.proceed(chain.request());
            CacheControl cacheControl = new CacheControl.Builder()
                    .maxAge(value, timeUnit)
                    .build();
            return response.newBuilder()
                    .header(CACHE_CONTROL, cacheControl.toString())
                    .build();
        };
    }

    private Cache provideCache() {
        Cache cache = null;
        try {
            cache = new Cache(new File(App.getInstance().getCacheDir(), HTTP_CACHE), CACHE_SIZE);
        } catch (Exception e) {
            Log.e(TAG, "Could not create Cache!");
        }
        return cache;
    }


    private ConnectionSpec connectionSpec() {
        return new ConnectionSpec.Builder(ConnectionSpec.MODERN_TLS)
                .tlsVersions(TlsVersion.TLS_1_0, TlsVersion.TLS_1_1, TlsVersion.TLS_1_2, TlsVersion.TLS_1_3)
                .cipherSuites(
                        CipherSuite.TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,
                        CipherSuite.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,
                        CipherSuite.TLS_DHE_RSA_WITH_AES_128_GCM_SHA256,
                        CipherSuite.TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA,
                        CipherSuite.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256)
                .build();
    }
    private void enableCertificatePinning(OkHttpClient.Builder okHttpBuilder){
        /*if(!BuildConfig.DEBUG) { // Only enable certificate pinning at release mode
            String hostname = "tools.vipdesk.com";
            CertificatePinner.Builder builder = new CertificatePinner.Builder()
                    .add(hostname, "sha256/+EbQHL7Y+phaqq6thsIAji2LBj+rZkml/649n2EiY6A=")
                    .add(hostname, "sha256/980Ionqp3wkYtN9SZVgMzuWQzJta1nfxNPwTem1X0uc=")
                    .add(hostname, "sha256/du6FkDdMcVQ3u8prumAo6t3i3G27uMP2EOhR8R0at/U=");
            hostname = "apiservice-stg.aspirelifestyles.com";
            builder
                    .add(hostname, "sha256/dKMmua4iAkV0y7Oz+3/0+qOPVcwIM2M3R/oqPBmY4j4=")
                    .add(hostname, "sha256/980Ionqp3wkYtN9SZVgMzuWQzJta1nfxNPwTem1X0uc=")
                    .add(hostname, "sha256/du6FkDdMcVQ3u8prumAo6t3i3G27uMP2EOhR8R0at/U=")

                    .add(hostname, "sha256/QA1bd4IunEJlWUFID/xFh672cxLw+1iockbbpqH0iWw=")
                    .add(hostname, "sha256/hIBbkSty62Y4PvvLLkYxEHhqoAK2s9JgMSFHh6HeR60=")
                    .add(hostname, "sha256/x4QzPSC810K5/cMjb05Qm4k3Bw5zBn4lTdO/nEW/Td4=");
            okHttpBuilder.certificatePinner(builder.build());
        }*/
        /*String hostname = "apiservice.vipdesk.com";
        CertificatePinner certificatePinner = new CertificatePinner.Builder()
                .add(hostname, "sha256/AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=")
                .build();
        OkHttpClient client = new OkHttpClient.Builder()
                .certificatePinner(certificatePinner)
                .build();

        Request request = new Request.Builder()
                .url("https://" + hostname)
                .build();
        try {
            client.newCall(request).execute();
        } catch (IOException e) {
            e.printStackTrace();
        }*/
    }
}
