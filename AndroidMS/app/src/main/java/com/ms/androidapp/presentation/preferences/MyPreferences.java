package com.ms.androidapp.presentation.preferences;

import com.api.aspire.data.entity.preference.PreferenceData;

import java.util.Arrays;
import java.util.List;

/**
 * Created by vinh.trinh on 7/26/2017.
 */

public class MyPreferences {

    private List<String> cuisines;
    private List<String> hotels;
    private List<String> transportation;

    void load(String[] cuisines, String[] hotels, String[] transportation) {
        this.cuisines = Arrays.asList(Arrays.copyOfRange(cuisines, 1, cuisines.length));
        this.hotels = Arrays.asList(Arrays.copyOfRange(hotels, 1, hotels.length));
        this.transportation = Arrays.asList(Arrays.copyOfRange(transportation, 1, transportation.length));
    }

    String cuisine(int pos) {
        if(pos == 0) return PreferenceData.NA_VALUE;
        return cuisines.get(pos-1);
    }

    String hotel(int pos) {
        if(pos == 0) return PreferenceData.NA_VALUE;
        return hotels.get(pos-1);
    }

    String vehicle(int pos) {
        if(pos == 0) return PreferenceData.NA_VALUE;
        return transportation.get(pos-1);
    }

    int cuisine(String val) {
        int index = cuisines.indexOf(val);
        return index == -1 ? 0 : index+1;
    }

    int hotel(String val) {
        int index = hotels.indexOf(val);
        return index == -1 ? 0 : index+1;
    }

    int vehicle(String val) {
        int index = transportation.indexOf(val);
        return index == -1 ? 0 : index+1;
    }
}
