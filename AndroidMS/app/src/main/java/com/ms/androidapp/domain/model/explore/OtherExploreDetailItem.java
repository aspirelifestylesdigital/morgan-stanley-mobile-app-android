package com.ms.androidapp.domain.model.explore;

import android.annotation.SuppressLint;
import android.text.TextUtils;

import com.ms.androidapp.App;
import com.ms.androidapp.R;
import com.ms.androidapp.common.constant.AppConstant;

/**
 * Created by vinh.trinh on 6/13/2017.
 */

@SuppressLint("ParcelCreator")
public class OtherExploreDetailItem extends ExploreRViewItem {

    public String benefit;
    public String termsOfUse;
    public String category;
    public String subCategory;
    public OtherExploreDetailItem(int id, String title, String description, String imageURL, boolean hasStar, String summary, int dataIndex) {
        super(id, title, description, imageURL, hasStar, summary, dataIndex);
    }

    public OtherExploreDetailItem setBenefit(String benefit){
        this.benefit = benefit;
        return this;
    }
    public OtherExploreDetailItem setTermsOfUse(String termsOfUse){
        this.termsOfUse = termsOfUse;
        return this;
    }
    public OtherExploreDetailItem setCategory(String category){
        this.category = category;
        return this;
    }
    public OtherExploreDetailItem setSubCategory(String subCategory){
        this.subCategory = subCategory;
        return this;
    }
    public String getDescriptionExcludeImageTag(){
        if(description.contains("<img")){
            int boldTagFoundInd = description.indexOf("<b>");
            if(boldTagFoundInd > -1) {
                return description.substring(boldTagFoundInd);
            }
        }
        return description;
    }
    public String getImageTagFromDescription(){
        if(description.contains("<img")){
            int boldTagFoundInd = description.indexOf("<b>");
            if(boldTagFoundInd > -1) {
                return description.substring(0, boldTagFoundInd);
            }
        }
        return "";
    }
    public String getDisplayTermsOfUse(){
        if(TextUtils.isEmpty(termsOfUse)){
            return App.getInstance().getApplicationContext().getString(R.string.mastercard_text);
        }
        return (termsOfUse + "<br/><br/>" + App.getInstance().getApplicationContext().getString(R.string.mastercard_text));
    }

    @Override
    public String getSuggestedToAC() {
        AppConstant.EXPLORE_CATEGORY categoryEnum = AppConstant.EXPLORE_CATEGORY.getEnum(category);
        if(categoryEnum == null){
            categoryEnum = AppConstant.EXPLORE_CATEGORY.getEnum(subCategory);
        }
        if(categoryEnum != null){
            switch (categoryEnum){
                case DINING: // Dining
                case CULINARY_EXPERIENCE:
                    return getSuggestedDining();
                case HOTEL: // Hotel
                    return getSuggestedHotel();
                case GOLF: // Golf
                case GOLF_EXPERIENCE:
                case GOLF_MERCHANDISE:
                    return getSuggestedGolf();
                case CAR_RENTAL:
                    return getSuggestedCarRental();
                case CAR_TRANSFER:
                    return getSuggestedCarTransfer();
                case ENTERTAINMENT:
                case ENTERTAINMENT_EXPERIENCE:
                    return getSuggestedEntertainment();
                case TOUR_SUBCAT:
                    return getSuggestedTour();
                case AIRPORT_SERVICES_SUBCAT:
                    return getSuggestedFlight();
                case CRUISE:
                    return getSuggestedCruise();
                case FLOWER:
                    return getSuggestedFlower();
                case TRAVEL:
                    return getSuggestedPrivateJet();
                case VACATION_PACKAGES:
                    return getSuggestedVacationPackages();
                default:
                    return getSuggestedOther();
            }
        }else{
            return getSuggestedOther();
        }
    }
    private String getSuggestedOther(){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(AppConstant.PREFILL_AC_NAME + title);
        stringBuilder.append("\n" + AppConstant.PREFILL_AC_OTHER_COMMENTS);
        return stringBuilder.toString();
    }
    private String getSuggestedDining(){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(AppConstant.PREFILL_AC_NAME + title);
        stringBuilder.append("\n" + AppConstant.PREFILL_AC_DATE_TIME);
        stringBuilder.append("\n" + AppConstant.PREFILL_AC_NO_OF_ADULTS);
        stringBuilder.append("\n" + AppConstant.PREFILL_AC_NO_OF_CHILDREN);
        stringBuilder.append("\n" + AppConstant.PREFILL_AC_CITY);
        stringBuilder.append("\n" + AppConstant.PREFILL_AC_STATE);
        stringBuilder.append("\n" + AppConstant.PREFILL_AC_COUNTRY);
        stringBuilder.append("\n" + AppConstant.PREFILL_AC_OTHER_COMMENTS);
        return stringBuilder.toString();
    }
    private String getSuggestedHotel(){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(AppConstant.PREFILL_AC_NAME + title);
        stringBuilder.append("\n" + AppConstant.PREFILL_AC_CHECK_IN);
        stringBuilder.append("\n" + AppConstant.PREFILL_AC_CHECK_OUT);
        stringBuilder.append("\n" + AppConstant.PREFILL_AC_NO_OF_ADULTS);
        stringBuilder.append("\n" + AppConstant.PREFILL_AC_NO_OF_CHILDREN);
        stringBuilder.append("\n" + AppConstant.PREFILL_AC_CITY);
        stringBuilder.append("\n" + AppConstant.PREFILL_AC_STATE);
        stringBuilder.append("\n" + AppConstant.PREFILL_AC_COUNTRY);
        stringBuilder.append("\n" + AppConstant.PREFILL_AC_OTHER_COMMENTS);
        return stringBuilder.toString();
    }
    private String getSuggestedGolf(){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(AppConstant.PREFILL_AC_NAME + title);
        stringBuilder.append("\n" + AppConstant.PREFILL_AC_DATE);
        stringBuilder.append("\n" + AppConstant.PREFILL_AC_NO_OF_GOLFERS);
        stringBuilder.append("\n" + AppConstant.PREFILL_AC_CITY);
        stringBuilder.append("\n" + AppConstant.PREFILL_AC_STATE);
        stringBuilder.append("\n" + AppConstant.PREFILL_AC_COUNTRY);
        stringBuilder.append("\n" + AppConstant.PREFILL_AC_OTHER_COMMENTS);
        return stringBuilder.toString();
    }
    private String getSuggestedCarRental(){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(AppConstant.PREFILL_AC_DRIVER);
        stringBuilder.append("\n" + AppConstant.PREFILL_AC_PICKUP_DATE);
        stringBuilder.append("\n" + AppConstant.PREFILL_AC_PICKUP_LOCATION);
        stringBuilder.append("\n" + AppConstant.PREFILL_AC_DROPOFF_LOCATION);
        stringBuilder.append("\n" + AppConstant.PREFILL_AC_NO_IN_PARTY);
        stringBuilder.append("\n" + AppConstant.PREFILL_AC_OTHER_COMMENTS);
        return stringBuilder.toString();
    }
    private String getSuggestedCarTransfer(){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(AppConstant.PREFILL_AC_PICKUP_DATE);
        stringBuilder.append("\n" + AppConstant.PREFILL_AC_PICKUP_LOCATION);
        stringBuilder.append("\n" + AppConstant.PREFILL_AC_DROPOFF_LOCATION);
        stringBuilder.append("\n" + AppConstant.PREFILL_AC_NO_IN_PARTY);
        stringBuilder.append("\n" + AppConstant.PREFILL_AC_OTHER_COMMENTS);
        return stringBuilder.toString();
    }
    private String getSuggestedEntertainment(){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(AppConstant.PREFILL_AC_NAME);
        stringBuilder.append("\n" + AppConstant.PREFILL_AC_DATE);
        stringBuilder.append("\n" + AppConstant.PREFILL_AC_NO_OF_TICKETS);
        stringBuilder.append("\n" + AppConstant.PREFILL_AC_CITY);
        stringBuilder.append("\n" + AppConstant.PREFILL_AC_STATE);
        stringBuilder.append("\n" + AppConstant.PREFILL_AC_COUNTRY);
        stringBuilder.append("\n" + AppConstant.PREFILL_AC_OTHER_COMMENTS);
        return stringBuilder.toString();
    }
    private String getSuggestedTour(){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(AppConstant.PREFILL_AC_NAME);
        stringBuilder.append("\n" + AppConstant.PREFILL_AC_START_DATE);
        stringBuilder.append("\n" + AppConstant.PREFILL_AC_END_DATE);
        stringBuilder.append("\n" + AppConstant.PREFILL_AC_START_LOCATION);
        stringBuilder.append("\n" + AppConstant.PREFILL_AC_END_LOCATION);
        stringBuilder.append("\n" + AppConstant.PREFILL_AC_NO_OF_ADULTS);
        stringBuilder.append("\n" + AppConstant.PREFILL_AC_NO_OF_CHILDREN);
        stringBuilder.append("\n" + AppConstant.PREFILL_AC_NO_OF_INFANTS);
        stringBuilder.append("\n" + AppConstant.PREFILL_AC_OTHER_COMMENTS);
        return stringBuilder.toString();
    }
    private String getSuggestedFlight(){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(AppConstant.PREFILL_AC_START_DATE);
        stringBuilder.append("\n" + AppConstant.PREFILL_AC_END_DATE);
        stringBuilder.append("\n" + AppConstant.PREFILL_AC_START_LOCATION);
        stringBuilder.append("\n" + AppConstant.PREFILL_AC_END_LOCATION);
        stringBuilder.append("\n" + AppConstant.PREFILL_AC_NO_OF_ADULTS);
        stringBuilder.append("\n" + AppConstant.PREFILL_AC_NO_OF_CHILDREN);
        stringBuilder.append("\n" + AppConstant.PREFILL_AC_NO_OF_INFANTS);
        stringBuilder.append("\n" + AppConstant.PREFILL_AC_OTHER_COMMENTS);
        return stringBuilder.toString();
    }
    private String getSuggestedCruise(){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(AppConstant.PREFILL_AC_NAME + title);
        stringBuilder.append("\n" + AppConstant.PREFILL_AC_NO_OF_ADULTS);
        stringBuilder.append("\n" + AppConstant.PREFILL_AC_NO_OF_CHILDREN);
        stringBuilder.append("\n" + AppConstant.PREFILL_AC_OTHER_COMMENTS);
        return stringBuilder.toString();
    }
    private String getSuggestedFlower(){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(AppConstant.PREFILL_AC_ARRANGEMENT);
        stringBuilder.append("\n" + AppConstant.PREFILL_AC_DELIVERY_DATE);
        stringBuilder.append("\n" + AppConstant.PREFILL_AC_DELIVERY_ADDRESS);
        stringBuilder.append("\n" + AppConstant.PREFILL_AC_QUANTITY);
        stringBuilder.append("\n" + AppConstant.PREFILL_AC_OTHER_COMMENTS);
        return stringBuilder.toString();
    }
    private String getSuggestedPrivateJet(){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(AppConstant.PREFILL_AC_START_DATE);
        stringBuilder.append("\n" + AppConstant.PREFILL_AC_START_LOCATION);
        stringBuilder.append("\n" + AppConstant.PREFILL_AC_END_LOCATION);
        stringBuilder.append("\n" + AppConstant.PREFILL_AC_NO_OF_PASSENGERS);
        stringBuilder.append("\n" + AppConstant.PREFILL_AC_OTHER_COMMENTS);
        return stringBuilder.toString();
    }
    private String getSuggestedVacationPackages(){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(AppConstant.PREFILL_AC_NAME + title);
        stringBuilder.append("\n" + AppConstant.PREFILL_AC_NO_OF_ADULTS);
        stringBuilder.append("\n" + AppConstant.PREFILL_AC_NO_OF_CHILDREN);
        stringBuilder.append("\n" + AppConstant.PREFILL_AC_OTHER_COMMENTS);
        return stringBuilder.toString();
    }
}
