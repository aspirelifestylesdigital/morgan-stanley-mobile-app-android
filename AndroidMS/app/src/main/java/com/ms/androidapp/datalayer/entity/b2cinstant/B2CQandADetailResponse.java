package com.ms.androidapp.datalayer.entity.b2cinstant;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ms.androidapp.datalayer.entity.QuestionAndAnswers;

/**
 * Created by Thu Nguyen on 6/1/2017.
 */

public class B2CQandADetailResponse {
    @SerializedName("GetQuestionAndAnswersResult")
    @Expose
    private QuestionAndAnswers questionAndAnswers;

    public QuestionAndAnswers getQuestionAndAnswers() {
        return questionAndAnswers;
    }

    public void setQuestionAndAnswers(QuestionAndAnswers questionAndAnswers) {
        this.questionAndAnswers = questionAndAnswers;
    }
}
