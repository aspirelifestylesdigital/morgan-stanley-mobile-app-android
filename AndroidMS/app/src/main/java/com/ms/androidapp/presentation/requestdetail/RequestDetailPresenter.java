package com.ms.androidapp.presentation.requestdetail;

import android.content.Context;

import com.api.aspire.data.preference.PreferencesStorageAspire;
import com.api.aspire.data.repository.ProfileDataAspireRepository;
import com.api.aspire.domain.repository.ProfileAspireRepository;
import com.api.aspire.domain.usecases.LoadProfile;
import com.ms.androidapp.datalayer.entity.askconcierge.ACResponse;
import com.ms.androidapp.datalayer.repository.RequestRepository;
import com.ms.androidapp.domain.model.Metadata;
import com.ms.androidapp.domain.model.Profile;
import com.ms.androidapp.domain.model.RequestDetailData;
import com.ms.androidapp.domain.usecases.CreateRequest;
import com.ms.androidapp.domain.usecases.GetAccessToken;
import com.ms.androidapp.domain.usecases.MapProfileApp;

import org.json.JSONException;

import java.util.concurrent.TimeUnit;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by vinh.trinh on 9/13/2017.
 */

public class RequestDetailPresenter implements RequestDetail.Presenter {

    private CompositeDisposable compositeDisposable;
    private RequestDetail.View view;
    private GetAccessToken getAccessToken;
    private CreateRequest askConcierge;
    private LoadProfile loadProfile;
//    private Metadata metadata;

    private final String transactionID;
    private RequestDetailData data;

    RequestDetailPresenter(Context context, RequestDetailData data) {
        compositeDisposable = new CompositeDisposable();

        PreferencesStorageAspire ps = new PreferencesStorageAspire(context);
        this.getAccessToken = new GetAccessToken(ps);
        MapProfileApp mapLogic = new MapProfileApp();
        ProfileAspireRepository profileRepository = new ProfileDataAspireRepository(ps, mapLogic);
        this.loadProfile = new LoadProfile(profileRepository);
        this.askConcierge = new CreateRequest(new RequestRepository());

        transactionID = data.transactionID;
        this.data = data;

//        try {
//            this.metadata = ps.metadata();
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
    }

    RequestDetailData getData() {
        return data;
    }

    @Override
    public void attach(RequestDetail.View view) {
        this.view = view;
    }

    @Override
    public void detach() {
        compositeDisposable.dispose();
        view = null;
    }

    @Override
    public void getRequestDetail() {
        view.showProgressDialog();
        compositeDisposable.add(Single.just(data)
                .map(data1 -> {
                    data1.buildJsonDetail();
                    this.data = data1;
                    DetailViewData viewData = new DetailViewData(data1);
                    viewData.details = detailText(data1, false);
                    return viewData;
                })
                .delay(2, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<DetailViewData>() {
                    @Override
                    public void onSuccess(DetailViewData data) {
                        view.showData(data);
                        view.dismissProgressDialog();
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.dismissProgressDialog();
                    }
                }));
    }

    @Override
    public String content(boolean create) {
        try {
            return detailText(data, create);
        } catch (JSONException e) {
            return "";
        }
    }

    @Override
    public void dismissRequest() {
//        view.showProgressDialog();
//        compositeDisposable.add(Single.zip(getAccessToken.execute(), loadProfile.buildUseCaseSingle(null), this::buildParams)
//                .flatMap(params -> askConcierge.execute(params)).subscribeOn(Schedulers.computation())
//                .observeOn(AndroidSchedulers.mainThread())
//        .subscribeWith(new DisposableSingleObserver<ACResponse>() {
//            @Override
//            public void onSuccess(ACResponse acResponse) {
//                view.dismissProgressDialog();
//                if(acResponse.getStatus() != null && acResponse.getStatus().isSuccess()) {
//                    view.onCancelDone();
//                } else {
//                    view.showErrorMessage("");
//                }
//            }
//
//            @Override
//            public void onError(Throwable e) {
//                view.dismissProgressDialog();
//                view.showErrorMessage("");
//            }
//        }));
    }

    private CreateRequest.Params buildParams(String accessToken, Profile profile) {
        /*CreateRequest.RequestContent requestContent = new CreateRequest.RequestContent(
                data.detail,
                data.responseMode.contains("Email"),
                data.responseMode.contains("Mobile")
        );
        CreateRequest.Params params = new CreateRequest.Params(
                profile,
                metadata,
                data.detail,
                profile.
        );
        params.transactionID(transactionID);
        params.editType(CreateRequest.EDIT_TYPE.CANCEL);
        return params;*/
        return null;
    }

    private String detailText(RequestDetailData data, boolean ignoreDate) throws JSONException {
        TextDetailBuilder textDetail = new TextDetailBuilder(data, data.detail(), ignoreDate);
        switch (data.requestType.toUpperCase()) {
            case "D RESTAURANT":
                return textDetail.dining();
            case "T HOTEL AND B&B":
                return textDetail.hotel();
            case "S GOLF":
                return textDetail.golf();
            case "T LIMO AND SEDAN":
                return textDetail.carRental();
            case "E CONCERT/THEATER":
                return textDetail.entertainment();
            case "C SIGHTSEEING/TOURS":
                return textDetail.tour();
            case "I AIRPORT SERVICES":
                return textDetail.flight();
            case "T CRUISE":
                return textDetail.cruise();
            case "G FLOWERS/GIFT BASKET":
                return textDetail.flower();
            case "T OTHER":
                return textDetail.privateJet();
            case "O CLIENT SPECIFIC":
                return textDetail.others();
        }
        return "";
    }
}
