package com.ms.androidapp.presentation.requestlist;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ms.androidapp.R;
import com.ms.androidapp.domain.model.RequestItemView;
import com.ms.androidapp.presentation.base.BaseFragment;
import com.ms.androidapp.presentation.widget.OnItemClickListener;
import com.ms.androidapp.presentation.widget.RecyclerViewScrollListener;

import java.util.List;

/**
 * Created by vinh.trinh on 9/11/2017.
 */

public class FragmentRequestList extends BaseFragment implements OnItemClickListener {

    private RecyclerView recyclerView;
    private RequestAdapter requestAdapter;
    private RecyclerViewScrollListener scrollListener;
    private RequestListListener listener;

    public static FragmentRequestList newInstance(boolean openList) {
        Bundle args = new Bundle();
        args.putBoolean("openlist", openList);
        FragmentRequestList fragment = new FragmentRequestList();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof RequestListListener) {
            listener = (RequestListListener) context;
        } else {
            throw new ClassCastException(context.toString() + " must implement RequestListListener.");
        }
        handler = new Handler();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_request_list, container, false);
        recyclerView = (RecyclerView) view;
        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(llm);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        boolean openList = getArguments().getBoolean("openlist");
        requestAdapter = new RequestAdapter(getContext(), openList ? RequestAdapter.TYPE.OPEN :
                RequestAdapter.TYPE.CLOSE);
        requestAdapter.setItemClickListener(this);
        recyclerView.setAdapter(requestAdapter);
        setupLoadMore();
    }

    void loadList(List<RequestItemView> data) {
        requestAdapter.append(data);
    }

    @Override
    public void onItemClick(int pos) {
        boolean openList = getArguments().getBoolean("openlist");
        if(openList)
            listener.onOpenedItemClick(requestAdapter.getItem(pos));
        else
            listener.onClosedItemClick(requestAdapter.getItem(pos));
    }

    private void setupLoadMore() {
        scrollListener = new RecyclerViewScrollListener() {
            @Override
            public void onScrollUp() {}

            @Override
            public void onScrollDown() {}

            @Override
            public void onLoadMore() {
                listener.listLoadMore();
            }
        };
        recyclerView.addOnScrollListener(scrollListener);
    }

    void showLoadMore() {
        handler.post(() -> requestAdapter.showLoading(true));
    }

    void stopLoadMore() {
        handler.post(() -> {
            requestAdapter.showLoading(false);
            scrollListener.loadDone();
        });
    }

    int itemCount() {
        return requestAdapter == null ? 0 : requestAdapter.getItemCount();
    }

    void clear() {
        requestAdapter.clear();
    }

    interface RequestListListener {
        void onOpenedItemClick(RequestItemView item);
        void onClosedItemClick(RequestItemView item);
        void listLoadMore();
    }

    private Handler handler;
}
