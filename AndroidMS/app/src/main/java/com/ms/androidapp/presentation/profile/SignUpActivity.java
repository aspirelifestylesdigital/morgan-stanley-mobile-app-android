package com.ms.androidapp.presentation.profile;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.WindowManager;

import com.ms.androidapp.App;
import com.ms.androidapp.R;
import com.ms.androidapp.common.GPSChecker;
import com.ms.androidapp.common.constant.AppConstant;
import com.api.aspire.common.constant.ErrCode;
import com.ms.androidapp.domain.model.Profile;
import com.ms.androidapp.presentation.LocationPermissionHandler;
import com.ms.androidapp.presentation.base.BaseActivity;
import com.ms.androidapp.presentation.checkout.SignInActivity;
import com.ms.androidapp.presentation.home.HomeActivity;
import com.ms.androidapp.presentation.widget.DialogHelper;
import com.ms.androidapp.presentation.widget.ViewUtils;

/**
 * Created by vinh.trinh on 4/27/2017.
 */

public class SignUpActivity extends BaseActivity implements ProfileFragment.ProfileEventsListener, CreateProfile.View {

    private CreateProfilePresenter presenter;
    private DialogHelper dialogHelper;
    ProfileFragment fragment;
    LocationPermissionHandler locationPermissionHandler = new LocationPermissionHandler();

    private CreateProfilePresenter buildPresenter() {
        return new CreateProfilePresenter(getApplicationContext());
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.activity_fragment_holder);
        presenter = buildPresenter();
        presenter.attach(this);
        dialogHelper = new DialogHelper(this);
        if (savedInstanceState == null) {
            fragment = ProfileFragment.newInstance(ProfileFragment.PROFILE.CREATE);
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.fragment_place_holder,
                            fragment,
                            ProfileFragment.class.getSimpleName())
                    .commit();
            // Track GA
            App.getInstance().track(AppConstant.GA_TRACKING_SCREEN_NAME.SIGN_UP.getValue());
        }
    }

    @Override
    protected void onDestroy() {
        presenter.detach();
        super.onDestroy();
    }

//    @Override
//    public void onProfileSubmit(Profile profile, String password) {
//
//        presenter.createProfile(profile, password);
//    }

    @Override
    public void showProfileCreatedDialog() {
        dialogHelper.alert(null, getString(R.string.profile_created_message), dialog -> proceedToHome());
    }

    private void proceedToHome() {
        ViewUtils.hideSoftKey(getCurrentFocus());
        Intent intent = new Intent(this, HomeActivity.class);
        startActivity(intent);
        finish();

        // Track GA with "Sign up" event
        App.getInstance().track(AppConstant.GA_TRACKING_CATEGORY.AUTHENTICATION.getValue(),
                AppConstant.GA_TRACKING_ACTION.CLICK.getValue(),
                AppConstant.GA_TRACKING_LABEL.SIGN_UP.getValue());
    }

    @Override
    public void showErrorDialog(ErrCode errCode, String extraMsg) {
        if(!dialogHelper.networkUnavailability(errCode, extraMsg)){
            if (ErrCode.USER_EXISTS_ERROR.name().equalsIgnoreCase(extraMsg)) {
                dialogHelper.alert(App.getInstance().getString(R.string.errorTitle), App.getInstance().getString(R.string.errorCreateAccount));
            } /*else if (errCode == ErrCode.API_ERROR) {
                dialogHelper.alert("ERROR!", extraMsg);
            }*/ else {
                dialogHelper.showGeneralError();
            }
        }
    }

    @Override
    public void showProgressDialog() {
        dialogHelper.showProgressCancelableUnEnable();
    }

    @Override
    public void dismissProgressDialog() {
        dialogHelper.dismissProgress();
    }

    @Override
    public void onProfileCancel() {
        ViewUtils.hideSoftKey(getCurrentFocus());
        onBackPressed();
    }

    @Override
    public void onProfileInputError(String message) {
        dialogHelper.profileDialog(message,dialog ->{
            if(fragment != null) {
                fragment.getView().postDelayed(() -> fragment.invokeSoftKey(), 100);
            }
        } );
    }

    @Override
    public void onFragmentCreated() {}

    @Override
    public void onLocationSwitchOn() {
        if(!locationPermissionHandler.hasPermission(this)) {
            locationPermissionHandler.requestPermission(this);
        } else{
            if(!GPSChecker.GPSEnable(getApplicationContext())) {
                dialogHelper.action(null,"To enable, please go to Settings and turn on Location Service for this app.",
                        "Setting","Cancel",
                        (dialogInterface, i) -> GPSChecker.openGPSSetting(this));
            }
        }
    }

    @Override
    public void onSubmitProfile(Profile profile) {

    }

    @Override
    public void onSubmitProfileAndSecurity(Profile profile, String question, String answer) {

    }

    @Override
    public void onSubmitSecurityQuestion(String question, String answer) {

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        boolean granted = locationPermissionHandler.handlingPermissionResult(requestCode, grantResults);
        if(!granted) fragment.switchOff();
        else if (!GPSChecker.GPSEnable(getApplicationContext())) {
            dialogHelper.action(null, "To enable, please go to Settings and turn on Location Service for this app.",
                    "Setting", "Cancel",
                    (dialogInterface, i) -> GPSChecker.openGPSSetting(this));
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, SignInActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
        finish();
    }
}
