package com.ms.androidapp.presentation.profile;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;

import com.api.aspire.common.constant.ErrorApi;
import com.ms.androidapp.App;
import com.ms.androidapp.R;
import com.ms.androidapp.common.GPSChecker;
import com.ms.androidapp.common.constant.AppConstant;
import com.api.aspire.common.constant.ErrCode;
import com.ms.androidapp.common.constant.ResultCode;
import com.ms.androidapp.domain.model.Profile;
import com.ms.androidapp.presentation.LocationPermissionHandler;
import com.ms.androidapp.presentation.base.CommonActivity;
import com.ms.androidapp.presentation.widget.DialogHelper;
import com.ms.androidapp.presentation.widget.ViewUtils;

public class MyProfileActivity extends CommonActivity implements ProfileFragment.ProfileEventsListener,
        EditProfile.View{

    private EditProfilePresenter presenter;
    private DialogHelper dialogHelper;
    private ProfileFragment myFragment;
    LocationPermissionHandler locationPermissionHandler = new LocationPermissionHandler();

    private EditProfilePresenter editProfilePresenter() {
        return new EditProfilePresenter(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.acticity_dummy_content);
        setTitle("My Profile");
        if (savedInstanceState == null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.fragment_place_holder
                            , ProfileFragment.newInstance(ProfileFragment.PROFILE.EDIT)
                            , ProfileFragment.class.getSimpleName())
                    .commit();
        }
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        toolbar.setClickable(true);
        toolbar.setOnClickListener(view -> {
            if (getCurrentFocus() != null) {
                ViewUtils.hideSoftKey(getCurrentFocus());
            }
        });
        dialogHelper = new DialogHelper(this);
        presenter = editProfilePresenter();
        presenter.attach(this);

        if (savedInstanceState == null) {
            // Track GA
            App.getInstance().track(AppConstant.GA_TRACKING_SCREEN_NAME.MY_PROFILE.getValue());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    protected void onDestroy() {
        presenter.detach();
        super.onDestroy();
    }

    @Override
    public void onFragmentCreated() {
        presenter.getProfileLocal();
        presenter.getProfile();
    }

    @Override
    public void onLocationSwitchOn() {
        if (!locationPermissionHandler.hasPermission(this)) {
            locationPermissionHandler.requestPermission(this);
        } else {
            if (!GPSChecker.GPSEnable(getApplicationContext())) {
                dialogHelper.action(null, "To enable, please go to Settings and turn on Location Service for this app.",
                        "Setting", "Cancel",
                        (dialogInterface, i) -> GPSChecker.openGPSSetting(this));
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        boolean granted = locationPermissionHandler.handlingPermissionResult(requestCode, grantResults);
        if (!granted) myFragment.switchOff();
        else if (!GPSChecker.GPSEnable(getApplicationContext())) {
            dialogHelper.action(null, "To enable, please go to Settings and turn on Location Service for this app.",
                    "Setting", "Cancel",
                    (dialogInterface, i) -> GPSChecker.openGPSSetting(this));
        }
    }

    @Override
    public void onProfileCancel() {
        presenter.getProfileLocal();
    }

    @Override
    public void displayProfile(Profile profile, boolean fromRemote) {
        myFragment = (ProfileFragment) getSupportFragmentManager()
                .findFragmentByTag(ProfileFragment.class.getSimpleName());
        if (myFragment != null) {
            myFragment.loadProfile(profile, fromRemote);
        }
    }
    @Override
    public void profileUpdated() {
        dialogHelper.alert(null, getString(R.string.profile_updated_message));
        if (myFragment != null) {
            myFragment.reloadProfileAfterUpdated();
        }
    }


    @Override
    public void showErrorDialog(ErrCode errCode, String extraMsg) {
        if (dialogHelper.networkUnavailability(errCode, extraMsg))
            return;
        if (ErrorApi.isGetTokenError(extraMsg)) {
            dialogHelper.showGetTokenError();
        } else if (extraMsg.equalsIgnoreCase(ErrCode.CHANGE_SECURITY_QUESTION_ERROR.name())) {
            dialogHelper.showGeneralError();
        }
        /*else if(errCode == ErrCode.API_ERROR) dialogHelper.alert("ERROR!", extraMsg);*/
        else dialogHelper.showGeneralError();
    }

    @Override
    public void showProgressDialog() {
        dialogHelper.showProgressCancelableUnEnable();
    }

    @Override
    public void dismissProgressDialog() {
        dialogHelper.dismissProgress();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            if (getCurrentFocus() != null) {
                ViewUtils.hideSoftKey(getCurrentFocus());
            }
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onProfileInputError(String message) {
        dialogHelper.profileDialog(message, dialog -> {
            if (myFragment != null) {
                myFragment.getView().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        myFragment.invokeSoftKey();
                    }
                }, 100);

            }
        });
    }

    @Override
    public void onSubmitProfile(Profile profile) {
        setResult(ResultCode.RESULT_OK); //-- refresh profile homFragment
        presenter.updateProfile(profile);
    }

    @Override
    public void onSubmitProfileAndSecurity(Profile profile, String question, String answer) {
        setResult(ResultCode.RESULT_OK); //-- refresh profile homFragment
        presenter.updateProfileAndSecurity(profile, question, answer);
    }

    @Override
    public void onSubmitSecurityQuestion(String question, String answer) {
        presenter.updateSecurityQuestion(question, answer);
    }
}
