package com.ms.androidapp.presentation.info;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.ms.androidapp.App;
import com.ms.androidapp.R;
import com.ms.androidapp.common.constant.AppConstant;
import com.api.aspire.common.constant.ErrCode;
import com.ms.androidapp.common.constant.IntentConstant;
import com.ms.androidapp.datalayer.entity.GetClientCopyResult;
import com.ms.androidapp.datalayer.repository.B2CDataRepository;
import com.ms.androidapp.domain.usecases.GetMasterCardCopy;
import com.ms.androidapp.presentation.base.CommonActivity;
import com.ms.androidapp.presentation.widget.DialogHelper;
import com.support.mylibrary.widget.JustifiedTextView;

/**
 * Created by ThuNguyen on 6/19/2017.
 */

public class MasterCardUtilityActivity extends CommonActivity implements MasterCardUtility.View {

    MasterCardUtilityPresenter presenter;
    DialogHelper dialogHelper;
    private boolean delayAction = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isSwipeBack = false;
        setContentView(R.layout.activity_mastercard_copy_utility);
        back.setVisibility(View.GONE);
        toolbar.setNavigationIcon(R.drawable.ic_close_2);
        // Get mastercard utility type
        AppConstant.MASTERCARD_COPY_UTILITY type = (AppConstant.MASTERCARD_COPY_UTILITY) getIntent().getSerializableExtra(IntentConstant.MASTERCARD_COPY_UTILITY);
        switch (type){
            case TermsOfUse:
                title.setText(R.string.menu_term_of_use);
                if(savedInstanceState == null){
                    // Track GA
                    App.getInstance().track(AppConstant.GA_TRACKING_SCREEN_NAME.TERMS_OF_USE.getValue());
                }
                break;
            case Privacy:
                title.setText(R.string.menu_privacy_policy);
                if(savedInstanceState == null){
                    // Track GA
                    App.getInstance().track(AppConstant.GA_TRACKING_SCREEN_NAME.PRIVACY_POLICY.getValue());
                }
                break;
            case About:
                title.setText(R.string.menu_about);
                if(savedInstanceState == null){
                    // Track GA
                    App.getInstance().track(AppConstant.GA_TRACKING_SCREEN_NAME.ABOUT_THIS_APP.getValue());
                }
                break;
        }
        if(savedInstanceState == null) {
            MasterCardUtilityFragment masterCardUtilityFragment = MasterCardUtilityFragment
                    .newInstance(type);
            masterCardUtilityFragment.setJustifyTextViewListener(justifyTextViewListener);

            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.fragment_place_holder
                            , masterCardUtilityFragment
                            , MasterCardUtilityFragment.class.getSimpleName())
                    .commit();
        }
        dialogHelper = new DialogHelper(this);
        presenter = new MasterCardUtilityPresenter(new GetMasterCardCopy(new B2CDataRepository()));
        presenter.attach(this);
        presenter.getMasterCardCopy(type.getValue());

    }

    @Override
    public void onBackPressed() {
        finish();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public void onGetMasterCardCopy(GetClientCopyResult result) {
        if (result!=null){
            ((MasterCardUtilityFragment)getSupportFragmentManager()
                    .findFragmentByTag(MasterCardUtilityFragment.class.getSimpleName()))
                    .renderContent(result.getText());
        }
    }

    @Override
    public void loadEmptyContent() {
        MasterCardUtilityFragment fragment = ((MasterCardUtilityFragment)getSupportFragmentManager()
                .findFragmentByTag(MasterCardUtilityFragment.class.getSimpleName()));
        if(fragment == null) delayAction = true;
        else fragment.renderContent("");
    }

    void onFragmentCreated() {
        MasterCardUtilityFragment fragment = ((MasterCardUtilityFragment)getSupportFragmentManager()
                .findFragmentByTag(MasterCardUtilityFragment.class.getSimpleName()));
        if(delayAction) fragment.renderContent("");
    }

    @Override
    public void showErrorMessage(ErrCode errCode) {
        if(errCode == ErrCode.CONNECTIVITY_PROBLEM)
            dialogHelper.networkUnavailability(ErrCode.CONNECTIVITY_PROBLEM,null);
        else dialogHelper.showGeneralError();
    }

    JustifiedTextView.IJustifyTextViewListener justifyTextViewListener = new JustifiedTextView.IJustifyTextViewListener() {
        @Override
        public void onContentScroll(boolean hitToBottom) {
        }

        @Override
        public void onHyperLinkClicked(String url) {
            new DialogHelper(MasterCardUtilityActivity.this).showLeavingAlert(url);
        }
    };

    @Override
    protected void onDestroy() {
        presenter.detach();
        super.onDestroy();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home) {
            super.onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
