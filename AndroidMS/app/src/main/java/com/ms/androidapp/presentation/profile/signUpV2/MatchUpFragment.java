package com.ms.androidapp.presentation.profile.signUpV2;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.ScrollView;


import com.ms.androidapp.R;
import com.ms.androidapp.common.logic.Validator;
import com.ms.androidapp.presentation.base.BaseSwipeBackFragment;
import com.ms.androidapp.presentation.widget.DialogHelper;
import com.ms.androidapp.presentation.widget.ViewKeyboardListener;
import com.ms.androidapp.presentation.widget.ViewUtils;
import com.support.mylibrary.widget.ErrorIndicatorEditText;
import com.support.mylibrary.widget.swipeback.SwipeBackLayoutV2;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class MatchUpFragment extends BaseSwipeBackFragment {

    @BindView(R.id.edt_email)
    ErrorIndicatorEditText edtEmail;
    @BindView(R.id.btn_submit)
    Button btnSubmit;
    @BindView(R.id.flEmailMask)
    View flEmailMask;
    @BindView(R.id.match_up_parent_view)
    View rootView;
    @BindView(R.id.contain_view)
    View linearContainView;
    @BindView(R.id.scroll_view)
    ScrollView scrollView;
    @BindView(R.id.content_wrapper)
    RelativeLayout contentWrapper;

    private ErrorIndicatorEditText edtCurrentError = null;
    private Validator validator;
    private MatchUpEventsListener listener;
    private DialogHelper dialogHelper;
    private ViewKeyboardListener keyboardListener;

    public static MatchUpFragment newInstance() {
        Bundle args = new Bundle();
        MatchUpFragment fragment = new MatchUpFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof MatchUpFragment.MatchUpEventsListener) {
            listener = (MatchUpFragment.MatchUpEventsListener) context;
        } else {
            throw new ClassCastException(context.toString() + " must implement MatchUpEventsListener.");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_match_up, container, false);
        getSwipeBackLayout().addSwipeListener(new SwipeBackLayoutV2.OnSwipeListener() {
            @Override
            public void onDragStateChange(int state) {
            }

            @Override
            public void onEdgeTouch(int edgeFlag) {
            }

            @Override
            public void onDragScrolled(float scrollPercent) {
                if (scrollPercent <= 0.0f) {
                    getActivity().getWindow().getDecorView().setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryDark));
                } else {
                    getActivity().getWindow().getDecorView().setBackgroundColor(Color.TRANSPARENT);
                }
            }
        });

        return attachToSwipeBack(view);
    }

    @OnClick({R.id.match_up_parent_view,
            R.id.match_up_rootview,
            R.id.scroll_view,
            R.id.content_wrapper,
            R.id.content_submit,
            R.id.contain_view})
    public void onClick(View view) {
        ViewUtils.hideSoftKey(view);
        if (getActivity().getCurrentFocus() != null) {
            getActivity().getCurrentFocus().clearFocus();
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setTitle(getString(R.string.register).toUpperCase());
        validator = new Validator();
        keyboardInteractListener();

        edtEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                String string = s.toString().trim();
                boolean isEnable = string.length() > 0;

                btnSubmit.setEnabled(isEnable);
                btnSubmit.setClickable(isEnable);
            }
        });
        dialogHelper = new DialogHelper(getActivity());
    }

    public interface MatchUpEventsListener {
        void onMatchUpSubmit(String email);
    }

    private boolean formValidation() {
        edtEmail.hideErrorColorLine();

        edtCurrentError = null;
        List<String> listErrorContent = new ArrayList<>();

        //-- email
        if (!validator.email(edtEmail.getText().toString())) {
            edtCurrentError = edtEmail;
            edtEmail.showErrorColorLine();
            if (!TextUtils.isEmpty(edtEmail.getText().toString())) {
                listErrorContent.add(getString(R.string.input_err_email));
            }
        }//- else do nothing

        //-- message error
        StringBuilder stringBuilder = new StringBuilder();
        for (String contentError : listErrorContent) {
            stringBuilder.append(contentError);
        }
        String errMessage = stringBuilder.toString().trim();

        //-- handle validation
        if (edtCurrentError != null) {
            edtCurrentError.requestFocus();
            edtCurrentError.showErrorColorLine();
            edtCurrentError.setSelection(edtCurrentError.getText().length());
            showDialogInputError(errMessage);
            return false;

        }

        return true;
    }

    private void showDialogInputError(String errMessage) {
        dialogHelper.profileDialog(errMessage, dialog -> {
            if (btnSubmit != null) {
                btnSubmit.postDelayed(this::showKeyboardFocusError, 100);
            }
        });
    }

    void showKeyboardFocusError() {
        if (edtCurrentError != null) {
            edtCurrentError.requestFocus();
            edtCurrentError.showErrorColorLine();
            edtCurrentError.setCursorVisible(true);
            edtCurrentError.setSelection(edtCurrentError.getText().toString().length());
            ViewUtils.showSoftKey(edtCurrentError);
        }
    }

    @OnClick(R.id.btn_submit)
    void onSubmit(View view) {
        ViewUtils.hideSoftKey(view);
        if (formValidation()) {
            listener.onMatchUpSubmit(edtEmail.getText().toString());
        }// else do nothing
    }

    /**
     * handle click on back hide keyboard close cursor
     */
    private void keyboardInteractListener() {
        ViewKeyboardListener.KeyboardEvent event = new ViewKeyboardListener.KeyboardEvent() {
            @Override
            public void showKeyboard() {
                //dummy do nothing
                scrollView.smoothScrollTo(0, (int) contentWrapper.getTop());
            }

            @Override
            public void hideKeyboard() {
                //dummy do nothing
            }

            @Override
            public View getCurrentFocus() {
                if(getActivity() == null) return null;
                return getActivity().getCurrentFocus();
            }
        };
        //-- add listen keyboard
        keyboardListener = new ViewKeyboardListener(rootView, event);
        keyboardListener.setViewFocusChangeListener(linearContainView);
    }

    @Override
    public void onStop() {
        ViewUtils.hideSoftKey(getView());
        if(keyboardListener != null){
            keyboardListener.removeListener();
        }
        super.onStop();
    }

}
