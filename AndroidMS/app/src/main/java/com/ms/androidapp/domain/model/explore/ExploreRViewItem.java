package com.ms.androidapp.domain.model.explore;

import android.os.Parcel;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.ms.androidapp.presentation.explore.DiningSortingCriteria;

/**
 * Created by vinh.trinh on 5/12/2017.
 */

public class ExploreRViewItem extends ExploreRView implements Comparable<ExploreRViewItem>{
    public static final Creator<ExploreRViewItem> CREATOR = new Creator<ExploreRViewItem>() {
        @Override
        public ExploreRViewItem createFromParcel(Parcel in) {
            return new ExploreRViewItem(in);
        }

        @Override
        public ExploreRViewItem[] newArray(int size) {
            return new ExploreRViewItem[size];
        }
    };
    public final int id;
    public final String title;
    public final String description;
    public final String summary;
    public final boolean hasStar;
    public final String imageURL;
    public final int dataListIndex;
    private String categoryName;
    private String subCategoryName;
    private DiningSortingCriteria sortingCriteria;

    public ExploreRViewItem(int id, String title, String description, String imageURL, boolean hasStar,
                            String summary, int dataIndex) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.summary = summary;
        this.imageURL = imageURL;
        this.hasStar = hasStar;
        itemType = ItemType.NORMAL;
        this.dataListIndex = dataIndex;
    }

    protected ExploreRViewItem(Parcel in) {
        id = in.readInt();
        title = in.readString();
        description = in.readString();
        summary = in.readString();
        imageURL = in.readString();
        hasStar = in.readInt() == 1;
        String itemTypeStr = in.readString();
        if(TextUtils.isEmpty(itemTypeStr)){
            itemType = ItemType.NORMAL;
        }else{
            itemType = ItemType.getEnum(itemTypeStr);
        }
        dataListIndex = in.readInt();
        categoryName = in.readString();
        subCategoryName = in.readString();
        sortingCriteria = in.readParcelable(DiningSortingCriteria.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(title);
        dest.writeString(description);
        dest.writeString(summary);
        dest.writeString(imageURL);
        dest.writeInt(hasStar ? 1 : 0);
        dest.writeString(itemType != null ? itemType.getValue() : "");
        dest.writeInt(dataListIndex);
        dest.writeString(categoryName);
        dest.writeString(subCategoryName);
        dest.writeParcelable(sortingCriteria, 0);
    }

    @Override
    public int compareTo(@NonNull ExploreRViewItem o) {
        return this.title.compareToIgnoreCase(o.title);
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public String getImageUrl() {
        return imageURL;
    }

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public String getSuggestedToAC() {
        return null;
    }

    public String categoryName() {
        return categoryName;
    }

    public void categoryName(String cat) {
        this.categoryName = cat;
    }

    public String subCategoryName() {
        return subCategoryName;
    }

    public void subCategoryName(String subcat) {
        this.subCategoryName = subcat;
    }

    public DiningSortingCriteria getSortingCriteria() {
        return sortingCriteria;
    }

    public void setSortingCriteria(DiningSortingCriteria sortingCriteria) {
        this.sortingCriteria = sortingCriteria;
    }
    public String getDisplayCategory(){
        if("Airport Services".equalsIgnoreCase(subCategoryName) || "Travel Services".equalsIgnoreCase(subCategoryName)){
            return subCategoryName;
        }
        if("Sightseeing".equalsIgnoreCase(subCategoryName)){
            return "Tours";
        }
        if("Culinary Experiences".equalsIgnoreCase(subCategoryName)){
            return "Dining";
        }
        if("Entertainment Experiences".equalsIgnoreCase(subCategoryName) ||
                "Major Sports Events".equalsIgnoreCase(subCategoryName) ||
                "Tickets".equalsIgnoreCase(categoryName)){
            return "Entertainment";
        }
        if("Golf Merchandise".equalsIgnoreCase(categoryName) || "Golf Experiences".equalsIgnoreCase(subCategoryName)){
            return "Golf";
        }
        if("Wine".equalsIgnoreCase(categoryName) || "Retail Shopping".equalsIgnoreCase(categoryName)){
            return "Shopping";
        }
        if("Private Jet Travel".equalsIgnoreCase(categoryName)){
            return "Travel";
        }
        return categoryName;
    }
}
