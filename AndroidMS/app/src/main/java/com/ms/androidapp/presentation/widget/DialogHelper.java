package com.ms.androidapp.presentation.widget;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.api.aspire.data.preference.PreferencesStorageAspire;
import com.api.aspire.domain.usecases.GetToken;
import com.api.aspire.domain.usecases.SignOut;
import com.ms.androidapp.App;
import com.ms.androidapp.R;
import com.ms.androidapp.common.constant.CityData;
import com.api.aspire.common.constant.ErrCode;
import com.ms.androidapp.domain.usecases.MapProfileApp;
import com.ms.androidapp.presentation.base.BaseSwipeBackActivity;
import com.ms.androidapp.presentation.base.CommonActivity;
import com.ms.androidapp.presentation.checkout.SignInActivity;

import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by vinh.trinh on 4/26/2017.
 */

public class DialogHelper {

    private Activity activity;
    private ProgressDialog progressDialog;

    public DialogHelper(Activity activity) {
        this.activity = activity;
    }

    public void alert(String title, String message) {
        alert(title, message,null);
    }

    public void alert(String title, String message, DialogInterface.OnDismissListener dismissListener) {
        Dialog dialog = new MyDialogBuilder(activity, title, message)
                .positiveText(R.string.text_ok)
                .onDismiss(dismissListener)
                .single(true)
                .build();
        dialog.show();
    }

    public void action(String title, String message, String positiveText, String negativeText,
                       DialogInterface.OnClickListener takeAction) {
        Dialog dialog = new MyDialogBuilder(activity, title, message)
                .positiveText(positiveText)
                .negativeText(negativeText)
                .onPositive(takeAction)
                .build();
        dialog.show();
    }
    public void action(String title, String message, String positiveText, String negativeText,
                       DialogInterface.OnClickListener takeAction, DialogInterface.OnClickListener cancelAction) {
        Dialog dialog = new MyDialogBuilder(activity, title, message)
                .positiveText(positiveText)
                .negativeText(negativeText)
                .onPositive(takeAction)
                .onNegative(cancelAction)
                .build();
        dialog.show();
    }

    public void showProgress() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        } else {
            progressDialog = createProgressDialog();
        }
        progressDialog.show();
        progressDialog.setContentView(R.layout.layout_progress_loading);
    }
    public void showProgressCancelableUnEnable(){
        showProgress();
        if (progressDialog != null) {
            progressDialog.setCancelable(false);
        }
    }
    public void dismissProgress() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    public MyDialogBuilder createBuilder(int title, int content, int positiveText, int negativeText) {
        return new MyDialogBuilder(activity, title, content)
                .positiveText(positiveText)
                .negativeText(negativeText);
    }

    public void showLeavingAlert(String url) {
        Dialog dialog = new MyDialogBuilder(activity, R.string.text_alert, R.string.text_leaving_message)
                .onPositive((dialog1, which) -> {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW,
                            Uri.parse(url));
                    activity.startActivity(browserIntent);
                }).build();
        dialog.show();
    }

    public void showLeavingAlert(DialogInterface.OnClickListener onClickListener){
        Dialog dialog = new MyDialogBuilder(activity, R.string.text_alert, R.string.text_leaving_message)
                .onPositive(onClickListener).build();
        dialog.show();
    }

    public void profileDialog(String message, DialogInterface.OnDismissListener dismissListener) {
        final Dialog customDialog = new Dialog(activity, R.style.AppDialogTheme);
        customDialog.setContentView(R.layout.custom_alert_dialog);
        TextView titleView = ButterKnife.findById(customDialog, R.id.title);
        TextView messageView = ButterKnife.findById(customDialog, R.id.message);
        Button positiveBtn = ButterKnife.findById(customDialog, R.id.positive_btn);
        Button negativeBtn = ButterKnife.findById(customDialog, R.id.negative_btn);

        positiveBtn.setText(R.string.text_ok);
        titleView.setText(R.string.input_err_fields);
        messageView.setText(message);
        messageView.setGravity(Gravity.START);
        negativeBtn.setVisibility(View.GONE);
        positiveBtn.setBackground(null);
        positiveBtn.setOnClickListener(v -> customDialog.dismiss());
        customDialog.setCancelable(true);
        customDialog.setCanceledOnTouchOutside(true);
        customDialog.setOnDismissListener(dismissListener);
        customDialog.show();
    }

    private ProgressDialog createProgressDialog() {
        ProgressDialog progressDialog = new ProgressDialog(activity) {
            @Override
            public boolean onTouchEvent(@NonNull MotionEvent event) {
                Rect homeUpRect;
                if (activity instanceof CommonActivity) {
                    homeUpRect = ((CommonActivity) activity).getHomeUpRect();
                } else if (activity instanceof BaseSwipeBackActivity) {
                    homeUpRect = ((BaseSwipeBackActivity) activity).getHomeUpRect();
                } else {
                    //-- do nothing else
                    homeUpRect = null;
                }

                if (homeUpRect != null && homeUpRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                    activity.onBackPressed();
                }
                return false;
            }
        };
        progressDialog.getWindow().setDimAmount(0.2f);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        progressDialog.setIndeterminate(true);
        return progressDialog;
    }

    public boolean networkUnavailability(ErrCode errCode, String extraMessage) {
        return networkUnavailability(errCode, extraMessage, null);
    }
    public boolean networkUnavailability(ErrCode errCode, String extraMessage, DialogInterface.OnDismissListener onDismissListener) {
        if(errCode == ErrCode.CONNECTIVITY_PROBLEM || (extraMessage != null && extraMessage.contains("Unable to resolve host"))) {
            Dialog dialog = new MyDialogBuilder(activity,"Cannot Get Data",
                    "Turn on cellular data or use Wi-Fi to access Personal Concierge Service.")
                    .positiveText("SETTINGS")
                    .negativeText("OK")
                    .onPositive((dialogInterface, i) -> activity.startActivity(new Intent(Settings.ACTION_SETTINGS)))
                    .onNegative((dialogInterface, i) -> dialogInterface.dismiss())
                    .onDismiss(onDismissListener)
                    .autoDismiss(false)
                    .build();
            dialog.show();
            return true;
        }
        return false;
    }
    public void showGeneralError() {
        Dialog dialog = new MyDialogBuilder(activity, "Cannot Get Data",
                "An error has occurred. Check your internet settings or try again.")
                .onPositive((dialog1, which) -> {
                    // Go to wifi settings
                    activity.startActivity(new Intent(Settings.ACTION_SETTINGS));
                })
                .onNegative((dialogInterface, i) -> dialogInterface.dismiss())
                .positiveText("SETTINGS")
                .negativeText("OK")
                .autoDismiss(false)
                .build();
        dialog.show();
    }


    public void showGetTokenError() {
        Dialog dialog = new MyDialogBuilder(activity, App.getInstance().getString(R.string.errorTitle),
                App.getInstance().getString(R.string.errorForceSignOut))
                .onPositive((dialog1, which) -> {

                    PreferencesStorageAspire preferencesStorageAspire = new PreferencesStorageAspire(activity.getApplicationContext());
                    SignOut signOut = new SignOut(preferencesStorageAspire);
                    GetToken getTokenCase = new GetToken(preferencesStorageAspire, new MapProfileApp());
                    // revoke token
                    String token = getTokenCase.getAuthTokenCurrent();
                    if (!TextUtils.isEmpty(token)) {
                        getTokenCase.revokeToken(token)
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe();
                    }

                    signOut.setSignOutProfile(() -> {
                        //remove select city data
                        CityData.reset();
                    });

                    dialog1.dismiss();
                    Intent intent = new Intent(activity, SignInActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    activity.startActivity(intent);
                    activity.finish();

                })
                .single(true)
                .positiveText(App.getInstance().getString(R.string.text_ok))
                .autoDismiss(false)
                .build();
        dialog.show();
    }
}
