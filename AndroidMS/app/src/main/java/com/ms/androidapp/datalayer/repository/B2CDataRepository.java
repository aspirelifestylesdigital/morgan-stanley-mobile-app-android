package com.ms.androidapp.datalayer.repository;

import com.ms.androidapp.datalayer.entity.ContentFulls;
import com.ms.androidapp.datalayer.entity.GetClientCopyResult;
import com.ms.androidapp.datalayer.entity.GetContentFullResult;
import com.ms.androidapp.datalayer.entity.GetQuestionsAndAnswersResult;
import com.ms.androidapp.datalayer.entity.GetTilesResult;
import com.ms.androidapp.datalayer.entity.QuestionAndAnswers;
import com.ms.androidapp.datalayer.entity.QuestionsAndAnswer;
import com.ms.androidapp.datalayer.entity.SearchContent;
import com.ms.androidapp.datalayer.entity.Tiles;
import com.ms.androidapp.datalayer.entity.b2ccontent.B2CContentDetailResponse;
import com.ms.androidapp.datalayer.entity.b2ccontent.B2CContentFullRequest;
import com.ms.androidapp.datalayer.entity.b2cinstant.B2CQandADetailRequest;
import com.ms.androidapp.datalayer.entity.b2cinstant.B2CQandADetailResponse;
import com.ms.androidapp.datalayer.entity.b2cinstant.B2CQandARequest;
import com.ms.androidapp.datalayer.entity.b2cinstant.B2CQandAResponse;
import com.ms.androidapp.datalayer.entity.b2ctile.B2CTilesRequest;
import com.ms.androidapp.datalayer.entity.b2ctile.B2CTilesResponse;
import com.ms.androidapp.datalayer.entity.b2cutility.B2CGetMasterCardCopyRequest;
import com.ms.androidapp.datalayer.entity.b2cutility.B2CGetMasterCardCopyResponse;
import com.ms.androidapp.datalayer.entity.search.SearchRequest;
import com.ms.androidapp.datalayer.entity.search.SearchResponse;
import com.ms.androidapp.datalayer.entity.search.SearchResult;
import com.ms.androidapp.datalayer.retro2client.AppHttpClient;
import com.ms.androidapp.domain.repository.B2CRepository;

import java.util.List;

import io.reactivex.Single;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by tung.phan on 5/31/2017.
 */

public class B2CDataRepository implements B2CRepository {

    @Override
    public Single<List<QuestionsAndAnswer>> getCityGuides(Integer categoryId, Integer paging) {
        B2CQandARequest body = new B2CQandARequest(categoryId);
        body.setPageSize(20);
        body.setPageNumber(paging);
        Call<B2CQandAResponse> request = AppHttpClient.getInstance().getB2CInstantApi()
                .getQuestionsAndAnswers(body);
        return Single.create(e -> {
            Response<B2CQandAResponse> response = request.execute();
            GetQuestionsAndAnswersResult result = response.body().getGetQuestionsAndAnswersResult();
            if(response.isSuccessful() && result != null) {
                 if(result.getSuccess()) {
                     e.onSuccess(result.getQuestionsAndAnswers());
                 } else {
                     e.onError(new Exception(result.getStatus()));
                 }
            } else {
                e.onError(new Exception(response.errorBody().string()));
            }
        });
    }

    @Override
    public Single<List<QuestionsAndAnswer>> getDiningList(Integer categoryId, Integer paging) {
        B2CQandARequest body = new B2CQandARequest(categoryId);
        body.setPageSize(20);
        body.setPageNumber(paging);
        Call<B2CQandAResponse> request = AppHttpClient.getInstance().getB2CInstantApi()
                .getQuestionsAndAnswers(body);
        return Single.create(e -> {
            Response<B2CQandAResponse> response = request.execute();
            GetQuestionsAndAnswersResult result = response.body().getGetQuestionsAndAnswersResult();
            if(response.isSuccessful() && result != null) {
                if(result.getSuccess()) {
                    e.onSuccess(result.getQuestionsAndAnswers());
                } else {
                    e.onError(new Exception(result.getStatus()));
                }
            } else {
                e.onError(new Exception(response.errorBody().string()));
            }
        });
    }

    @Override
    public Single<QuestionAndAnswers> getQandADetail(Integer categoryId, Integer questionId) {
        B2CQandADetailRequest body = new B2CQandADetailRequest();
        body.setCategoryID(categoryId);
        body.setQuestionID(questionId);
        Call<B2CQandADetailResponse> request = AppHttpClient.getInstance().getB2CInstantApi()
                .getQuestionAndAnswers(body);
        return Single.create(e -> {
            Response<B2CQandADetailResponse> response = request.execute();
            QuestionAndAnswers result = response.body().getQuestionAndAnswers();
            if(response.isSuccessful() && result != null) {
                if(result.getSuccess()) {
                    e.onSuccess(result);
                } else {
                    e.onError(new Exception(result.getStatus()));
                }
            } else {
                e.onError(new Exception(response.errorBody().string()));
            }
        });
    }

    @Override
    public Single<List<Tiles>> getTilesList(String... categories) {
        B2CTilesRequest body = new B2CTilesRequest(categories);
        Call<B2CTilesResponse> request = AppHttpClient.getInstance().getB2CTilesApi().getTiles(body);
        return Single.create(e -> {
            Response<B2CTilesResponse> response = request.execute();
            GetTilesResult result = response.body().getResult();
            if(response.isSuccessful() && result != null) {
                if(result.getSuccess()) {
                    e.onSuccess(result.getTiles());
                } else {
                    e.onError(new Exception(result.getStatus()));
                }
            } else {
                e.onError(new Exception(response.errorBody().string()));
            }
        });
    }

    @Override
    public Single<List<Tiles>> getGalleries() {
        return getTilesList("App Gallery");
    }

    @Override
    public Single<ContentFulls> getContentFull(Integer contentId) {
        B2CContentFullRequest body = new B2CContentFullRequest(contentId);
        Call<B2CContentDetailResponse> request = AppHttpClient.getInstance().getB2CContentApi()
                .getContentFull(body);
        return Single.create(e -> {
            Response<B2CContentDetailResponse> response = request.execute();
            GetContentFullResult result = response.body().getResult();
            if(response.isSuccessful() && result != null) {
                if(result.getSuccess()) {
                    e.onSuccess(result.getContent());
                } else {
                    e.onError(new Exception(result.getStatus()));
                }
            } else {
                e.onError(new Exception(response.errorBody().string()));
            }
        });
    }

    @Override
    public Single<GetClientCopyResult> getMasterCardCopy(String type) {
        B2CGetMasterCardCopyRequest body = new B2CGetMasterCardCopyRequest(type);
        Call<B2CGetMasterCardCopyResponse> request = AppHttpClient.getInstance().getB2CUtilityApi()
                .getMasterCardCopy(body);
        return Single.create(e -> {
            Response<B2CGetMasterCardCopyResponse> response = request.execute();
            GetClientCopyResult result = response.body().getResult();
            if(response.isSuccessful() && result != null) {
                if(result.getSuccess()) {
                    e.onSuccess(result);
                } else {
                    e.onError(new Exception(result.getStatus()));
                }
            } else {
                e.onError(new Exception(response.errorBody().string()));
            }
        });
    }

    @Override
    public Single<List<SearchContent>> searchByTerm(String term, Integer paging, String...cities) {
        return Single.create(e -> {
            SearchRequest body = new SearchRequest(term);
            body.setPageSize(20);
            body.setPageNumber(paging);
            if(cities != null) body.setCities(cities);
            Call<SearchResponse> call = AppHttpClient.getInstance().getB2CUtilityApi()
                    .searchByKeyword(body);
            Response<SearchResponse> response = call.execute();
            SearchResult result = response.body().searchResult();
            if(response.isSuccessful() && result != null) {
                if(result.success()) {
                    e.onSuccess(result.searchResults());
                } else {
                    e.onError(new Exception(result.status()));
                }
            } else {
                e.onError(new Exception(response.errorBody().string()));
            }
        });
    }
}
