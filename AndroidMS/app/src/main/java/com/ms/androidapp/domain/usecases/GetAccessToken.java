package com.ms.androidapp.domain.usecases;

import com.api.aspire.data.preference.PreferencesStorageAspire;

import io.reactivex.Single;

/**
 * Created by vinh.trinh on 7/27/2017.
 */

public class GetAccessToken {

    private PreferencesStorageAspire preferencesStorage;

    public GetAccessToken(PreferencesStorageAspire preferencesStorage) {
        this.preferencesStorage = preferencesStorage;
    }

    public Single<String> execute() {
        return Single.create(e -> {
//            Metadata metadata = preferencesStorage.metadata();
//            OAuthApi oAuthApi = AppHttpClient.getInstance().oAuthApi();
//            // request token
//            Call<ReqTokenResponse> requestTokenCall = oAuthApi.requestToken(
//                    metadata.uuid, metadata.onlineMemberID);
//            Response<ReqTokenResponse> requestTokenResponse = requestTokenCall.execute();
//            if(!requestTokenResponse.isSuccessful()) {
//                e.onError(new Exception(requestTokenResponse.errorBody().string()));
//                return;
//            }
//            if(requestTokenResponse.body().status() == null || !requestTokenResponse.body().status().equals("Valid")) {
//                String message = requestTokenResponse.body().message();
//                e.onError(new BackendException(message));
//                return;
//            }
//            String requestToken = requestTokenResponse.body().getRequestToken();
//            // access token
//            Call<AccessTokenResponse> accessTokenCall = oAuthApi.accessToken(requestToken, metadata.onlineMemberID);
//            Response<AccessTokenResponse> accessTokenResponse = accessTokenCall.execute();
//            if(!accessTokenResponse.isSuccessful()) {
//                e.onError(new Exception(accessTokenResponse.errorBody().string()));
//                return;
//            }
//            if(accessTokenResponse.body().message() != null || !accessTokenResponse.body().status()) {
//                String message = accessTokenResponse.body().message();
//                e.onError(new BackendException(message));
//                return;
//            }
//            e.onSuccess(accessTokenResponse.body().accessToken());
        });
    }
}
