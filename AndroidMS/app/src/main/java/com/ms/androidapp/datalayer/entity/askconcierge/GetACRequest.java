package com.ms.androidapp.datalayer.entity.askconcierge;

import com.google.gson.annotations.SerializedName;
import com.ms.androidapp.BuildConfig;

/**
 * Created by vinh.trinh on 8/29/2017.
 */

public class GetACRequest {

    @SerializedName("AccessToken")
    private final String accessToken;
    @SerializedName("ConsumerKey")
    private final String consumerKey;
    @SerializedName("EPCCaseID")
    private String EPCCaseID;
    @SerializedName("Functionality")
    private final String functionality;
    @SerializedName("OnlineMemberId")
    private final String onlineMemberId;
    @SerializedName("TransactionID")
    private String transactionID;
    @SerializedName("RowStart")
    private Integer rowStart;
    @SerializedName("RowEnd")
    private Integer rowEnd;

    public void individual(String EPCCaseID, String transactionID) {
        this.EPCCaseID = EPCCaseID;
        this.transactionID = transactionID;
        this.rowStart = null;
        this.rowEnd = null;
    }

    public void list(int start, int end) {
        this.EPCCaseID = null;
        this.transactionID = null;
        this.rowStart = start;
        this.rowEnd = end;
    }

    public GetACRequest(String accessToken, String onlineMemberId) {
        this.accessToken = accessToken;
        this.consumerKey = BuildConfig.WS_ConciergeOauth_ConsumerKey;
        this.functionality = "GetRecentRequests";
        this.onlineMemberId = onlineMemberId;
    }
}
