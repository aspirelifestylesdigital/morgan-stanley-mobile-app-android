package com.ms.androidapp.presentation.profile;

import com.api.aspire.common.constant.ErrCode;
import com.ms.androidapp.domain.model.Profile;
import com.ms.androidapp.presentation.base.BasePresenter;

/**
 * Created by vinh.trinh on 5/11/2017.
 */

public interface EditProfile {

    interface View {
        void displayProfile(Profile profile, boolean fromRemote);
        void showErrorDialog(ErrCode errCode, String extraMsg);
        void showProgressDialog();
        void dismissProgressDialog();
        void profileUpdated();
    }

    interface Presenter extends BasePresenter<View> {
        void getProfile();
        void updateProfile(Profile profile);
        void abort();

        void getProfileLocal();

        void updateSecurityQuestion(String question, String answer);

        void updateProfileAndSecurity(Profile profile, String question, String answer);
    }
}
