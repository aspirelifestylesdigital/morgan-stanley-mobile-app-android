package com.ms.androidapp.presentation.profile.signUpV2;

import com.api.aspire.common.constant.ErrCode;
import com.api.aspire.data.entity.userprofile.pma.ProfilePMAMapView;
import com.ms.androidapp.domain.model.Profile;
import com.ms.androidapp.presentation.base.BasePresenter;

public interface SignUpV2 {
    interface View {
        void showProfileCreatedDialog();

        void showErrorDialog(ErrCode errCode, String extraMsg);

        void showProgressDialog();

        void dismissProgressDialog();

        void showMatchUpDialog();

        void showExistPMAAccountDialog(ProfilePMAMapView profilePMAMapView);

        void showCreateNewAccountDialog(String email);
    }

    interface Presenter extends BasePresenter<View> {
        void createProfile(Profile profile);
        void matchUpProfile(String email);
    }
}

