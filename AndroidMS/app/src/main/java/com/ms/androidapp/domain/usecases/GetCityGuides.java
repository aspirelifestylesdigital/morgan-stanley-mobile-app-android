package com.ms.androidapp.domain.usecases;

import com.ms.androidapp.datalayer.entity.Answer;
import com.ms.androidapp.datalayer.entity.QuestionsAndAnswer;
import com.ms.androidapp.domain.model.explore.CityGuideDetailItem;
import com.ms.androidapp.domain.model.explore.ExploreRView;
import com.ms.androidapp.domain.model.explore.ExploreRViewItem;
import com.ms.androidapp.domain.repository.B2CRepository;
import com.ms.androidapp.presentation.explore.ExplorePresenter;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;

/**
 * Created by tung.phan on 6/1/2017.
 */

public class GetCityGuides extends UseCase<List<ExploreRViewItem>, GetCityGuides.Params> {
    private B2CRepository b2CRepository;
    private List<Answer> data;

    public GetCityGuides(B2CRepository b2CRepository) {
        super();
        this.b2CRepository = b2CRepository;
        data = new ArrayList<>();
    }

    @Override
    Observable<List<ExploreRViewItem>> buildUseCaseObservable(Params params) {
        return null;
    }

    @Override
    public Single<List<ExploreRViewItem>> buildUseCaseSingle(Params params) {
        if(params.paging == ExplorePresenter.DEFAULT_PAGE) data.clear();
        return b2CRepository.getCityGuides(params.categoryID, params.paging)
                .map(questionsAndAnswers -> {
            int length = data.size();
            List<Answer> newData = new ArrayList<>();
            for (QuestionsAndAnswer qa : questionsAndAnswers) {
                newData.addAll(qa.getAnswers());
            }
            data.addAll(newData);
            return viewData(length, newData);
        });
    }

    private List<ExploreRViewItem> viewData(int startIndex, List<Answer> dataList) {
        final List<ExploreRViewItem> exploreRViewList = new ArrayList<>();
        int length = dataList.size();
        for (int i = 0; i < length; i++) {
            Answer qa = dataList.get(i);
            final ExploreRViewItem exploreRViewItem = viewDatum(startIndex + i, qa);
            exploreRViewList.add(exploreRViewItem);
        }
        return exploreRViewList;
    }

    private ExploreRViewItem viewDatum(int dataIndex, Answer answer) {
        String insiderTip = "";
        String insiderTipSeg = "<b>Insider tip</b>:";
        if(answer.getAnswerText().contains(insiderTipSeg)){
            int insiderTipInd = answer.getAnswerText().indexOf(insiderTipSeg);
            insiderTip = answer.getAnswerText().substring(insiderTipInd + insiderTipSeg.length()).trim().replace("\n", "").replace("\r", "");
        }

        ExploreRViewItem exploreRViewItem = new ExploreRViewItem(
                answer.getID(),
                answer.getName().trim(),
                answer.getAddress3(),
                null,
                false,
                insiderTip,
                dataIndex
        );
        exploreRViewItem.setItemType(ExploreRViewItem.ItemType.CITY_GUIDE);
        return exploreRViewItem;
    }

    public CityGuideDetailItem getItemView(int index) {
        Answer answer = data.get(index);
        CityGuideDetailItem item = new CityGuideDetailItem.Builder(
                answer.getName(),
                answer.getAddress(),
                answer.getCity(),
                answer.getState(),
                answer.getZipCode(),
                answer.getAnswerText())
                .address2(answer.getAddress2())
                .address3(answer.getAddress3())
                .url(answer.getURL())
                .build();
        item.setItemType(ExploreRView.ItemType.CITY_GUIDE);
        return item;
    }

    @Override
    Completable buildUseCaseCompletable(Params params) {
        return null;
    }

    public static final class Params {

        private final Integer categoryID;
        private Integer paging;

        public Params(Integer categoryID) {
            this.categoryID = categoryID;
            paging = 0;
        }

        public Params paging(int paging) {
            this.paging = paging;
            return this;
        }
    }
}
