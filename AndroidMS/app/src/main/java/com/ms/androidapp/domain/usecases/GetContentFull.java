package com.ms.androidapp.domain.usecases;

import com.ms.androidapp.domain.mapper.MappingDetailRViewItem;
import com.ms.androidapp.domain.model.explore.OtherExploreDetailItem;
import com.ms.androidapp.domain.repository.B2CRepository;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;

/**
 * Created by ThuNguyen on 6/6/2017.
 */

public class GetContentFull extends UseCase<OtherExploreDetailItem, GetContentFull.Params> {

    private B2CRepository b2CRepository;

    public GetContentFull(B2CRepository b2CRepository) {
        this.b2CRepository = b2CRepository;
    }

    @Override
    Observable<OtherExploreDetailItem> buildUseCaseObservable(Params params) {
        return null;
    }

    @Override
    Single<OtherExploreDetailItem> buildUseCaseSingle(Params params) {
        return b2CRepository.getContentFull(params.contentId)
                .map(MappingDetailRViewItem::transformContentForDetail);
    }

    @Override
    Completable buildUseCaseCompletable(Params params) {
        return null;
    }

    public static final class Params {
        final Integer contentId;

        public Params(Integer contentId) {
            this.contentId = contentId;
        }
    }
}
