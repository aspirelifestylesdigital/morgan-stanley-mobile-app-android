package com.ms.androidapp.datalayer.entity.b2cinstant;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ms.androidapp.BuildConfig;

/**
 * Created by vinh.trinh on 6/5/2017.
 */

public class B2CQandARequest {
    @SerializedName("subDomain")
    @Expose
    public final String subDomain;
    @SerializedName("password")
    @Expose
    public final String secretKey;
    @SerializedName("categoryID")
    @Expose
    public final Integer categoryID;
    @SerializedName("pageSize")
    @Expose
    private Integer pageSize;
    @SerializedName("pageNumber")
    @Expose
    private Integer pageNumber;

    public B2CQandARequest(Integer categoryID) {
        subDomain = BuildConfig.WS_B2C_SUBDOMAIN;
        secretKey = BuildConfig.WS_B2C_SECRET;
        this.categoryID = categoryID;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public void setPageNumber(Integer pageNumber) {
        this.pageNumber = pageNumber;
    }
}
