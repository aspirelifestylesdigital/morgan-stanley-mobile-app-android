package com.ms.androidapp.datalayer.entity.oauth;

import com.google.gson.annotations.SerializedName;

/**
 * Created by vinh.trinh on 6/19/2017.
 */
public class ReqTokenResponse {
    @SerializedName("ConsumerKey")
    private String consumerKey;
    @SerializedName("RequestToken")
    private String requestToken;
    @SerializedName("ExpirationTime")
    private String expirationTime;
    @SerializedName("Status")
    private String status;
    @SerializedName("message")
    private Message[] messages;

    public String consumerKey() {
        return consumerKey;
    }

    public String getRequestToken() {
        return requestToken;
    }

    public String message() {
        return messages != null && messages.length > 0 ? messages[0].message : "";
    }

    public String expirationTime() {
        return expirationTime;
    }

    public String status() {
        return status;
    }

    private class Message {
        @SerializedName("message")
        private String message;
    }
}
