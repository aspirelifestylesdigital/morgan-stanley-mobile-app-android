package com.ms.androidapp.presentation.venuedetail;

import com.ms.androidapp.domain.model.explore.CityGuideDetailItem;
import com.ms.androidapp.presentation.base.BasePresenter;

/**
 * Created by ThuNguyen on 6/13/2017.
 */

public interface CityGuideDetail {

    interface View {
        void onGetCityGuideDetailFinished(CityGuideDetailItem cityGuideDetailItem);
        void onUpdateFailed();
    }

    interface Presenter extends BasePresenter<View> {
        void getCityGuideDetail(Integer categoryId, Integer itemId);
    }

}
