package com.ms.androidapp.presentation.challengequestion;

import com.api.aspire.common.constant.ErrCode;
import com.api.aspire.data.repository.SecurityQuestionDataRepository;
import com.api.aspire.domain.model.SecurityQuestion;
import com.api.aspire.domain.usecases.GetSecurityQuestions;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class ListChallengeQuestionPresenter implements ListChallengeQuestion.Presenter {
    CompositeDisposable disposables;
    ListChallengeQuestion.View view;

    GetSecurityQuestions getChallengeQuestions;
    private List<SecurityQuestion> cacheSecurityQuestions;

    public ListChallengeQuestionPresenter() {
        disposables = new CompositeDisposable();
        getChallengeQuestions = new GetSecurityQuestions(new SecurityQuestionDataRepository());
    }

    @Override
    public void attach(ListChallengeQuestion.View view) {
        this.view = view;
    }

    @Override
    public void getChallengeQuestions() {
        //-- get list question local - don't need check internet
//        if (!App.getInstance().hasNetworkConnection()) {
//            view.dismissProgressDialog();
//            view.showErrorMessage(ErrCode.CONNECTIVITY_PROBLEM, null);
//            return;
//        }

        disposables.add(
                getChallengeQuestions
                        .param(new GetSecurityQuestions.Param())
                        .on(Schedulers.io(), AndroidSchedulers.mainThread())
                        .execute(new DisposableSingleObserver<List<SecurityQuestion>>() {
                            @Override
                            public void onSuccess(List<SecurityQuestion> securityQuestions) {
                                cacheSecurityQuestions = securityQuestions;
                                if (view == null) {
                                    return;
                                }
                                view.getChallengeQuestions(securityQuestions);
                            }

                            @Override
                            public void onError(Throwable e) {
                                if (view == null) {
                                    return;
                                }

                                view.showErrorMessage(ErrCode.UNKNOWN_ERROR, e.getMessage());
                            }
                        })
        );
    }

    @Override
    public String getQuestionContent(int position) {
        if (cacheSecurityQuestions == null
                || cacheSecurityQuestions.isEmpty()
                || cacheSecurityQuestions.get(position) == null) {
            return "";
        }

        SecurityQuestion securityQuestion = cacheSecurityQuestions.get(position);
        return securityQuestion.getQuestionText();
    }


    @Override
    public void detach() {
        view = null;
        disposables.dispose();
    }
}
