package com.ms.androidapp.presentation.profile;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import com.ms.androidapp.App;
import com.ms.androidapp.R;
import com.ms.androidapp.common.constant.AppConstant;
import com.api.aspire.common.constant.ErrCode;
import com.ms.androidapp.common.logic.Validator;
import com.ms.androidapp.presentation.base.CommonActivity;
import com.ms.androidapp.presentation.widget.DialogHelper;
import com.ms.androidapp.presentation.widget.ViewKeyboardListener;
import com.ms.androidapp.presentation.widget.ViewUtils;
import com.support.mylibrary.widget.ErrorIndicatorEditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by vinh.trinh on 7/24/2017.
 */

public class ForgotPasswordActivity extends CommonActivity implements ForgotPassword.View {

    @BindView(R.id.holder)
    View rootView;
    @BindView(R.id.edt_email)
    ErrorIndicatorEditText edtEmail;
    @BindView(R.id.content_wrapper)
    LinearLayout contentWrapper;
    @BindView(R.id.scrollView)
    ScrollView scroll;
    @BindView(R.id.btn_submit)
    Button btnSubmit;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    private ForgotPasswordPresenter presenter;
    private DialogHelper dialogHelper;
    private Validator validator;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        ButterKnife.bind(this);
        btnSubmit.setEnabled(false);
//        final Handler handler = new Handler();
        presenter = new ForgotPasswordPresenter();
        presenter.attach(this);
        dialogHelper = new DialogHelper(this);
        validator = new Validator();

        btnSubmit.setEnabled(false);
        btnSubmit.post(new Runnable() {
            @Override
            public void run() {
                btnSubmit.setClickable(false);
            }
        });
//        edtEmail.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                handler.postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        scroll.scrollTo(0, scroll.getBottom());
//                    }
//                }, 350);
//            }
//        });
        edtEmail.setOnFocusChangeListener((view1, b) -> {
            if (b) {
//                handler.postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        scroll.scrollTo(0, scroll.getBottom());
//                    }
//                }, 350);
            }else{
                if (edtEmail.getText().toString().trim().length() == 0) {
                    edtEmail.setText("");
                    btnSubmit.setEnabled(false);
                }
                ViewUtils.hideSoftKey(edtEmail);
            }
        });

        edtEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void afterTextChanged(Editable editable) {
                boolean isEnabled = editable.toString().trim().length() > 0;
                btnSubmit.setEnabled(isEnabled);
                btnSubmit.setClickable(isEnabled);
            }
        });
        if(savedInstanceState == null){
            // Track GA
            App.getInstance().track(AppConstant.GA_TRACKING_SCREEN_NAME.FORGOT_PASSWORD.getValue());
        }

        toolbar.setOnClickListener(view -> ViewUtils.hideSoftKey(toolbar));
        hideToolbarElevation();
        keyboardInteractListener();
    }

    /** handle click on back hide keyboard close cursor */
    private void keyboardInteractListener() {
        ViewKeyboardListener.KeyboardEvent event = new ViewKeyboardListener.KeyboardEvent() {
            @Override
            public void showKeyboard() {
                //dummy do nothing
            }

            @Override
            public void hideKeyboard() {
                //dummy do nothing
            }

            @Override
            public View getCurrentFocus() {
                return ForgotPasswordActivity.this.getCurrentFocus();
            }
        };
        //-- add listen keyboard
        new ViewKeyboardListener(rootView, event);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            ViewUtils.hideSoftKey(edtEmail);
            super.onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.btn_submit)
    void onSubmit(View view) {
        ViewUtils.hideSoftKey(edtEmail);
        String email = edtEmail.getText().toString();
        if (!validator.email(email)) {
            dialogHelper.profileDialog(getString(R.string.input_err_invalid_email), dialog -> {
                if(view != null) {
                    view.postDelayed(() -> invokeSoftKey(view), 100);
                }
            });
        } else {
            showProgressDialog();
            presenter.retrievePassword(email);
        }
    }

    @Override
    public void showProgressDialog() {
        dialogHelper.showProgress();
    }

    @Override
    public void dismissProgressDialog() {
        dialogHelper.dismissProgress();
    }

    @Override
    public void showErrorMessage(ErrCode errCode, String extraMsg) {
        if (dialogHelper.networkUnavailability(errCode, extraMsg)) return;
        if(errCode == ErrCode.API_ERROR) {
            dialogHelper.alert(getResources().getString(R.string.input_err_fields),
                    getResources().getString(R.string.input_err_invalid_email));
        } else {
            dialogHelper.showGeneralError();
        }
    }

    @Override
    public void showSuccessMessage() {
        dialogHelper.alert(null, getString(R.string.retrieve_password_message), dialog -> onBackPressed());
    }

    void invokeSoftKey(View view) {
        if(edtEmail != null) {
            edtEmail.requestFocus();
            edtEmail.setError("");
            edtEmail.setCursorVisible(true);
            edtEmail.setSelection(edtEmail.getText().toString().length());
        }
        ViewUtils.showSoftKey(view);
    }

    @Override
    public void onViewPositionChanged(float fractionAnchor, float fractionScreen) {
        if(fractionScreen == 1.0) ViewUtils.hideSoftKey(edtEmail);
        super.onViewPositionChanged(fractionAnchor, fractionScreen);
    }

    @Override
    public void onBackPressed() {
        ViewUtils.hideSoftKey(getCurrentFocus());
        super.onBackPressed();
    }
}
