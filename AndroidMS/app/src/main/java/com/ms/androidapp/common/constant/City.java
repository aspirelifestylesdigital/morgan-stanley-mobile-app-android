package com.ms.androidapp.common.constant;

/**
 * Created by vinh.trinh on 6/2/2017.
 */

public class City {
    public final String name;
    public final String geographic;
    public final boolean usCity;
    public final int diningCode;
    public final int cityGuideCode;

    public City(String name, String geographic, boolean usCity, int diningCode, int cityGuideCode, int dining, int bar, int spa, int shopping, int culture) {
        this(name, geographic, usCity, diningCode, cityGuideCode);
        CityData.cityGuide.append(cityGuideCode, new CityGuide(dining, bar, spa, shopping, culture));
    }

    /*public City(String name, String geographic, boolean usCity, int diningCode, int cityGuideCode, int accommodation) {
        this(name, geographic, usCity, diningCode, cityGuideCode);
        CityData.cityGuide.append(cityGuideCode, new CityGuide(accommodation));
    }*/

    public City(String name, String geographic, boolean usCity, int diningCode, int cityGuideCode) {
        this.name = name;
        this.geographic = geographic;
        this.diningCode = diningCode;
        this.cityGuideCode = cityGuideCode;
        this.usCity = usCity;
    }
}
