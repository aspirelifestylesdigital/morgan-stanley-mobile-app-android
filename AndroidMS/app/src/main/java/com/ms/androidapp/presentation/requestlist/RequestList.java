package com.ms.androidapp.presentation.requestlist;

import com.ms.androidapp.domain.model.RequestItemView;
import com.ms.androidapp.presentation.base.BasePresenter;

import java.util.List;

/**
 * Created by vinh.trinh on 8/30/2017.
 */

public interface RequestList {

    interface View {
        void showProgressDialog();
        void dismissProgressDialog();
        void showListLoadMore(boolean open);
        void hideListLoadMore(boolean open);
        int openCount();
        int closeCount();
        void loadData(List<RequestItemView> openList, List<RequestItemView> closeList);
        void onFetchError();
    }

    interface Presenter extends BasePresenter<View>{
        void fetchList();
        void loadMore();
        void refresh();
    }

}