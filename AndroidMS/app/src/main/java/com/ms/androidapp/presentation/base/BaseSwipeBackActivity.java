package com.ms.androidapp.presentation.base;

import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatDelegate;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.ms.androidapp.R;
import com.support.mylibrary.widget.swipeback.SwipeBackLayoutV2;

/**
 * Created by  on 10/24/2018.
 * This activity using for all child activity with back button on toolbar.
 * Support back button navigate, swipe right to close.
 */

public abstract class BaseSwipeBackActivity extends BaseActivity
        implements BaseSwipeBackFragment.OnAddFragmentListener, SwipeBackLayoutV2.SwipeBackEvent {

    static {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        }
    }

    protected Rect homeUpRect;

    //Using this field to create swipe right to close child activity
    private SwipeBackLayoutV2 swipeBackLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        onActivityCreate();

    }

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        super.setContentView(layoutResID);
    }

    void onActivityCreate() {
        getWindow().getDecorView().setBackgroundColor(ContextCompat.getColor(this, R.color.ms_colorPrimary));
        swipeBackLayout = new SwipeBackLayoutV2(this);
        ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        swipeBackLayout.setLayoutParams(params);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        swipeBackLayout.attachToActivity(this);
    }

    @Override
    public <T extends View> T findViewById(int id) {
        View view = super.findViewById(id);
        if (view == null && swipeBackLayout != null) {
            return swipeBackLayout.findViewById(id);
        }
        return super.findViewById(id);
    }

    @Override
    public void setContentView(View view) {
        super.setContentView(view);
    }

    @Override
    public void setContentView(View view, ViewGroup.LayoutParams params) {
        super.setContentView(view, params);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean swipeBackPriority() {
        return getSupportFragmentManager().getBackStackEntryCount() <= 1;
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
            finish();
        } else {
            super.onBackPressed();
        }
    }

    private void addFragment(Fragment toFragment) {
        Fragment currentFragment = getSupportFragmentManager().findFragmentById(getFrameLayoutId());
        if (currentFragment != null) {
            getSupportFragmentManager().beginTransaction()
//                    .setCustomAnimations(R.anim.h_fragment_enter, R.anim.h_fragment_exit, R.anim.h_fragment_pop_enter, R.anim.h_fragment_pop_exit)
                    .setCustomAnimations(0,0)
                    .add(getFrameLayoutId(), toFragment, toFragment.getClass().getSimpleName())
                    .hide(currentFragment)
                    .addToBackStack(toFragment.getClass().getSimpleName())
                    .commit();
        }
    }

    public void loadFirstFragment(Fragment toFragment) {
        getSupportFragmentManager().beginTransaction()
                .add(getFrameLayoutId(), toFragment, toFragment.getClass().getSimpleName())
                .commit();
    }

    public abstract int getFrameLayoutId();

    @Override
    public void onAddFragment(Fragment toFragment) {
        addFragment(toFragment);
    }

    @Override
    public void setRectButtonBack(Rect homeUpRect) {
        this.homeUpRect = homeUpRect;
    }

    public Rect getHomeUpRect() {
        return homeUpRect;
    }
}
