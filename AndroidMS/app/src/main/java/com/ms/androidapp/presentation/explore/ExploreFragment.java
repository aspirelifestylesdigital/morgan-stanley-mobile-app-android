package com.ms.androidapp.presentation.explore;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ms.androidapp.App;
import com.ms.androidapp.R;
import com.ms.androidapp.common.constant.AppConstant;
import com.ms.androidapp.common.constant.CityData;
import com.ms.androidapp.common.constant.CityGuide;
import com.api.aspire.common.constant.ErrCode;
import com.ms.androidapp.common.constant.IntentConstant;
import com.ms.androidapp.common.constant.RequestCode;
import com.ms.androidapp.common.constant.ResultCode;
import com.ms.androidapp.datalayer.datasource.AppGeoCoder;
import com.ms.androidapp.datalayer.repository.B2CDataRepository;
import com.ms.androidapp.domain.model.LatLng;
import com.ms.androidapp.domain.model.explore.CityGuideDetailItem;
import com.ms.androidapp.domain.model.explore.ExploreRView;
import com.ms.androidapp.domain.model.explore.ExploreRViewItem;
import com.ms.androidapp.domain.usecases.AccommodationSearch;
import com.ms.androidapp.domain.usecases.GetAccommodationByCategories;
import com.ms.androidapp.domain.usecases.GetCityGuides;
import com.ms.androidapp.domain.usecases.GetDinningList;
import com.ms.androidapp.presentation.base.BaseFragment;
import com.ms.androidapp.presentation.cityguidecategory.CityGuideCategoriesActivity;
import com.ms.androidapp.presentation.home.HomeActivity;
import com.ms.androidapp.presentation.search.SearchActivity;
import com.ms.androidapp.presentation.selectcategory.CategoryPresenter;
import com.ms.androidapp.presentation.selectcategory.SelectCategoryActivity;
import com.ms.androidapp.presentation.selectcity.SelectCityActivity;
import com.ms.androidapp.presentation.venuedetail.CityGuideDetailActivity;
import com.ms.androidapp.presentation.venuedetail.DiningDetailActivity;
import com.ms.androidapp.presentation.venuedetail.OtherExploreDetailActivity;
import com.ms.androidapp.presentation.widget.ListItemDecoration;
import com.ms.androidapp.presentation.widget.OnItemClickListener;
import com.ms.androidapp.presentation.widget.RecyclerViewScrollListener;
import com.ms.androidapp.presentation.widget.ViewUtils;
import com.support.mylibrary.common.Utils;
import com.support.mylibrary.widget.ClickGuard;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.OnClick;

public class ExploreFragment extends BaseFragment implements Explore.View,
        OnItemClickListener {
    private static final String SELECT_CATEGORY_TEXT = "select_category";
    private static final String LAST_SELECTED_TEXT = "last_selected";
    private static final String SELECTED_SUB_CATEGORY_NAME = "selected_sub_category_name";
    private static final String SELECTED_SUB_CATEGORY_RES_ID = "selected_sub_category_res_id";
    private static final String DATA_ITEM_LIST = "data_item_list";
    private static final String SEARCH_TERM_TEXT = "search_term";
    private static final String IS_WITH_OFFERS_TEXT = "is_with_offers";
    private static final String EXPLORE_ACTION_TYPE_TEXT = "explore_action_type";
    private static final String PAGING_TEXT = "paging";
    private static final String SUB_CATEGORY_INDEX = "sub_category_index";
    @BindView(R.id.recommend_view)
    RecyclerView recommendView;
    @BindView(R.id.location_btn)
    Button btnLocation;
    @BindView(R.id.category_btn)
    Button btnCategory;
    @BindView(R.id.search_btn)
    Button btnSearch;
    @BindView(R.id.search_box)
    FrameLayout searchBox;
    @BindView(R.id.edt_search)
    EditText edtSearch;
    @BindView(R.id.offer_wrapper)
    LinearLayout withOffersWrapper;
    @BindView(R.id.tv_no_data)
    TextView tvNoData;
    @BindView(R.id.btn_ask)
    Button btnAskConcierge;
    @BindView(R.id.loading)
    ProgressBar loadingProgressBar;
    private ExplorePresenter presenter;
    private int lastSelected = -1;
    private String selectedSubCategoryName;
    private int selectedSubCategoryResId;
    private EXPLORE_ACTION_TYPE exploreActionType = EXPLORE_ACTION_TYPE.EXPLORE_NORMAL_LIST;
    private ExploreRViewAdapter exploreRViewAdapter;
    private ListItemDecoration dividerItemDecoration;
    private RecyclerViewScrollListener scrollListener;
    private boolean shouldReload = false;

    public static ExploreFragment newInstance(LatLng location, String cuisine) {
        Bundle args = new Bundle();
        args.putParcelable("sorting_criteria_1", location);
        args.putString("sorting_criteria_2", cuisine);
        ExploreFragment fragment = new ExploreFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private ExplorePresenter buildPresenter() {
        B2CDataRepository b2CDataRepository = new B2CDataRepository();
        AppGeoCoder appGeoCoder = new AppGeoCoder();
        GetAccommodationByCategories other = new GetAccommodationByCategories(b2CDataRepository);
        GetCityGuides cityGuides = new GetCityGuides(b2CDataRepository);
        GetDinningList dinningList = new GetDinningList(b2CDataRepository, appGeoCoder);
        AccommodationSearch search = new AccommodationSearch(b2CDataRepository, appGeoCoder);
        return new ExplorePresenter(other, dinningList, cityGuides, search);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = buildPresenter();
        presenter.attach(this);
        preferencesChange(getArguments().getParcelable("sorting_criteria_1"),
                getArguments().getString("sorting_criteria_2"));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_explore, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recommendView.setLayoutManager(layoutManager);
        dividerItemDecoration = new ListItemDecoration(recommendView.getContext());
        recommendView.addItemDecoration(dividerItemDecoration);
        exploreRViewAdapter = new ExploreRViewAdapter(getContext(), new ArrayList<>(), this);
        recommendView.setAdapter(exploreRViewAdapter);
        dividerItemDecoration.setAdapterItemCount(0);

        ViewUtils.drawableTop(btnLocation, R.drawable.ic_location);
        ViewUtils.drawableTop(btnCategory, R.drawable.ic_category);
        ViewUtils.drawableTop(btnSearch, R.drawable.ic_search);
        ViewUtils.drawableEnd(edtSearch, R.drawable.ic_clear);
        setupLoadMore();
        edtSearch.setOnTouchListener((v, event) -> {
            if(event.getAction() == MotionEvent.ACTION_UP) {
                if(event.getRawX() >= (edtSearch.getRight() - edtSearch.getCompoundDrawables()[2].getBounds().width()- 20)) {
                    // your action here
                    clearSearch(true);
                    return true;
                }else{
                    // Search with the given keyword
                    searchWithGivenKeyword();
                    return true;
                }
            }
            return false;
        });
        ClickGuard.guard(btnLocation, btnCategory, btnSearch);

        if(savedInstanceState == null) {
            // Track GA
            App.getInstance().track(AppConstant.GA_TRACKING_SCREEN_NAME.EXPLORE.getValue());
            if (CityData.citySelected()) {
                handleSelectedCityResult();
            }
        }else{
            // Retrieve the data stored on the bundle state
            lastSelected = savedInstanceState.getInt(LAST_SELECTED_TEXT, -1);
            selectedSubCategoryName = savedInstanceState.getString(SELECTED_SUB_CATEGORY_NAME);
            selectedSubCategoryResId = savedInstanceState.getInt(SELECTED_SUB_CATEGORY_RES_ID);
            String selectedCategory = savedInstanceState.getString(SELECT_CATEGORY_TEXT, "all");
            List<ExploreRViewItem> dataItemList = savedInstanceState.getParcelableArrayList(DATA_ITEM_LIST);
            String searchTerm = savedInstanceState.getString(SEARCH_TERM_TEXT);
            boolean isWithOffer = savedInstanceState.getBoolean(IS_WITH_OFFERS_TEXT);
            exploreActionType = EXPLORE_ACTION_TYPE.getEnum(savedInstanceState.getString(EXPLORE_ACTION_TYPE_TEXT));
            int paging = savedInstanceState.getInt(PAGING_TEXT, 1);
            int cityGuideIndex = savedInstanceState.getInt(SUB_CATEGORY_INDEX, -1);
            // Apply to object
            presenter.setLastSelectedCity(CityData.cityName());
            presenter.setSelectedCategory(selectedCategory);
            presenter.setCityGuideIndex(cityGuideIndex);
            presenter.setPaging(paging);
            presenter.setTerm(searchTerm);
            presenter.setWithOffers(isWithOffer);

            btnLocation.setText(CityData.cityName());

            switch (exploreActionType){
                case EXPLORE_NORMAL_LIST:
                    String displayCategory = capitalWords(selectedCategory);
                    btnCategory.setText(displayCategory);
                    btnSearch.setText(getResources().getString(R.string.text_search));
                    getActivity().setTitle(R.string.explore);
                    //(int) getResources().getDimension(R.dimen.padding_medium)
                    btnSearch.setPadding(0, (int) getResources().getDimension(R.dimen.padding_medium), 0, 0);
                    ViewUtils.drawableTop(btnSearch, R.drawable.ic_search);

                    break;
                case EXPLORE_CITY_GUIDE_LIST:
                    btnCategory.setText(selectedCategory);
                    //reset text for btn Search and also remove the icon
                    btnSearch.setText(getResources().getString(R.string.text_discover_more));
                    getActivity().setTitle(selectedSubCategoryName);
                    ViewUtils.emptyDrawable(btnSearch);
                    btnSearch.setPadding(0,Utils.dip2px(getContext(), 20),0,(int) getResources().getDimension(R.dimen.padding_smallest));
                    break;
                case EXPLORE_SEARCH:
                    displayCategory = capitalWords(selectedCategory);
                    btnCategory.setText(displayCategory);
                    btnSearch.setText(getResources().getString(R.string.text_search));
                    getActivity().setTitle(R.string.explore);
                    //(int) getResources().getDimension(R.dimen.padding_medium)
                    btnSearch.setPadding(0, (int) getResources().getDimension(R.dimen.padding_medium), 0, 0);
                    ViewUtils.drawableTop(btnSearch, R.drawable.ic_search);
                    showSearchBox(searchTerm, isWithOffer);
                    break;
            }
            if(dataItemList != null && dataItemList.size() > 0){
                tvNoData.setVisibility(View.GONE);
                btnAskConcierge.setVisibility(View.GONE);
                exploreRViewAdapter.swapData(dataItemList);
                dividerItemDecoration.setAdapterItemCount(exploreRViewAdapter.getItemCount());
            }else{
                tvNoData.setText(getString(R.string.empty_search_result_notify));
                tvNoData.setVisibility(View.VISIBLE);
                btnAskConcierge.setVisibility(View.VISIBLE);
            }
        }

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(LAST_SELECTED_TEXT, lastSelected);
        outState.putString(SELECTED_SUB_CATEGORY_NAME, selectedSubCategoryName);
        outState.putInt(SELECTED_SUB_CATEGORY_RES_ID, selectedSubCategoryResId);
        outState.putString(SELECT_CATEGORY_TEXT, presenter.getSelectedCategory());
        outState.putParcelableArrayList(DATA_ITEM_LIST, exploreRViewAdapter.getData());
        outState.putString(SEARCH_TERM_TEXT, presenter.searchTerm());
        outState.putBoolean(IS_WITH_OFFERS_TEXT, presenter.isWithOffers());
        outState.putString(EXPLORE_ACTION_TYPE_TEXT, exploreActionType.getValue());
        outState.putInt(PAGING_TEXT, presenter.getPaging());
        outState.putInt(SUB_CATEGORY_INDEX, presenter.getCityGuideIndex());
    }

    @OnClick(R.id.location_btn)
    public void clickBtnLocation(View view) {
        Intent intent = new Intent(getActivity(), SelectCityActivity.class);
        startActivityForResult(intent, RequestCode.SELECT_CITY);
    }

    @OnClick(R.id.category_btn)
    public void clickBtnCategory(View view) {
        Intent intent = new Intent(getContext(), SelectCategoryActivity.class);
        startActivityForResult(intent, RequestCode.SELECT_CATEGORY);
    }

    @OnClick(R.id.search_btn)
    public void clickBtnSearch(Button btnSearch) {
        //handle case btn Discover more pressed
        if (btnSearch.getText().toString()
                .equalsIgnoreCase("discover\nmore") && CityData.guideCode()>0) {
            //start sub category select
            Intent intent = new Intent(getContext(), CityGuideCategoriesActivity.class);
            intent.putExtra(IntentConstant.CATEGORY_ID, CityData.guideCode());
            startActivityForResult(intent, RequestCode.SELECT_SUB_CATEGORY);
        } else if(btnSearch.getText().toString()
                .equalsIgnoreCase("search")) {
            Intent intent = new Intent(getActivity(), SearchActivity.class);
            startActivityForResult(intent, RequestCode.SEARCH_INPUT);
        }
    }

    @OnClick(R.id.btn_clear_offers)
    public void clearOffers() {
        presenter.offHasOffers();
        withOffersWrapper.setVisibility(View.GONE);
    }

    @OnClick(R.id.btn_ask)
    public void clickAskConcierge(View view) {
        ((HomeActivity) getActivity()).navigateToAskConcierge();
    }

    @Override
    public void onDestroy() {
        presenter.detach();
        super.onDestroy();
    }

    @Override
    public void hideLoading(boolean more) {
        if(more) {
            exploreRViewAdapter.showLoading(false);
        } else {
            loadingProgressBar.setVisibility(View.GONE);
        }
        scrollListener.loadDone();
    }

    @Override
    public void showLoading(boolean more) {
        if(more) {
            exploreRViewAdapter.showLoading(true);
        } else {
            loadingProgressBar.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void updateRecommendAdapter(List<ExploreRViewItem> data) {
        tvNoData.setVisibility(View.GONE);
        btnAskConcierge.setVisibility(View.GONE);
        exploreRViewAdapter.setSearchAction(presenter.inSearchingMode());
        exploreRViewAdapter.swapData(data);
        dividerItemDecoration.setAdapterItemCount(exploreRViewAdapter.getItemCount());
    }

    @Override
    public void onUpdateFailed(String message) {
        ((HomeActivity)getActivity()).dialogHelper.showGeneralError();
    }

    @Override
    public void emptyData() {
        exploreRViewAdapter.clear();
    }

    @Override
    public void onSearchNoData() {
        if(exploreRViewAdapter.isEmpty()) {
            tvNoData.setText(getString(R.string.empty_search_result_notify));
            tvNoData.setVisibility(View.VISIBLE);
            btnAskConcierge.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void loadDataDone() {
        if(scrollListener != null) {
            scrollListener.loadDone();
        }
    }

    @Override
    public List<ExploreRViewItem> historyData() {
        return exploreRViewAdapter.getData();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == RequestCode.SELECT_CITY) {
            handleSelectedCityResult();
            exploreActionType = EXPLORE_ACTION_TYPE.EXPLORE_NORMAL_LIST;
        } else if (requestCode == RequestCode.SELECT_CATEGORY) {
            handleSelectCategoryResult(resultCode, data);
            exploreActionType = EXPLORE_ACTION_TYPE.EXPLORE_NORMAL_LIST;
        } else if (requestCode == RequestCode.SELECT_SUB_CATEGORY) {
            btnCategory.setText(getString(R.string.city_guide));
            handleSubCategorySelected(data);
            exploreActionType = EXPLORE_ACTION_TYPE.EXPLORE_CITY_GUIDE_LIST;
        } else if (requestCode == RequestCode.SEARCH_INPUT) {
            handleSearchInputResult(resultCode, data);
            exploreActionType = EXPLORE_ACTION_TYPE.EXPLORE_SEARCH;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void handleSelectCategoryResult(int resultCode, Intent data) {
        if (resultCode == ResultCode.RESULT_OK && data != null) {
            handleNormalCategorySelected(data);
            getActivity().setTitle(getString(R.string.explore));
        } else if (resultCode == ResultCode.RESULT_OK_WITH_ID && data != null) {
            String superCategory = data.getStringExtra(IntentConstant.SUPPER_CATEGORY);
            btnCategory.setText(superCategory);
            handleSubCategorySelected(data);
        }
    }

    private void handleNormalCategorySelected(Intent data) {
        String category = data.getStringExtra(IntentConstant.SELECTED_CATEGORY);
        if(TextUtils.isEmpty(category)) {
            category = "All";
            CategoryPresenter.CATEGORY_ALL = true;
        } else {
            CategoryPresenter.CATEGORY_ALL = false;
        }
        if(category.equalsIgnoreCase("dining")) {
            ((HomeActivity) getActivity()).requestLocation();
        }

        presenter.handleCategorySelection(category);
        String displayCategory = capitalWords(category);
        btnCategory.setText(displayCategory);
        btnSearch.setText(getResources().getString(R.string.text_search));
        //(int) getResources().getDimension(R.dimen.padding_medium)
        btnSearch.setPadding(0, (int) getResources().getDimension(R.dimen.padding_medium), 0, 0);
        ViewUtils.drawableTop(btnSearch, R.drawable.ic_search);

        // Track "select category"
        App.getInstance().track(AppConstant.GA_TRACKING_CATEGORY.CATEGORY_SELECTION.getValue(),
                AppConstant.GA_TRACKING_ACTION.SELECT.getValue(),
                displayCategory);
    }

    public void handleSelectedCityResult() {
        String selectedCity = CityData.cityName();
        btnLocation.setText(selectedCity);
        presenter.handleCitySelection();
        ((HomeActivity) getActivity()).requestLocation();
    }

    private void handleSubCategorySelected(Intent data) {
        CategoryPresenter.CATEGORY_ALL = false;
        if (data == null) {
            return;
        }
        int categoryIndex = data.getIntExtra(IntentConstant.INDEX_CITY_GUIDE_CATEGORY_SELECT, 0);
        presenter.cityGuideCategorySelection(categoryIndex);
        selectedSubCategoryName = data.getStringExtra(IntentConstant.SUB_CATEGORY_NAME);
        getActivity().setTitle(selectedSubCategoryName);
        selectedSubCategoryResId = data.getIntExtra(IntentConstant.SUB_CATEGORY_IMAGE, 0);
        // Track city guide category
        String trackingLabel = "";
        switch (categoryIndex){
            case CityGuide.BAR_LIST_INDEX:
                trackingLabel = AppConstant.GA_TRACKING_LABEL.CITY_GUIDE_BAR_CLUB.getValue();
                break;
            case CityGuide.CULTURE_LIST_INDEX:
                trackingLabel = AppConstant.GA_TRACKING_LABEL.CITY_GUIDE_CULTURE.getValue();
                break;
            case CityGuide.DINNING_LIST_INDEX:
                trackingLabel = AppConstant.GA_TRACKING_LABEL.CITY_GUIDE_DINING.getValue();
                break;
            case CityGuide.SHOPPING_LIST_INDEX:
                trackingLabel = AppConstant.GA_TRACKING_LABEL.CITY_GUIDE_SHOPPING.getValue();
                break;
            case CityGuide.SPA_LIST_INDEX:
                trackingLabel = AppConstant.GA_TRACKING_LABEL.CITY_GUIDE_SPAS.getValue();
                break;
        }
        if(!TextUtils.isEmpty(trackingLabel)){
            App.getInstance().track(AppConstant.GA_TRACKING_CATEGORY.CATEGORY_SELECTION.getValue(),
                    AppConstant.GA_TRACKING_ACTION.SELECT.getValue(),
                    trackingLabel);
        }

        //reset text for btn Search and also remove the icon
        btnSearch.setText(getResources().getString(R.string.text_discover_more));
        ViewUtils.emptyDrawable(btnSearch);
        btnSearch.setPadding(0,Utils.dip2px(getContext(), 20),0,(int) getResources().getDimension(R.dimen.padding_smallest));
    }

    private void handleSearchInputResult(int resultCode, Intent data) {
        if(resultCode == ResultCode.RESULT_OK) {
            emptyData();
            String term = data.getStringExtra(Intent.EXTRA_TEXT);
            boolean withOffers = data.getBooleanExtra(Intent.EXTRA_REFERRER, false);
            showSearchBox(term, withOffers);
            presenter.searchByTerm(term, withOffers);
        } else if(!presenter.inSearchingMode() && shouldReload) {
            presenter.getAccommodationData();
            shouldReload = false;
        }
    }

    @Override
    public void onItemClick(int pos) {
        // highlight selected item
        if(lastSelected > -1) exploreRViewAdapter.notifyItemChanged(lastSelected);
        exploreRViewAdapter.notifyItemChanged(pos);
        lastSelected = pos;

        ExploreRViewItem listItem = exploreRViewAdapter.getItem(pos);
        ExploreRView detailViewData = presenter.detailItem(listItem.getItemType(), listItem.dataListIndex);
        if(detailViewData == null) return;
        Intent intent;
        switch (detailViewData.getItemType()) {
            case DINING:
                intent = new Intent(getContext(), DiningDetailActivity.class);
                break;
            case CITY_GUIDE:
                if(detailViewData instanceof CityGuideDetailItem) {
                    ((CityGuideDetailItem) detailViewData)
                            .setSubCategoryName(selectedSubCategoryName)
                            .setImageResId(selectedSubCategoryResId);
                }

                intent = new Intent(getContext(), CityGuideDetailActivity.class);
//                intent.putExtra(IntentConstant.CATEGORY_NAME, selectedCity);
                break;
            case NORMAL:
                intent = new Intent(getContext(), OtherExploreDetailActivity.class);
                break;
            default:
                throw new IllegalStateException("type must be one of above");
        }
        intent.putExtra(IntentConstant.EXPLORE_DETAIL, detailViewData);
        startActivity(intent);
    }

    private void setupLoadMore() {
        scrollListener = new RecyclerViewScrollListener() {

            @Override
            public void onScrollUp() {}

            @Override
            public void onScrollDown() {}

            @Override
            public void onLoadMore() {
                presenter.loadMore();
            }
        };
        recommendView.addOnScrollListener(scrollListener);
    }

    private String capitalWords(String text) {
        /*char[] chars = text.toCharArray();
        for (int i=chars.length-1; i >= 1; i--) {
            chars[i] = Character.toLowerCase(chars[i]);
            if(chars[i] == ' ' && i-1>=0) {
                chars[i+1] = Character.toUpperCase(chars[i+1]);
            }
        }
        return new String(chars);*/
        StringBuffer capBuffer = new StringBuffer();
        Matcher capMatcher = Pattern.compile("([a-z])([a-z]*)", Pattern.CASE_INSENSITIVE).matcher(text);
        while (capMatcher.find()){
            capMatcher.appendReplacement(capBuffer, capMatcher.group(1).toUpperCase() + capMatcher.group(2).toLowerCase());
        }

        return capMatcher.appendTail(capBuffer).toString();
    }

    private void showSearchBox(String term, boolean withOffers) {
        tvNoData.setVisibility(View.GONE);
        btnAskConcierge.setVisibility(View.GONE);
        searchBox.setVisibility(View.VISIBLE);
        SpannableStringBuilder str;
        int startInd, endInd;
        if(TextUtils.isEmpty(term)) {
            String text = getString(R.string.search_text_result, "offers");
            str = new SpannableStringBuilder(text);
            startInd = text.lastIndexOf("offers");
            endInd = startInd + "offers".length();
        } else {
            String text = getString(R.string.search_text_result, term);
            str = new SpannableStringBuilder(text);
            startInd = text.lastIndexOf(term);
            endInd = startInd + term.length();
        }
        str.setSpan(new StyleSpan(android.graphics.Typeface.BOLD), startInd, endInd, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        edtSearch.setText(str);

        if(withOffers)
            withOffersWrapper.setVisibility(View.VISIBLE);
        else
            withOffersWrapper.setVisibility(View.GONE);
    }

    @Override
    public void clearSearch(boolean repeat) {
        searchBox.setVisibility(View.GONE);
        tvNoData.setVisibility(View.GONE);
        btnAskConcierge.setVisibility(View.GONE);
        withOffersWrapper.setVisibility(View.GONE);
        presenter.clearSearch();
        emptyData();
        if(repeat) {
            shouldReload = true;
            Intent intent = new Intent(getActivity(), SearchActivity.class);
            startActivityForResult(intent, RequestCode.SEARCH_INPUT);
        }
    }

    private void searchWithGivenKeyword() {
        // Go to Search screen
        Intent intent = new Intent(getActivity(), SearchActivity.class);
        intent.putExtra(Intent.EXTRA_TEXT, presenter.searchTerm());
        intent.putExtra(Intent.EXTRA_REFERRER, presenter.isWithOffers());
        startActivityForResult(intent, RequestCode.SEARCH_INPUT);
    }

    public void preferencesChange(LatLng latLng, String cuisine) {
        presenter.applyDiningSortingCriteria(latLng, cuisine);
    }

    @Override
    public void noInternetMessage() {
        ((HomeActivity)getActivity()).dialogHelper.networkUnavailability(ErrCode.CONNECTIVITY_PROBLEM, null,
                new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialogInterface) {
                        scrollListener.loadDone();
                    }
                });
    }

    @Override
    public void cityGuideNotAvailable() {
        /*tvNoData.setText(getString(R.string.no_enlisted_city_guide));
        tvNoData.setVisibility(View.VISIBLE);*/
        tvNoData.setText(getString(R.string.empty_search_result_notify));
        tvNoData.setVisibility(View.VISIBLE);
        btnAskConcierge.setVisibility(View.VISIBLE);
    }

    public enum EXPLORE_ACTION_TYPE{
        EXPLORE_NORMAL_LIST("explore_normal_list"),
        EXPLORE_CITY_GUIDE_LIST("explore_city_guide_list"),
        EXPLORE_SEARCH("explore_search");
        private static final Map<String, EXPLORE_ACTION_TYPE> enumsByValue = new HashMap<String, EXPLORE_ACTION_TYPE>();
        static {
            for (EXPLORE_ACTION_TYPE type : EXPLORE_ACTION_TYPE.values()) {
                enumsByValue.put(type.value, type);
            }
        }

        private final String value;
        private EXPLORE_ACTION_TYPE(String value) {
            this.value = value;
        }

        public static EXPLORE_ACTION_TYPE getEnum(String value) {
            return enumsByValue.get(value);
        }

        public String getValue() {
            return value;
        }
    }
}