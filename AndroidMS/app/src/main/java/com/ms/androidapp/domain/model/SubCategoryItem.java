package com.ms.androidapp.domain.model;

/**
 * Created by tung.phan on 6/1/2017.
 */

public class SubCategoryItem {

    private int imageResources;
    private String subCategoryName;
    private int id;//this id using to query questions & answers later

    public int getImageResources() {
        return imageResources;
    }

    public void setImageResources(int imageResources) {
        this.imageResources = imageResources;
    }

    public String getSubCategoryName() {
        return subCategoryName;
    }

    public void setSubCategoryName(String subCategoryName) {
        this.subCategoryName = subCategoryName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

}
