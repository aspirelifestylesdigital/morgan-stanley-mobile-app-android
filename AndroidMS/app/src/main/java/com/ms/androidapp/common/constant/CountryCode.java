package com.ms.androidapp.common.constant;

import android.text.TextUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by vinh.trinh on 9/15/2017.
 */

public class CountryCode {
    private static List<String> codes = new ArrayList<>(Arrays.asList("93", "355", "213", "1684",
            "376",
            "244",
            "1264",
            "672",
            "1268",
            "54",
            "374",
            "297",
            "61",
            "43",
            "994",
            "1242",
            "973",
            "880",
            "1246",
            "375",
            "32",
            "501",
            "229",
            "1441",
            "975",
            "591",
            "387",
            "267",
            "55",
            "55",
            "246",
            "673",
            "359",
            "226",
            "257",
            "855",
            "237",
            "1",
            "238",
            "1345",
            "236",
            "235",
            "56",
            "86",
            "61",
            "61",
            "57",
            "269",
            "242",
            "243",
            "682",
            "506",
            "225",
            "385",
            "53",
            "357",
            "420",
            "45",
            "253",
            "1767",
            "1809",
            "593",
            "20",
            "503",
            "240",
            "291",
            "372",
            "251",
            "500",
            "298",
            "679",
            "358",
            "33",
            "594",
            "689",
            "262",
            "241",
            "220",
            "995",
            "49",
            "233",
            "350",
            "30",
            "299",
            "1473",
            "590",
            "1671",
            "502",
            "224",
            "245",
            "592",
            "509",
            "379",
            "504",
            "852",
            "36",
            "354",
            "91",
            "62",
            "98",
            "964",
            "353",
            "972",
            "39",
            "1876",
            "81",
            "962",
            "7",
            "254",
            "686",
            "82",
            "965",
            "996",
            "856",
            "371",
            "961",
            "266",
            "231",
            "218",
            "423",
            "370",
            "352",
            "853",
            "389",
            "261",
            "265",
            "60",
            "960",
            "223",
            "356",
            "692",
            "596",
            "222",
            "230",
            "262",
            "52",
            "691",
            "373",
            "377",
            "976",
            "1664",
            "212",
            "258",
            "95",
            "364",
            "674",
            "977",
            "31",
            "599",
            "687",
            "64",
            "505",
            "227",
            "234",
            "683",
            "672",
            "850",
            "1670",
            "47",
            "968",
            "92",
            "680",
            "970",
            "507",
            "675",
            "595",
            "51",
            "63",
            "64",
            "48",
            "351",
            "1787",
            "974",
            "262",
            "40",
            "7",
            "250",
            "290",
            "1869",
            "1758",
            "508",
            "1784",
            "685",
            "378",
            "239",
            "966",
            "211",
            "381",
            "248",
            "232",
            "65",
            "421",
            "386",
            "677",
            "525",
            "27",
            "500",
            "34",
            "94",
            "249",
            "597",
            "47",
            "268",
            "46",
            "41",
            "963",
            "886",
            "992",
            "255",
            "66",
            "670",
            "228",
            "690",
            "676",
            "1868",
            "216",
            "90",
            "993",
            "1649",
            "688",
            "256",
            "380",
            "971",
            "44",
            "1",
            "1581",
            "598",
            "998",
            "678",
            "58",
            "84",
            "1284",
            "1340",
            "681",
            "212",
            "967",
            "260",
            "263"
    ));

    public static boolean contains(String code) {
        return codes.contains(code);
    }

    public static int indexOf(String code, String zipCode) {
        if (code.equals("1")) {
            if (TextUtils.isEmpty(zipCode))
                return codes.lastIndexOf(code);
            if (TextUtils.isDigitsOnly(zipCode.substring(0, 1)))
                return codes.lastIndexOf(code);
            else
                return codes.indexOf(code);
        }
        return codes.indexOf(code);
    }
}
