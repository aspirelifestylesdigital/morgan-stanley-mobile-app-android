package com.ms.androidapp.presentation.profile;

import android.content.Context;

import com.api.aspire.data.datasource.RemoteChangeRecoveryQuestionDataStore;
import com.api.aspire.data.datasource.RemoteUserProfileOKTADataStore;
import com.api.aspire.data.preference.PreferencesStorageAspire;
import com.api.aspire.data.repository.ChangeSecurityQuestionDataRepository;
import com.api.aspire.data.repository.ProfileDataAspireRepository;
import com.api.aspire.data.repository.UserProfileOKTADataRepository;
import com.api.aspire.domain.model.ProfileAspire;
import com.api.aspire.domain.model.UserProfileOKTA;
import com.api.aspire.domain.usecases.ChangeSecurityQuestion;
import com.api.aspire.domain.usecases.GetAccessToken;
import com.api.aspire.domain.usecases.GetToken;
import com.api.aspire.domain.usecases.LoadProfile;
import com.api.aspire.domain.usecases.SaveProfile;
import com.ms.androidapp.App;
import com.api.aspire.common.constant.ErrCode;
import com.ms.androidapp.domain.mapper.profile.MapProfile;
import com.ms.androidapp.domain.model.Profile;
import com.ms.androidapp.domain.usecases.MapProfileApp;

import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableCompletableObserver;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by vinh.trinh on 5/11/2017.
 */

public class EditProfilePresenter implements EditProfile.Presenter {

    private CompositeDisposable disposables;
    private EditProfile.View view;
    private LoadProfile loadProfile;
    private SaveProfile saveProfile;
    private GetAccessToken getAccessToken;

    private MapProfile mapProfile;
    private ChangeSecurityQuestion changeSecurityQuestion;

    //- variable cache
    private ProfileAspire profileCache;
    private UserProfileOKTA userProfileOKTA;

    EditProfilePresenter(Context c) {
        disposables = new CompositeDisposable();
        MapProfileApp mapLogic = new MapProfileApp();
        PreferencesStorageAspire preferencesStorage = new PreferencesStorageAspire(c);
        ProfileDataAspireRepository profileRepository = new ProfileDataAspireRepository(preferencesStorage,mapLogic);
        GetToken getToken = new GetToken(preferencesStorage, mapLogic);
        this.loadProfile = new LoadProfile(profileRepository);
        this.getAccessToken = new GetAccessToken(loadProfile, getToken);
        this.saveProfile = new SaveProfile(profileRepository);
        this.changeSecurityQuestion = new ChangeSecurityQuestion(
                new UserProfileOKTADataRepository(new RemoteUserProfileOKTADataStore()),
                new ChangeSecurityQuestionDataRepository(new RemoteChangeRecoveryQuestionDataStore()),
                mapLogic
        );
        this.mapProfile = new MapProfile();
    }

    @Override
    public void attach(EditProfile.View view) {
        this.view = view;
    }

    @Override
    public void detach() {
        disposables.dispose();
        view = null;
    }

    @Override
    public void getProfile() {
        view.showProgressDialog();
        disposables.add(
                loadProfile.loadRemote(getAccessToken)
                        .map(profileAspire -> {
                            this.profileCache = profileAspire;
                            return mapProfile.getProfile(profileAspire);
                        }).flatMap(profile ->
                        changeSecurityQuestion.getUserProfileOKTA(profile.getEmail())
                                .map(userProfileOKTA -> {
                                    this.userProfileOKTA = userProfileOKTA;
                                    mapProfile.getProfileRecoveryQuestion(profile, userProfileOKTA);
                                    return profile;
                                }))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new LoadProfileObserver())
        );
    }

    private class LoadProfileObserver extends DisposableSingleObserver<Profile> {

        @Override
        public void onSuccess(@NonNull Profile profile) {
            if (view == null)
                return;
            view.displayProfile(profile, true);
            view.dismissProgressDialog();
            dispose();
        }

        @Override
        public void onError(@NonNull Throwable e) {
            if (view == null)
                return;
            view.dismissProgressDialog();
            view.showErrorDialog(ErrCode.UNKNOWN_ERROR, e.getMessage());
            dispose();
        }
    }

    @Override
    public void getProfileLocal(){
        disposables.add(
                loadProfile.loadStorage()
                        .map(profileAspire -> {
                            this.profileCache = profileAspire;
                            return mapProfile.getProfile(profileAspire);
                        })
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new LoadProfileLocalObserver())
        );
    }

    private final class LoadProfileLocalObserver extends DisposableSingleObserver<Profile> {

        @Override
        public void onSuccess(Profile profile) {
            if (view == null)
                return;
            view.displayProfile(profile, false);
            dispose();
        }

        @Override
        public void onError(Throwable e) {
            if (view == null)
                return;
            view.showErrorDialog(ErrCode.UNKNOWN_ERROR, e.getMessage());
            dispose();
        }
    }

    @Override
    public void updateSecurityQuestion(String question, String answer) {
        if (!App.getInstance().hasNetworkConnection()) {
            view.showErrorDialog(ErrCode.CONNECTIVITY_PROBLEM, null);
            return;
        }

        view.showProgressDialog();
        disposables.add(
                changeSecurityQuestion.updateChangeQuestion(userProfileOKTA,
                        question, answer, profileCache.getSecretKeyDecrypt())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new SaveProfileObserver())
        );
    }

    @Override
    public void updateProfileAndSecurity(Profile profile, String question, String answer) {
        if (!App.getInstance().hasNetworkConnection()) {
            view.showErrorDialog(ErrCode.CONNECTIVITY_PROBLEM, null);
            return;
        }
        if (profileCache == null) {
            //not happen here
            return;
        }
        view.showProgressDialog();
        ProfileAspire profileAspire = mapProfile.updateProfile(profileCache, profile);
        disposables.add(
                handleUpdateProfile(profileAspire)
                        .andThen(changeSecurityQuestion.updateChangeQuestion(userProfileOKTA,
                                question, answer, profileCache.getSecretKeyDecrypt()))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new SaveProfileObserver())
        );
    }

    @Override
    public void updateProfile(Profile profile) {
        if(!App.getInstance().hasNetworkConnection()) {
            view.showErrorDialog(ErrCode.CONNECTIVITY_PROBLEM, null);
            return;
        }
        if(profileCache == null){
            //not happen here
            return;
        }
        view.showProgressDialog();
        ProfileAspire profileAspire = mapProfile.updateProfile(profileCache, profile);
        disposables.add(
                handleUpdateProfile(profileAspire)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new SaveProfileObserver())
        );
    }

    private Completable handleUpdateProfile(final ProfileAspire profile) {
        return getAccessToken.execute(profile)
                .flatMapCompletable(accessToken -> saveProfile.buildUseCaseCompletable(new SaveProfile.Params(profile, accessToken)));
    }

    private final class SaveProfileObserver extends DisposableCompletableObserver {

        @Override
        public void onComplete() {
            if (view == null) return;

            view.profileUpdated();
            view.dismissProgressDialog();
            dispose();
        }

        @Override
        public void onError(Throwable e) {
            if (view == null) return;

            view.dismissProgressDialog();
            view.showErrorDialog(ErrCode.UNKNOWN_ERROR, e.getMessage());
            dispose();
        }
    }

    @Override
    public void abort() {
        disposables.clear();
    }

}
