package com.ms.androidapp.datalayer.entity;

import com.google.gson.annotations.SerializedName;

/**
 * Created by vinh.trinh on 6/6/2017.
 */

public class Tiles {
    @SerializedName("ID")
    private Integer ID;
    @SerializedName("TileImage")
    private String image;
    @SerializedName("TileLink")
    private String link;
    @SerializedName("TileText")
    private String text;
    @SerializedName("Title")
    private String title;
    @SerializedName("ShortDescription")
    private String shortDescription;
    @SerializedName("GeographicRegion")
    private String geographicRegion;
    @SerializedName("SubCategory")
    private String subCategory;
    @SerializedName("Category")
    private String category;
    public Integer ID() {
        return ID;
    }

    public String image() {
        return image;
    }

    public String text() {
        return text;
    }

    public String title() {
        return title;
    }

    public String shortDescription() {
        return shortDescription;
    }

    public String geographicRegion() {
        return geographicRegion;
    }

    public String subCategory() {
        return subCategory;
    }

    public String category() {
        return category;
    }
}
