package com.ms.androidapp.common.logic;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Parcelable;

import com.ms.androidapp.App;
import com.ms.androidapp.R;
import com.ms.androidapp.domain.model.explore.ExploreRView;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by ThuNguyen on 6/21/2017.
 */

public class ShareHelper {
    public static final int SHARE_REQUEST_CODE = 1001;

    private static ShareHelper instance;
    private ShareHelper(){
    }

    public static ShareHelper getInstance() {
        if(instance == null){
            instance = new ShareHelper();
        }
        return instance;
    }

    private File outputFile;

    public void share(Activity currentActivity, ExploreRView data, Bitmap bitmap){
        if(bitmap != null){
            // Check permission before store image
            if(PermissionUtils.checkOrRequestExternalStoragePermission(currentActivity, PermissionUtils.EXTERNAL_STORAGE_REQUEST_CODE)){
                // Save bitmap
                if(outputFile != null && outputFile.exists()){
                    outputFile.delete();
                }
                outputFile = FileUtils.getOutputMediaFile();
                if(FileUtils.saveBitmap(outputFile, bitmap)){
                    shareTextAndBitmap(currentActivity, data, Uri.fromFile(outputFile));
                }

            }
        }else{
            shareOnlyText(currentActivity, data);
        }
    }
    private void shareOnlyText(Activity currentActivity, ExploreRView data){
        Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
//        shareIntent.putExtra(Intent.EXTRA_TITLE, data.getTitle());
//        shareIntent.putExtra(Intent.EXTRA_SUBJECT, data.getTitle());
        shareIntent.putExtra(Intent.EXTRA_TEXT, data.getTitle() + "\n\n" + App.getInstance().getString(R.string.mastercard_website));
        Intent shareChooserIntent = Intent.createChooser(shareIntent, "Share via");
        currentActivity.startActivityForResult(shareChooserIntent , SHARE_REQUEST_CODE);
    }
    private void shareTextAndBitmap(Activity currentActivity, ExploreRView data, Uri uri){
        /*Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
//        shareIntent.putExtra(Intent.EXTRA_TITLE, data.getTitle());
//        shareIntent.putExtra(Intent.EXTRA_SUBJECT, data.getTitle());
        shareIntent.putExtra(Intent.EXTRA_TEXT, data.getTitle() + "\n\n" + App.getInstance().getString(R.string.mastercard_website));
        shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

        Intent clipboardIntent = new Intent();
        clipboardIntent.setComponent(new ComponentName("com.google.android.apps.docs", "com.google.android.apps.docs.app.SendTextToClipboardActivity"));
        clipboardIntent.setAction(Intent.ACTION_SEND);
        clipboardIntent.setType("text/plain");
        clipboardIntent.putExtra(Intent.EXTRA_TEXT, App.getInstance().getString(R.string.mastercard_website));

        Intent shareChooserIntent = Intent.createChooser(shareIntent, "Share via");
        shareChooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[] { clipboardIntent });
        currentActivity.startActivityForResult(shareChooserIntent , SHARE_REQUEST_CODE);*/
        Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        shareIntent.putExtra(Intent.EXTRA_TEXT, data.getTitle() + "\n\n" + App.getInstance().getString(R.string.mastercard_website));
        shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

        Intent intent = createChooser(currentActivity.getPackageManager(), shareIntent, "Share via");
        currentActivity.startActivity(intent);
    }
    private static final String[] CLASSNAME_TO_FILTER = {
//            "com.android.bluetooth.opp.BluetoothOppLauncherActivity", //Bluetooth
            "com.google.android.apps.docs.app.SendTextToClipboardActivity", //Copy to clipboard
            "com.google.android.apps.docs.drive.clipboard.SendTextToClipboardActivity", //Copy to clipboard
//            "com.google.android.keep.activities.EditorActivity", //Google Keep
//            "com.google.android.apps.docs.shareitem.UploadSharedItemActivity", //Upload to Drive
//            "com.google.android.apps.translate.TranslateActivity"//Google Translate
    };

    public static Intent createChooser(PackageManager packageManager, Intent intent, String title) {
        final List<Intent> filtered = filterShareIntent(packageManager, intent);
        final Intent chooser = Intent.createChooser(filtered.remove(filtered.size()-1), title);
        return chooser.putExtra(Intent.EXTRA_INITIAL_INTENTS, filtered.toArray(new Parcelable[] {}));
    }

    private static List<Intent> filterShareIntent(PackageManager packageManager, Intent intent) {
        final List<Intent> intentList = new ArrayList<>();
        final List<ResolveInfo> possible = packageManager.queryIntentActivities(intent,
                PackageManager.MATCH_DEFAULT_ONLY);

        if (possible == null || possible.isEmpty()) {
            intentList.add(intent);
            return intentList;
        }

        for (ResolveInfo resolveInfo : possible) {
            intentList.add(createSameIntent(intent, resolveInfo));
        }

        return intentList;
    }

    private static Intent createSameIntent(Intent source, ResolveInfo resolveInfo) {
        final Intent intent = new Intent(source.getAction());
        intent.setType(source.getType());
        intent.putExtras(source.getExtras());
        intent.setPackage(resolveInfo.activityInfo.packageName);
        intent.setClassName(resolveInfo.activityInfo.packageName, resolveInfo.activityInfo.name);
        if(Arrays.asList(CLASSNAME_TO_FILTER).contains(resolveInfo.activityInfo.name)){
            intent.putExtra(Intent.EXTRA_TEXT, App.getInstance().getString(R.string.mastercard_website));
        }
        return intent;
    }
}
