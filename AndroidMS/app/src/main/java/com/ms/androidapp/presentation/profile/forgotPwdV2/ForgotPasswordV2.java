package com.ms.androidapp.presentation.profile.forgotPwdV2;

import com.api.aspire.common.constant.ErrCode;
import com.ms.androidapp.presentation.base.BasePresenter;

/**
 * Created by vinh.trinh on 7/24/2017.
 */

public interface ForgotPasswordV2 {
    interface View {
        void showProgressDialog();
        void dismissProgressDialog();
        void showErrorMessage(ErrCode errCode, String extraMsg);
        void showSuccessMessage();
    }

    interface Presenter extends BasePresenter<View> {
        void retrievePassword(String email, String recoveryQuestion, String answer, String password);
    }
}
