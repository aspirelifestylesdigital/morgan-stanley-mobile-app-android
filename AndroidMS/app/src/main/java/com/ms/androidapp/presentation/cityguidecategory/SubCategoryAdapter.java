package com.ms.androidapp.presentation.cityguidecategory;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.ms.androidapp.R;
import com.ms.androidapp.common.glide.GlideHelper;
import com.ms.androidapp.domain.model.SubCategoryItem;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by vinh.trinh on 5/10/2017.
 */

public class SubCategoryAdapter extends RecyclerView.Adapter<SubCategoryAdapter.SubCategoryViewHolder> {
    private final int expectedImageWidth;

    private OnCategoryItemClickListener listener;
    private Context context;
    private List<SubCategoryItem> subCategoryItems;

    public SubCategoryAdapter(Context context, List<SubCategoryItem> subCategoryItems) {
        this.context = context;
        this.subCategoryItems = subCategoryItems;
        expectedImageWidth =  (int)context.getResources().getDimension(R.dimen.recommend_item_image_height);
    }

    public void add(SubCategoryItem subCategoryItem) {
        this.subCategoryItems.add(subCategoryItem);
        notifyDataSetChanged();
    }

    public void swapData(List<SubCategoryItem> data) {
        this.subCategoryItems.clear();
        this.subCategoryItems.addAll(data);
        notifyDataSetChanged();
    }

    public SubCategoryItem getItem(int index) {
        return subCategoryItems.get(index);
    }

    @Override
    public SubCategoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new SubCategoryViewHolder(
                ((LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE))
                        .inflate(R.layout.sub_category_item, parent, false));
    }

    @Override
    public void onBindViewHolder(SubCategoryViewHolder holder, int position) {
        final SubCategoryItem subCategoryItem = subCategoryItems.get(position);
        holder.subCategoryName.setText(subCategoryItem.getSubCategoryName());
//        Picasso.with(context)
//                .load(subCategoryItem.getImageResources())
//                .placeholder(R.drawable.img_placeholder)
//                .fit()
//                .into(holder.image);
        GlideHelper.getInstance().loadImage(subCategoryItem.getImageResources(),
                R.drawable.img_placeholder, holder.image, expectedImageWidth);
    }

    @Override
    public int getItemCount() {
        return subCategoryItems.size();
    }

    void setListener(OnCategoryItemClickListener listener) {
        this.listener = listener;
    }

    class SubCategoryViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.image)
        AppCompatImageView image;
        @BindView(R.id.sub_category_name)
        AppCompatTextView subCategoryName;
        @BindView(R.id.parent_view)
        RelativeLayout parentView;

        SubCategoryViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(v -> {
                if(listener != null) listener.onItemClick(getAdapterPosition());
            });
        }
    }

    public interface OnCategoryItemClickListener {
        void onItemClick(int index);
    }
}
