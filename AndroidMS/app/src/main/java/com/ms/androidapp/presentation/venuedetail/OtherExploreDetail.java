package com.ms.androidapp.presentation.venuedetail;

import com.ms.androidapp.domain.model.explore.OtherExploreDetailItem;
import com.ms.androidapp.presentation.base.BasePresenter;

/**
 * Created by vinh.trinh on 6/13/2017.
 */

public interface OtherExploreDetail {

    interface View {
        void onGetContentFullFinished(OtherExploreDetailItem exploreRViewItem);
        void onUpdateFailed();
        void noInternetMessage();
    }

    interface Presenter extends BasePresenter<View> {
        void getContent(Integer contentId);
    }

}
