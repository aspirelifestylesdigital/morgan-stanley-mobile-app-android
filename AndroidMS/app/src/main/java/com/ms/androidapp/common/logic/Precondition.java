package com.ms.androidapp.common.logic;

/**
 * Created by tung.phan on 5/17/2017.
 * final class to verify object. Throws exception in case it's invalid.
 */

public final class Precondition {

    private Precondition(){

    }

    public static <T> T checkNotNull(T reference) {
        if (reference == null) {
            throw new NullPointerException();
        }
        return reference;
    }
}
