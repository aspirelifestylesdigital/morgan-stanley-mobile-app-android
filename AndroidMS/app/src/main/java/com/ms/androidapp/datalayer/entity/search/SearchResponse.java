package com.ms.androidapp.datalayer.entity.search;

import com.google.gson.annotations.SerializedName;

/**
 * Created by vinh.trinh on 6/22/2017.
 */

public class SearchResponse {

    @SerializedName("SearchIAAndCCAResult")
    private SearchResult searchResult;

    public SearchResult searchResult() {
        return searchResult;
    }
}
