package com.ms.androidapp.domain.mapper;

import android.util.SparseArray;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by vinh.trinh on 7/25/2017.
 */


public class CCACategoriesMapper {

    public static final List<Integer> DINING_ID_LIST = Arrays.asList(179, 259, 261, 263, 264, 1241, 1341);
    public static final List<Integer> FLOWERS_ID_LIST = Arrays.asList(15, 934);
    public static final List<Integer> ENTERTAINMENT_ID_LIST = Arrays.asList(28, 1244, 1245);
    public static final List<Integer> SPECIALTY_TRAVEL_ID_LIST = Arrays.asList(36, 259, 261, 263, 264, 1237, 1238, 1239, 1240, 1241, 1244, 1245);
    public static final List<Integer> GOLF_ID_LIST = Arrays.asList(16, 27, 31, 976, 1237, 1238, 1239, 1240);
    public static final List<Integer> VACATION_PACKAGE_ID_LIST = Arrays.asList(35, 1257, 1261, 1266, 1267, 1270, 1274, 1280, 1281, 1284,1285,
            1287, 1313, 1314, 1315, 1316, 1317, 1318, 1319, 1320, 1321);
    public static final List<Integer> CRUISE_ID_LIST = Arrays.asList(25, 1067, 1077, 1086, 1115, 1325, 1326, 1330, 1331, 1370, 1371, 1372, 1373, 1374, 1375);
    public static final List<Integer> TOUR_ID_LIST = Arrays.asList(17,931);
    public static final List<Integer> TRAVEL_ID_LIST = Arrays.asList(1035);
    public static final List<Integer> AIRPORT_SERVICE_ID_LIST = Arrays.asList(1037,1038);
    public static final List<Integer> TRAVEL_SERVICE_ID_LIST = Arrays.asList(13, 23, 1246, 1250);
    public static final List<Integer> TRANSPORTATION_ID_LIST = Arrays.asList(24, 314, 315, 316, 1027, 1251);
    public static final List<Integer> SHOPPING_ID_LIST = Arrays.asList(14, 15, 610, 934, 1247); //934, 15, 610, 1358, 1359, 1247, 14
    public static final List<Integer> RETAIL_SHOPPING_ID_LIST = Arrays.asList(610);
    private final SparseArray<String> categoryNameMapper;
    private final Map<String, List<Integer>> categoryIDMapper;
    private final List<Integer> ccaMultipleCities = new ArrayList<>();

    public CCACategoriesMapper() {
        categoryNameMapper = null;
        categoryIDMapper = null;
        initCCAMultipleCities();
    }

    public CCACategoriesMapper(WHICH wh) {
        if(wh == WHICH.NAME) {
            categoryNameMapper = new SparseArray<>();
            // Put shopping first
            putIdToMapCategoryName(categoryNameMapper, SHOPPING_ID_LIST, "Shopping");
            // Put flower
            putIdToMapCategoryName(categoryNameMapper, FLOWERS_ID_LIST, "Flowers");
            // Put entertainment
            putIdToMapCategoryName(categoryNameMapper, ENTERTAINMENT_ID_LIST, "Entertainment");
            // Put golf
            putIdToMapCategoryName(categoryNameMapper, GOLF_ID_LIST, "Golf");
            // Put vacation packages
            putIdToMapCategoryName(categoryNameMapper, VACATION_PACKAGE_ID_LIST, "Vacation Packages");
            // Put cruise
            putIdToMapCategoryName(categoryNameMapper, CRUISE_ID_LIST, "Cruise");
            // Put tours
            putIdToMapCategoryName(categoryNameMapper, TOUR_ID_LIST, "Tours");
            // Put travel
            putIdToMapCategoryName(categoryNameMapper, TRAVEL_ID_LIST, "Travel");
            // Put dining
            putIdToMapCategoryName(categoryNameMapper, DINING_ID_LIST, "Dining");
            // Put airport services
            putIdToMapCategoryName(categoryNameMapper, AIRPORT_SERVICE_ID_LIST, "Airport Services");
            // Put travel services
            putIdToMapCategoryName(categoryNameMapper, TRAVEL_SERVICE_ID_LIST, "Travel Services");
            // Put transportation
            putIdToMapCategoryName(categoryNameMapper, TRANSPORTATION_ID_LIST, "Transportation");

        } else {
            categoryNameMapper = null;
        }
        if(wh == WHICH.ID) {
            categoryIDMapper = new HashMap<>();
           /* categoryIDMapper.put("entertainment", Arrays.asList(28,1138,1139,1245,1244));
            categoryIDMapper.put("golf", Arrays.asList(31,16,976,27,1237,1238,1239,1240));
            categoryIDMapper.put("tours", Arrays.asList(931,17));
            categoryIDMapper.put("airport services", Arrays.asList(1037,1038,1243));
            categoryIDMapper.put("travel services", Arrays.asList(1246,1250,13,23));
            categoryIDMapper.put("shopping", Arrays.asList(27,934,15,1247,14,31,16,976));
            categoryIDMapper.put("dining", Arrays.asList(179,259,261,1241,263,264));
            categoryIDMapper.put("cruise", Arrays.asList(25,1067,1077,1086,1115,1325,1326,1328,1330,1331,1370,1371,1372,1373,1374,1375));*/

            //categoryIDMapper.put("entertainment", Arrays.asList(28,1137,1023,1138,1139,1244,1245));
            categoryIDMapper.put("entertainment", ENTERTAINMENT_ID_LIST);

            //categoryIDMapper.put("golf", Arrays.asList(16,27,31,976,1237,1238,1239,1240));
            categoryIDMapper.put("golf", GOLF_ID_LIST);

            //categoryIDMapper.put("tours", Arrays.asList(931,17));
            categoryIDMapper.put("tours", TOUR_ID_LIST);

            //categoryIDMapper.put("airport services", Arrays.asList(1037,1038,1243));
            categoryIDMapper.put("airport services", AIRPORT_SERVICE_ID_LIST);

            //categoryIDMapper.put("travel services", Arrays.asList(13,23,1025,1246,1248,1250));
            categoryIDMapper.put("travel services", TRAVEL_SERVICE_ID_LIST);

            //categoryIDMapper.put("shopping", Arrays.asList(14,16,15,27,31,610,934,976,1247));
            categoryIDMapper.put("shopping", SHOPPING_ID_LIST);

            //categoryIDMapper.put("dining", Arrays.asList(179,259,261,263,264,1241));
            categoryIDMapper.put("dining", DINING_ID_LIST);

            //categoryIDMapper.put("cruise", Arrays.asList(25,1067,1070,1071,1077,1081,1115,1116,1117,1325,1326,1327,1328,1329,1330,1331,1332,1333,1374,1086));
            categoryIDMapper.put("cruise", CRUISE_ID_LIST);


        } else {
            categoryIDMapper = null;
        }
        initCCAMultipleCities();
    }

    private void initCCAMultipleCities(){
        // Add all except Hotel, Cruise and Vacation packages, Retail shopping, Speciality Travel
        ccaMultipleCities.addAll(AIRPORT_SERVICE_ID_LIST);
        ccaMultipleCities.addAll(FLOWERS_ID_LIST);
        ccaMultipleCities.addAll(GOLF_ID_LIST);
        ccaMultipleCities.addAll(TRAVEL_ID_LIST);
        ccaMultipleCities.addAll(ENTERTAINMENT_ID_LIST);
        ccaMultipleCities.addAll(TRANSPORTATION_ID_LIST);
        ccaMultipleCities.addAll(TOUR_ID_LIST);
        ccaMultipleCities.addAll(TRAVEL_SERVICE_ID_LIST);
        ccaMultipleCities.addAll(SHOPPING_ID_LIST);

        // Remove the specialty travel and retail shopping
        ccaMultipleCities.removeAll(SPECIALTY_TRAVEL_ID_LIST);
        ccaMultipleCities.removeAll(RETAIL_SHOPPING_ID_LIST);

    }

    private void putIdToMapCategoryName(SparseArray sparseArray, List<Integer> idList, String name){
        for(int id : idList){
            sparseArray.put(id, name);
        }
    }

    public String categoryName(Integer id) {
        return categoryNameMapper.get(id);
    }

    public List<Integer> ids(String category) {
        return categoryIDMapper.get(category);
    }

    public boolean shouldFilter(String category) {
        return categoryIDMapper.keySet().contains(category);
    }

    public boolean multipleCities(int ID) {
        return ccaMultipleCities.contains(ID);
    }

    public enum WHICH {NAME, ID}
}
