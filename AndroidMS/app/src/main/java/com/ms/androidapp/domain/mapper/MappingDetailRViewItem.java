package com.ms.androidapp.domain.mapper;

import com.ms.androidapp.datalayer.entity.Answer;
import com.ms.androidapp.datalayer.entity.ContentFulls;
import com.ms.androidapp.datalayer.entity.QuestionAndAnswers;
import com.ms.androidapp.domain.model.explore.CityGuideDetailItem;
import com.ms.androidapp.domain.model.explore.DiningDetailItem;
import com.ms.androidapp.domain.model.explore.OtherExploreDetailItem;

/**
 * Created by tung.phan on 6/1/2017.
 */

public class MappingDetailRViewItem {

    public static DiningDetailItem transformDiningDetail(QuestionAndAnswers questionAndAnswers){
        Answer answer = questionAndAnswers.getAnswers().get(0);
        return new DiningDetailItem.Builder(
                answer.getName(),
                answer.getAddress(),
                answer.getCity(),
                answer.getZipCode(),
                answer.getPrice(),
                answer.getUserDefined1(),
                answer.getAnswerText(),
                answer.getHoursOfOperation(),
                answer.getImageURL())
                .address2(answer.getAddress2())
                .address3(answer.getAddress3())
                .benefits(answer.getOffer2())
                .state(answer.getState())
                .country(answer.getCountry())
                .url(answer.getURL())
                .coordination(38.8993277f, -77.0846062f)
                .build();
    }
    public static CityGuideDetailItem transformCityGuideDetail(QuestionAndAnswers questionAndAnswers){
        Answer answer = questionAndAnswers.getAnswers().get(0);
        return new CityGuideDetailItem.Builder(
                answer.getName(),
                answer.getAddress(),
                answer.getCity(),
                answer.getState(),
                answer.getZipCode(),
                answer.getAnswerText())
                .address2(answer.getAddress2())
                .address3(answer.getAddress3())
                .url(answer.getURL())
                .imageUrl(answer.getImageURL())
                .build();
    }

    public static OtherExploreDetailItem transformContentForDetail(ContentFulls contentFulls) {
        return new OtherExploreDetailItem(
                contentFulls.getID(),
                contentFulls.getTitle(),
                contentFulls.getDescription(),
                null,
                contentFulls.isBenefitInd() || contentFulls.isOfferInd(),
                contentFulls.getDescription(),0)
                .setBenefit(contentFulls.getOfferText())
                .setTermsOfUse(contentFulls.getTermsOfUse())
                .setCategory(contentFulls.getCategory())
                .setSubCategory(contentFulls.getSubCategory());
    }
}
