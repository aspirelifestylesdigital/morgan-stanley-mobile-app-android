package com.ms.androidapp.datalayer.entity.b2ccontent;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ms.androidapp.BuildConfig;

/**
 * Created by ThuNguyen on 6/5/2017.
 */

public class B2CContentFullRequest {

    @SerializedName("subDomain")
    @Expose
    public final String subDomain;
    @SerializedName("password")
    @Expose
    public final String secretKey;
    @SerializedName("contentID")
    @Expose
    public final Integer contentID;

    public B2CContentFullRequest(Integer contentID) {
        subDomain = BuildConfig.WS_B2C_SUBDOMAIN;
        secretKey = BuildConfig.WS_B2C_SECRET;
        this.contentID = contentID;
    }
}
