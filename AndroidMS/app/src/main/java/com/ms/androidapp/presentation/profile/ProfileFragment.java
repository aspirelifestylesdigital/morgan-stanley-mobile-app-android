package com.ms.androidapp.presentation.profile;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.SwitchCompat;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.api.aspire.common.constant.ErrCode;
import com.api.aspire.domain.model.SecurityQuestion;
import com.ms.androidapp.R;
import com.ms.androidapp.common.logic.MaskerHelper;
import com.ms.androidapp.common.logic.Validator;
import com.ms.androidapp.domain.model.Profile;
import com.ms.androidapp.presentation.base.BaseFragment;
import com.ms.androidapp.presentation.challengequestion.ListChallengeQuestion;
import com.ms.androidapp.presentation.challengequestion.ListChallengeQuestionPresenter;
import com.ms.androidapp.presentation.widget.CustomSpinner;
import com.ms.androidapp.presentation.widget.DropdownAdapter;
import com.ms.androidapp.presentation.widget.PasswordInputFilter;
import com.ms.androidapp.presentation.widget.ViewScrollViewListener;
import com.ms.androidapp.presentation.widget.ViewUtils;
import com.support.mylibrary.widget.ClickGuard;
import com.support.mylibrary.widget.ErrorIndicatorEditText;
import com.support.mylibrary.widget.LetterSpacingTextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by vinh.trinh on 4/27/2017.
 */

public class ProfileFragment extends BaseFragment implements ListChallengeQuestion.View {

    enum PROFILE {CREATE, EDIT}

    private static final String ARG_WHICH = "which";
    @BindView(R.id.profile_rootview)
    LinearLayout llProfileParentView;
    @BindView(R.id.scroll_view)
    ScrollView mainScrollView;
    @BindView(R.id.llParent)
    LinearLayout llParent;
    @BindView(R.id.fakeViewToMoveButtonDown)
    View fakeViewToMoveButtonDown;
    @BindView(R.id.tv_title)
    AppCompatTextView tvTitle;
    @BindView(R.id.edt_first_name)
    ErrorIndicatorEditText edtFirstName;
    @BindView(R.id.edt_last_name)
    ErrorIndicatorEditText edtLastName;
    @BindView(R.id.edt_email)
    ErrorIndicatorEditText edtEmail;
    @BindView(R.id.flEmailMask)
    View emailMask;
    @BindView(R.id.edt_phone)
    ErrorIndicatorEditText edtPhone;
    @BindView(R.id.edt_pwd)
    ErrorIndicatorEditText edtPassword;
    @BindView(R.id.edt_pwd_confirm)
    ErrorIndicatorEditText edtConfirmPassword;
    @BindView(R.id.switch_location)
    SwitchCompat locationSwitchCompat;
    @BindView(R.id.checkbox_acknowledge)
    AppCompatCheckBox chbAcknowledge;
    @BindView(R.id.btn_submit)
    AppCompatButton btnSubmit;
    @BindView(R.id.btn_cancel)
    AppCompatButton btnCancel;
    @BindView(R.id.content_wrapper)
    LinearLayout contentWrapper;
    @BindView(R.id.tvAcknow)
    LetterSpacingTextView tvAcknow;
    @BindView(R.id.tvSelectSalutation)
    LetterSpacingTextView tvSelectSalutation;
    @BindView(R.id.vSalutation)
    View viewSalutation;

    //-- security question
    @BindView(R.id.tvChallengeQuestion)
    TextView tvChallengeQuestion;
    @BindView(R.id.edt_answer_question)
    ErrorIndicatorEditText edtAnswerQuestion;

    private CustomSpinner mSpinnerChallengeQuestion;
    private ListChallengeQuestionPresenter listChallengeQuestionPresenter;
    private QuestionComparator questionComparator;

    private ProfileEventsListener listener;
    private Profile profile = new Profile();
    private ProfileComparator profileComparator;
    private Validator validator = new Validator();
    private FocusChangeListener firstNameMasker = new FocusChangeListener(MaskerHelper.INPUT.NAME);
    private FocusChangeListener lastNameMasker = new FocusChangeListener(MaskerHelper.INPUT.NAME);
    private FocusChangeListener emailMasker = new FocusChangeListener(MaskerHelper.INPUT.EMAIL);
    private FocusChangeListener phoneMasker = new FocusChangeListener(MaskerHelper.INPUT.NAME);

    private TextWatcher phonePattern;
    private AppCompatEditText editText;
    private String errMessage;
    private int hasFocus;
    PROFILE which;
    private boolean isKeyboardShow;
    private CustomSpinner mSpinnerSalutation;
    private boolean isEdtMode;
    private boolean profileFromUpdate = true;
    private boolean isShowLocationAlert = true;
    private boolean isAnswerMasker = true ;

    private boolean isCheckHasAnswer = true;
    String cacheStringQuestion = "";
    private AnswerFocusChangeListener answerFocusChangeListener;

    public static ProfileFragment newInstance(PROFILE profile) {
        Bundle args = new Bundle();
        args.putString(ARG_WHICH, profile.name());
        ProfileFragment fragment = new ProfileFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        isShowLocationAlert = true;
        if (context instanceof ProfileEventsListener) {
            listener = (ProfileEventsListener) context;
        } else {
            throw new ClassCastException(context.toString() + " must implement ProfileEventsListener.");
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        storeUp();
        outState.putParcelable("input", profile);
        if (profileComparator != null)
            outState.putParcelable("original", profileComparator.original);
        super.onSaveInstanceState(outState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_profile, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        listener.onFragmentCreated();
        which = PROFILE.valueOf(getArguments().getString(ARG_WHICH));
        if (which == PROFILE.EDIT) {
            isEdtMode = true;
            edtEmail.setTextColor(getResources().getColor(R.color.heading_gray));
            edtEmail.setInputType(InputType.TYPE_NULL);
            edtEmail.setCursorVisible(false);
            emailMask.setVisibility(View.VISIBLE);
            edtEmail.setEnabled(false);
            edtPassword.setVisibility(View.GONE);
            edtConfirmPassword.setVisibility(View.GONE);
            view.findViewById(R.id.checkbox_container).setVisibility(View.GONE);
            tvTitle.setVisibility(View.GONE);
            btnSubmit.setText(getString(R.string.update));
            btnSubmit.setEnabled(false);
            btnSubmit.setClickable(false);
            btnCancel.setEnabled(false);
            btnCancel.setClickable(false);
        } else {
            btnSubmit.setEnabled(false);
            btnSubmit.post(new Runnable() {
                @Override
                public void run() {
                    btnSubmit.setClickable(false);
                }
            });
            edtPhone.setImeOptions(EditorInfo.IME_ACTION_NEXT);
            tvSelectSalutation.setVisibility(View.VISIBLE);
            InputFilter[] filtersPassword = new InputFilter[]{new PasswordInputFilter(), new InputFilter.LengthFilter(getResources().getInteger(R.integer.password_max_length_characters))};
            edtPassword.setFilters(filtersPassword);
            edtConfirmPassword.setFilters(filtersPassword);
//            setupTermOfUseClickable();
        }
        setAnswerMaskOnView();
        mSpinnerSalutation = setUpCustomSpinner(tvSelectSalutation, getSalutation());
        mSpinnerSalutation.setNone(false);
        edtFirstName.setOnFocusChangeListener(firstNameMasker);
        edtLastName.setOnFocusChangeListener(lastNameMasker);
        if (which == PROFILE.CREATE) {
            edtEmail.setOnFocusChangeListener(emailMasker);
        }
        edtPhone.setOnFocusChangeListener(phoneMasker);
        initTextInteractListener();
//        phonePattern = new TextPattern(edtPhone,
//                "#-###-###-####",
//                2);
        phonePattern = new TextWatcher() {
            String strBefore = "";
            int selection;

            @Override
            public void beforeTextChanged(CharSequence s, int index, int before, int count) {
                strBefore = s.toString();
                selection = edtPhone.getSelectionEnd();
            }

            @Override
            public void onTextChanged(CharSequence sequence, int index, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(edtPhone == null) return;
                edtPhone.removeTextChangedListener(phonePattern);
                String text = editable.toString().trim();
                // remove + when input after number
                if (text.lastIndexOf("+") > 0) {
                    edtPhone.setText(strBefore);
                    edtPhone.setSelection(selection);
                }

                if(text.length() > 0 && !text.contains("+")){
                    text = "+" + text;
                    edtPhone.setText(text);
                    edtPhone.setSelection(text.length());
                }else if(text.length() == 1 && text.equalsIgnoreCase("+")){
                    //- for back text empty
                    text = "";
                    edtPhone.setText(text);
                    edtPhone.setSelection(text.length());
                }

                edtPhone.addTextChangedListener(phonePattern);
            }
        };
        edtPhone.addTextChangedListener(phonePattern);
        edtPhone.setOnFocusChangeListener(phoneMasker);
        chbAcknowledge.setOnCheckedChangeListener((buttonView, isChecked) -> {
            btnSubmit.setEnabled(isChecked);
            btnSubmit.setClickable(isChecked);
        });
        locationSwitchCompat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean on = !locationSwitchCompat.isChecked();
                if (which == PROFILE.EDIT && profileComparator != null)
                    profileComparator.onChanged();
                if (on) listener.onLocationSwitchOn();
            }
        });

        tvAcknow.setOnClickListener(view1 -> chbAcknowledge.setChecked(!chbAcknowledge.isChecked()));
        if (savedInstanceState != null) {
            loadProfile(savedInstanceState.getParcelable("original"), false);
        }
        ClickGuard.guard(btnSubmit, btnCancel);
        llParent.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean focus) {
                if (focus) {
                    hideSoftKey();
                }
            }
        });
        new ViewScrollViewListener(mainScrollView, this::handleClickOutsideEdt);
        //- security question
        listChallengeQuestionPresenter = new ListChallengeQuestionPresenter();
        listChallengeQuestionPresenter.attach(this);
        listChallengeQuestionPresenter.getChallengeQuestions();
        answerFocusChangeListener = new AnswerFocusChangeListener();
        edtAnswerQuestion.setOnFocusChangeListener(answerFocusChangeListener);
//        questionComparator = new QuestionComparator(edtAnswerQuestion, btnSubmit, btnCancel);

    }

    private List<String> getSalutation() {
        return Arrays.asList(getResources().getStringArray(R.array.cate_salutations));
    }

    private CustomSpinner setUpCustomSpinner(TextView anchor,
                                             List<String> data) {
        CustomSpinner customSpinner = new CustomSpinner(getContext(), anchor);

        DropdownAdapter dropdownAdapter = new DropdownAdapter(getContext(),
                R.layout.item_dropdown,
                data, 42);
        customSpinner.setDropdownAdapter(dropdownAdapter);
        customSpinner.setConfigHeight(data.size());
//        int maxHeight = data.size() * 42;
//        if (maxHeight < 280) {
//            customSpinner.setPobpupHeight(maxHeight);
//        } else {
//            customSpinner.setPobpupHeight(280);
//        }
        return customSpinner;
    }

    private void initTextInteractListener() {
        llProfileParentView.getViewTreeObserver().addOnGlobalLayoutListener(() -> {
            Rect r = new Rect();
            llProfileParentView.getWindowVisibleDisplayFrame(r);
            int screenHeight = llProfileParentView.getRootView().getHeight();

            int keypadHeight = screenHeight - r.bottom;

            if (keypadHeight > screenHeight * 0.15) {
                if (!isKeyboardShow) {
                    isKeyboardShow = true;
                    // keyboard is opened
                    mainScrollView.smoothScrollTo(0, (int) contentWrapper.getTop());
                    scaleView();
                    processAfterKeyboardToggle(true);
                }
            } else {
                // keyboard is closed
                if (isKeyboardShow) {
                    isKeyboardShow = false;
                    if(fakeViewToMoveButtonDown != null){
                        fakeViewToMoveButtonDown.setVisibility(View.VISIBLE);
                    }
                    processAfterKeyboardToggle(false);
                }
            }
        });
    }

    private void scaleView() {
        Animation anim = new ScaleAnimation(
                1f, 1f, // Start and end values for the X axis scaling
                fakeViewToMoveButtonDown.getY(),
                fakeViewToMoveButtonDown.getY() + fakeViewToMoveButtonDown.getHeight(), // Start and end values for the Y axis scaling
                Animation.RELATIVE_TO_SELF, 0f, // Pivot point of X scaling
                Animation.RELATIVE_TO_SELF, 1f); // Pivot point of Y scaling
        anim.setFillAfter(false); // Needed to keep the result of the animation
        anim.setDuration(0);
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                fakeViewToMoveButtonDown.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
        fakeViewToMoveButtonDown.startAnimation(anim);
    }

    @OnClick(R.id.btn_submit)
    void onSubmit(View view) {
        profileFromUpdate = true;
        hideSoftKey();
        storeUp();
        hasFocus = checkHasFocus();
        resetErrorView();
        if ((profileComparator.isQuestion()
                || profileComparator.isAnswerChanged() || getAnswerInputSpaceOnView())
                && profileComparator.isProfileChanged()) {

            if (formValidationAll()) {
                String question = getQuestionContentOnView();
                handleProfilePreSubmit();
                listener.onSubmitProfileAndSecurity(profile, question, getAnswerOnView());
            }
        } else if (profileComparator.isQuestion()
                || profileComparator.isAnswerChanged() || getAnswerInputSpaceOnView()) {
            if (formValidationSecurity()) {
                String question = getQuestionContentOnView();
                handleProfilePreSubmit();
                listener.onSubmitSecurityQuestion(question, getAnswerOnView());
            }
        } else {
            if (formValidation()) {
                handleProfilePreSubmit();
                listener.onSubmitProfile(profile);
            }
        }

    }

    private void handleProfilePreSubmit() {
        // Replace the "+" sign of phone number
        profile.setPhone(profile.getPhone().replace("+", ""));
        String foundCountryCode = validator.findCountryCodeFromScratch(profile.getPhone());
        profile.setCountryCode(foundCountryCode);
        profile.locationToggle(!locationSwitchCompat.isChecked());
    }

    private String getAnswerOnView() {
        return isAnswerMasker ? answerFocusChangeListener.original : edtAnswerQuestion.getText().toString().trim();
    }

    private boolean getAnswerInputSpaceOnView() {
        String answer = isAnswerMasker ? answerFocusChangeListener.original : edtAnswerQuestion.getText().toString();
        boolean result = false;
        if (!TextUtils.isEmpty(answer)
                && answer.replace(" ","").length() == 0) {
            result = true;
        }
        return result;
    }

    @OnClick(R.id.btn_cancel)
    void onCancel(View view) {
        profileFromUpdate = false;

        setAnswerMaskOnView();
        if (answerFocusChangeListener != null) {
            answerFocusChangeListener.resetAnswerField();
        }

        hideSoftKey();
        hasFocus = checkHasFocus();
        resetErrorView();
        listener.onProfileCancel();
    }
    private void handleClickOutsideEdt(View view){
        ViewUtils.hideSoftKey(view);
        if (view.getId() == R.id.flEmailMask) {
            edtEmail.setText(emailMasker.original);
        } else {
            if (edtEmail != null && emailMasker != null &&
                    !TextUtils.isEmpty(edtEmail.getText().toString()) &&
                    edtEmail.getText().toString().equalsIgnoreCase(emailMasker.original)) {

                edtEmail.setText(MaskerHelper.mask(MaskerHelper.INPUT.EMAIL, emailMasker.original));
            }//do nothing
        }
    }

    @OnClick({R.id.profile_rootview, R.id.content_wrapper, R.id.flEmailMask, R.id.llParent})
    public void onClick(View view) {
        hideSoftKey();
        if (view.getId() == R.id.flEmailMask) {
            edtEmail.setText(emailMasker.original);
        }
    }

    private void hideSoftKey() {
        ViewUtils.hideSoftKey(getView());
    }

    private void processAfterKeyboardToggle(boolean isKeyboardShow) {
        View currentView = getActivity().getCurrentFocus();
        if (currentView != null && currentView instanceof EditText) {
            if (isKeyboardShow) {
                ((EditText) currentView).setCursorVisible(true);
            } else {
                currentView.clearFocus();
                if(llParent != null){
                    llParent.requestFocus();
                }
            }
        }
    }

    private boolean formValidationAll() {
        checkoutAll();
        if (editText != null) {
//            editText.requestFocus();
//            editText.setError("");
//            editText.setSelection(editText.getText().length());
            viewFocusError();
            listener.onProfileInputError(errMessage);
            return false;
        }else if(isNonSelectSalutation()){
            errMessage = getString(R.string.input_err_salutation);
            listener.onProfileInputError(errMessage);
            return false;
        }
        return true;
    }

    private void checkoutAll() {
        editText = null;
        StringBuilder messageBuilder = buildError();

        String answer = getAnswerOnView();

        if (!validator.answerSecurity(answer)) {
            if (editText == null) editText = edtAnswerQuestion;
            edtAnswerQuestion.setError("");
            //-- check last text non have new line
            if (messageBuilder.length() > 0
                    && !messageBuilder.substring(messageBuilder.toString().length() - 1).equals("\n")) {
                messageBuilder.append("\n");
            }
            messageBuilder.append(getString(R.string.invalid_answer));
        } else {
            edtAnswerQuestion.setError(null);
        }

        errMessage = messageBuilder.toString();
    }

    private boolean formValidationSecurity() {
        checkoutSecurity();
        if (editText != null) {
            viewFocusError();
            listener.onProfileInputError(errMessage);
            return false;
        }
        return true;
    }

    private void checkoutSecurity() {
        editText = null;
        StringBuilder messageBuilder = new StringBuilder();

        String valAnswer = getAnswerOnView();

        if (!validator.answerSecurity(valAnswer)) {
            editText = edtAnswerQuestion;
            edtAnswerQuestion.setError("");
            messageBuilder.append(getString(R.string.invalid_answer));
        } else {
            edtAnswerQuestion.setError(null);
        }

        errMessage = messageBuilder.toString();
    }

    private boolean formValidation() {
        checkout();
        if (editText != null) {
//            editText.requestFocus();
//            editText.setError("");
//            editText.setSelection(editText.getText().length());
            viewFocusError();
            listener.onProfileInputError(errMessage);
            return false;
        } else if (isNonSelectSalutation()) {
            errMessage = getString(R.string.input_err_salutation);
            listener.onProfileInputError(errMessage);
            return false;
        }
        return true;
    }

    private boolean isNonSelectSalutation() {
        return tvSelectSalutation.getText().toString().equals(getString(R.string.my_profile_text_salutation_place_holder));
    }

    private void checkout() {
        editText = null;
        StringBuilder messageBuilder = buildError();
        errMessage = messageBuilder.toString();
    }

    private StringBuilder buildError() {
        StringBuilder messageBuilder = new StringBuilder();
        if (isNonSelectSalutation()) {
            viewSalutation.setBackgroundColor(Color.parseColor("#ffff4444"));
            if (messageBuilder.length() > 0) {
                messageBuilder.append("\n");
            }
            messageBuilder.append(getString(R.string.input_err_salutation));
        } else {
            viewSalutation.setBackgroundColor(Color.parseColor("#99A1A8"));
        }

        if (!validator.name(profile.getFirstName())) {
            editText = edtFirstName;
            edtFirstName.setError("");
            if (profile.getFirstName().length() > 0) {
                if (messageBuilder.length() > 0) {
                    messageBuilder.append("\n");
                }
                messageBuilder.append(getString(R.string.input_err_invalid_name,
                        "first"));
            }

        } else if (!validator.specialChars(profile.getFirstName())) {
            editText = edtFirstName;
            edtFirstName.setError("");
            if (profile.getFirstName().length() > 0) {
                if (messageBuilder.length() > 0) {
                    messageBuilder.append("\n");
                }
                messageBuilder.append(getString(R.string.input_err_special_char_first_name));
            }
        } else {
            edtFirstName.setError(null);
        }

        if (!validator.name(profile.getLastName())) {
            if (editText == null) editText = edtLastName;
            edtLastName.setError("");
            if (profile.getLastName().length() > 0) {
                if (messageBuilder.length() > 0) {
                    messageBuilder.append("\n");
                }
                messageBuilder.append(getString(R.string.input_err_invalid_name,
                        "last"));
            }

        } else if (!validator.specialChars(profile.getLastName())) {
            if (editText == null) editText = edtLastName;
            edtLastName.setError("");
            if (profile.getLastName().length() > 0) {
                if (messageBuilder.length() > 0) {
                    messageBuilder.append("\n");
                }
                messageBuilder.append(getString(R.string.input_err_special_char_last_name));
            }
        } else {
            edtLastName.setError(null);
        }
        if (!validator.email(profile.getEmail())) {
            if (editText == null) editText = edtEmail;
            edtEmail.setError("");
            if (profile.getEmail().length() > 0) {
                if (messageBuilder.length() > 0) {
                    messageBuilder.append("\n");
                }
                messageBuilder.append(getString(R.string.input_err_invalid_email));
            }
        } else {
            edtEmail.setError(null);
        }
        //-- phone
        String phoneOnView;
        if (edtPhone.hasFocus()) {
            phoneOnView = edtPhone.getText().toString().trim();
        } else {
            phoneOnView = phoneMasker.original.trim();
        }
        if (!validator.phoneFormatValidator(phoneOnView)) {
            if (editText == null) editText = edtPhone;
            edtPhone.setError("");
            if (profile.getPhone()
                    .length() > 0) {
                if (messageBuilder.length() > 0) {
                    messageBuilder.append("\n");
                }
                messageBuilder.append(getString(R.string.input_err_invalid_phone));
            }
        } else {
            String phoneWithOnlyDigit = profile.getPhone().replace("+", "");
            String foundCountryCode = validator.findCountryCodeFromScratch(phoneWithOnlyDigit);
            if (TextUtils.isEmpty(foundCountryCode)) {
                if (editText == null) editText = edtPhone;
                edtPhone.setError("");
                if (profile.getPhone()
                        .length() > 0) {
                    if (messageBuilder.length() > 0) {
                        messageBuilder.append("\n");
                    }
                    messageBuilder.append(getString(R.string.input_err_invalid_country_code));
                }
            } else {
                if (phoneWithOnlyDigit.equalsIgnoreCase(foundCountryCode)) { // Full phone with only country code
                    if (editText == null) editText = edtPhone;
                    edtPhone.setError("");
                    if (profile.getPhone()
                            .length() > 0) {
                        if (messageBuilder.length() > 0) {
                            messageBuilder.append("\n");
                        }
                        messageBuilder.append(getString(R.string.input_err_invalid_phone));
                    }
                } else {
                    edtPhone.setError(null);
                }
            }
        }

//        String password = edtPassword.getText().toString();
//        String reTypePwd = edtConfirmPassword.getText().toString();
//        if (which == PROFILE.CREATE) {
//            if (!validator.secretValidator(password)) {
//                if (editText == null) {
//                    editText = edtPassword;
//                }
//                edtPassword.setError("");
//                if (password.length() > 0) {
//                    if (messageBuilder.length() > 0) {
//                        messageBuilder.append("\n");
//                    }
//                    messageBuilder.append(getString(R.string.input_err_invalid_password));
//                }
//            } else {
//                edtPassword.setError(null);
//            }
//
//            if (!validator.secretValidator(reTypePwd)) {
//                if (editText == null) {
//                    editText = edtConfirmPassword;
//                }
//                edtConfirmPassword.setError("");
//            }
//            boolean emptyPassword = reTypePwd.length() == 0;
//            if (!emptyPassword) {
//                if (!validator.password(password,
//                        reTypePwd)) {
//                    if (editText == null) {
//                        editText = edtConfirmPassword;
//                    }
//                    edtConfirmPassword.setError("");
//
//                    if (messageBuilder.length() > 0) {
//                        messageBuilder.append("\n");
//
//                    }
//
//                    messageBuilder.append(getString(R.string.input_err_pwd_confirmation_failed));
//                } else {
//                    edtConfirmPassword.setError(null);
//                }
//            }
//
//            if (editText == null && password.length() == 0) {
//                editText = edtPassword;
//            }
//        }
        /*if (editText != null) {
            if (getActivity().getCurrentFocus() != null && getActivity().getCurrentFocus() instanceof ErrorIndicatorEditText) {
                if (((ErrorIndicatorEditText) getActivity().getCurrentFocus()).isError()) {
                    editText = (ErrorIndicatorEditText) getActivity().getCurrentFocus();
                }
            }
        }*/
        if (editText != null) {
            String temp = messageBuilder.toString();
            messageBuilder = new StringBuilder("");
            if (profile.anyIsEmpty()) {
                messageBuilder.append(getString(R.string.input_err_required));
            }

            if (messageBuilder.length() > 0 && temp.length() > 0) {
                messageBuilder.append("\n");
            }
            messageBuilder.append(temp);

        }
        return messageBuilder;
    }

    private class FocusChangeListener
            implements View.OnFocusChangeListener {

        MaskerHelper.INPUT input;
        private String original = "";

        FocusChangeListener(MaskerHelper.INPUT type) {
            input = type;
        }

        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            AppCompatEditText view = (AppCompatEditText) v;
            /*if (input == MaskerHelper.INPUT.PHONE) {
                if (hasFocus) {
                    if (original.length() == 0) {
                        //view.removeTextChangedListener(phonePattern);
                        view.setText(original);
                        view.setSelection(2);
                        //view.addTextChangedListener(phonePattern);
                        return;
                    }
                } else {
                    String text = view.getText().toString();
                    if (text.length() == 2) {
                        //view.removeTextChangedListener(phonePattern);
                        view.setText(original);
                        //view.addTextChangedListener(phonePattern);
                        return;
                    }
                }
            }*/
            if (hasFocus) {
                view.setText(original);
                if (isEdtMode && view != edtEmail) {
                    edtEmail.setText(MaskerHelper.mask(MaskerHelper.INPUT.EMAIL, emailMasker.original));
                }

            } else {
                original = view.getText().toString().trim();
                if(view.getId() == edtPhone.getId()){
                    edtPhone.removeTextChangedListener(phonePattern);
                    view.setText(MaskerHelper.mask(input, original));
                    edtPhone.addTextChangedListener(phonePattern);
                }else{
                    view.setText(MaskerHelper.mask(input, original));
                }
            }
            view.setCursorVisible(hasFocus);

        }
    }

    private void storeUp() {
        firstNameMasker.original = firstNameMasker.original.trim();
        lastNameMasker.original = lastNameMasker.original.trim();
        emailMasker.original = emailMasker.original.trim();
        phoneMasker.original = phoneMasker.original.trim();
        String firstName = firstNameMasker.original;
        String lastName = lastNameMasker.original;
        String email = emailMasker.original;
        String phone = phoneMasker.original;
        profile.setFirstName(firstName);
        profile.setLastName(lastName);
        profile.setPhone(phone);
        profile.setEmail(email);
        profile.setSalutation(mSpinnerSalutation.getTextSelectedSafe());
    }

    private int checkHasFocus() {
        if (edtFirstName.hasFocus()) {
            profile.setFirstName(edtFirstName.getText().toString().trim());
            return 1;
        }
        if (edtLastName.hasFocus()) {
            profile.setLastName(edtLastName.getText().toString().trim());
            return 2;
        }
        if (edtEmail.hasFocus()) {
            if (which == PROFILE.CREATE) {
                profile.setEmail(edtEmail.getText().toString().trim());
            }
            return 3;
        }
        if (edtPhone.hasFocus()) {
            profile.setPhone(edtPhone.getText().toString().trim());
            return 4;
        }
        // zip code
        return 5;
    }

    void reloadProfileAfterUpdated() {
        if (profile != null) {
            String question = getQuestionContentOnView();
            profile.setQuestion(question);
        }
        loadProfile(profile, false);
        //set 7* when update profile success
        setAnswerMaskOnView();
    }

    private String getQuestionContentOnView() {
        if (listChallengeQuestionPresenter == null
                || mSpinnerChallengeQuestion == null) {
            return "";
        }

        return listChallengeQuestionPresenter.getQuestionContent(
                mSpinnerChallengeQuestion.getSelectionPosition());
    }

    private void setAnswerMaskOnView() {
        edtAnswerQuestion.setText(MaskerHelper.mask(MaskerHelper.INPUT.ANSWER,""));
    }

    void loadProfile(Profile profile, boolean fromRemote) {
        if (profile == null) {
            return;
        }

        //<editor-fold desc="Security ">
        handleQuestionOnView(profile.getQuestion());
        //</editor-fold>

        // Always add "+" at the first place in phone number
        final String phoneOnView = profile.getDisplayCountryCodeAndPhone();
//        profile.addPlusSignAtTheFirstIfAny();

        firstNameMasker.original = profile.getFirstName();
        lastNameMasker.original = profile.getLastName();
        phoneMasker.original = phoneOnView;
        emailMasker.original = profile.getEmail();
        edtFirstName.setText(profile.getFirstName());
        edtLastName.setText(profile.getLastName());
        edtEmail.setText(profile.getEmail());
        edtPhone.setText(phoneOnView);
        if (profileFromUpdate) {
            edtFirstName.getOnFocusChangeListener().onFocusChange(edtFirstName, false);
            edtLastName.getOnFocusChangeListener().onFocusChange(edtLastName, false);
            //edtEmail.getOnFocusChangeListener().onFocusChange(edtEmail, false);
            edtPhone.getOnFocusChangeListener().onFocusChange(edtPhone, false);
        } else {
            edtFirstName.getOnFocusChangeListener().onFocusChange(edtFirstName, hasFocus == 1);
            edtLastName.getOnFocusChangeListener().onFocusChange(edtLastName, hasFocus == 2);
            //edtEmail.getOnFocusChangeListener().onFocusChange(edtEmail, hasFocus == 3);
            edtPhone.getOnFocusChangeListener().onFocusChange(edtPhone, hasFocus == 4);
        }
        edtEmail.setText(MaskerHelper.mask(MaskerHelper.INPUT.EMAIL, emailMasker.original));

        if (profile.getSalutation() != null && !profile.getSalutation().equals("")) {
            if (getSalutation().contains(profile.getSalutation())) {
                mSpinnerSalutation.setSelectionPosition(getSalutation().indexOf(profile.getSalutation()));
            }
        }
        profileComparator = new ProfileComparator(
                edtFirstName,
                edtLastName,
                edtEmail,
                edtPhone,
                btnSubmit,
                btnCancel,
                locationSwitchCompat,
                mSpinnerSalutation,
                edtAnswerQuestion);
        profileComparator.original = profile;
        boolean on = profile.isLocationOn();
        locationSwitchCompat.setChecked(!on);
        if (fromRemote) {
            if (on && isShowLocationAlert) {
                listener.onLocationSwitchOn();
            }
            isShowLocationAlert = false;
        }
        resetErrorView();

        btnSubmit.setEnabled(false);
        btnSubmit.setClickable(false);
        btnCancel.setEnabled(false);
        btnCancel.setClickable(false);
    }

    private void handleQuestionOnView(String question) {
        //- load remote get question
        if(TextUtils.isEmpty(question)
                && !TextUtils.isEmpty(answerFocusChangeListener.getCacheQuestionProfile())){
            question = answerFocusChangeListener.getCacheQuestionProfile();
        }
        answerFocusChangeListener.original = "";
        if (!TextUtils.isEmpty(question)
                && mSpinnerChallengeQuestion != null) {
            int indexOfQuestion = mSpinnerChallengeQuestion.getTextPosition(question);
            mSpinnerChallengeQuestion.setSelectionPosition(indexOfQuestion);
            answerFocusChangeListener.setCacheQuestionProfile(question);
        }
    }

    void viewFocusError() {
        if (editText != null) {
            if(editText.getId() == edtAnswerQuestion.getId()){
                answerFocusChangeListener.setResetField(false);
                editText.requestFocus();
                answerFocusChangeListener.setResetField(true);
            }else{
                editText.requestFocus();
            }
            editText.setError("");
            editText.setCursorVisible(true);
            editText.setSelection(editText.getText().toString().length());
        }

    }

    void invokeSoftKey() {
        viewFocusError();
        if (editText != null) {
            ViewUtils.showSoftKey(editText);
        }
    }
    void switchOff() {
        locationSwitchCompat.setChecked(true);
        profile.locationToggle(false);
        if (which == PROFILE.EDIT) {
            profileComparator.onChanged();
        }
    }

    public interface ProfileEventsListener {
        void onProfileInputError(String message);

        void onProfileCancel();

        void onFragmentCreated();

        void onLocationSwitchOn();

        void onSubmitProfile(Profile profile);

        void onSubmitProfileAndSecurity(Profile profile, String question, String answer);

        void onSubmitSecurityQuestion(String question, String answer);
    }

    private void resetErrorView() {
        edtFirstName.setError(null);
        edtLastName.setError(null);
        edtEmail.setError(null);
        edtPhone.setError(null);
        edtPassword.setError(null);
        edtAnswerQuestion.hideErrorColorLine();
    }


//    private void setupTermOfUseClickable() {
//
//        String s = getString(R.string.acknowledge);
//        String termAndCondition = getString(R.string.term_of_use);
//        int termStart = s.indexOf(termAndCondition);
//        int termEnd = termStart + termAndCondition.length();
//        String privacyPolicy = getString(R.string.privacy);
//        int privacyStart = s.indexOf(privacyPolicy);
//        int privacyEnd = privacyStart + privacyPolicy.length();
//
//
//        SpannableString ss = new SpannableString(s);
//        ClickableSpan span1 = new ClickableSpan() {
//            @Override
//            public void onClick(View textView) {
//                openWebView(AppConstant.MASTERCARD_COPY_UTILITY.TermsOfUse);
//            }
//
//            @Override
//            public void updateDrawState(final TextPaint textPaint) {
//                textPaint.setColor(getResources().getColor(R.color.white));
//                textPaint.setUnderlineText(true);
//            }
//        };
//
//        ClickableSpan span2 = new ClickableSpan() {
//            @Override
//            public void onClick(View textView) {
//                openWebView(AppConstant.MASTERCARD_COPY_UTILITY.Privacy);
//            }
//
//            @Override
//            public void updateDrawState(final TextPaint textPaint) {
//                textPaint.setColor(getResources().getColor(R.color.white));
//                textPaint.setUnderlineText(true);
//            }
//        };
//
//        ss.setSpan(span1, termStart, termEnd, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
//        ss.setSpan(span2, privacyStart, privacyEnd, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
//        tvAcknow.setText(ss);
//        tvAcknow.setMovementMethod(LinkMovementMethod.getInstance());
//    }
//
//    void openWebView(AppConstant.MASTERCARD_COPY_UTILITY utilityType) {
//        Intent intent = new Intent(this.getActivity(), MasterCardUtilityActivity.class);
//        intent.putExtra(IntentConstant.MASTERCARD_COPY_UTILITY, utilityType);
//        this.getActivity().startActivity(intent);
//    }

    @Override
    public void onDestroyView() {
        listChallengeQuestionPresenter.detach();
        super.onDestroyView();
    }

    //<editor-fold desc="Security Question">
    @Override
    public void showProgressDialog() {

    }

    @Override
    public void dismissProgressDialog() {

    }

    @Override
    public void showErrorMessage(ErrCode errCode, String extraMsg) {

    }

    @Override
    public void showSuccessMessage() {

    }

    @Override
    public void getChallengeQuestions(List<SecurityQuestion> questions) {
        mSpinnerChallengeQuestion = setUpCustomSpinnerQuestion(tvChallengeQuestion, questions);
        mSpinnerChallengeQuestion.setNone(false);
    }

    private CustomSpinner setUpCustomSpinnerQuestion(TextView anchor,
                                                     List<SecurityQuestion> data) {
        CustomSpinner customSpinner = new CustomSpinner(getActivity(), anchor);

        List<String> questionChallenges = new ArrayList<>();
        for (SecurityQuestion item : data) {
            questionChallenges.add(item.getQuestionText());
        }

        DropdownAdapter dropdownAdapter = new DropdownAdapter(getActivity(),
                R.layout.item_challenge_question_dropdown,
                questionChallenges, 52);
        customSpinner.setDropdownAdapter(dropdownAdapter);
        customSpinner.setmSpinnerListener(spinnerListener);
        customSpinner.setPopupHeightListQuestion();
        return customSpinner;
    }

    CustomSpinner.SpinnerListener spinnerListener = new CustomSpinner.SpinnerListener() {
        @Override
        public void onArchorViewClick() {
            ViewUtils.hideSoftKey(getView());
            if (getActivity() != null &&
                    getActivity().getCurrentFocus() != null){

                getActivity().getCurrentFocus().clearFocus();
            }
        }

        @Override
        public void onItemSelected(int index) {
            if (answerFocusChangeListener == null
                    || mSpinnerChallengeQuestion == null
                    || profileComparator == null
                    || edtAnswerQuestion == null) {
                return;
            }

            String questionCurrent = answerFocusChangeListener.getCacheQuestionProfile();
            String newQuestion = listChallengeQuestionPresenter.getQuestionContent(index);
            if(newQuestion.equalsIgnoreCase(questionCurrent)){
                profileComparator.setQuestion(false);
                setAnswerMaskOnView();

            }else {
                //-- change question
                profileComparator.setQuestion(true);
                edtAnswerQuestion.setText("");
            }

            profileComparator.handleSelectQuestion();
            answerFocusChangeListener.original = "";

        }
    };

    private class AnswerFocusChangeListener implements View.OnFocusChangeListener {

        private boolean isResetField = true;
        private String original = "";
        private String cacheQuestionProfile = "";

        @Override
        public void onFocusChange(View view, boolean hasFocus) {
            if (hasFocus) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mainScrollView.scrollTo(0, mainScrollView.getBottom());
                    }
                }, 500);
                if (edtAnswerQuestion != null) {
                    isAnswerMasker = false;
                    edtAnswerQuestion.setCursorVisible(true);
                    //-- touch answer field -> clear out
                    if (isResetField) {
                        if (TextUtils.isEmpty(original)
                                || MaskerHelper.answerMaskValue(original)) {
                            edtAnswerQuestion.setText("");
                        } else {
                            edtAnswerQuestion.setText(original);
                        }

                    } else if (!TextUtils.isEmpty(original)) {
                        edtAnswerQuestion.setText(original);
                    } else {
                        edtAnswerQuestion.setText("");
                    }
                }
            } else {
                String tempAnswer = edtAnswerQuestion.getText().toString().trim();
                original = MaskerHelper.answerMaskValue(tempAnswer) ? "" : tempAnswer;

                //<editor-fold desc="Rule show field answer">
                //-- case: field answer have text -> mask text
                //-- case: field answer empty touch out
                //- same question : show 7*
                //- different question: always show empty
                isAnswerMasker = true;
                if (mSpinnerChallengeQuestion == null) {
                    return;
                }
                String answerOnView;
                if(TextUtils.isEmpty(original)){
                    String questionCurrent = mSpinnerChallengeQuestion.getTextSelectedSafe();

                    if(cacheQuestionProfile.equalsIgnoreCase(questionCurrent)){
                        //- same question : show 7*
                        answerOnView = MaskerHelper.mask(MaskerHelper.INPUT.ANSWER, "");
                    }else {
                        answerOnView = "";
                    }

                }else{
                    answerOnView = MaskerHelper.mask(MaskerHelper.INPUT.ANSWER, original);
                }
                edtAnswerQuestion.setText(answerOnView);
                //</editor-fold>
            }
        }

        void setResetField(boolean resetField) {
            isResetField = resetField;
        }

        public void setCacheQuestionProfile(String cacheQuestionProfile) {
            this.cacheQuestionProfile = cacheQuestionProfile;
        }

        public String getCacheQuestionProfile() {
            return cacheQuestionProfile;
        }

        public void resetAnswerField() {
            original = "";
        }
    }
    //</editor-fold>
}
