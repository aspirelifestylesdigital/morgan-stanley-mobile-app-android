package com.ms.androidapp.presentation.base;

import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.api.aspire.data.preference.PreferencesStorageAspire;
import com.api.aspire.domain.usecases.GetToken;
import com.ms.androidapp.App;
import com.ms.androidapp.common.constant.AppConstant;
import com.ms.androidapp.domain.usecases.MapProfileApp;

import java.util.concurrent.TimeUnit;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Thu Nguyen on 11/15/2017.
 */

public class BaseActivity extends AppCompatActivity {

    private static Integer numOfVisibleInstances = 0;
    private static int lastNumOfVisibleInstances = 0;

    @Override
    protected void onResume() {
        super.onResume();
        synchronized (numOfVisibleInstances) {
            numOfVisibleInstances ++;
            Log.d("ThuNguyen", "onResume() with " + "numOfVisibleInstances = " + numOfVisibleInstances);
            if(lastNumOfVisibleInstances <= 0){ // App actually open
                Log.d("ThuNguyen", "with open from app");
                // Track GA
                App.getInstance().track(AppConstant.GA_TRACKING_CATEGORY.USER_INTERACTIVITY.getValue(),
                                        AppConstant.GA_TRACKING_ACTION.OPEN.getValue(),
                                        AppConstant.GA_TRACKING_LABEL.OPEN_APP.getValue());
            }
            lastNumOfVisibleInstances = numOfVisibleInstances;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        synchronized (numOfVisibleInstances){
            numOfVisibleInstances --;
            Log.d("ThuNguyen", "onPause() with numOfVisibleInstances = " + numOfVisibleInstances);
            Single.just("").delay(1000, TimeUnit.MILLISECONDS)
                    .observeOn(Schedulers.io())
                    .subscribe(s -> {
                        if(lastNumOfVisibleInstances > 0 && numOfVisibleInstances == 0){ // App actually close
                            Log.d("ThuNguyen", "with close app");
                            // Track GA
                            App.getInstance().track(AppConstant.GA_TRACKING_CATEGORY.USER_INTERACTIVITY.getValue(),
                                    AppConstant.GA_TRACKING_ACTION.LEAVE.getValue(),
                                    AppConstant.GA_TRACKING_LABEL.LEAVE_APP.getValue());

                            // revoke Token
                            GetToken getTokenCase = new GetToken(
                                    new PreferencesStorageAspire(this),
                                    new MapProfileApp()
                            );
                            getTokenCase.revokeTokenInApp()
                                    .subscribeOn(Schedulers.io())
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribe();
                        }
                        lastNumOfVisibleInstances = numOfVisibleInstances;
                    });
        }
    }
}
