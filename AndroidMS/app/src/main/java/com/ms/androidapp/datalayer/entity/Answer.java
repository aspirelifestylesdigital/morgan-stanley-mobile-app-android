package com.ms.androidapp.datalayer.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by tung.phan on 6/1/2017.
 */

public class Answer {
    @SerializedName("Address")
    @Expose
    private String address;
    @SerializedName("Address2")
    @Expose
    private String address2;
    @SerializedName("Address3")
    @Expose
    private String address3;
    @SerializedName("AnswerText")
    @Expose
    private String answerText;
    @SerializedName("City")
    @Expose
    private String city;
    @SerializedName("CommentCount")
    @Expose
    private Integer commentCount;
    @SerializedName("Country")
    @Expose
    private String country;
    @SerializedName("HasContentSystemOffers")
    @Expose
    private Boolean hasContentSystemOffers;
    @SerializedName("HoursOfOperation")
    @Expose
    private String hoursOfOperation;
    @SerializedName("ID")
    @Expose
    private Integer iD;
    @SerializedName("ImageURL")
    @Expose
    private String imageURL;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("Offer")
    @Expose
    private String offer;
    @SerializedName("Offer2")
    @Expose
    private String offer2;
    @SerializedName("Phone")
    @Expose
    private String phone;
    @SerializedName("Price")
    @Expose
    private Integer price;
    @SerializedName("RatingsAverage")
    @Expose
    private Integer ratingsAverage;
    @SerializedName("RatingsCount")
    @Expose
    private Integer ratingsCount;
    @SerializedName("State")
    @Expose
    private String state;
    @SerializedName("URL")
    @Expose
    private String uRL;
    @SerializedName("UsefulCount")
    @Expose
    private Integer usefulCount;
    @SerializedName("UserDefined1")
    @Expose
    private String userDefined1;
    @SerializedName("UserDefined2")
    @Expose
    private String userDefined2;
    @SerializedName("ZipCode")
    @Expose
    private String zipCode;

    public String getAddress() {
        return address;
    }

    public String getAddress2() {
        return address2;
    }

    public String getAddress3() {
        return address3;
    }

    public String getAnswerText() {
        return answerText;
    }

    public String getCity() {
        return city;
    }

    public Integer getCommentCount() {
        return commentCount;
    }

    public String getCountry() {
        return country;
    }

    public Boolean getHasContentSystemOffers() {
        return hasContentSystemOffers;
    }

    public String getHoursOfOperation() {
        return hoursOfOperation;
    }

    public Integer getID() {
        return iD;
    }

    public String getImageURL() {
        return imageURL;
    }

    public String getName() {
        return name;
    }

    public String getOffer() {
        return offer;
    }

    public String getOffer2() {
        return offer2;
    }

    public String getPhone() {
        return phone;
    }

    public Integer getPrice() {
        return price;
    }

    public Integer getRatingsAverage() {
        return ratingsAverage;
    }

    public Integer getRatingsCount() {
        return ratingsCount;
    }

    public String getState() {
        return state;
    }

    public String getURL() {
        return uRL;
    }

    public Integer getUsefulCount() {
        return usefulCount;
    }

    public String getUserDefined1() {
        return userDefined1;
    }

    public String getUserDefined2() {
        return userDefined2;
    }

    public String getZipCode() {
        return zipCode;
    }
}
