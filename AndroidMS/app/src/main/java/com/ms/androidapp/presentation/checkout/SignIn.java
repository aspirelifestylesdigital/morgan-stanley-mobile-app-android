package com.ms.androidapp.presentation.checkout;

import com.api.aspire.common.constant.ErrCode;
import com.api.aspire.domain.model.ProfileAspire;
import com.ms.androidapp.presentation.base.BasePresenter;

/**
 * Created by vinh.trinh on 4/26/2017.
 */

public interface SignIn {

    interface View {
        void proceedToHome();
        void showErrorDialog(ErrCode errCode, String extraMsg);
        void showProgressDialog();
        void dismissProgressDialog();
    }

    interface Presenter extends BasePresenter<View> {
        void doSignIn(String email, String password);
        void abort();
    }

}
