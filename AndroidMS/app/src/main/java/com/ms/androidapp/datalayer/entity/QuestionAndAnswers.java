package com.ms.androidapp.datalayer.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Thu Nguyen on 6/1/2017.
 */

public class QuestionAndAnswers {
    @SerializedName("Answers")
    @Expose
    private List<Answer> answers;
    @SerializedName("Question")
    @Expose
    private Question question;
    @SerializedName("Status")
    @Expose
    private String status;
    @SerializedName("Success")
    @Expose
    private Boolean success;

    public List<Answer> getAnswers() {
        return answers;
    }

    public void setAnswers(List<Answer> answers) {
        this.answers = answers;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }
}
