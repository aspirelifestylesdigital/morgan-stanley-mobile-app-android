package com.ms.androidapp.presentation.widget;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import java.lang.ref.WeakReference;

/**
 * Created by Thu Nguyen on 12/8/2017.
 */

public class DiscardSpaceTextWatcher implements TextWatcher {
    public interface ITextWatcherListener{
        void onWatcherFinished();
    }
    ITextWatcherListener textWatcherListener;
    WeakReference<EditText> editTextWeakReference;
    int selection;
    String before;

    public DiscardSpaceTextWatcher(EditText editText, ITextWatcherListener textWatcherListener){
        editTextWeakReference = new WeakReference<EditText>(editText);
        this.textWatcherListener = textWatcherListener;
    }
    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        selection = editTextWeakReference.get().getSelectionEnd();
        before = charSequence.toString();
    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
        editTextWeakReference.get().removeTextChangedListener(this);
        String newText = editable.toString().replace(" ", "");
        editTextWeakReference.get().getText().clear();
        editTextWeakReference.get().getText().append(newText);
        if(newText.equalsIgnoreCase(before)) {
            editTextWeakReference.get().setSelection(selection);
        }else if(newText.length() > before.length()){
            int sel = Math.max(0, Math.min(selection + 1, newText.length()));
            editTextWeakReference.get().setSelection(sel);
        }else{
            int sel = Math.max(0, Math.min(selection - 1, newText.length()));
            editTextWeakReference.get().setSelection(sel);
        }
        if(textWatcherListener != null){
            textWatcherListener.onWatcherFinished();
        }
        editTextWeakReference.get().addTextChangedListener(this);
    }
}
