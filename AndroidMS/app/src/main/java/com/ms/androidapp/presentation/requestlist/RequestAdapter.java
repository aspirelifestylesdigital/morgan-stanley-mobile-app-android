package com.ms.androidapp.presentation.requestlist;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ms.androidapp.R;
import com.ms.androidapp.domain.model.RequestItemView;
import com.ms.androidapp.presentation.widget.FooterLoaderAdapter;
import com.ms.androidapp.presentation.widget.OnItemClickListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vinh.trinh on 8/31/2017.
 */

public class RequestAdapter extends FooterLoaderAdapter<RequestAdapter.ViewHolder> {

    enum TYPE { OPEN, CLOSE }
    private List<RequestItemView> dataView;
    private TYPE type;
    private OnItemClickListener listener;
    private int selectedPosition = -1;

    RequestAdapter(Context context, TYPE type) {
        super(context);
        dataView = new ArrayList<>();
        this.type = type;
    }

    @Override
    public long getYourItemId(int position) {
        return position;
    }

    @Override
    public RequestAdapter.ViewHolder getYourItemViewHolder(LayoutInflater inflater, ViewGroup parent) {
        return new ViewHolder(inflater.inflate(R.layout.request_list_item, parent, false));
    }

    @Override
    public void bindYourViewHolder(RecyclerView.ViewHolder holder, int position) {
        holder.itemView.setSelected(position == selectedPosition);
        RequestItemView datum = dataView.get(position);
        RequestAdapter.ViewHolder viewHolder = (ViewHolder) holder;
        viewHolder.vTitle.setText(datum.title);
        viewHolder.vDate.setText(datum.date);
        viewHolder.vLabelDate.setText(datum.dateLabel);
        viewHolder.icon.setVisibility(datum.status == RequestItemView.STATUS.PENDING ?
                View.INVISIBLE : View.VISIBLE);
    }

    void setItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    void clear() {
        selectedPosition = -1;
        setItemCount(0);
        dataView.clear();
        notifyDataSetChanged();
    }

    void append(List<RequestItemView> newData) {
        dataView.addAll(newData);
        updateItemCount();
        notifyDataSetChanged();
    }

    RequestItemView getItem(int pos) {
        return dataView.get(pos);
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private View itemView;
        private TextView vTitle, vDate, vLabelDate;
        private ImageView icon;

        ViewHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;
            vTitle = itemView.findViewById(R.id.request_item_title);
            vDate = itemView.findViewById(R.id.request_item_date);
            vLabelDate = itemView.findViewById(R.id.label_above);
            icon = itemView.findViewById(R.id.icon_arrow);

            itemView.setOnClickListener(view -> {
                int position = getAdapterPosition();
                int pastSelected = selectedPosition;
                selectedPosition = position;
                if(pastSelected > -1 && pastSelected != position) notifyItemChanged(pastSelected);
                notifyItemChanged(position);
                if(dataView.get(position).status == RequestItemView.STATUS.PENDING) return;
                if(listener != null) listener.onItemClick(position);
            });
        }
    }

    private void updateItemCount() {
        setItemCount(this.dataView.size());
    }

}
