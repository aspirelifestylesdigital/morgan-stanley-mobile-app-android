package com.ms.androidapp.datalayer.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by tung.phan on 6/1/2017.
 */

public class Question {
    @Expose
    private Integer hitCount;
    @SerializedName("HotQuestion")
    @Expose
    private Boolean hotQuestion;
    @SerializedName("ID")
    @Expose
    private Integer iD;
    @SerializedName("LastUpdateDate")
    @Expose
    private String lastUpdateDate;
    @SerializedName("QuestionText")
    @Expose
    private String questionText;
    @SerializedName("ShortQuestionText")
    @Expose
    private String shortQuestionText;
    @SerializedName("UserDefined1")
    @Expose
    private String userDefined1;
    @SerializedName("UserDefined2")
    @Expose
    private String userDefined2;

    public Integer getHitCount() {
        return hitCount;
    }

    public void setHitCount(Integer hitCount) {
        this.hitCount = hitCount;
    }

    public Boolean getHotQuestion() {
        return hotQuestion;
    }

    public void setHotQuestion(Boolean hotQuestion) {
        this.hotQuestion = hotQuestion;
    }

    public Integer getID() {
        return iD;
    }

    public void setID(Integer iD) {
        this.iD = iD;
    }

    public String getLastUpdateDate() {
        return lastUpdateDate;
    }

    public void setLastUpdateDate(String lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    public String getQuestionText() {
        return questionText;
    }

    public void setQuestionText(String questionText) {
        this.questionText = questionText;
    }

    public String getShortQuestionText() {
        return shortQuestionText;
    }

    public void setShortQuestionText(String shortQuestionText) {
        this.shortQuestionText = shortQuestionText;
    }

    public String getUserDefined1() {
        return userDefined1;
    }

    public void setUserDefined1(String userDefined1) {
        this.userDefined1 = userDefined1;
    }

    public String getUserDefined2() {
        return userDefined2;
    }

    public void setUserDefined2(String userDefined2) {
        this.userDefined2 = userDefined2;
    }
}
