package com.ms.androidapp;

import android.app.Application;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.TextUtils;
import android.util.Log;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.analytics.ecommerce.Product;
import com.google.android.gms.analytics.ecommerce.ProductAction;
import com.ms.androidapp.common.constant.AppConstant;

import io.reactivex.internal.functions.Functions;
import io.reactivex.plugins.RxJavaPlugins;

/**
 * Created by vinh.trinh on 4/24/2017.
 */

public class App extends Application {

    private static App instance;
    private static Tracker gTracker;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        RxJavaPlugins.setErrorHandler(Functions.<Throwable>emptyConsumer());
        byPassSSLCertificate();
        if(BuildConfig.DEBUG) {
            /*if (LeakCanary.isInAnalyzerProcess(this)) {
                // This process is dedicated to LeakCanary for heap analysis.
                // You should not init your app in this process.
                return;
            }
            LeakCanary.install(this);*/
        }
        GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
        analytics.setLocalDispatchPeriod(5);
        // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
        gTracker = analytics.newTracker(BuildConfig.GA_TRACKER_ID);

        // Start a new session with the hit.
        gTracker.send(new HitBuilders.EventBuilder()
                .setNewSession()
                .build());
    }

    public boolean hasNetworkConnection() {
        ConnectivityManager cm =
                (ConnectivityManager) instance.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnectedOrConnecting());
    }

    public static App getInstance() {
        return instance;
    }

    private void byPassSSLCertificate(){
        // Create a trust manager that does not validate certificate chains
        /*TrustManager[] trustAllCerts = new TrustManager[]{
                new X509TrustManager() {
                    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                        return null;
                    }
                    public void checkClientTrusted(
                            java.security.cert.X509Certificate[] certs, String authType) {
                    }
                    public void checkServerTrusted(
                            java.security.cert.X509Certificate[] certs, String authType) {
                    }
                }
        };

        // Install the all-trusting trust manager
        try {
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (Exception e) {}*/
    }
    public void track(String eventCategory, String eventAction, String eventLabel){
        Log.i("ThuNguyen", "eventCategory: " + eventCategory + ", eventAction: " + eventAction + ", eventLabel: " + eventLabel);
        if(TextUtils.isEmpty(eventLabel)) {
            gTracker.send(new HitBuilders.EventBuilder()
                    .setCategory(eventCategory)
                    .setAction(eventAction)
                    .setValue(1)
                    .build());
        }else{
            gTracker.send(new HitBuilders.EventBuilder()
                    .setCategory(eventCategory)
                    .setAction(eventAction)
                    .setLabel(eventLabel)
                    .setValue(1)
                    .build());
        }
    }
    public void track(String screenName){
        Log.i("Thu Nguyen", "Screen name = " + screenName);
        gTracker.setScreenName(screenName);
        gTracker.send(new HitBuilders.ScreenViewBuilder()
                .build());
    }
    public void track(String productCategory, String productName){
        Product product = new Product().setQuantity(1).setCategory(productCategory).setName(productName).setBrand(AppConstant.TRACK_EVENT_PRODUCT_BRAND);
        HitBuilders.EventBuilder builder = new HitBuilders.EventBuilder();
        builder.setProductAction(new ProductAction(ProductAction.ACTION_CHECKOUT).setCheckoutStep(1));
        builder.addProduct(product);
        builder.addImpression(product, "ConciergeRequest");
        gTracker.send(builder.build());
    }
}
