package com.ms.androidapp.common.constant;

/**
 * Created by tung.phan on 6/1/2017.
 */

public interface SubCategory {

    String ACCOMMODATIONS = "ACCOMMODATIONS";
    String BARS = "BARS/CLUBS";
    String CULTURE = "CULTURE";
    String DINING = "DINING";
    String SHOPPING = "SHOPPING";
    String SPAS = "SPAS";

}
