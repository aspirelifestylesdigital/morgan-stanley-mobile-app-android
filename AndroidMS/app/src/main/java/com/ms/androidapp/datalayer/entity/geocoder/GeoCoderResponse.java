package com.ms.androidapp.datalayer.entity.geocoder;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vinh.trinh on 8/14/2017.
 */

public class GeoCoderResponse {

    @SerializedName("status")
    private String status;
    @SerializedName("results")
    private Result[] results;

    public boolean success() {
        return "OK".equals(status);
    }

    public Location location() {
        return results[0].geometry.location;
    }

    public AddressComponent[] addressComponents() {
        return results[0].addressComponents;
    }

    public String getFormattedAddress(){return results[0].formattedAddress;}

    public static class AddressComponent implements Parcelable{
        public static final Creator<AddressComponent> CREATOR = new Creator<AddressComponent>() {
            @Override
            public AddressComponent createFromParcel(Parcel in) {
                return new AddressComponent(in);
            }

            @Override
            public AddressComponent[] newArray(int size) {
                return new AddressComponent[size];
            }
        };
        static final String areaLevel1 = "administrative_area_level_1";
        static final String areaLevel2 = "administrative_area_level_2";
        static final String locality = "locality";
        @SerializedName("short_name")
        private String shortName;
        @SerializedName("long_name")
        private String longName;
        @SerializedName("types")
        private List<String> types;
        public AddressComponent(Parcel parcel){
            shortName = parcel.readString();
            longName = parcel.readString();
            types = new ArrayList<>();
            parcel.readStringList(types);
        }

        public String name() {
            return shortName;
        }

        public String getLongName() {
            return longName;
        }

        public List<String> getTypes(){return types;}

        public boolean isSameCity(String fullAddress){
            return (types != null && longName != null && (types.contains(locality) || types.contains(areaLevel1) || types.contains(areaLevel2))
                    && fullAddress.toLowerCase().contains(longName.toLowerCase()));
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(shortName);
            parcel.writeString(longName);
            parcel.writeStringList(types);
        }
    }

    private class Result {
        @SerializedName("geometry")
        private Geometry geometry;
        @SerializedName("formatted_address")
        private String formattedAddress;
        @SerializedName("address_components")
        private AddressComponent[] addressComponents;
    }

    private class Geometry {
        @SerializedName("location")
        private Location location;
    }

    public class Location {
        @SerializedName("lat")
        private double lat;
        @SerializedName("lng")
        private double lng;

        public double getLat() {
            return lat;
        }

        public double getLng() {
            return lng;
        }
    }
}
