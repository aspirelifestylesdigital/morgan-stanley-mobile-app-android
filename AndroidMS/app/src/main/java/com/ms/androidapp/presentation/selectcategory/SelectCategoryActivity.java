package com.ms.androidapp.presentation.selectcategory;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.ms.androidapp.App;
import com.ms.androidapp.R;
import com.ms.androidapp.common.constant.AppConstant;
import com.ms.androidapp.common.constant.CityData;
import com.ms.androidapp.common.constant.IntentConstant;
import com.ms.androidapp.common.constant.RequestCode;
import com.ms.androidapp.common.constant.ResultCode;
import com.ms.androidapp.presentation.LocationPermissionHandler;
import com.ms.androidapp.presentation.base.CommonActivity;
import com.ms.androidapp.presentation.cityguidecategory.CityGuideCategoriesActivity;
import com.ms.androidapp.presentation.widget.DialogHelper;

import butterknife.BindView;

/**
 * Created by vinh.trinh on 5/10/2017.
 */

public class SelectCategoryActivity extends CommonActivity implements
        CategoryAdapter.OnCategoryItemClickListener
        , Category.View {

    @BindView(R.id.selection_recycle_view)
    RecyclerView categoriesListView;
    private CategoryAdapter adapter;
    private DialogHelper dialogHelper;
    private CategoryPresenter presenter;
    LocationPermissionHandler locationPermissionHandler = new LocationPermissionHandler();

    private CategoryPresenter presenter() {
        return new CategoryPresenter(this);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.common_recyclerview_activity_layout);
        setTitle(R.string.category_title);
        setToolbarColor(R.color.ms_colorPrimary);
        findViewById(R.id.container).setBackgroundColor(Color.WHITE);
        presenter = presenter();
        presenter.attach(this);
        dialogHelper = new DialogHelper(this);
        categoriesListView.setLayoutManager(new GridLayoutManager(this, 3));
        adapter = new CategoryAdapter(this);
        adapter.setListener(this);
        categoriesListView.setAdapter(adapter);
        presenter.loadProfile();

        if(savedInstanceState == null){
            // Track GA
            App.getInstance().track(AppConstant.GA_TRACKING_SCREEN_NAME.CATEGORY_LIST.getValue());
        }
    }

    @Override
    public void onItemClick(String category) {
        if (category.equalsIgnoreCase(adapter.getCategoryList()[14])) {//click on City Guide
            //return the category id of the current city selected if that city support city guide
            int cityId = CityData.guideCode();
            //start sub category select
            Intent intent = new Intent(this, CityGuideCategoriesActivity.class);
            intent.putExtra(IntentConstant.CATEGORY_ID, cityId);
            startActivityForResult(intent, RequestCode.SELECT_SUB_CATEGORY);
        } else {
            if(category.equals("DINING")) {
                if(presenter.isLocationProfileSettingOn()) {
                    if(!locationPermissionHandler.hasPermission(this)) {
                        locationPermissionHandler.requestPermission(this);
                    } else {
                        presenter.locationServiceCheck(getApplicationContext());
                    }
                } else {
                    returnResult(category);
                }
            } else {
                returnResult(category);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        boolean granted = locationPermissionHandler.handlingPermissionResult(requestCode, grantResults);
        if(granted) {
            presenter.locationServiceCheck(getApplicationContext());
        } else {
            proceedWithDiningCategory();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RequestCode.SELECT_SUB_CATEGORY) {
            if (resultCode == ResultCode.RESULT_OK && null != data) {
                data.putExtra(IntentConstant.SUPPER_CATEGORY
                        , getResources().getString(R.string.city_guide));
                setResult(ResultCode.RESULT_OK_WITH_ID, data);
                finish();
            }
        }
        if(requestCode == 11234) {
            returnResult("DINING");
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detach();
    }

    @Override
    public void proceedWithDiningCategory() {
        returnResult("DINING");
    }

    @Override
    public void askForLocationSetting() {
        dialogHelper.action(null, "To enable, please go to Settings and turn on Location Service for this app.", "Setting", "Cancel",
                (dialogInterface, i) -> openGPSSetting(),
                (dialogInterface, i) -> returnResult("DINING"));
    }

    private void returnResult(String category) {
        Intent intent = new Intent();
        intent.putExtra(IntentConstant.SELECTED_CATEGORY, category);
        setResult(ResultCode.RESULT_OK, intent);
        finish();
    }

    private void openGPSSetting() {
        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivityForResult(intent, 11234);
    }
}
