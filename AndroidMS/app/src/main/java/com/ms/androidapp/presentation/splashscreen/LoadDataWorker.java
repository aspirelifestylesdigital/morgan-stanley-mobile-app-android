package com.ms.androidapp.presentation.splashscreen;

import android.content.Context;

import com.api.aspire.data.preference.PreferencesStorageAspire;
import com.api.aspire.data.repository.ProfileDataAspireRepository;
import com.api.aspire.domain.usecases.GetAccessToken;
import com.api.aspire.domain.usecases.GetToken;
import com.api.aspire.domain.usecases.LoadProfile;
import com.ms.androidapp.domain.usecases.MapProfileApp;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by vinh.trinh on 8/31/2017.
 */

public class LoadDataWorker {

    public void load(Context context) {

        MapProfileApp mapLogic = new MapProfileApp();
        PreferencesStorageAspire preferencesStorage = new PreferencesStorageAspire(context);
        LoadProfile loadProfile = new LoadProfile(new ProfileDataAspireRepository(preferencesStorage, mapLogic));
        GetToken getToken = new GetToken(preferencesStorage, mapLogic);
        GetAccessToken getAccessToken = new GetAccessToken(loadProfile, getToken);

        loadProfile.refreshProfile(getAccessToken)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe();
    }
}
