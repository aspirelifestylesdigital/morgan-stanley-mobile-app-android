package com.ms.androidapp.datalayer.entity;

import com.google.gson.annotations.SerializedName;

/**
 * Created by vinh.trinh on 6/5/2017.
 */

public class ContentFulls {

    @SerializedName("BenefitInd")
    private boolean benefitInd;
    @SerializedName("Category")
    private String category;
    @SerializedName("Description")
    private String description;
    @SerializedName("GeographicRegion")
    private String geographicRegion;
    @SerializedName("HotInd")
    private boolean hotInd;
    @SerializedName("ID")
    private Integer ID;
    @SerializedName("OfferInd")
    private boolean offerInd;
    @SerializedName("OfferText")
    private String offerText;
    @SerializedName("ParentCategory")
    private String parentCategory;
    @SerializedName("SubCategory")
    private String subCategory;
    @SerializedName("TermsOfUse")
    private String termsOfUse;
    @SerializedName("Title")
    private String title;

    public boolean isBenefitInd() {
        return benefitInd;
    }

    public void setBenefitInd(boolean benefitInd) {
        this.benefitInd = benefitInd;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getGeographicRegion() {
        return geographicRegion;
    }

    public void setGeographicRegion(String geographicRegion) {
        this.geographicRegion = geographicRegion;
    }

    public boolean isHotInd() {
        return hotInd;
    }

    public void setHotInd(boolean hotInd) {
        this.hotInd = hotInd;
    }

    public Integer getID() {
        return ID;
    }

    public void setID(Integer ID) {
        this.ID = ID;
    }

    public boolean isOfferInd() {
        return offerInd;
    }

    public void setOfferInd(boolean offerInd) {
        this.offerInd = offerInd;
    }

    public String getOfferText() {
        return offerText;
    }

    public void setOfferText(String offerText) {
        this.offerText = offerText;
    }

    public String getParentCategory() {
        return parentCategory;
    }

    public void setParentCategory(String parentCategory) {
        this.parentCategory = parentCategory;
    }

    public String getSubCategory() {
        return subCategory;
    }

    public void setSubCategory(String subCategory) {
        this.subCategory = subCategory;
    }

    public String getTermsOfUse() {
        return termsOfUse;
    }

    public void setTermsOfUse(String termsOfUse) {
        this.termsOfUse = termsOfUse;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
