package com.ms.androidapp.presentation.explore;

/**
 * Created by vinh.trinh on 7/12/2017.
 */

public class SearchConditions {

    public final String term;
    public final Boolean withOffers;

    public SearchConditions(String term, Boolean withOffers) {
        this.term = term;
        this.withOffers = withOffers;
    }
}