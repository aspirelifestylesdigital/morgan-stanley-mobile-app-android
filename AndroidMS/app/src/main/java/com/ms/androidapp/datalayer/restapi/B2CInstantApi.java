package com.ms.androidapp.datalayer.restapi;

import com.ms.androidapp.datalayer.entity.b2cinstant.B2CQandADetailRequest;
import com.ms.androidapp.datalayer.entity.b2cinstant.B2CQandADetailResponse;
import com.ms.androidapp.datalayer.entity.b2cinstant.B2CQandARequest;
import com.ms.androidapp.datalayer.entity.b2cinstant.B2CQandAResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * for City Guide & Dining List business
 */
public interface B2CInstantApi {
    /**
    /**
     * City Guide & Dining List
     */
    @POST("instantanswers/GetQuestionsAndAnswers")
    Call<B2CQandAResponse>
    getQuestionsAndAnswers(@Body B2CQandARequest body);

    /**
     * City Guide & Dining Detail
     */
    @POST("instantanswers/GetQuestionAndAnswers")
    Call<B2CQandADetailResponse>
    getQuestionAndAnswers(@Body B2CQandADetailRequest body);
}
