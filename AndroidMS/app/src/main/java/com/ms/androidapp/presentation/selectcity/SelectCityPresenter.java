package com.ms.androidapp.presentation.selectcity;

import android.content.Context;
import android.text.TextUtils;

import com.api.aspire.common.constant.ConstantAspireApi;
import com.api.aspire.data.preference.PreferencesStorageAspire;
import com.api.aspire.data.repository.ProfileDataAspireRepository;
import com.api.aspire.domain.mapper.profile.ProfileLogicCore;
import com.api.aspire.domain.model.ProfileAspire;
import com.api.aspire.domain.usecases.GetAccessToken;
import com.api.aspire.domain.usecases.GetToken;
import com.api.aspire.domain.usecases.LoadProfile;
import com.api.aspire.domain.usecases.SaveProfile;
import com.api.aspire.domain.usecases.SelectCityCase;
import com.ms.androidapp.App;
import com.ms.androidapp.common.constant.CityData;
import com.ms.androidapp.domain.usecases.MapProfileApp;

import org.json.JSONException;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class SelectCityPresenter implements SelectCity.Presenter {

    private SelectCity.View view;

    private PreferencesStorageAspire preferencesStorage;
    private SelectCityCase selectCityCase;

    public SelectCityPresenter(Context c) {
        MapProfileApp mapLogic = new MapProfileApp();
        this.preferencesStorage = new PreferencesStorageAspire(c);
        ProfileDataAspireRepository profileRepository = new ProfileDataAspireRepository(preferencesStorage, mapLogic);
        LoadProfile loadProfile = new LoadProfile(profileRepository);
        GetToken getToken = new GetToken(preferencesStorage, mapLogic);
        GetAccessToken getAccessToken = new GetAccessToken(loadProfile, getToken);
        SaveProfile saveProfile = new SaveProfile(profileRepository);
        selectCityCase = new SelectCityCase(getAccessToken, loadProfile, saveProfile);
    }

    @Override
    public void attach(SelectCity.View view) {
        this.view = view;
    }

    @Override
    public void detach() {
        this.view = null;
    }

    @Override
    public void saveSelectCity(final String cityNameVal) {
        if (TextUtils.isEmpty(cityNameVal)) {
            return;
        }

        ProfileAspire profile = null;
        //save local
        if (CityData.setSelectedCity(cityNameVal)) {
            PreferencesStorageAspire.Editor editor = preferencesStorage.editor();
            try {
                profile = preferencesStorage.profile();
                //update new city
                ProfileLogicCore.setAppUserPreference(profile.getAppUserPreferences(),
                        ConstantAspireApi.APP_USER_PREFERENCES.SELECTED_CITY_KEY, cityNameVal);
                editor.profile(profile);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            editor.selectedCity(cityNameVal).build().saveAsync();
        }

        //save remote - don't stuck here. if can not save city in Remote, will save when upload Profile
        if (!App.getInstance().hasNetworkConnection()) {
            return;
        }
        //run background
        if (profile != null) {
            selectCityCase.saveProfileSelectCity(profile)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe();
        }
    }

}
