package com.ms.androidapp.common.constant;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by ThuNguyen on 6/19/2017.
 */

public class AppConstant {
    public static final String TRACK_EVENT_PRODUCT_BRAND = "LX";

    public enum MASTERCARD_COPY_UTILITY{
        TermsOfUse("AndroidTOU"),
        About("About"),
        Privacy("Privacy");
        private static final Map<String, MASTERCARD_COPY_UTILITY> enumsByValue = new HashMap<String, MASTERCARD_COPY_UTILITY>();
        static {
            for (MASTERCARD_COPY_UTILITY type : MASTERCARD_COPY_UTILITY.values()) {
                enumsByValue.put(type.value, type);
            }
        }

        public static MASTERCARD_COPY_UTILITY getEnum(String value) {
            return enumsByValue.get(value);
        }
        private final String value;
        private MASTERCARD_COPY_UTILITY(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    // Explore category type
    public enum EXPLORE_CATEGORY{
        DINING("Dining"), // Dining
        CULINARY_EXPERIENCE("Culinary Experiences"), // Dining
        HOTEL("Hotels"), // Hotel
        FLOWER("Flowers"), // Flower Delivery
        ENTERTAINMENT("Tickets"), // Entertainment
        ENTERTAINMENT_EXPERIENCE("Entertainment Experiences"), // Entertainment
        GOLF("Golf"), // Golf
        GOLF_EXPERIENCE("Golf Experiences"), // Golf
        GOLF_MERCHANDISE("Golf Merchandise"),// Golf
        VACATION_PACKAGES("Vacation Packages"), // Vacation packages
        CRUISE("Cruises"), // Cruise
        TOUR_SUBCAT("Sightseeing"), // Tour
        TRAVEL("Private Jet Travel"), // Private Jet
        TRAVEL_SERVICES_SUBCAT("Travel Services"),
        AIRPORT_SERVICES_SUBCAT("Airport Services"), // Flight
        CAR_RENTAL("Car Rental"), // Car rental
        CAR_TRANSFER("Limos and Private Car Service"), // Car transfer
        WINE("Wine"),
        RETAIL_SHOPPING("Retail Shopping");
        private static final Map<String, EXPLORE_CATEGORY> enumsByValue = new HashMap<String, EXPLORE_CATEGORY>();
        static {
            for (EXPLORE_CATEGORY type : EXPLORE_CATEGORY.values()) {
                enumsByValue.put(type.value, type);
            }
        }

        public static EXPLORE_CATEGORY getEnum(String value) {
            return enumsByValue.get(value);
        }
        private final String value;
        private EXPLORE_CATEGORY(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }
    // Concierge functionality type
    public enum CONCIERGE_REQUEST_TYPE {
        DINING("D Restaurant"), // Dining
        //RECOMMEND_DINING("Recommend a Dining"),
        //SELFSERVE_DINING("SelfServe a Dining"),
        HOTEL("T HOTEL AND B&B"), // Hotel
//        RECOMMEND_HOTEL("Recommend a Hotel"),
//        SELFSERVE_HOTEL("SelfServe a Hotel"),
        OTHERS("O Client Specific"),
        GOLF("S GOLF"), // OK
        //RECOMMEND_GOLF("Recommend a Golf"),
        CAR_RENTAL_TRANSFER("T Limo and Sedan"), // OK
        EVENT("E CONCERT/THEATER"), // OK
//        RECOMMEND_EVENT("Recommend a Event"),
//        SELFSERVE_EVENT("SelfServe a Event"),
        TOUR("C SIGHTSEEING/TOURS"), // OK
//        RECOMMEND_TOUR("Recommend a Tour"),
//        SELFSERVE_TOUR("SelfServe a Tour"),
        FLIGHT("I AIRPORT SERVICES"), // OK
        CRUISE("T CRUISE"), // OK
        YACHT("T YACHT"),
        FLOWER_DELIVERY("G FLOWERS/GIFT BASKET"), // OK
        SPA("P SPA/SALON"),
        PRIVATE_JET("T OTHER"); // OK
        //VACATION_PACKAGES("C SIGHTSEEING/TOURS");
        private static final Map<String, CONCIERGE_REQUEST_TYPE> enumsByValue = new HashMap<String, CONCIERGE_REQUEST_TYPE>();
        static {
            for (CONCIERGE_REQUEST_TYPE type : CONCIERGE_REQUEST_TYPE.values()) {
                enumsByValue.put(type.value, type);
            }
        }

        public static CONCIERGE_REQUEST_TYPE getEnum(String value) {
            return enumsByValue.get(value);
        }
        private final String value;
        private CONCIERGE_REQUEST_TYPE(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }
    public enum GA_TRACKING_SCREEN_NAME{
        SPLASH("Splash"),
        SIGN_IN("Sign in"),
        SIGN_UP("Sign up"),
        FORGOT_PASSWORD("Forgot secret"),
        HOME("Home"),
        ASK_CONCIERGE("Ask concierge"),
        EXPLORE("Explore"),
        VENUE_DETAIL("Venue detail"),
        SEARCH("Search"),
        CITY_LIST("City list"),
        CATEGORY_LIST("Category list"),
        MY_PROFILE("My profile"),
        PREFERENCES("Preferences"),
        GALLERY("Gallery"),
        CHANGE_PASSWORD("Change secret"),
        ABOUT_THIS_APP("About this app"),
        PRIVACY_POLICY("Privacy policy"),
        TERMS_OF_USE("Terms of use");
        private static final Map<String, GA_TRACKING_SCREEN_NAME> enumsByValue = new HashMap<String, GA_TRACKING_SCREEN_NAME>();
        static {
            for (GA_TRACKING_SCREEN_NAME type : GA_TRACKING_SCREEN_NAME.values()) {
                enumsByValue.put(type.value, type);
            }
        }

        public static GA_TRACKING_SCREEN_NAME getEnum(String value) {
            return enumsByValue.get(value);
        }
        private final String value;
        private GA_TRACKING_SCREEN_NAME(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }
    public enum GA_TRACKING_CATEGORY{
        AUTHENTICATION("Authentication"),
        USER_INTERACTIVITY("User interactivity"),
        REQUEST("Request"),
        CITY_SELECTION("City selection"),
        CATEGORY_SELECTION("Category selection"),
        SIGN_OUT("Sign out");
        private static final Map<String, GA_TRACKING_CATEGORY> enumsByValue = new HashMap<String, GA_TRACKING_CATEGORY>();
        static {
            for (GA_TRACKING_CATEGORY type : GA_TRACKING_CATEGORY.values()) {
                enumsByValue.put(type.value, type);
            }
        }

        public static GA_TRACKING_CATEGORY getEnum(String value) {
            return enumsByValue.get(value);
        }
        private final String value;
        private GA_TRACKING_CATEGORY(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }
    public enum GA_TRACKING_ACTION{
        CLICK("Click"),
        OPEN("Open"),
        LEAVE("Leave"),
        SUBMIT("Submit"),
        VIEW("View"),
        SELECT("Select");
        private static final Map<String, GA_TRACKING_ACTION> enumsByValue = new HashMap<String, GA_TRACKING_ACTION>();
        static {
            for (GA_TRACKING_ACTION type : GA_TRACKING_ACTION.values()) {
                enumsByValue.put(type.value, type);
            }
        }

        public static GA_TRACKING_ACTION getEnum(String value) {
            return enumsByValue.get(value);
        }
        private final String value;
        private GA_TRACKING_ACTION(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }
    public enum GA_TRACKING_LABEL{
        SIGN_IN("Sign in"),
        SIGN_UP("Sign up"),
        SIGN_OUT("Sign out"),
        OPEN_APP("Open the app"),
        LEAVE_APP("Leave the app"),
        REQUEST_ACCEPTED("Request accepted"),
        REQUEST_DENIED("Request denied"),
        CITY_GUIDE_BAR_CLUB("City Guide - Bars/Clubs"),
        CITY_GUIDE_CULTURE("City Guide - Culture"),
        CITY_GUIDE_DINING("City Guide - Dining"),
        CITY_GUIDE_SHOPPING("City Guide - Shopping"),
        CITY_GUIDE_SPAS("City Guide - Spas");
        private static final Map<String, GA_TRACKING_LABEL> enumsByValue = new HashMap<String, GA_TRACKING_LABEL>();
        static {
            for (GA_TRACKING_LABEL type : GA_TRACKING_LABEL.values()) {
                enumsByValue.put(type.value, type);
            }
        }

        public static GA_TRACKING_LABEL getEnum(String value) {
            return enumsByValue.get(value);
        }
        private final String value;
        private GA_TRACKING_LABEL(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    // For prefill key to ask concierge
    public static final String PREFILL_AC_CITY = "City: ";
    public static final String PREFILL_AC_STATE = "State: ";
    public static final String PREFILL_AC_COUNTRY = "Country: ";
    public static final String PREFILL_AC_OTHER_COMMENTS = "Other comments: ";

    public static final String PREFILL_AC_NAME = "Name: ";
    public static final String PREFILL_AC_DATE_TIME = "Date/time: ";
    public static final String PREFILL_AC_NO_OF_ADULTS = "# of adults: ";
    public static final String PREFILL_AC_NO_OF_CHILDREN = "# of children: ";
    public static final String PREFILL_AC_CHECK_IN = "Check in: ";
    public static final String PREFILL_AC_CHECK_OUT = "Check out: ";
    public static final String PREFILL_AC_DATE = "Date: ";
    public static final String PREFILL_AC_NO_OF_GOLFERS = "# of golfers: ";
    public static final String PREFILL_AC_DRIVER = "Driver: ";
    public static final String PREFILL_AC_PICKUP_DATE = "Pickup date: ";
    public static final String PREFILL_AC_PICKUP_LOCATION = "Pick up location: ";
    public static final String PREFILL_AC_DROPOFF_LOCATION = "Drop off location: ";
    public static final String PREFILL_AC_NO_IN_PARTY = "# in party: ";
    public static final String PREFILL_AC_NO_OF_TICKETS = "# of tickets: ";
    public static final String PREFILL_AC_START_DATE = "Start date: ";
    public static final String PREFILL_AC_END_DATE = "End date: ";
    public static final String PREFILL_AC_START_LOCATION = "Start location: ";
    public static final String PREFILL_AC_END_LOCATION = "End location: ";
    public static final String PREFILL_AC_NO_OF_INFANTS = "# of infants: ";
    public static final String PREFILL_AC_ARRANGEMENT = "Arrangement: ";
    public static final String PREFILL_AC_DELIVERY_DATE = "Delivery date: ";
    public static final String PREFILL_AC_DELIVERY_ADDRESS = "Delivery address: ";
    public static final String PREFILL_AC_QUANTITY = "Quantity: ";
    public static final String PREFILL_AC_NO_OF_PASSENGERS = "# of passengers: ";
}
