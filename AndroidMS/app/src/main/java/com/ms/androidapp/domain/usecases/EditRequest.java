package com.ms.androidapp.domain.usecases;

import com.ms.androidapp.domain.repository.ACRepository;

/**
 * Created by vinh.trinh on 8/30/2017.
 */

public class EditRequest extends CreateRequest {

    public EditRequest(ACRepository repository) {
        super(repository);
    }
}
