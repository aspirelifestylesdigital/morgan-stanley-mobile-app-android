package com.ms.androidapp.presentation.splashscreen;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.view.View;

import com.api.aspire.data.preference.PreferencesStorageAspire;
import com.ms.androidapp.App;
import com.ms.androidapp.BuildConfig;
import com.ms.androidapp.R;
import com.ms.androidapp.common.constant.AppConstant;
import com.ms.androidapp.presentation.base.BaseActivity;
import com.ms.androidapp.presentation.checkout.SignInActivity;
import com.ms.androidapp.presentation.home.HomeActivity;
import com.ms.androidapp.presentation.widget.DialogHelper;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class SplashscreenActivity extends BaseActivity {

    @BindView(R.id.fullscreen_content)
    View contentView;
    DialogHelper dialogHelper;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen);
        ButterKnife.bind(this);
        hide();
        dialogHelper = new DialogHelper(this);
        if(!BuildConfig.DEBUG && isRootedDevice()) {
            dialogHelper.alert(null, getString(R.string.text_device_rooted_error), dialogInterface -> finish());
        } else {
            navigate();
        }
        // Track GA
        App.getInstance().track(AppConstant.GA_TRACKING_SCREEN_NAME.SPLASH.getValue());
    }

    void navigate() {
        final Handler handler = new Handler();
        handler.postDelayed(() -> {
            PreferencesStorageAspire preferencesStorage = new PreferencesStorageAspire(SplashscreenActivity.this);
            boolean profileCreated = preferencesStorage.profileCreated();
            boolean hasForgotPwd = preferencesStorage.hasForgotPwd();
            if(!profileCreated || hasForgotPwd) {
                Intent intent = new Intent(this, SignInActivity.class);
                startActivity(intent);
                finish();
            } else {
                new LoadDataWorker().load(getApplicationContext());
                Intent intent = new Intent(SplashscreenActivity.this, HomeActivity.class);
                startActivity(intent);
                finish();
            }
        }, 3000);
    }

    private void hide() {
        // Hide UI first
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
        if(contentView != null) {
            contentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        }
    }

    private boolean isRootedDevice() {
        boolean found = false;
        String[] places = {"/sbin/su", "/system/bin/su", "/system/xbin/su", "/system/xbin/sudo",
                "/data/local/xbin/su", "/data/local/bin/su",
                "/system/sd/xbin/su", "/system/bin/failsafe/su", "/data/local/su",
                "/magisk/.core/bin/su", "/su/bin/su"};
        for (String where : places) {
            if (new File(where).exists()) {
                found = true;
                break;
            }
        }
        return found;
    }
}