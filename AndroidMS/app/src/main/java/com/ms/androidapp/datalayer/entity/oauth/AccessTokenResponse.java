package com.ms.androidapp.datalayer.entity.oauth;

import com.google.gson.annotations.SerializedName;

/**
 * Created by vinh.trinh on 6/19/2017.
 */

public class AccessTokenResponse {
    @SerializedName("ConsumerKey")
    private String consumerKey;
    @SerializedName("AccessToken")
    private String accessToken;
    @SerializedName("Status")
    private Boolean status;
    @SerializedName("Message")
    private String message;
    @SerializedName("ExpirationTime")
    private String expirationTime;

    public String consumerKey() {
        return consumerKey;
    }

    public String accessToken() {
        return accessToken;
    }

    public Boolean status() {
        return status;
    }

    public String message() {
        return message;
    }

    public String expirationTime() {
        return expirationTime;
    }
}
