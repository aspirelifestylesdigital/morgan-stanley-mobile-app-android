package com.ms.androidapp.presentation.request;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ms.androidapp.R;
import com.ms.androidapp.presentation.base.BaseFragment;

import butterknife.OnClick;

/**
 * Created by vinh.trinh on 5/24/2017.
 */

public class ThankYouFragment extends BaseFragment {

    private ViewEventsListener listener;

    public static ThankYouFragment newInstance() {
        Bundle args = new Bundle();
        ThankYouFragment fragment = new ThankYouFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ViewEventsListener) {
            listener = (ViewEventsListener) context;
        } else {
            throw new ClassCastException(context.toString()
                    + " must implement ThankYouFragment.ViewEventsListener.");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_ask_thankyou, container, false);
    }

    @OnClick(R.id.btn_another)
    public void anotherRequestClick(View view) {
        listener.anotherRequest();
    }

    @OnClick(R.id.btn_explore)
    public void exploreClick(View view) {
        listener.toExplore();
    }

    interface ViewEventsListener {
        void anotherRequest();
        void toExplore();
    }
}
