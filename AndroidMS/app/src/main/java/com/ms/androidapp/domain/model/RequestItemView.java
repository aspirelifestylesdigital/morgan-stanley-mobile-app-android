package com.ms.androidapp.domain.model;

/**
 * Created by vinh.trinh on 8/30/2017.
 */

public class RequestItemView {

    public enum STATUS {PENDING, OPEN, CLOSED}

    public final String title;
    public final String date;
    public final String dateLabel;
    public final int dataListIndex;
    public final STATUS status;

    public RequestItemView(String title, String date, int index, STATUS status) {
        this.title = title;
        this.date = date;
        this.dataListIndex = index;
        this.status = status;
        switch (status) {
            case PENDING:
                dateLabel = "Pending:";
                break;
            case CLOSED:
                dateLabel = "Closed:";
                break;
            default:
                dateLabel = "Last Updated:";
                break;
        }
    }
}
