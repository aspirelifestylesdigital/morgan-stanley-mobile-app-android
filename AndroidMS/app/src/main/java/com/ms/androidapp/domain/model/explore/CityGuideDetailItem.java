package com.ms.androidapp.domain.model.explore;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.ms.androidapp.common.constant.AppConstant;

/**
 * Created by vinh.trinh on 6/8/2017.
 */

public class CityGuideDetailItem extends ExploreRView implements Parcelable {
    public static final Creator<CityGuideDetailItem> CREATOR = new Creator<CityGuideDetailItem>() {
        @Override
        public CityGuideDetailItem createFromParcel(Parcel in) {
            return new CityGuideDetailItem(in);
        }

        @Override
        public CityGuideDetailItem[] newArray(int size) {
            return new CityGuideDetailItem[size];
        }
    };
    public final String title;
    public final String address;
    public final String city;
    public final String state;
    public final String postalCode;
    public final String address3;
    public final String address2;
    public final String url;
    public final String insiderTips;
    public final String description;
    public int imageResId;
    public String subCategoryName;
    public String imageUrl;

    private CityGuideDetailItem(String title, String address, String city, String state, String postalCode,
                                String address3, String address2, String url, String insiderTips, String description, String imageUrl) {
        this.title = title;
        this.address = address;
        this.city = city;
        this.state = state;
        this.postalCode = postalCode;
        this.address3 = address3;
        this.address2 = address2;
        this.url = url;
        this.insiderTips = insiderTips;
        this.description = description;
        this.imageResId = 0;
        subCategoryName = null;
        itemType = ItemType.CITY_GUIDE;
        this.imageUrl = imageUrl;
    }

    protected CityGuideDetailItem(Parcel in) {
        title = in.readString();
        address = in.readString();
        city = in.readString();
        state = in.readString();
        postalCode = in.readString();
        address3 = in.readString();
        address2 = in.readString();
        url = in.readString();
        insiderTips = in.readString();
        description = in.readString();
        imageResId = in.readInt();
        subCategoryName = in.readString();
        itemType = ItemType.CITY_GUIDE;
        this.imageUrl = in.readString();
    }

    public CityGuideDetailItem setImageResId(int imageResId) {
        this.imageResId = imageResId;
        return this;
    }

    public CityGuideDetailItem setSubCategoryName(String subCategoryName) {
        this.subCategoryName = subCategoryName;
        return this;
    }

    public String getDisplayAddress(){
        String result = "";
        if(!TextUtils.isEmpty(address)){
            result += address;
        }
        if(!TextUtils.isEmpty(city)){
            if(!TextUtils.isEmpty(result)){
                result += " ";
            }
            result += city;
        }
        if(!TextUtils.isEmpty(state)){
            if(!TextUtils.isEmpty(result)){
                result += ", ";
            }
            result += state;
        }
        if(!TextUtils.isEmpty(postalCode)){
            if(TextUtils.isEmpty(state)){
               if(!TextUtils.isEmpty(result)){
                   result += ", ";
               }
            }else {
                if (!TextUtils.isEmpty(result)) {
                    result += " ";
                }
            }
            result += postalCode;
            if(!TextUtils.isEmpty(address3)){
                result += " " + address3;
            }
            if(!TextUtils.isEmpty(address2)){
                result += " " + address2;
            }
        }else{
            if(TextUtils.isEmpty(state)){
                if(!TextUtils.isEmpty(result)){
                    result += ", ";
                }
            }else {
                if (!TextUtils.isEmpty(result)) {
                    result += " ";
                }
            }
            if(!TextUtils.isEmpty(address3)){
                result += address3;
            }
            if(!TextUtils.isEmpty(address2)){
                result += " " + address2;
            }
        }
        return result;
    }

    public String getQuerySearchOnGoogleMap(){
        String result = "";
        if(!TextUtils.isEmpty(title)){
            result += title;
        }
        if(!TextUtils.isEmpty(address)){
            if(!TextUtils.isEmpty(result)){
                result += ", ";
            }
            result += address;
        }
        if(!TextUtils.isEmpty(state)){
            if(!TextUtils.isEmpty(result)){
                result += ", ";
            }
            result += state;
        }
        if(!TextUtils.isEmpty(city)){
            if(!TextUtils.isEmpty(result)){
                result += ", ";
            }
            result += city;
        }
        if(!TextUtils.isEmpty(postalCode)){
            if(!TextUtils.isEmpty(result)){
                result += ", ";
            }
            result += postalCode;
        }

        return result;
    }

    public String getQueryBeiJingSearchOnGoogleMap(){
        String result = "";
        if(!TextUtils.isEmpty(title)){
            result += title;
        }
        if(!TextUtils.isEmpty(address)){
            if(!TextUtils.isEmpty(result)){
                result += ", ";
            }
            result += address;
        }
        if(!TextUtils.isEmpty(city)){
            if(!TextUtils.isEmpty(result)){
                result += ", ";
            }
            result += city;
        }

        return result;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(address);
        dest.writeString(city);
        dest.writeString(state);
        dest.writeString(postalCode);
        dest.writeString(address3);
        dest.writeString(address2);
        dest.writeString(url);
        dest.writeString(insiderTips);
        dest.writeString(description);
        dest.writeInt(imageResId);
        dest.writeString(subCategoryName);
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public String getImageUrl() {
        return imageUrl;
    }

    @Override
    public Integer getId() {
        return 0;
    }

    @Override
    public String getSuggestedToAC() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(AppConstant.PREFILL_AC_NAME + title);
        stringBuilder.append("\n" + AppConstant.PREFILL_AC_OTHER_COMMENTS);
        return stringBuilder.toString();
    }

    public static class Builder {

        private String title;
        private String address;
        private String city;
        private String state;
        private String postalCode;
        private String address3;
        private String address2;
        private String url;
        private String description;
        private String imageUrl;
        public Builder(String title, String address, String city, String state, String postalCode,
                       String description) {
            this.title = title;
            this.address = address;
            this.city = city;
            this.state = state;
            this.postalCode = postalCode;
            this.description = description;
        }

        public Builder address3(String val) {
            this.address3 = inspect(val);
            return this;
        }

        public Builder address2(String val) {
            this.address2 = inspect(val);
            return this;
        }

        public Builder url(String val) {
            this.url = inspect(val);
            return this;
        }
        public Builder imageUrl(String imageUrl){
            this.imageUrl = imageUrl;
            return this;
        }
        public CityGuideDetailItem build() {
            StringBuilder descriptBuilder = new StringBuilder(description);
            String insiderTips = insiderTip(descriptBuilder);
            return new CityGuideDetailItem(title, address, city, state, postalCode, address3,
                    address2, url, insiderTips, descriptBuilder.toString(), imageUrl);
        }

        private String insiderTip(StringBuilder description) {
            String insiderTipSeg1 = "<b>Insider tip</b>:";
            String insiderTipSeg2 = "<b>Insider tip:</b>";
            int insiderTipInd = -1;
            if(description.toString().contains(insiderTipSeg1)){
                insiderTipInd = description.toString().indexOf(insiderTipSeg1);
            }else{
                insiderTipInd = description.toString().indexOf(insiderTipSeg2);
            }
            if(insiderTipInd > -1){
                String insiderTip = description.substring(insiderTipInd + insiderTipSeg1.length()).trim().replace("\n", "").replace("\r", "");
                String newDescription = description.substring(0, insiderTipInd).trim().replace("\n", "").replace("\r", "");
                description.delete(0, description.length());
                description.append(newDescription);
                return insiderTip;
            }
            return "";
        }

        private String inspect(String val) {
            if(val == null) return null;
            if(TextUtils.isEmpty(val.trim())) return null;
            return val;
        }
    }
}
