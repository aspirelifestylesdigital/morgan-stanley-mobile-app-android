package com.ms.androidapp.presentation.info;

import com.api.aspire.common.constant.ErrCode;
import com.ms.androidapp.datalayer.entity.GetClientCopyResult;
import com.ms.androidapp.presentation.base.BasePresenter;

/**
 * Created by vinh.trinh on 6/13/2017.
 */

public interface MasterCardUtility {

    interface View {
        void onGetMasterCardCopy(GetClientCopyResult result);
        void loadEmptyContent();
        void showErrorMessage(ErrCode errCode);
    }

    interface Presenter extends BasePresenter<View> {
        void getMasterCardCopy(String type);
    }

}
