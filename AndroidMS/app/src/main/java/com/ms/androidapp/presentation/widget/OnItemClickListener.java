package com.ms.androidapp.presentation.widget;

/**
 * Created by vinh.trinh on 9/13/2017.
 */

public interface OnItemClickListener {
    void onItemClick(int pos);
}
