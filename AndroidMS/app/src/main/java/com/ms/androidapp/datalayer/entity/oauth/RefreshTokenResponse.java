package com.ms.androidapp.datalayer.entity.oauth;

import com.google.gson.annotations.SerializedName;

/**
 * Created by vinh.trinh on 6/19/2017.
 */

public class RefreshTokenResponse {
    @SerializedName("success")
    private String success;
    @SerializedName("message")
    private String message;

    public String getSuccess() {
        return success;
    }

    public String getMessage() {
        return message;
    }
}
