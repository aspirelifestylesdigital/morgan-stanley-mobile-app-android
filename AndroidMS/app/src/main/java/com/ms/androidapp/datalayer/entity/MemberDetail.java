package com.ms.androidapp.datalayer.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by vinh.trinh on 6/19/2017.
 */
public class MemberDetail {
    @Expose
    @SerializedName("VeriCode")
    private String verificationCode;
    @Expose
    @SerializedName("OnlineMemberDetailId")
    private String onlineMemberDetailID;

    public MemberDetail() {
        this.verificationCode = "1001";
    }

    public MemberDetail(String onlineMemberDetailID) {
        this.verificationCode = "1001";
        this.onlineMemberDetailID = onlineMemberDetailID;
    }
}