package com.ms.androidapp.common.constant;

/**
 * Created by tung.phan on 5/12/2017.
 */

public interface APIFieldConstant {

    String LANGUAGE = "language";
    String ITEM_TYPE = "item_type";
    String SORT = "sort";
    String SIZE = "size";
    String PAGE = "page";

}

