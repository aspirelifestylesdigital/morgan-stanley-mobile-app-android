package com.ms.androidapp.datalayer.entity.b2ccontent;

import com.google.gson.annotations.SerializedName;
import com.ms.androidapp.datalayer.entity.GetContentFullResult;

/**
 * Created by Thu Nguyen on 6/5/2017.
 */

public class B2CContentDetailResponse {

    @SerializedName("GetContentFullResult")
    private GetContentFullResult result;

    public GetContentFullResult getResult() {
        return result;
    }

    public void setResult(GetContentFullResult result) {
        this.result = result;
    }
}
