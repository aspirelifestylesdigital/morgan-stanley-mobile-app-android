package com.ms.androidapp.domain.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Admin on 7/29/2017.
 */

public class LatLng implements Parcelable {
    public final double latitude;
    public final double longitude;

    public LatLng(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public static final LatLng INVALID = new LatLng(0, 0);

    protected LatLng(Parcel in) {
        latitude = in.readDouble();
        longitude = in.readDouble();
    }

    public static final Creator<LatLng> CREATOR = new Creator<LatLng>() {
        @Override
        public LatLng createFromParcel(Parcel in) {
            return new LatLng(in);
        }

        @Override
        public LatLng[] newArray(int size) {
            return new LatLng[size];
        }
    };

    public boolean isValid() {
        return this.latitude != INVALID.latitude && this.longitude != INVALID.longitude;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeDouble(latitude);
        parcel.writeDouble(longitude);
    }

    @Override
    public String toString() {
        return latitude + "," + longitude;
    }
}
