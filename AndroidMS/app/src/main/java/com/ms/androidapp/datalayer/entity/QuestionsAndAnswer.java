package com.ms.androidapp.datalayer.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by tung.phan on 6/1/2017.
 */

public class QuestionsAndAnswer {
    @SerializedName("Answers")
    @Expose
    private List<Answer> answers = null;
    @SerializedName("HitCount")
    @Expose
    private Integer hitCount;
    @SerializedName("HotQuestion")
    @Expose
    private Boolean hotQuestion;
    @SerializedName("ID")
    @Expose
    private Integer iD;
    @SerializedName("LastUpdateDate")
    @Expose
    private String lastUpdateDate;
    @SerializedName("QuestionText")
    @Expose
    private String questionText;
    @SerializedName("ShortQuestionText")
    @Expose
    private String shortQuestionText;
    @SerializedName("UserDefined1")
    @Expose
    private String userDefined1;
    @SerializedName("UserDefined2")
    @Expose
    private String userDefined2;

    public List<Answer> getAnswers() {
        return answers;
    }

    public Integer getHitCount() {
        return hitCount;
    }

    public Boolean getHotQuestion() {
        return hotQuestion;
    }

    public Integer getID() {
        return iD;
    }

    public String getLastUpdateDate() {
        return lastUpdateDate;
    }

    public String getQuestionText() {
        return questionText;
    }

    public String getShortQuestionText() {
        return shortQuestionText;
    }

    public String getUserDefined1() {
        return userDefined1;
    }

    public String getUserDefined2() {
        return userDefined2;
    }

}
