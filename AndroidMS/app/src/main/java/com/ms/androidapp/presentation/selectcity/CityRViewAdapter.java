package com.ms.androidapp.presentation.selectcity;

import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ms.androidapp.App;
import com.ms.androidapp.R;
import com.ms.androidapp.common.glide.GlideHelper;
import com.ms.androidapp.domain.model.CityRViewItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;

/**
 * Created by tung.phan on 5/30/2017.
 */

public class CityRViewAdapter
        extends RecyclerView.Adapter<CityRViewAdapter.CityItemViewHolder> {
    private final int expectedImageWidth;
    private final List<CityRViewItem> data;
    private CityRViewAdapterListener listenner;

    CityRViewAdapter(List<CityRViewItem> data, CityRViewAdapterListener listenner) {
        this.data = new ArrayList<>(data);
        this.listenner = listenner;
        expectedImageWidth = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 70, App.getInstance().getResources().getDisplayMetrics());
    }

    public CityRViewItem getItem(int pos) {
        return data.get(pos);
    }

    public void add(CityRViewItem datum) {
        this.data.add(datum);
        notifyDataSetChanged();
    }

    @Override
    public CityItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.city_rview_item, parent, false);
        return new CityItemViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CityItemViewHolder holder, int position) {
        final CityRViewItem cityRViewItem = data.get(position);
        bindView(holder, cityRViewItem);
    }

    private void bindView(CityItemViewHolder holder, CityRViewItem cityRViewItem) {
        holder.cityRegionName.setText(cityRViewItem.getCity());
        holder.StateCountry.setText(cityRViewItem.getState());
        GlideHelper.getInstance().loadImage(cityRViewItem.getImageResources(),
                R.drawable.img_placeholder, holder.image, expectedImageWidth);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public interface CityRViewAdapterListener {
        void onItemClick(int pos);
    }

    class CityItemViewHolder extends RecyclerView.ViewHolder {
        private ImageView image;
        private TextView cityRegionName, StateCountry;
        private RelativeLayout parentView;

        CityItemViewHolder(View view) {
            super(view);
            image = ButterKnife.findById(view, R.id.image);
            cityRegionName = ButterKnife.findById(view, R.id.city_region_name);
            StateCountry = ButterKnife.findById(view, R.id.state_country);
            parentView = ButterKnife.findById(view, R.id.parent_view);
            parentView.setOnClickListener(v -> listenner.onItemClick(getAdapterPosition()));
        }
    }
}
