package com.ms.androidapp.datalayer.entity.askconcierge;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

/**
 * Created by vinh.trinh on 5/16/2017.
 */

public class ACResponse {
    @SerializedName("transactionId")
    String transactionId;
    @SerializedName("status")
    Status status;

    public String getTransactionId() {
        return transactionId;
    }

    public Status getStatus() {
        return status;
    }

    public class Status {
        @SerializedName("success")
        String success;
        @SerializedName("message")
        String message;

        public boolean isSuccess() {
            return (!TextUtils.isEmpty(success) && (success.equalsIgnoreCase("success")
                    || success.equalsIgnoreCase("true") || success.equalsIgnoreCase("yes")));
        }

        public String getMessage() {
            return message;
        }
    }

    public static ACResponse empty() {
        return new ACResponse();
    }
}