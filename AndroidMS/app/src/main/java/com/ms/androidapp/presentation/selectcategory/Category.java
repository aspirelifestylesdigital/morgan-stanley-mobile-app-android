package com.ms.androidapp.presentation.selectcategory;

import android.content.Context;

import com.ms.androidapp.presentation.base.BasePresenter;


/**
 * Created by tung.phan on 5/31/2017.
 */

public interface Category {

    interface View {
        void proceedWithDiningCategory();
        void askForLocationSetting();
    }

    interface Presenter extends BasePresenter<Category.View> {

        void loadProfile();
        boolean isLocationProfileSettingOn();
        void locationServiceCheck(Context context);
    }
}
