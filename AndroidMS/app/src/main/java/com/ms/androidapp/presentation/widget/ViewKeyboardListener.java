package com.ms.androidapp.presentation.widget;

import android.graphics.Rect;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.EditText;

public class ViewKeyboardListener implements ViewTreeObserver.OnGlobalLayoutListener {

    private View rootView;
    private KeyboardEvent event;

    private boolean isKeyboardShow;

    /**
     * @param rootView: the view root of addOnGlobalLayout
     * @param event: callback after handle event keyboard
     **/
    public ViewKeyboardListener(View rootView, KeyboardEvent event) {
        this.rootView = rootView;
        this.event = event;
        this.rootView.getViewTreeObserver().addOnGlobalLayoutListener(this);
    }

    public void setRootViewFocusChangeListener(){
        this.rootView.setOnFocusChangeListener((view, focus) -> {
            if(focus){
                ViewUtils.hideSoftKey(view);
            }
        });
    }

    public void setViewFocusChangeListener(View viewFocus){
        viewFocus.setOnFocusChangeListener((view, focus) -> {
            if(focus){
                ViewUtils.hideSoftKey(view);
            }
        });
    }

    public void removeListener(){
        this.rootView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
    }

    @Override
    public void onGlobalLayout() {
        Rect r = new Rect();
        rootView.getWindowVisibleDisplayFrame(r);
        int screenHeight = rootView.getRootView().getHeight();

        int keypadHeight = screenHeight - r.bottom;
        if (keypadHeight > screenHeight * 0.15) {
            if (!isKeyboardShow) {
                isKeyboardShow = true;
                keyboardFocus(event.getCurrentFocus(),true);
                // keyboard is opened
                event.showKeyboard();
            }
        } else {
            // keyboard is closed
            if (isKeyboardShow) {
                isKeyboardShow = false;
                keyboardFocus(event.getCurrentFocus(),false);
                event.hideKeyboard();
            }
        }
    }

    /** @param isKeyboardShow: true: keyboard showing, otherwise keyboard hide*/
    private void keyboardFocus(final View viewCurrentFocus, final boolean isKeyboardShow){
        if (viewCurrentFocus instanceof EditText) {
            if (isKeyboardShow) {
                ((EditText) viewCurrentFocus).setCursorVisible(true);
            }else{
                viewCurrentFocus.clearFocus();
            }
        }
    }

    public interface KeyboardEvent {
        void showKeyboard();
        void hideKeyboard();
        View getCurrentFocus();
    }
}
