package com.ms.androidapp.common.logic;

/**
 * Created by ThuNguyen on 6/29/2017.
 */

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LinkGetter {
    private Pattern htmlTag;

    public LinkGetter() {
        htmlTag = Pattern.compile("<a\\b[^>]*href=\"[^>]*>(.*?)</a>");
    }

    public String removeInvalidHyperlink(String htmlText) {
        StringBuilder builder = new StringBuilder();
        builder.append(htmlText);
        String result = builder.toString();

        Matcher tagmatch = htmlTag.matcher(builder.toString());
        while (tagmatch.find()) {
            if(!tagmatch.group().contains("http")){
                int firstFoundIndex = result.indexOf(tagmatch.group());
                if(firstFoundIndex > -1){
                    result = result.substring(0, firstFoundIndex);
                }else {
                    result = result.replace(tagmatch.group(), "");
                }
            }
        }

        return result;
    }

}
