package com.ms.androidapp.presentation.base;

import android.content.Context;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.ms.androidapp.R;
import com.ms.androidapp.presentation.widget.ViewUtils;
import com.support.mylibrary.widget.swipeback.SwipeBackFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by YoKeyword on 16/4/21.
 */
public class BaseSwipeBackFragment extends SwipeBackFragment implements SwipeBackFragment.SwipeBackFragmentEvent {

    private Unbinder unbinder;
    protected OnAddFragmentListener mAddFragmentListener;

    @BindView(R.id.toolbar)
    protected Toolbar toolbar;
    @BindView(R.id.title)
    protected AppCompatTextView title;
    @BindView(android.R.id.home)
    protected AppCompatTextView back;

    final View.OnLayoutChangeListener onLayoutChangeListener = new View.OnLayoutChangeListener() {
        @Override
        public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft,
                                   int oldTop, int oldRight, int oldBottom) {
            View homeUpView = toolbar.getChildAt(toolbar.getChildCount()-1);
            if(homeUpView == null) return;
            int[] xy = new int[2];
            homeUpView.getLocationOnScreen(xy);
            Rect homeUpRect = new Rect(xy[0], xy[1], xy[0] + homeUpView.getWidth(), xy[1] + homeUpView.getHeight());
            if(mAddFragmentListener != null){
                mAddFragmentListener.setRectButtonBack(homeUpRect);
            }
            v.removeOnLayoutChangeListener(this);
        }
    };

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnAddFragmentListener) {
            mAddFragmentListener = (OnAddFragmentListener) context;
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        unbinder = ButterKnife.bind(this, view);
        setSwipeBackFragmentEvent(this);

        setup(view);
    }

    @Override
    public void onDestroy() {
        unbinder.unbind();
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mAddFragmentListener = null;
    }

    @Override
    public int getDefaultFragmentBackground() {
        return R.color.ms_colorPrimary;
    }

    protected void setup(View view) {
        ButterKnife.bind(this, view);
        if(toolbar == null) toolbar = view.findViewById(R.id.toolbar);
        if(title == null) title = view.findViewById(R.id.title);
        toolbar.setContentInsetsAbsolute(0, 0);
        toolbar.setContentInsetStartWithNavigation(0);
        toolbar.setContentInsetEndWithActions(0);

        Drawable icBack = ContextCompat.getDrawable(getContext(), R.drawable.ic_arrow_left);
        ViewUtils.menuTintColors(getContext(), icBack);
        toolbar.setNavigationIcon(icBack);
        toolbar.setNavigationOnClickListener(v -> {
            if(getActivity() instanceof BaseSwipeBackActivity){
                getActivity().onBackPressed();
            }
        });

        toolbar.setOnClickListener(ViewUtils::hideSoftKey);
        toolbar.addOnLayoutChangeListener(onLayoutChangeListener);

        if(Build.VERSION.SDK_INT >= 21) {
            toolbar.setElevation(0);
            toolbar.setStateListAnimator(null);
        }
    }

    public void setTitle(CharSequence title) {
        this.title.setText(title);
    }

    public void setTitle(int titleId) {
        this.title.setText(getString(titleId));
    }

    public interface OnAddFragmentListener {
        void onAddFragment(Fragment toFragment);
        void setRectButtonBack(Rect homeUpRect);
    }
}
