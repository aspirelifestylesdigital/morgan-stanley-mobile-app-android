package com.ms.androidapp.domain.usecases;

import android.text.TextUtils;

import com.ms.androidapp.datalayer.entity.askconcierge.ACRequest;
import com.ms.androidapp.datalayer.entity.askconcierge.ACResponse;
import com.ms.androidapp.domain.model.Metadata;
import com.ms.androidapp.domain.model.Profile;
import com.ms.androidapp.domain.repository.ACRepository;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by vinh.trinh on 5/17/2017.
 */

public class CreateRequest extends UseCase<ACResponse, CreateRequest.Params> {

    public enum EDIT_TYPE { AMEND, CANCEL }
    private ACRepository repository;

    public CreateRequest(ACRepository repository) {
        this.repository = repository;
    }

    @Override
    Observable<ACResponse> buildUseCaseObservable(Params params) {
        return null;
    }

    @Override
    Single<ACResponse> buildUseCaseSingle(Params params) {
        return Single.create(e -> {
//            ACRequest requestBody = createRequestBody(params);
            ACRequest requestBody = requestBody(params);
            Call<ACResponse> request = repository.createNewConciergeCase(requestBody);
            Response<ACResponse> response = request.execute();
            if(response.isSuccessful()) {
                e.onSuccess(response.body());
            } else {
                e.onError(new Exception(""));
            }
        });
    }

    @Override
    Completable buildUseCaseCompletable(Params params) {
        return null;
    }

    public Single<ACResponse> execute(Params params) {
        return params.profile == null ? Single.just(ACResponse.empty()) : buildUseCaseSingle(params);
    }

    private ACRequest requestBody(Params params) {
        /*return TextUtils.isEmpty(params.transactionID)? new ACRequest(params.accessToken, params.metadata.onlineMemberID, params.profile,
                params.content.content, buildPrefResponse(params)) :
                new ACRequest(params.profile, params.transactionID, params.metadata.onlineMemberID,
                        params.profile, params.content.content, buildPrefResponse(params), params.editType);*/
        return new ACRequest(
                params.profile,
                params.metadata.onlineMemberID,
                buildMessage(params),
                params.city,
                params.type);
    }
    private String buildMessage(Params params) {
        StringBuilder stringBuilder = new StringBuilder();

        if (!params.email && params.phone)
            stringBuilder.append("Respond by ").append(params.profile.getPhone());
        if (!params.phone && params.email)
            stringBuilder.append("Respond by ").append(params.profile.getEmail());
        if (params.email && params.phone )
            stringBuilder.append("Respond by ").append(params.profile.getPhone()).append(" or ").append(params.profile.getEmail());

        // Append category name or city name
        if(!TextUtils.isEmpty(params.city)){
            stringBuilder.append("  \t\r\n").append(params.city);
        }
        // Append sub-category or category name
        if(!TextUtils.isEmpty(params.type)){
            stringBuilder.append("  \t\r\n").append(params.type);
        }
        // Append original content finally
        stringBuilder.append("  \t\r\n").append(params.content);


        return stringBuilder.toString();
    }

    public static final class Params {
        private final Profile profile;
        private final Metadata metadata;
        private final String content;
        private final String city;
        private final String type;

        private final boolean email;
        private final boolean phone;

        public Params(Profile profile, Metadata metadata, String content, String city, String type,
                      boolean email, boolean phone) {
            this.profile = profile;
            this.metadata = metadata;
            this.content = content;
            this.city = city;
            this.type = type;
            this.email = email;
            this.phone = phone;
        }
    }
}
