package com.ms.androidapp.datalayer.entity.askconcierge;

import org.json.JSONObject;

/**
 * Created by vinh.trinh on 8/29/2017.
 */

public class ACRequestItem {
//    @SerializedName("RowNumber")
    private Integer rowNumber;
//    @SerializedName("EPCCaseID")
    private String EPCCaseID;
//    @SerializedName("TransactionID")
    private String transactionID;
//    @SerializedName("PREFRESPONSE")
    private String prefResponse;
//    @SerializedName("PHONENUMBER")
//    private String phone;
//    @SerializedName("EMAILADDRESS1")
//    private String email;
//    @SerializedName("REQUESTSTATUS")
    private String status;
//    @SerializedName("REQUESTTYPE")
    private String type;

    private String functionality;
//    @SerializedName("CITY")
//    private String city;
//    @SerializedName("COUNTRY")
//    private String country;
//    @SerializedName("EVENTDATE")
    private String eventDate;
//    @SerializedName("PICKUPDATE")
    private String pickupDate;
//    @SerializedName("STARTDATE")
    private String startDate;
//    @SerializedName("ENDDATE")
    private String endDate;
//    @SerializedName("CREATEDDATE")
    private String createdDate;
//    @SerializedName("REQUESTMODE")
    private String mode;
    private String city;
    private String state;
    private String country;
//    @SerializedName("REQUESTDETAILS")
    private String details;
    private Integer children;
    private Integer adults;

    public ACRequestItem(JSONObject json) {
        rowNumber = json.optInt("RowNumber", -1);
        EPCCaseID = json.optString("EPCCaseID");
        transactionID = json.optString("TransactionID");
        prefResponse = json.optString("PREFRESPONSE");
        status = json.optString("REQUESTSTATUS");
        type = json.optString("REQUESTTYPE");
        functionality = json.optString("FUNCTIONALITY");
        eventDate = json.optString("EVENTDATE");
        pickupDate = json.optString("PICKUPDATE");
        startDate = json.optString("STARTDATE");
        endDate = json.optString("ENDDATE");
        createdDate = json.optString("CREATEDDATE");
        mode = json.optString("REQUESTMODE");
        city = json.optString("CITY");
        state = json.optString("STATE");
        country = json.optString("COUNTRY");
        details = json.optString("REQUESTDETAILS");
        children = json.optInt("NUMBEROFCHILDRENS");
        adults = json.optInt("NUMBEROFADULTS");
    }

    public Integer rowNumber() {
        return rowNumber;
    }

    public String EPCCaseID() {
        return EPCCaseID;
    }

    public String transactionID() {
        return transactionID;
    }

    public String prefResponse() {
        return prefResponse;
    }

    public String status() {
        return status;
    }

    public String type() {
        return type;
    }

    public String functionality(){return functionality;}

    public String eventDate() {
        return eventDate;
    }

    public String pickupDate() {
        return pickupDate;
    }

    public String startDate() {
        return startDate;
    }

    public String endDate() {
        return endDate;
    }

    public String createdDate() {
        return createdDate;
    }

    public String mode() {
        return mode;
    }

    public String details() {
        return details;
    }

    public Integer numberOfChildren() {
        return children;
    }

    public Integer numberOfAdults() {
        return adults;
    }

    public String city(){return city;}
    public String state(){return state;}
    public String country(){return country;}
}
