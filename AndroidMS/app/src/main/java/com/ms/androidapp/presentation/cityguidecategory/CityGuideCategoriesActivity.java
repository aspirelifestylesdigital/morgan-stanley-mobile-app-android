package com.ms.androidapp.presentation.cityguidecategory;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.ms.androidapp.R;
import com.ms.androidapp.common.constant.IntentConstant;
import com.ms.androidapp.common.constant.ResultCode;
import com.ms.androidapp.domain.model.SubCategoryItem;
import com.ms.androidapp.presentation.base.CommonActivity;
import com.ms.androidapp.presentation.selectcity.DividerItemDecoration;

import java.util.List;

import butterknife.BindView;

import static com.ms.androidapp.common.constant.IntentConstant.CATEGORY_ID;

/**
 * Created by vinh.trinh on 5/10/2017.
 */

public class CityGuideCategoriesActivity extends CommonActivity implements
        SubCategoryAdapter.OnCategoryItemClickListener, SubCategory.View {
    @BindView(R.id.container)
    View container;
    @BindView(R.id.selection_recycle_view)
    RecyclerView subCategoryRView;
    @BindView(R.id.loading)
    ProgressBar loading;
    private SubCategoryAdapter subCategoryAdapter;
    private SubCategoryPresenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.common_recyclerview_activity_layout);
        setTitle(R.string.city_guide_category_title);
        container.setBackgroundColor(Color.WHITE);
        presenter = new SubCategoryPresenter();
        presenter.attach(this);
//        int padding = Utils.dip2px(this, 20);
//        subCategoryRView.setPadding(padding, padding, padding, 0);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        subCategoryRView.setLayoutManager(layoutManager);
        /*DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(subCategoryRView.getContext(),
                DividerItemDecoration.VERTICAL);
        dividerItemDecoration.setDrawable(getResources().getDrawable(R.drawable.divider_drawable));
        subCategoryRView.addItemDecoration(dividerItemDecoration);*/
        subCategoryRView.addItemDecoration(new DividerItemDecoration(this));
        initIntentExtra();
    }

    private void initIntentExtra() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null && bundle.getInt(CATEGORY_ID) != 0) {
            presenter.getSubCategories(bundle.getInt(CATEGORY_ID));
        }
    }

    @Override
    public void onItemClick(int index) {
        SubCategoryItem item = subCategoryAdapter.getItem(index);
        Intent intent = new Intent();
        intent.putExtra(IntentConstant.INDEX_CITY_GUIDE_CATEGORY_SELECT, index);
        intent.putExtra(IntentConstant.SUB_CATEGORY_NAME, item.getSubCategoryName());
        intent.putExtra(IntentConstant.SUB_CATEGORY_IMAGE, item.getImageResources());
        setResult(ResultCode.RESULT_OK, intent);
        finish();
    }

    @Override
    protected void onDestroy() {
        presenter.detach();
        super.onDestroy();
    }

    @Override
    public void updateSubCategoryRViewAdapter(List<SubCategoryItem> subCategoryItems) {
        if (subCategoryAdapter == null) {
            subCategoryAdapter = new SubCategoryAdapter(this, subCategoryItems);
            subCategoryRView.setAdapter(subCategoryAdapter);
            subCategoryAdapter.setListener(this);
        } else {
            subCategoryAdapter.swapData(subCategoryItems);
        }
    }

    @Override
    public void showLoading() {
        loading.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        loading.setVisibility(View.GONE);
    }
}
