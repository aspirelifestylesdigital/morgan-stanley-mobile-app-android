package com.ms.androidapp.datalayer.entity.search;

import com.google.gson.annotations.SerializedName;
import com.ms.androidapp.BuildConfig;

import java.util.Arrays;
import java.util.List;

/**
 * Created by vinh.trinh on 6/22/2017.
 */

public class SearchRequest {
    @SerializedName("subDomain")
    public final String subDomain;
    @SerializedName("password")
    public final String secretKey;
    @SerializedName("searchString")
    public final String searchString;
    @SerializedName("maxResults")
    private int maxResults;
    @SerializedName("pageSize")
    private int pageSize;
    @SerializedName("pageNumber")
    private int pageNumber;
    @SerializedName("cities")
    private List<String> cities;

    public SearchRequest(String term) {
        subDomain = BuildConfig.WS_B2C_SUBDOMAIN;
        secretKey = BuildConfig.WS_B2C_SECRET;
        this.searchString = term;
    }

    public void setMaxResults(int maxResults) {
        this.maxResults = maxResults;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public void setCities(String... cities) {
        this.cities = Arrays.asList(cities);
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }
}
