package com.ms.androidapp.presentation.requestdetail;

import android.text.TextUtils;
import android.util.Pair;

import com.ms.androidapp.domain.model.RequestDetailData;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

/**
 * Created by vinh.trinh on 9/19/2017.
 */

public class TextDetailBuilder {

    private RequestDetailData data;
    private JSONObject jsonDetail;
    private SimpleDateFormat dateParser;
    private SimpleDateFormat datePrinter;
    private SimpleDateFormat simpleDatePrinter;
    private List<String> nameKeys;
    private boolean ignoreDate;

    public TextDetailBuilder() {
        nameKeys = Arrays.asList("restaurantname", "hotelname", "golfcoursename",
                "eventname", "tourname", "cruisename");
    }

    TextDetailBuilder(RequestDetailData data, JSONObject jsonData, boolean ignoreDate) {
        this.data = data;
        this.jsonDetail = jsonData;
        dateParser = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        datePrinter = new SimpleDateFormat("MM/dd/yyyy | h:mm a", Locale.US);
        simpleDatePrinter = new SimpleDateFormat("MM/dd/yyyy", Locale.US);
        this.ignoreDate = ignoreDate;
    }

    String dining() {
        List<Pair<String, String>> map = new ArrayList<>();

        String name = getDetailValue("restaurantname");
        if(!TextUtils.isEmpty(name)) map.add(new Pair<>("Name", name));
        eventDate(map);
        childrenAndAdults(map);
        location(map);
        comments(map);
        return buildString(map);
    }

    String hotel() {
        List<Pair<String, String>> map = new ArrayList<>();

        String name = getDetailValue("hotelname");
        if(!TextUtils.isEmpty(name)) map.add(new Pair<>("Name", name));
        map.add(new Pair<>("Check in", simpleDate(data.startDate)));
        map.add(new Pair<>("Check out", simpleDate(data.endDate)));
        childrenAndAdults(map);
        location(map);
        comments(map);
        return buildString(map);
    }

    String golf() {
        List<Pair<String, String>> map = new ArrayList<>();

        String name = getDetailValue("golfcoursename");
        if(!TextUtils.isEmpty(name)) map.add(new Pair<>("Name", name));
        map.add(new Pair<>("Date", formatDate(data.startDate)));
        map.add(new Pair<>("# of golfers", String.valueOf(data.numberOfAdults)));
        location(map);
        comments(map);
        return buildString(map);
    }

    String carRental() {
        List<Pair<String, String>> map = new ArrayList<>();

        String name = getDetailValue("drivername");
        if(!TextUtils.isEmpty(name)) map.add(new Pair<>("Driver", name));
        map.add(new Pair<>("Pickup date", formatDate(data.pickupDate)));
        map.add(new Pair<>("Pick up location", getDetailValue("picklocation")));
        map.add(new Pair<>("Drop off location", getDetailValue("droplocation")));
        numberInParty(map);
        comments(map);
        return buildString(map);
    }

    String entertainment() {
        List<Pair<String, String>> map = new ArrayList<>();

        map.add(new Pair<>("Name", getDetailValue("eventname")));
        map.add(new Pair<>("Date", formatDate(data.eventDate)));
        numberOfTickets(map);
        location(map);
        comments(map);
        return buildString(map);
    }

    String tour() {
        List<Pair<String, String>> map = new ArrayList<>();

        String name = getDetailValue("tourname");
        if(!TextUtils.isEmpty(name)) map.add(new Pair<>("Name", name));
        map.add(new Pair<>("Start date", formatDate(data.startDate)));
        map.add(new Pair<>("End date", formatDate(data.endDate)));
        map.add(new Pair<>("Start location", getDetailValue("startlocation")));
        map.add(new Pair<>("End location", getDetailValue("endlocation")));
        childrenAndAdults(map);
        map.add(new Pair<>("# of infants", getNumberValue("numberofinfants")));

        comments(map);
        return buildString(map);
    }

    String flight() {
        List<Pair<String, String>> map = new ArrayList<>();
        map.add(new Pair<>("Start date", formatDate(data.startDate)));
        map.add(new Pair<>("End date", formatDate(data.endDate)));
        map.add(new Pair<>("Start location", getDetailValue("startlocation")));
        map.add(new Pair<>("End location", getDetailValue("endlocation")));
        childrenAndAdults(map);
        map.add(new Pair<>("# of infants", getNumberValue("numberofinfants")));
        comments(map);
        return buildString(map);
    }

    String flower() {
        List<Pair<String, String>> map = new ArrayList<>();

        map.add(new Pair<>("Arrangement", getDetailValue("flowertype")));
        map.add(new Pair<>("Delivery date", formatDate(data.eventDate)));
        map.add(new Pair<>("Delivery address", getDetailValue("daddress")));
        map.add(new Pair<>("Quantity", getNumberValue("bouquetquantity")));
        comments(map);
        return buildString(map);
    }

    String privateJet() {
        List<Pair<String, String>> map = new ArrayList<>();
        map.add(new Pair<>("Start date", formatDate(data.startDate)));
        map.add(new Pair<>("Start location", getDetailValue("startlocation")));
        map.add(new Pair<>("End location", getDetailValue("endlocation")));
        map.add(new Pair<>("# of passengers", getNumberValue("noticket")));
        comments(map);
        return buildString(map);
    }

    String cruise() {
        List<Pair<String, String>> map = new ArrayList<>();

        String name = getDetailValue("cruisename");
        if(!TextUtils.isEmpty(name)) map.add(new Pair<>("Name", name));
        map.add(new Pair<>("Start date", simpleDate(data.startDate)));
        map.add(new Pair<>("End date", simpleDate(data.endDate)));
        map.add(new Pair<>("Start location", getDetailValue("startlocation")));
        map.add(new Pair<>("End location", getDetailValue("endlocation")));
        childrenAndAdults(map);
        map.add(new Pair<>("# of infants", getNumberValue("numberofinfants")));
        comments(map);
        return buildString(map);
    }

    String others() {
        /*List<Pair<String, String>> map = new ArrayList<>();
        comments(map);*/
        return "Other comments: Common Message";
    }

    String vacationPackage() {
        return "";
    }

    private String buildString(List<Pair<String, String>> map) {
        StringBuilder sb = new StringBuilder();
        for (Pair<String, String> val : map) {
            sb.append(val.first).append(": ").append(val.second).append("\n");
        }
        return sb.toString();
    }

    String nameMapping(String type) {
        switch (type.toUpperCase()) {
            case "D RESTAURANT":
                return "Restaurant Request";
            case "T HOTEL AND B&B":
                return "HOTEL AND B&B Request";
            case "S GOLF":
                return "Golf Request";
            case "T LIMO AND SEDAN":
                return "Limo and Sedan Request";
            case "E CONCERT/THEATER":
                return "Entertainment";
            case "C SIGHTSEEING/TOURS":
                return "Sightseeing/Tours Request";
            case "I AIRPORT SERVICES":
                return "Airport Services Request";
            case "T CRUISE":
                return "Cruise Request";
            case "G FLOWERS/GIFT BASKET":
                return "Flowers/Gift Basket Request";
            case "T OTHER":
                return "Private Jet Request";
        }
        return "Other Request";
    }

    public String getName(String type, String detail) throws JSONException {
        JSONObject jsonDetail = new JSONObject();
        String[] data = detail.split("\\|");
        for (String d : data) {
            String datum[] = d.split(":", 2);
            if (datum.length == 2) {
                jsonDetail.put(datum[0].toLowerCase().trim(), datum[1]);
            }
        }
        String nameSuffix = null;
        Iterator<String> jsonKeys = jsonDetail.keys();
        while (jsonKeys.hasNext()) {
            String key = jsonKeys.next();
            if(nameKeys.contains(key)) {
                nameSuffix = jsonDetail.getString(key);
                break;
            }
        }

        return TextUtils.isEmpty(nameSuffix) ? nameMapping(type) :
                nameMapping(type) + " - " + nameSuffix;
    }

    private void childrenAndAdults(List<Pair<String, String>> map) {
        map.add(new Pair<>("# of adults", String.valueOf(data.numberOfAdults)));
        map.add(new Pair<>("# of children", String.valueOf(data.numberOfChildren)));
    }

    private void location(List<Pair<String, String>> map) {
        map.add(new Pair<>("City", data.city));
        map.add(new Pair<>("State", data.state));
        map.add(new Pair<>("Country", data.country));
    }

    private void numberInParty(List<Pair<String, String>> map) {
        map.add(new Pair<>("# in party", String.valueOf(data.numberOfAdults)));
    }

    private void numberOfTickets(List<Pair<String, String>> map) {
        map.add(new Pair<>("# in tickets", String.valueOf(data.numberOfAdults)));
    }

    private void eventDate(List<Pair<String, String>> map) {
        map.add(new Pair<>("Date/time", ignoreDate ? "" : formatDate(data.eventDate)));
    }

    private void comments(List<Pair<String, String>> map) {
        map.add(new Pair<>("Other comments", getDetailValue("specialreq")));
    }

    private String getDetailValue(String key) {
        return jsonDetail.optString(key, "");
    }

    private String getNumberValue(String key) {
        return jsonDetail.optString(key, "0");
    }

    private String formatDate(String val) {
        if(ignoreDate) return "";
        Date date;
        try {
            date = dateParser.parse(val);
            return datePrinter.format(date);
        } catch (ParseException e) {
            return "";
        }
    }

    private String simpleDate(String val) {
        if(ignoreDate) return "";
        Date date;
        try {
            date = dateParser.parse(val);
            return simpleDatePrinter.format(date);
        } catch (ParseException e) {
            return "";
        }
    }
}
