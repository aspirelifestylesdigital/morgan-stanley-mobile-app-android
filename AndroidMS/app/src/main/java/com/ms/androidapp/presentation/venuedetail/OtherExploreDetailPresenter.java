package com.ms.androidapp.presentation.venuedetail;

import com.ms.androidapp.App;
import com.ms.androidapp.domain.model.explore.OtherExploreDetailItem;
import com.ms.androidapp.domain.usecases.GetContentFull;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by vinh.trinh on 6/13/2017.
 */

public class OtherExploreDetailPresenter implements OtherExploreDetail.Presenter {

    private GetContentFull getContentFull;
    private OtherExploreDetail.View view;
    private CompositeDisposable disposables;

    OtherExploreDetailPresenter(GetContentFull getContentFull) {
        disposables = new CompositeDisposable();
        this.getContentFull = getContentFull;
    }


    @Override
    public void attach(OtherExploreDetail.View view) {
        this.view = view;
    }

    @Override
    public void detach() {
        disposables.dispose();
        this.view = null;
    }

    @Override
    public void getContent(Integer contentId) {
        if(!App.getInstance().hasNetworkConnection()) {
            view.onUpdateFailed();
            view.noInternetMessage();
            return;
        }
        disposables.add(getContentFull.param(new GetContentFull.Params(contentId))
                .on(Schedulers.io(), AndroidSchedulers.mainThread())
                .execute(new OtherExploreDetailPresenter.GetContentFullObserver()));
    }

    private final class GetContentFullObserver extends DisposableSingleObserver<OtherExploreDetailItem> {

        @Override
        public void onSuccess(OtherExploreDetailItem exploreRViewItem) {
            view.onGetContentFullFinished(exploreRViewItem);
            dispose();
        }

        @Override
        public void onError(Throwable e) {
            view.onUpdateFailed();
            dispose();
        }
    }
}
