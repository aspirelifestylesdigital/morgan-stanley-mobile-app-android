package com.ms.androidapp.presentation.widget;

import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.widget.ListPopupWindow;
import android.widget.TextView;

import com.ms.androidapp.R;

import static com.ms.androidapp.presentation.widget.ViewUtils.convertDpToPx;

/**
 * Created by anh.trinh on 8/8/2017.
 */

public class CustomSpinner {
    private TextView anchorView;
    private ListPopupWindow listPopupWindow;
    private Context context;
    private DropdownAdapter dropdownAdapter;
    private int selectionPosition;

    private int colorDefaultUnSelect = Color.parseColor("#99A1A8");
    private int colorSelected = Color.parseColor("#011627");

    public boolean isNone() {
        return isNone;
    }

    public void setNone(final boolean none) {
        isNone = none;
        this.dropdownAdapter.setNone(isNone);
        this.dropdownAdapter.notifyDataSetChanged();
    }

    boolean isNone = true;

    public int getSelectionPosition() {
        return selectionPosition;
    }

    public int getTextPosition(String text) {
        if (dropdownAdapter == null || dropdownAdapter.getListItems() == null) return -1;

        for (int i = 0; i < dropdownAdapter.getListItems().size(); i++) {
            String question = dropdownAdapter.getListItems().get(i);
            if (question.equalsIgnoreCase(text)) {
                return i;
            }
        }
        return -1;
    }

    public String getTextSelectedSafe() {
        if (dropdownAdapter == null
                || dropdownAdapter.getListItems() == null
                || getSelectionPosition() == -1) {
            return "";
        } else {
            return dropdownAdapter.getListItems().get(getSelectionPosition());
        }
    }

    public void setSelectionPosition(final int selectionPosition) {
        if (selectionPosition == -1) return;

        this.selectionPosition = selectionPosition;
        this.listPopupWindow.setSelection(this.selectionPosition);
        this.dropdownAdapter.setCurrentSelected(selectionPosition);
        if (selectionPosition == 0 && isNone) {
            anchorView.setTextColor(colorDefaultUnSelect);
        } else {
            anchorView.setTextColor(colorSelected);
        }
        String val = dropdownAdapter.getItem(selectionPosition);
        val = TextUtils.isEmpty(val) ? "" : val;
        anchorView.setText(textModifier == null ? val : textModifier.modify(val));
    }

    /*private String countryCodeText(String val) {
        int last = val.lastIndexOf(')');
        int first = val.indexOf('(');
        if(first > -1 && last > -1) {
            int firstCloseParenthese = val.indexOf(')');
            return val.substring(first+1, firstCloseParenthese) + val.substring(firstCloseParenthese+1);
        }
        return val;
    }*/

    public interface SpinnerListener {
        void onArchorViewClick();

        void onItemSelected(int index);
    }

    public interface TextModifier {
        String modify(String val);
    }

    public void setTextModifier(TextModifier tm) {
        this.textModifier = tm;
    }

    public void setmSpinnerListener(final SpinnerListener mSpinnerListener) {
        this.mSpinnerListener = mSpinnerListener;
    }

    private TextModifier textModifier;
    private SpinnerListener mSpinnerListener;

    public CustomSpinner(Context context,
                         final TextView anchorView) {
        this.context = context;
        this.anchorView = anchorView;
        listPopupWindow = new ListPopupWindow(anchorView.getContext());
        anchorView.setOnClickListener(view -> {
            if (mSpinnerListener != null) {
                mSpinnerListener.onArchorViewClick();
            }
            if(listPopupWindow.isShowing()){
                listPopupWindow.dismiss();
                anchorView.setBackgroundResource(R.drawable.bg_spinner_hide);
            }else {
                listPopupWindow.show();
                anchorView.setBackgroundResource(R.drawable.bg_spinner_show);
            }
        });
        listPopupWindow.setModal(true);
        listPopupWindow.setAnchorView(anchorView);
        listPopupWindow.setOnDismissListener(() -> anchorView.setBackgroundResource(R.drawable.bg_spinner_hide));
        listPopupWindow.setOnItemClickListener((adapterView, view, i, l) -> {
            if (mSpinnerListener != null) {
                mSpinnerListener.onItemSelected(i);
            }
            if (i == 0 &&isNone) {
                anchorView.setTextColor(colorDefaultUnSelect);
            } else {
                anchorView.setTextColor(colorSelected);
            }
            String val = adapterView.getItemAtPosition(i).toString();
            anchorView.setText(textModifier == null ? val : textModifier.modify(val));
            selectionPosition = i;
            dropdownAdapter.setCurrentSelected(i);
            listPopupWindow.dismiss();

        });
    }

    public void setPobpupHeight(int popupHeight) {
        listPopupWindow.setHeight(convertDpToPx(popupHeight, context));
    }

    public void setPopupHeightListQuestion() {
        if(dropdownAdapter != null
                && dropdownAdapter.getListItems() != null){
            int dataSize = dropdownAdapter.getListItems().size();
            int maxHeight = dataSize * convertDpToPx(52, context);
            if (maxHeight < 245) {
                listPopupWindow.setHeight(maxHeight);
            } else {
                listPopupWindow.setHeight(convertDpToPx(245, context));
            }
        }
    }
    public void setDefaultUnSelect(String textHint){
        anchorView.setText(textHint);
        anchorView.setTextColor(colorDefaultUnSelect);
    }

    public void setPopupHeightPx(int popupHeightPx) {
        listPopupWindow.setHeight(popupHeightPx);
    }

    public DropdownAdapter getDropdownAdapter() {
        return dropdownAdapter;
    }

    public void setDropdownAdapter(final DropdownAdapter dropdownAdapter) {
        this.dropdownAdapter = dropdownAdapter;
        this.dropdownAdapter.setNone(isNone);
        listPopupWindow.setAdapter(dropdownAdapter);
    }

    /* calculate follow pixel */
    public void setConfigHeight(int totalDataSize){
        if (context == null) {
            return;
        }
        /* total item show on spinner */
        final int totalItemShow = 5;

        final int heightItem = context.getResources().getDimensionPixelSize(R.dimen.heightItemDropDown);
        int maxHeight = totalDataSize * heightItem;
        final int heightItemShow = totalItemShow * heightItem;

        if (maxHeight < heightItemShow) {
            setPopupHeightPx(maxHeight);
        } else {
            setPopupHeightPx(heightItemShow);
        }
    }

    public void setCurrentSelected(){
        this.dropdownAdapter.setCurrentSelected(-1);
    }

}
