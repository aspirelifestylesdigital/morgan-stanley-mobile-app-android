package com.ms.androidapp.presentation.venuedetail;

import com.ms.androidapp.domain.model.explore.DiningDetailItem;
import com.ms.androidapp.presentation.base.BasePresenter;

/**
 * Created by ThuNguyen on 6/13/2017.
 */

public interface DiningDetail {

    interface View {
        void onGetDiningDetailFinished(DiningDetailItem diningDetailItem);
        void onUpdateFailed();
    }

    interface Presenter extends BasePresenter<View> {
        void getDiningDetail(Integer categoryId, Integer itemId);
    }

}
