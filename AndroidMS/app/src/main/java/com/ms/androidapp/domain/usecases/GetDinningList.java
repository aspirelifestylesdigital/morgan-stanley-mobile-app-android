package com.ms.androidapp.domain.usecases;

import android.text.TextUtils;

import com.ms.androidapp.common.constant.CityData;
import com.ms.androidapp.datalayer.datasource.AppGeoCoder;
import com.ms.androidapp.datalayer.entity.Answer;
import com.ms.androidapp.datalayer.entity.QuestionsAndAnswer;
import com.ms.androidapp.domain.model.explore.DiningDetailItem;
import com.ms.androidapp.domain.model.explore.ExploreRView;
import com.ms.androidapp.domain.model.explore.ExploreRViewItem;
import com.ms.androidapp.domain.repository.B2CRepository;
import com.ms.androidapp.presentation.explore.DiningSortingCriteria;
import com.ms.androidapp.presentation.explore.ExplorePresenter;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;

/**
 * Created by vinh.trinh on 6/2/2017.
 */

public class GetDinningList extends UseCase<List<ExploreRViewItem>, GetDinningList.Params> {
    private B2CRepository b2CRepository;
    private AppGeoCoder geoCoder;
    private List<QuestionsAndAnswer> data;

    public GetDinningList(B2CRepository b2CRepository, AppGeoCoder geoCoder) {
        super();
        this.b2CRepository = b2CRepository;
        this.geoCoder = geoCoder;
        data = new ArrayList<>();
    }

    @Override
    Observable<List<ExploreRViewItem>> buildUseCaseObservable(GetDinningList.Params params) {
        return null;
    }

    @Override
    public Single<List<ExploreRViewItem>> buildUseCaseSingle(GetDinningList.Params params) {
        if(params.paging == ExplorePresenter.DEFAULT_PAGE) data.clear();
        return b2CRepository.getDiningList(params.condition, params.paging)
                .map(questionsAndAnswers -> {
                    int length = data.size();
                    data.addAll(questionsAndAnswers);
                    return viewData(length, questionsAndAnswers);
                });
    }

    private List<ExploreRViewItem> viewData(int startIndex, List<QuestionsAndAnswer> data) {
        final List<ExploreRViewItem> exploreRViewList = new ArrayList<>();
        int length = data.size();
        for (int i = 0; i < length; i++) {
            QuestionsAndAnswer qa = data.get(i);
            final ExploreRViewItem exploreRViewItem = viewDatum(startIndex + i, qa);
            exploreRViewList.add(exploreRViewItem);
        }
        return exploreRViewList;
    }

    private ExploreRViewItem viewDatum(int dataIndex, QuestionsAndAnswer questionsAndAnswers) {
        Answer answer = questionsAndAnswers.getAnswers().get(0);
        ExploreRViewItem exploreRViewItem = new ExploreRViewItem(
                questionsAndAnswers.getID(),
                answer.getName().trim(),
                answer.getCity() + ", " + (CityData.isUSCity() ? answer.getState() : answer.getCountry()),
                answer.getImageURL(),
                !TextUtils.isEmpty(answer.getOffer2()),
                answer.getOffer2(),
                dataIndex
        );
        exploreRViewItem.setItemType(ExploreRViewItem.ItemType.DINING);
        exploreRViewItem.categoryName("Dining");
        String desireAddress = desireAddress(answer);
        /*Log.d("vinhtv", answer.getName());
        Log.d("vinhtv", desireAddress);
        Log.d("vinhtv", latLng.toString());*/
        exploreRViewItem.setSortingCriteria(new DiningSortingCriteria(
                geoCoder.getFullGeoCoder(desireAddress),
                answer.getUserDefined1()
        ));
        return exploreRViewItem;
    }

    public DiningDetailItem getItemView(int index) {
        QuestionsAndAnswer datum = data.get(index);
        Answer answer = datum.getAnswers().get(0);
        //LatLng latLng = geoCoder.getLocation(answer.getAddress());
        DiningDetailItem item = new DiningDetailItem.Builder(
                answer.getName(),
                answer.getAddress(),
                answer.getCity(),
                answer.getZipCode(),
                answer.getPrice(),
                answer.getUserDefined1(),
                answer.getAnswerText(),
                answer.getHoursOfOperation(),
                answer.getImageURL())
                .address2(answer.getAddress2())
                .address3(answer.getAddress3())
                .benefits(answer.getOffer2())
                .state(answer.getState())
                .country(answer.getCountry())
                .url(answer.getURL())
                .coordination(0, 0)
                .build();
        item.setItemType(ExploreRView.ItemType.DINING);
        return item;
    }

    private String desireAddress(Answer answer) {
        return answer.getAddress() +
                (TextUtils.isEmpty(answer.getCity().trim()) ? "" : ", " + answer.getCity()) +
                (TextUtils.isEmpty(answer.getState().trim()) ? "" : ", " + answer.getState()) +
                (TextUtils.isEmpty(answer.getZipCode().trim()) ? "" : " " + answer.getZipCode()) +
                (TextUtils.isEmpty(answer.getCountry().trim()) ? "" : ", " + answer.getCountry());
    }

    @Override
    Completable buildUseCaseCompletable(Params params) {
        return null;
    }

    public static final class Params {

        private final Integer condition;
        private final Integer paging;

        public Params(Integer categoryId) {
            this.condition = categoryId;
            paging = 0;
        }

        public Params(Integer categoryID, Integer paging) {
            this.condition = categoryID;
            this.paging = paging;
        }
    }
}
