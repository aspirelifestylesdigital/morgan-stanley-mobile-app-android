package com.ms.androidapp.presentation.cityguidecategory;

import com.ms.androidapp.domain.model.SubCategoryItem;
import com.ms.androidapp.presentation.base.BasePresenter;

import java.util.List;


/**
 * Created by tung.phan on 5/31/2017.
 */

public interface SubCategory {

    interface View {
        void showLoading();

        void hideLoading();

        void updateSubCategoryRViewAdapter(List<SubCategoryItem> subCategoryItems);
    }

    interface Presenter extends BasePresenter<SubCategory.View> {

        void getSubCategories(int categoryId);
    }
}
