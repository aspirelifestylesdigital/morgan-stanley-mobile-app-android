package com.ms.androidapp.presentation.requestlist;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;

import com.api.aspire.data.preference.PreferencesStorageAspire;
import com.ms.androidapp.R;
import com.api.aspire.common.constant.ErrCode;
import com.ms.androidapp.datalayer.repository.RequestRepository;
import com.ms.androidapp.domain.model.RequestDetailData;
import com.ms.androidapp.domain.model.RequestItemView;
import com.ms.androidapp.domain.usecases.GetAccessToken;
import com.ms.androidapp.domain.usecases.GetRequestList;
import com.ms.androidapp.presentation.base.CommonActivity;
import com.ms.androidapp.presentation.requestdetail.RequestDetailActivity;
import com.ms.androidapp.presentation.widget.DialogHelper;

import java.util.List;

import butterknife.BindView;

/**
 * Created by vinh.trinh on 8/30/2017.
 */

public class RequestListActivity extends CommonActivity implements RequestList.View,
        FragmentRequestList.RequestListListener{

    @BindView(R.id.tabs) TabLayout tabLayout;
    @BindView(R.id.viewpager) ViewPager viewPager;
    ViewPagerAdapter vpAdapter;
    private DialogHelper dialogHelper;

    private RequestList.Presenter presenter;

    private RequestListPresenter presenter() {
        PreferencesStorageAspire ps = new PreferencesStorageAspire(getApplicationContext());
        GetAccessToken getAccessToken = new GetAccessToken(ps);
        GetRequestList getRequestList = new GetRequestList(new RequestRepository());
        return new RequestListPresenter(ps, getAccessToken, getRequestList);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_list);
        setTitle("My Requests");
        setupViews();
        dialogHelper = new DialogHelper(this);
        presenter = presenter();
        presenter.attach(this);
        presenter.fetchList();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == 35567 && resultCode == RESULT_OK) {
            ((FragmentRequestList)vpAdapter.getItem(0)).clear();
            ((FragmentRequestList)vpAdapter.getItem(1)).clear();
            presenter.refresh();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onDestroy() {
        presenter.detach();
        super.onDestroy();
    }

    @Override
    public void showProgressDialog() {
        dialogHelper.showProgress();
    }

    @Override
    public void dismissProgressDialog() {
        dialogHelper.dismissProgress();
    }

    @Override
    public void showListLoadMore(boolean open) {
        if(open)
            ((FragmentRequestList)vpAdapter.getItem(0)).showLoadMore();
        else
            ((FragmentRequestList)vpAdapter.getItem(1)).showLoadMore();
    }

    @Override
    public void hideListLoadMore(boolean open) {
        if(open)
            ((FragmentRequestList)vpAdapter.getItem(0)).stopLoadMore();
        else
            ((FragmentRequestList)vpAdapter.getItem(1)).stopLoadMore();
    }

    @Override
    public int openCount() {
        return ((FragmentRequestList)vpAdapter.getItem(0)).itemCount();
    }

    @Override
    public int closeCount() {
        return ((FragmentRequestList)vpAdapter.getItem(1)).itemCount();
    }

    @Override
    public void loadData(List<RequestItemView> openList, List<RequestItemView> closeList) {
        ((FragmentRequestList)vpAdapter.getItem(0)).loadList(openList);
        ((FragmentRequestList)vpAdapter.getItem(1)).loadList(closeList);
    }

    @Override
    public void onFetchError() {
        dialogHelper.networkUnavailability(ErrCode.CONNECTIVITY_PROBLEM, null);
    }

    private void setupViews() {
        vpAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(vpAdapter);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}

            @Override
            public void onPageSelected(int position) {
                ((RequestListPresenter) presenter).switchList(position == 0);
                if((position == 0 && openCount() < 10)
                        || (position == 1 && closeCount() < 10)) {
                    presenter.fetchList();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {}
        });
        tabLayout.setupWithViewPager(viewPager);
        reduceMarginsInTabs(tabLayout, 20);
    }

    @Override
    public void onOpenedItemClick(RequestItemView item) {
        toDetail(item.dataListIndex);
    }

    @Override
    public void onClosedItemClick(RequestItemView item) {
        toDetail(item.dataListIndex);
    }

    long threshold = 0;
    @Override
    public void listLoadMore() {
        long current = System.currentTimeMillis();
        if(current - threshold > 300) {
            threshold = current;
            presenter.loadMore();
        }
    }

    private void toDetail(int dataIndex) {
        RequestDetailData data = ((RequestListPresenter)presenter).getDetail(dataIndex);
        if(data == null) return;
        Intent intent = new Intent(this, RequestDetailActivity.class);
        intent.putExtra("rd_data", data);
        startActivityForResult(intent, 35567);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {

        private Fragment open, close;

        ViewPagerAdapter(FragmentManager fm) {
            super(fm);
            open = FragmentRequestList.newInstance(true);
            close = FragmentRequestList.newInstance(false);
        }

        @Override
        public Fragment getItem(int position) {
            return position == 0 ? open : close;
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return position == 0 ? "Open" : "Closed";
        }
    }

    void reduceMarginsInTabs(TabLayout tabLayout, int marginOffset) {

        View tabStrip = tabLayout.getChildAt(0);
        if (tabStrip instanceof ViewGroup) {
            ViewGroup tabStripGroup = (ViewGroup) tabStrip;
            for (int i = 0; i < ((ViewGroup) tabStrip).getChildCount(); i++) {
                View tabView = tabStripGroup.getChildAt(i);
                if (tabView.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
                    ViewGroup.MarginLayoutParams mlp = (ViewGroup.MarginLayoutParams) tabView.getLayoutParams();
                    mlp.leftMargin = marginOffset;
                    mlp.rightMargin = marginOffset;
                }
            }
            tabLayout.requestLayout();
        }
    }
}
