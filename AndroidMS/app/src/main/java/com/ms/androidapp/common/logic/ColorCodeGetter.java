package com.ms.androidapp.common.logic;

/**
 * Created by ThuNguyen on 6/29/2017.
 */

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ColorCodeGetter {
    private Pattern colorCodeTag;

    public ColorCodeGetter() {
        colorCodeTag = Pattern.compile("#[a-fA-F0-9]{6,8}");
    }

    public String getColorCode(String htmlText) {
        StringBuilder builder = new StringBuilder();
        builder.append(htmlText);
        String result = builder.toString();

        Matcher tagmatch = colorCodeTag.matcher(builder.toString());
        while (tagmatch.find()) {
            return tagmatch.group();
        }

        return result;
    }

}
