package com.ms.androidapp.presentation.widget;

import android.support.v7.widget.AppCompatEditText;
import android.text.Editable;
import android.text.TextWatcher;

/**
 * Created by vinh.trinh on 5/26/2017.
 */

public class TextPattern implements TextWatcher {

    private AppCompatEditText editable;
    private Rules rules;
    private String strBefore;

    public TextPattern(AppCompatEditText editText, String pattern) {
        this.editable = editText;
        this.rules = new Rules(pattern);
    }

    public TextPattern(AppCompatEditText editText, String pattern, int remain) {
        this.editable = editText;
        this.rules = new Rules(pattern, remain);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int index, int before, int count) {
        strBefore = s.toString();
//        Log.e("test",s + " start: " + index + ", before" + before + ", count: " + count);
    }

    @Override
    public void onTextChanged(CharSequence sequence, int index, int before, int count) {
//        Log.e("test",s + " start: " + index + ", before" + before + ", count: " + count);
        if(count > 1) {
            return;
        }
        editable.removeTextChangedListener(this);
        if(before > 1) {
            editable.setText(strBefore);
            editable.setSelection(index);
            editable.addTextChangedListener(this);
            return;
        }
        int assertIndex = sequence.length() -1;
        if(assertIndex+1 == 0) {
            editable.addTextChangedListener(this);
            return;
        }
        boolean inserting = before == 0;
        char editing = inserting ? sequence.charAt(index) : strBefore.charAt(index);
        if(inserting) {
            if(index <= rules.remain-1) {
                editable.setText(strBefore);
                editable.setSelection(rules.remain);
                editable.addTextChangedListener(this);
                return;
            }
            // prevent unexpected character
            if(editing != '-' && !rules.number(editing)) {
                editable.setText(strBefore);
                editable.setSelection(index);
            }
            // prevent wrong dash position
            if(!rules.dashPosition(index) && editing == '-') {
                editable.setText(strBefore);
                editable.setSelection(index);
            }
            // automatically dash insert
            if(rules.dashPosition(assertIndex) && rules.number(editing)) {
                editable.setText(strBefore + "-" + editing);
                editable.setSelection(index + 2);
            }
            // handle middle cursor position
            if(index < assertIndex && rules.number(editing)) {
                int i = index;
                if(strBefore.charAt(index) == '-') i = i+1;
                String prefix = strBefore.substring(0, i);
                String suffix = strBefore.substring(i, assertIndex).replace("-", "");
                String filled = prefix + editing + rules.autoFillDash(i+1, suffix);
                editable.setText(filled);
                editable.setSelection(i + 1);
            }
        } else {
            if(index <= rules.remain-1) {
                editable.setText(strBefore);
                editable.setSelection(rules.remain);
                editable.addTextChangedListener(this);
                return;
            }
            // automatically dash remove && rearrange
            if(editing == '-' || index < assertIndex) {
                int i = (editing == '-') ? -1 : 0;
                String prefix = sequence.subSequence(0, index + i).toString();
                String suffix = sequence.subSequence(index, sequence.length())
                        .toString().replace("-", "");
                String filled = prefix + rules.autoFillDash(index+i, suffix);
                editable.setText(filled);
                editable.setSelection(index+i);
            }
        }
        editable.addTextChangedListener(this);
    }

    @Override
    public void afterTextChanged(Editable editable) {}

    private static class Rules {
        private boolean[] dashIndexes;
        int remain;

        Rules(String expectPattern) {
            dashIndexes = new boolean[expectPattern.length()];
            char[] chars = expectPattern.toCharArray();
            for (int i = 0; i < chars.length; i++) {
                dashIndexes[i] = chars[i] == '-';
            }
        }

        Rules(String expectPattern, int remain) {
            this(expectPattern);
            this.remain = remain;
        }

        boolean dashPosition(int index) {
            return dashIndexes[index];
        }

        boolean number(char c) {
            return 48 <= c && c <= 57;
        }

        String autoFillDash(int start, String str) {
            int limit = start + str.length();
            for (int i=start; i<limit; i++) {
                if(dashIndexes[i])
                    str = insertDash(i-start, str);
            }
            return str;
        }

        private String insertDash(int index, String string) {
            return  string.substring(0, index) + "-" + string.substring(index, string.length());
        }
    }
}