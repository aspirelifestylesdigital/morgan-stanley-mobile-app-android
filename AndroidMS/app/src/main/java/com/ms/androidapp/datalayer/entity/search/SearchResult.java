package com.ms.androidapp.datalayer.entity.search;

import com.google.gson.annotations.SerializedName;
import com.ms.androidapp.datalayer.entity.SearchContent;

import java.util.List;

/**
 * Created by vinh.trinh on 6/22/2017.
 */

public class SearchResult {
    @SerializedName("SearchResults")
    private List<SearchContent> searchResults;
    @SerializedName("Status")
    private String status;
    @SerializedName("Success")
    private Boolean success;

    public List<SearchContent> searchResults() {
        return searchResults;
    }

    public String status() {
        return status;
    }

    public Boolean success() {
        return success;
    }
}
