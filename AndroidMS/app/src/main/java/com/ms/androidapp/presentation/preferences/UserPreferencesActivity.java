package com.ms.androidapp.presentation.preferences;


import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.api.aspire.common.constant.ErrorApi;
import com.api.aspire.data.entity.preference.PreferenceData;
import com.ms.androidapp.App;
import com.ms.androidapp.R;
import com.ms.androidapp.common.constant.AppConstant;
import com.api.aspire.common.constant.ErrCode;
import com.ms.androidapp.common.constant.ResultCode;
import com.ms.androidapp.presentation.base.CommonActivity;
import com.ms.androidapp.presentation.widget.DialogHelper;

public class UserPreferencesActivity extends CommonActivity implements UserPreferences.View,
        UserPreferencesFragment.PreferencesListener {

    private DialogHelper dialogHelper;
    private UserPreferencesFragment preferencesFragment;
    private UserPreferencesPresenter presenter;
    PreferenceData original;

    private UserPreferencesPresenter presenter() {
        return new UserPreferencesPresenter(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.acticity_dummy_content);
        setTitle(R.string.title_preferences);
        if (savedInstanceState == null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.fragment_place_holder
                            , UserPreferencesFragment.newInstance()
                            , UserPreferencesFragment.class.getSimpleName())
                    .commit();
            // Track GA
            App.getInstance().track(AppConstant.GA_TRACKING_SCREEN_NAME.PREFERENCES.getValue());

        }
        dialogHelper = new DialogHelper(this);
        presenter = presenter();
        presenter.attach(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void loadPreferencesOnUI(PreferenceData data) {
        original = data;
        preferencesFragment.load(data);
    }

    @Override
    public void showPreferenceSavedDialog() {
        dialogHelper.alert(null, getString(R.string.preference_updated_message));
        if (preferencesFragment != null) {
            preferencesFragment.updateChange();
        }
    }

    @Override
    public void showErrorDialog(ErrCode errCode, String extraMsg) {
        if (dialogHelper.networkUnavailability(errCode, extraMsg))
            return;
        if (ErrorApi.isGetTokenError(extraMsg)) dialogHelper.showGetTokenError();
        else dialogHelper.showGeneralError();
    }

    @Override
    public void hideLoading() {
        if (!isDestroyed() && !isFinishing()) {
            dialogHelper.dismissProgress();
        }
    }

    @Override
    public void showLoading() {
        if (!isDestroyed() && !isFinishing()) {
            dialogHelper.showProgressCancelableUnEnable();
        }
    }

    @Override
    public void onFragmentCreated() {
        preferencesFragment = (UserPreferencesFragment) getSupportFragmentManager()
                .findFragmentByTag(UserPreferencesFragment.class.getSimpleName());
        presenter.loadPreferencesLocal();
        presenter.loadPreferences();
    }

    @Override
    public void onPreferencesSubmitted(String cuisine, String hotel, String transportation) {
        setResult(ResultCode.RESULT_OK);
        presenter.savePreferences(original.applyChanges(cuisine, hotel, transportation));
    }

    @Override
    public void onPreferencesCancel() {
        presenter.loadPreferencesLocal();
    }
}
