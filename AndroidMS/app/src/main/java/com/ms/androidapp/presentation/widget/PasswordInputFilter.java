package com.ms.androidapp.presentation.widget;

import android.text.InputFilter;
import android.text.Spanned;
import android.util.Log;

/**
 * Created by vinh.trinh on 10/2/2017.
 */

public class PasswordInputFilter implements InputFilter {
    @Override
    public CharSequence filter(CharSequence source, int start, int end,
                               Spanned dest, int dstart, int dend) {
        Log.d("ThuNguyen", "Start = " + start + ", End = " + end + ", Source = " + source);
        for (int i = start; i < end; i++) {
            if (source.charAt(i) == ' ') {
                return "";
            }
        }
        return null;
    }
}
