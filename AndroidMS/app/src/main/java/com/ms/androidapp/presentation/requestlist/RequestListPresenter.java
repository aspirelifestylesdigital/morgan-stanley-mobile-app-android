package com.ms.androidapp.presentation.requestlist;

import com.api.aspire.data.preference.PreferencesStorageAspire;
import com.ms.androidapp.App;
import com.ms.androidapp.domain.model.Metadata;
import com.ms.androidapp.domain.model.RequestDetailData;
import com.ms.androidapp.domain.model.RequestItemView;
import com.ms.androidapp.domain.usecases.GetAccessToken;
import com.ms.androidapp.domain.usecases.GetRequestList;

import org.json.JSONException;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by vinh.trinh on 8/30/2017.
 */

public class RequestListPresenter implements RequestList.Presenter {

    private RequestList.View view;
    private CompositeDisposable compositeDisposable;
    private GetAccessToken getAccessToken;
    private GetRequestList getRequestList;
    private int page = 1;
    private boolean reachEnd = false;
    private boolean openLoad = true;

    RequestListPresenter(PreferencesStorageAspire ps, GetAccessToken at, GetRequestList gql) {
        this.compositeDisposable = new CompositeDisposable();
        this.getAccessToken = at;
        this.getRequestList = gql;
    }

    RequestDetailData getDetail(int index) {
        if(!App.getInstance().hasNetworkConnection()) {
            view.onFetchError();
            return null;
        }
        try {
            return getRequestList.getDatum(index);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    void switchList(boolean open) {
        openLoad = open;
    }

    @Override
    public void fetchList() {
        if(reachEnd) return;
        if(!openLoad && view.openCount() > 0) page++;
        if(view.openCount() < 10 || view.closeCount() < 10) view.showProgressDialog();
        load();
    }

    @Override
    public void loadMore() {
        if(reachEnd) {
            dismissViewLoader();
            return;
        }
        page++;
        view.showListLoadMore(openLoad);
        load();
    }

    @Override
    public void refresh() {
        getRequestList.reset();
        page = 1;
        reachEnd = false;
        openLoad = true;
        fetchList();
    }

    @Override
    public void attach(RequestList.View view) {
        this.view = view;
    }

    @Override
    public void detach() {
        compositeDisposable.dispose();
        this.view = null;
    }

    private void load() {
        if(!App.getInstance().hasNetworkConnection()) {
            view.dismissProgressDialog();
            view.onFetchError();
            return;
        }
        compositeDisposable.add(getAccessToken.execute()
                .flatMap(accessToken -> {
                    GetRequestList.Params params = new GetRequestList.Params(
                            accessToken,
                            null, //- temp hold logic
                            page*20-19,
                            page*20
                    );
                    return getRequestList.buildUseCaseSingle(params);
                })
                .map(requestItemViews -> {
                    if(requestItemViews.size() == 0) reachEnd = true;
                    return separate(requestItemViews);
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new RequestDataObserver()));
    }

    private RequestData separate(List<RequestItemView> data) {
        List<RequestItemView> openList = new ArrayList<>();
        List<RequestItemView> closeList = new ArrayList<>();
        for (RequestItemView item : data) {
            if(item.status == RequestItemView.STATUS.CLOSED) closeList.add(item);
            else openList.add(item);
        }
        return new RequestData(openList, closeList);
    }

    private class RequestData {
        final List<RequestItemView> openList;
        final List<RequestItemView> closeList;

        RequestData(List<RequestItemView> openList, List<RequestItemView> closeList) {
            this.openList = openList;
            this.closeList = closeList;
        }
    }

    private class RequestDataObserver extends DisposableSingleObserver<RequestData> {

        @Override
        public void onSuccess(RequestData data) {
            dismissViewLoader();
            view.loadData(data.openList, data.closeList);
            if(shouldLoadMore(data)) {
                view.showListLoadMore(openLoad);
                loadMore();
            }
        }

        @Override
        public void onError(Throwable e) {
            dismissViewLoader();
            view.onFetchError();
        }
    }

    private boolean shouldLoadMore(RequestData data) {
        int fetchedAmount = 0, odd = 0;
        if(openLoad) {
            fetchedAmount = data.openList.size();
            if(fetchedAmount == 0 && view.openCount() == 0)
                return true;
            odd = view.openCount() > 10 ? view.openCount() % 10 : 0;
        }
        if(!openLoad) {
            fetchedAmount = data.closeList.size();
            if(fetchedAmount == 0 && view.closeCount() == 0)
                return true;
            odd = view.closeCount() > 10 ? view.closeCount() % 10 : 0;
        }
        return odd + fetchedAmount < 10;
    }

    private void dismissViewLoader() {
        if(view.openCount() > 0 && openLoad) {
            view.hideListLoadMore(true);
        }
        if(view.closeCount() > 0 && !openLoad) {
            view.hideListLoadMore(false);
        }
        view.dismissProgressDialog();
    }
}
