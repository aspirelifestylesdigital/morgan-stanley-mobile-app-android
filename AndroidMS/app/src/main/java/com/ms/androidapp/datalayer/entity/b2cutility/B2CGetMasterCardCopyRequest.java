package com.ms.androidapp.datalayer.entity.b2cutility;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ms.androidapp.BuildConfig;

/**
 * Created by vinh.trinh on 6/5/2017.
 */

public class B2CGetMasterCardCopyRequest {
    @SerializedName("subDomain")
    @Expose
    public final String subDomain;
    @SerializedName("password")
    @Expose
    public final String secretKey;
    @SerializedName("type")
    @Expose
    public final String type;

    public B2CGetMasterCardCopyRequest(String type) {
        subDomain = BuildConfig.WS_B2C_SUBDOMAIN;
        secretKey = BuildConfig.WS_B2C_SECRET;
        this.type = type;
    }
}
