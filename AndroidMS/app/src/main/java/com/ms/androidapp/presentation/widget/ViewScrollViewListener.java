package com.ms.androidapp.presentation.widget;

import android.view.MotionEvent;
import android.view.View;

public class ViewScrollViewListener {

    private View scrollView;
    private ScrollViewClickEvent event;

    private long lastTouchDown = 0;
    private static final int CLICK_ACTION_HOLD = 200;

    /**
     * @param scrollView: the scrollView
     **/
    public ViewScrollViewListener(View scrollView, ScrollViewClickEvent event) {
        this.scrollView = scrollView;
        this.event = event;
        this.scrollView.setOnTouchListener(this::onTouchScroll);
    }

    private boolean onTouchScroll(View view, MotionEvent ev) {
        if(ev.getAction() == MotionEvent.ACTION_DOWN){
            lastTouchDown = System.currentTimeMillis();
        }else if(ev.getAction() == MotionEvent.ACTION_UP){
            if (System.currentTimeMillis() - lastTouchDown < CLICK_ACTION_HOLD){
                event.onScrollViewClick(view);
            }
            lastTouchDown = 0;
        }
        return false;
    }

    public interface ScrollViewClickEvent {
        void onScrollViewClick(View view);
    }
}
