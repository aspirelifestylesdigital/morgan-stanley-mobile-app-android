package com.ms.androidapp.common.logic;

/**
 * Created by ThuNguyen on 6/9/2017.
 */

public class HtmlUtils {
    public static String adjustSomeHtmlTag(String html) {
        String result = html
                .replaceAll("[\r\n]+", "")
                .replace("<p>&nbsp;", "<br/>")
                .replace("</p>", "")
                .replace("&nbsp;", " ")
                .replace("<li>;", "<li><font size=\"16sp\">")
                .replace("</li>;", "</font><li>");
        // Replace <li>, <ul>, <ol> as my custom
        result = HtmlTagUtils.overrideTags(result);
        // Also remove redundant sentences
        result = StringUtils.removeRedundantToken(result);
        result = new LinkGetter().removeInvalidHyperlink(result);
        return result;
    }
}
