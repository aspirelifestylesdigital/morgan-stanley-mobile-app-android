package com.ms.androidapp.datalayer.entity;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by vinh.trinh on 6/6/2017.
 */

public class GetTilesResult {

    @SerializedName("Tiles")
    private List<Tiles> tiles;
    @SerializedName("Status")
    private String status;
    @SerializedName("Success")
    private Boolean success;

    public List<Tiles> getTiles() {
        return tiles;
    }

    public void setTiles(List<Tiles> tiles) {
        this.tiles = tiles;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }
}
