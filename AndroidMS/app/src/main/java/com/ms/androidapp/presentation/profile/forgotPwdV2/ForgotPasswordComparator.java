package com.ms.androidapp.presentation.profile.forgotPwdV2;

import android.support.v7.widget.AppCompatEditText;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class ForgotPasswordComparator {

    private EditText edtEmail, edtAnswerQuestion, edtForgotPwd, edtForgotRetypePwd;
    private Button btnSubmit;
    private boolean email, challengeQuestion, answer, forgotPwd, forgotRetypePwd;
    private FocusChangeListenerRemoveBlank focusChangeListenerRemoveBlank;

    public ForgotPasswordComparator(EditText edtEmail,
                             EditText edtAnswerQuestion,
                             EditText edtForgotPwd,
                             EditText edtForgotRetypePwd,
                             Button btnSubmit) {

        this.edtEmail = edtEmail;
        this.edtAnswerQuestion = edtAnswerQuestion;
        this.edtForgotPwd = edtForgotPwd;
        this.edtForgotRetypePwd = edtForgotRetypePwd;
        this.btnSubmit = btnSubmit;
        setUp();
    }

    private void setUp() {
        edtEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                String string = s.toString().trim();
                email = string.length() > 0;
                onChanged();
            }
        });
        edtAnswerQuestion.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                String string = s.toString().trim();
                answer = string.length() > 0;
                onChanged();
            }
        });
        edtForgotPwd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                String string = s.toString();
                forgotPwd = string.length() > 0;
                onChanged();
            }
        });
        edtForgotRetypePwd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                String string = s.toString();
                forgotRetypePwd = string.length() > 0;
                onChanged();
            }
        });

        focusChangeListenerRemoveBlank = new FocusChangeListenerRemoveBlank();
        edtEmail.setOnFocusChangeListener(focusChangeListenerRemoveBlank);
        edtAnswerQuestion.setOnFocusChangeListener(focusChangeListenerRemoveBlank);

    }

    private void onChanged() {
        boolean changed = email && challengeQuestion && answer && forgotPwd && forgotRetypePwd;
        btnSubmit.setEnabled(changed);
        btnSubmit.setClickable(changed);
    }

    public void handleChallengeQuestion(String value) {
        if(!TextUtils.isEmpty(value)){
            challengeQuestion = true;
        }else{
            challengeQuestion = false;
        }
        onChanged();
    }

    public void resetChallengeQuestion(){
        handleChallengeQuestion("");
    }

    private class FocusChangeListenerRemoveBlank implements View.OnFocusChangeListener{
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            AppCompatEditText view = (AppCompatEditText) v;
            if(view != null){
                if(!hasFocus){
                    view.setText(view.getText().toString().trim());
                }
            }
        }
    }
}
