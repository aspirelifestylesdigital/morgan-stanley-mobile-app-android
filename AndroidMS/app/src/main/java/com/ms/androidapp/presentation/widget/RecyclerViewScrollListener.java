package com.ms.androidapp.presentation.widget;

import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

/**
 * Created by vinh.trinh on 6/6/2017.
 */

public abstract class RecyclerViewScrollListener extends RecyclerView.OnScrollListener {

    private int scrolledDistance = 0;
    private static final int HIDE_THRESHOLD = 20;

//    private int previousTotal = 0; // The total number of items in the dataset after the last load
    private boolean loading = false; // True if we are still waiting for the last set of data to load.
    private final int visibleThreshold = 2; // The minimum amount of items to have below your current scroll position before loading more.

    private boolean infiniteScrollingEnabled = true;

    private boolean controlsVisible = true;

    public RecyclerViewScrollListener() {
//        Log.i(TAG, "construct");
    }

    // So TWO issues here.
    // 1. When the data is refreshed, we need to change previousTotal to 0.
    // 2. When we switch fragments and it loads itself from some place, for some
    // reason gridLayoutManager returns stale data and hence re-assigning it every time.

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);

        RecyclerView.LayoutManager manager = recyclerView.getLayoutManager();

        int visibleItemCount = recyclerView.getChildCount();
        int currentFirstVisible, totalItemCount;
        if (manager instanceof GridLayoutManager) {
            GridLayoutManager gridLayoutManager = (GridLayoutManager)manager;
            currentFirstVisible = gridLayoutManager.findFirstVisibleItemPosition();
            totalItemCount = gridLayoutManager.getItemCount();
        } else {
            LinearLayoutManager linearLayoutManager = (LinearLayoutManager)manager;
            currentFirstVisible = linearLayoutManager.findFirstVisibleItemPosition();
            totalItemCount = linearLayoutManager.getItemCount();
        }

        if (infiniteScrollingEnabled) {
            /*if (loading) {
                if (totalItemCount > previousTotal) {
                    loading = false;
                    previousTotal = totalItemCount;
                }
            }*/

            if (!loading && (totalItemCount - visibleItemCount <= currentFirstVisible + visibleThreshold)) {
                // End has been reached
                // do something
                if(dy > 0) { // only when scrolling down
                    loading = true;
                    onLoadMore();
                }
            }
        }

        if (currentFirstVisible == 0) {
            if (!controlsVisible) {
                onScrollUp();
                controlsVisible = true;
            }
            return;
        }

        if (scrolledDistance > HIDE_THRESHOLD && controlsVisible) {
            onScrollDown();
            controlsVisible = false;
            scrolledDistance = 0;
        } else if (scrolledDistance < -HIDE_THRESHOLD && !controlsVisible) {
            onScrollUp();
            controlsVisible = true;
            scrolledDistance = 0;
        }

        if ((controlsVisible && dy>0) || (!controlsVisible && dy <0)) {
            scrolledDistance+=dy;
        }
    }

    public abstract void onScrollUp();
    public abstract void onScrollDown();
    public abstract void onLoadMore();

    public void loadDone() {
        loading = false;
    }

}