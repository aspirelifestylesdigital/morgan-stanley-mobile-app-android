package com.ms.androidapp.presentation.request;

import android.content.Context;

import com.api.aspire.common.constant.ConciergeCaseType;
import com.api.aspire.data.entity.askconcierge.ConciergeCaseAspireResponse;
import com.api.aspire.data.preference.PreferencesStorageAspire;
import com.api.aspire.data.repository.AspireConciergeRepository;
import com.api.aspire.data.repository.ProfileDataAspireRepository;
import com.api.aspire.domain.usecases.AskConciergeAspire;
import com.api.aspire.domain.usecases.GetToken;
import com.api.aspire.domain.usecases.LoadProfile;
import com.ms.androidapp.App;
import com.api.aspire.common.constant.ErrCode;
import com.ms.androidapp.BuildConfig;
import com.ms.androidapp.common.constant.AppConstant;
import com.ms.androidapp.domain.usecases.MapProfileApp;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by vinh.trinh on 5/16/2017.
 */

public class AskConciergePresenter implements CreateNewConciergeCase.Presenter {

    private CompositeDisposable compositeDisposable;
    private CreateNewConciergeCase.View view;
    private String city;
    private String category;

    private AskConciergeAspire askConciergeApi;

    AskConciergePresenter(Context context) {
        compositeDisposable = new CompositeDisposable();
        MapProfileApp mapLogic = new MapProfileApp();
        PreferencesStorageAspire preferencesStorage = new PreferencesStorageAspire(context);
        this.askConciergeApi = new AskConciergeAspire(mapLogic,
                new AspireConciergeRepository(),
                new GetToken(preferencesStorage, mapLogic),
                new LoadProfile(new ProfileDataAspireRepository(preferencesStorage, mapLogic))
        );
    }

    @Override
    public void attach(CreateNewConciergeCase.View view) {
        this.view = view;
    }

    @Override
    public void detach() {
        compositeDisposable.dispose();
        this.view = null;
    }

    @Override
    public void param(String city, String category) {
        this.city = city;
        this.category = category;
    }

    @Override
    public void sendRequest(String content, boolean email, boolean phone) {
        if (!App.getInstance().hasNetworkConnection()) {
            view.showErrorDialog(ErrCode.CONNECTIVITY_PROBLEM, null);
            return;
        }
        view.showProgressDialog();

        compositeDisposable.add(
                askConciergeApi.execute(new AskConciergeAspire.Params(
                        ConciergeCaseType.CREATE,
                        content,
                        city,
                        category,
                        email,
                        phone,
                        BuildConfig.AS_BIN,
                        BuildConfig.AS_PROGRAM,
                        AppConstant.CONCIERGE_REQUEST_TYPE.OTHERS.getValue()))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new AskConciergeObserver())
        );
    }

    private final class AskConciergeObserver extends DisposableSingleObserver<ConciergeCaseAspireResponse> {

        @Override
        public void onSuccess(ConciergeCaseAspireResponse acResponse) {
            view.dismissProgressDialog();
            if (acResponse.isEmpty()) {
                view.showErrorDialog(ErrCode.UNKNOWN_ERROR, "Failed to retrieve your profile.");
            } else if (acResponse.isSuccess()) {
                view.onRequestSuccessfullySent();
            } else {
                view.showErrorDialog(ErrCode.UNKNOWN_ERROR, acResponse.getMessage());
            }
            dispose();
        }

        @Override
        public void onError(Throwable e) {
            view.dismissProgressDialog();
            view.showErrorDialog(ErrCode.UNKNOWN_ERROR, e.getMessage());
            dispose();
        }
    }
}
