package com.ms.androidapp.presentation.profile.signUpV2;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.Menu;
import android.view.WindowManager;
import android.widget.FrameLayout;

import com.api.aspire.common.constant.ErrCode;
import com.api.aspire.common.constant.ErrorApi;
import com.api.aspire.data.entity.userprofile.pma.ProfilePMAMapView;
import com.api.aspire.domain.usecases.ChangeSecurityQuestion;
import com.ms.androidapp.App;
import com.ms.androidapp.R;
import com.ms.androidapp.common.GPSChecker;
import com.ms.androidapp.common.constant.AppConstant;
import com.ms.androidapp.domain.model.Profile;
import com.ms.androidapp.presentation.LocationPermissionHandler;
import com.ms.androidapp.presentation.base.BaseSwipeBackActivity;
import com.ms.androidapp.presentation.checkout.SignInActivity;
import com.ms.androidapp.presentation.home.HomeActivity;
import com.ms.androidapp.presentation.profile.forgotPwdV2.ForgotPasswordV2Activity;
import com.ms.androidapp.presentation.widget.DialogHelper;
import com.ms.androidapp.presentation.widget.ViewUtils;

import butterknife.BindView;

/*
 * flow
 * 1. MatchUpFragment
 * 2. CreateProfileFragment
 * 3. PasswordFragment
 * */
public class SignUpV2Activity extends BaseSwipeBackActivity
        implements CreateProfileFragment.CreateProfileEventsListener,
        MatchUpFragment.MatchUpEventsListener, SignUpV2.View, PasswordFragment.PasswordEventsListener {
    private SignUpV2Presenter presenter;
    private DialogHelper dialogHelper;
    private ProfilePMAMapView profilePMAMapView;

    @BindView(R.id.fragment_place_holder)
    FrameLayout fragmentHolder;

    LocationPermissionHandler locationPermissionHandler = new LocationPermissionHandler();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.activity_sign_up_v2);
        presenter = new SignUpV2Presenter(this);
        presenter.attach(this);
        dialogHelper = new DialogHelper(this);
        if (savedInstanceState == null) {
            loadFirstFragment(MatchUpFragment.newInstance());

            // Track ga
            App.getInstance().track(AppConstant.GA_TRACKING_SCREEN_NAME.SIGN_UP.getValue());
        }
    }

    @Override
    protected void onDestroy() {
        presenter.detach();
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public void onProfileSubmit(Profile message, ChangeSecurityQuestion.Param param) {

        presenter.setProfilePMAMapView(profilePMAMapView);
        presenter.setSecurityQuestionParam(param);

        presenter.createProfile(message);
    }

    @Override
    public void showProfileCreatedDialog() {
        dialogHelper.alert(null, getString(R.string.profile_created_message), dialog -> proceedToHome());
    }


    private void proceedToHome() {
        ViewUtils.hideSoftKey(fragmentHolder);
        Intent intent = new Intent(this, HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);

        // Track GA with "Sign up" event
        App.getInstance().track(AppConstant.GA_TRACKING_CATEGORY.AUTHENTICATION.getValue(),
                AppConstant.GA_TRACKING_ACTION.CLICK.getValue(),
                AppConstant.GA_TRACKING_LABEL.SIGN_UP.getValue());
    }

    @Override
    public void showErrorDialog(ErrCode errCode, String extraMsg) {
        if (dialogHelper.networkUnavailability(errCode, extraMsg)) {
            return;
        } else if (ErrorApi.isGetTokenError(extraMsg)) {
            dialogHelper.showGetTokenError();
        } else if (ErrCode.USER_EXISTS_ERROR.name().equalsIgnoreCase(extraMsg)) {
            dialogHelper.alert(App.getInstance().getString(R.string.errorTitle), App.getInstance().getString(R.string.errorCreateAccount));
        } else if (extraMsg.equalsIgnoreCase(ErrCode.CREATE_ACCOUNT_ASPIRE_PARAM_ERROR.name())) {
            dialogHelper.alert(getString(R.string.invalid_create_pwd_title),
                    getString(R.string.invalid_create_pwd));
        } else {
            dialogHelper.showGeneralError();
        }
    }

    @Override
    public void showProgressDialog() {
        dialogHelper.showProgressCancelableUnEnable();
    }

    @Override
    public void dismissProgressDialog() {
        dialogHelper.dismissProgress();
    }

    @Override
    public void showMatchUpDialog() {
        dialogHelper.action("", getString(R.string.match_up_found_email),
                getString(R.string.lb_sign_in),getString(R.string.forgot_password_screen),
                (dialogInterface, i) -> {
                    Intent intent = new Intent(SignUpV2Activity.this, SignInActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(intent);
                }, (dialogInterface, i) -> {
                    Intent intent = new Intent(this, SignInActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(intent);
                    Intent forgotIntent = new Intent(SignUpV2Activity.this, ForgotPasswordV2Activity.class);
                    startActivity(forgotIntent);
                });
    }

    @Override
    public void showExistPMAAccountDialog(final ProfilePMAMapView profilePMAMatchUp) {
        dialogHelper.alert("", getString(R.string.match_up_found_email), dialogInterface -> {
            dialogInterface.dismiss();
            onAddFragment(CreateProfileFragment.newInstance(profilePMAMatchUp));
        });
    }

    @Override
    public void showCreateNewAccountDialog(String email) {
        dialogHelper.alert("", getString(R.string.match_up_create_new_account), dialogInterface -> {
            dialogInterface.dismiss();
            ProfilePMAMapView profilePMAMapView = ProfilePMAMapView.empty();
            profilePMAMapView.setEmail(email);
            onAddFragment(CreateProfileFragment.newInstance(profilePMAMapView));
        });
    }


    @Override
    public void onProfileInputError(String message) {
        dialogHelper.profileDialog(message, dialog -> {
            //get current Create fragment show error and show keyboard
            if (getSupportFragmentManager() == null) {
                return;
            }
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_place_holder);
            if (fragment instanceof CreateProfileFragment) {
                final CreateProfileFragment createFragment = (CreateProfileFragment) fragment;
                if (createFragment.getView() != null) {
                    createFragment.getView().postDelayed(createFragment::showSoftKey, 100);
                }
            }
        });
    }

    @Override
    public void onProfilePMACreate(ProfilePMAMapView profilePMAMapView, Profile profile) {
        this.profilePMAMapView = profilePMAMapView;
        onAddFragment(PasswordFragment.newInstance(profile));
    }

    @Override
    public void onMatchUpSubmit(String email) {
        presenter.matchUpProfile(email);
    }



    @Override
    public int getFrameLayoutId() {
        return R.id.fragment_place_holder;
    }

    @Override
    public void onBackPressed() {
        ViewUtils.hideSoftKey(this.getCurrentFocus());
        int count = getSupportFragmentManager().getBackStackEntryCount();

        if (count == 0) {
            super.onBackPressed();
            //additional code
        } else {
            getSupportFragmentManager().popBackStack();
        }

    }

    @Override
    public void onLocationSwitchOn() {
        if (!locationPermissionHandler.hasPermission(this)) {
            locationPermissionHandler.requestPermission(this);
        } else {
            if (!GPSChecker.GPSEnable(getApplicationContext())) {
                dialogHelper.action(null, "To enable, please go to Settings and turn on Location Service for this app.",
                        "Setting", "Cancel",
                        (dialogInterface, i) -> GPSChecker.openGPSSetting(this));
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        boolean granted = locationPermissionHandler.handlingPermissionResult(requestCode, grantResults);
        if (!granted) {
            //get current fragment
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_place_holder);
            if (fragment instanceof CreateProfileFragment) {
                final CreateProfileFragment createFragment = (CreateProfileFragment) fragment;
                createFragment.switchOff();
            }
        } else if (!GPSChecker.GPSEnable(getApplicationContext())) {
            dialogHelper.action(null, "To enable, please go to Settings and turn on Location Service for this app.",
                    "Setting", "Cancel",
                    (dialogInterface, i) -> GPSChecker.openGPSSetting(this));
        }
    }
}