package com.ms.androidapp.presentation.inspiregallery;

import com.api.aspire.common.constant.ErrCode;
import com.ms.androidapp.domain.model.GalleryViewPagerItem;
import com.ms.androidapp.presentation.base.BasePresenter;

import java.util.List;

/**
 * Created by Thu Nguyen on 6/13/2017.
 */

public interface GetGallery {

    interface View {
        void onGetGalleryListFinished(List<GalleryViewPagerItem> galleryItemList);
        void dismissProgressDialog();
        void showErrorMessage(ErrCode errCode);
    }

    interface Presenter extends BasePresenter<View> {
        void getGalleryList();
    }

}
