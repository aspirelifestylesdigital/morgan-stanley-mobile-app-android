package com.ms.androidapp.presentation.profile.forgotPwdV2;

import com.api.aspire.common.constant.ErrCode;
import com.api.aspire.data.datasource.RemoteUserProfileOKTADataStore;
import com.api.aspire.data.repository.ForgotPasswordDataRepository;
import com.api.aspire.data.repository.UserProfileOKTADataRepository;
import com.api.aspire.domain.usecases.MapProfileBase;
import com.api.aspire.domain.usecases.RetrievePassword;
import com.ms.androidapp.App;
import com.ms.androidapp.domain.usecases.MapProfileApp;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableCompletableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by vinh.trinh on 7/24/2017.
 */

public class ForgotPasswordV2Presenter implements ForgotPasswordV2.Presenter {

    private CompositeDisposable disposables;
    private RetrievePassword retrievePassword;
    private ForgotPasswordV2.View view;

    public ForgotPasswordV2Presenter() {
        MapProfileBase mapLogic = new MapProfileApp();
        disposables = new CompositeDisposable();
        this.retrievePassword = new RetrievePassword(mapLogic,
                new ForgotPasswordDataRepository(),
                new UserProfileOKTADataRepository(new RemoteUserProfileOKTADataStore())
        );
    }

    @Override
    public void attach(ForgotPasswordV2.View view) {
        this.view = view;
    }

    @Override
    public void detach() {
        view = null;
        disposables.dispose();
    }

    @Override
    public void retrievePassword(String email, String recoveryQuestion, String answer, String password) {
        if (!App.getInstance().hasNetworkConnection()) {
            view.dismissProgressDialog();
            view.showErrorMessage(ErrCode.CONNECTIVITY_PROBLEM, null);
            return;
        }

        disposables.add(
                retrievePassword.param(new RetrievePassword.Param(email, password, recoveryQuestion, answer))
                        .on(Schedulers.io(), AndroidSchedulers.mainThread())
                        .execute(new DisposableCompletableObserver() {
                            @Override
                            public void onComplete() {
                                if (view == null) {
                                    return;
                                }
                                view.dismissProgressDialog();
                                view.showSuccessMessage();
                            }

                            @Override
                            public void onError(Throwable e) {
                                if (view == null) {
                                    return;
                                }
                                view.dismissProgressDialog();
                                view.showErrorMessage(ErrCode.UNKNOWN_ERROR, e.getMessage());
                            }
                        })
        );
    }
}
