package com.ms.androidapp.common.glide;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;

import com.bumptech.glide.DrawableTypeRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.target.Target;
import com.ms.androidapp.App;

/**
 * Created by ThuNguyen on 6/28/2017.
 */

public class GlideHelper {
    private static GlideHelper instance;
    private GlideHelper(){
    }
    public static GlideHelper getInstance(){
        if(instance == null){
            instance = new GlideHelper();
        }
        return instance;
    }

    /**
     *
     * @param source // String to load url, Integer to load resId
     * @param placeHolderResId
     * @param ivInto
     * @param expectedWidthSize
     */
    public void loadImage(Object source, int placeHolderResId, ImageView ivInto, int expectedWidthSize, Target<Bitmap> bitmapTarget){
        DrawableTypeRequest drawableTypeRequest = Glide.with(App.getInstance().getApplicationContext()).load(source);
        if(expectedWidthSize != 0){
            drawableTypeRequest.override(expectedWidthSize, Target.SIZE_ORIGINAL);
        }else{ // Expected width is screen width
            expectedWidthSize = App.getInstance().getResources().getDisplayMetrics().widthPixels;
            drawableTypeRequest.override(expectedWidthSize, Target.SIZE_ORIGINAL);
        }
        if(placeHolderResId != 0){
            drawableTypeRequest.placeholder(placeHolderResId);
        }
        drawableTypeRequest
                .asBitmap()
                .centerCrop()
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                        ivInto.setImageBitmap(resource);
                        if(bitmapTarget != null){
                            bitmapTarget.onResourceReady(resource, glideAnimation);
                        }
                    }

                    @Override
                    public void onLoadFailed(Exception e, Drawable errorDrawable) {
                        super.onLoadFailed(e, errorDrawable);
                        ivInto.setImageBitmap(null);
//                        if(bitmapTarget != null){
//                            bitmapTarget.onResourceReady(null, null);
//                        }
                    }
                });
    }
    public void loadImage(Object source, int placeHolderResId, ImageView ivInto, int expectedWidthSize){
        loadImage(source, placeHolderResId, ivInto, expectedWidthSize, null);
    }
}
