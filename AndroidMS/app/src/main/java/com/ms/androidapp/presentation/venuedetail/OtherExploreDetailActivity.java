package com.ms.androidapp.presentation.venuedetail;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.BaseTarget;
import com.bumptech.glide.request.target.SizeReadyCallback;
import com.ms.androidapp.App;
import com.ms.androidapp.R;
import com.ms.androidapp.common.constant.AppConstant;
import com.api.aspire.common.constant.ErrCode;
import com.ms.androidapp.common.constant.IntentConstant;
import com.ms.androidapp.common.glide.GlideHelper;
import com.ms.androidapp.common.logic.HtmlTagHandler;
import com.ms.androidapp.common.logic.HtmlUtils;
import com.ms.androidapp.common.logic.PermissionUtils;
import com.ms.androidapp.common.logic.ShareHelper;
import com.ms.androidapp.common.logic.TextViewLinkHandler;
import com.ms.androidapp.common.logic.urlimage.URLImageParser;
import com.ms.androidapp.datalayer.repository.B2CDataRepository;
import com.ms.androidapp.domain.model.explore.ExploreRView;
import com.ms.androidapp.domain.model.explore.OtherExploreDetailItem;
import com.ms.androidapp.domain.usecases.GetContentFull;
import com.ms.androidapp.presentation.base.CommonActivity;
import com.ms.androidapp.presentation.request.AskConciergeActivity;
import com.ms.androidapp.presentation.widget.DialogHelper;
import com.support.mylibrary.widget.LetterSpacingTextView;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by ThuNguyen on 6/8/2017.
 */

public class OtherExploreDetailActivity extends CommonActivity implements OtherExploreDetail.View{
    @BindView(R.id.loading)
    ProgressBar pbLoading;

    @BindView(R.id.explore_detail_layout)
    View exploreDetailLayout;
    @BindView(R.id.tvStub)
    TextView tvStub;
    @BindView(R.id.explore_detail_image)
    ImageView ivDetailImage;
    @BindView(R.id.explore_action_book)
    ImageButton ivActionBook;
    @BindView(R.id.explore_action_share)
    ImageButton ivActionShare;
    @BindView(R.id.explore_name)
    TextView tvExploreName;
    @BindView(R.id.explore_your_benefit_root_layout)
    View benefitLayout;
    @BindView(R.id.iv_benefit)
    ImageView ivBenefit;
    @BindView(R.id.your_benefit_content)
    LetterSpacingTextView tvBenefitContent;
    @BindView(R.id.explore_description_layout)
    View exploreDescriptionLayout;
    @BindView(R.id.explore_description)
    LetterSpacingTextView tvExploreDescription;
    @BindView(R.id.explore_terms_of_use_layout)
    View termsOfUseLayout;
    @BindView(R.id.explore_terms_of_use)
    LetterSpacingTextView tvTermsOfUse;
    OtherExploreDetailPresenter explorePresenter;
    DialogHelper dialogHelper;
    Bitmap detailBitmap;
    private OtherExploreDetailItem exploreRViewItem;
    private TextViewLinkHandler textViewLinkHandler = new TextViewLinkHandler() {
        @Override
        public void onLinkClick(String url) {
            new DialogHelper(OtherExploreDetailActivity.this).showLeavingAlert(url);
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_other_explore_detail);
        setToolbarColor(R.color.ms_colorPrimary);

        // Make hyperlink in textview clickable
        tvExploreDescription.setMovementMethod(textViewLinkHandler);
        tvBenefitContent.setMovementMethod(textViewLinkHandler);
        tvTermsOfUse.setMovementMethod(textViewLinkHandler);

        // Suppress the letter spacing
        ((LetterSpacingTextView)title).setLetterSpacing_(0f);
        title.setAllCaps(true);
        // Get data
        ExploreRView exploreRView = getIntent().getParcelableExtra(IntentConstant.EXPLORE_DETAIL);
        setTitle(Html.fromHtml(exploreRView.getTitle()));

        // Call explore detail
        pbLoading.setVisibility(View.VISIBLE);
        exploreDetailLayout.setVisibility(View.INVISIBLE);

        dialogHelper = new DialogHelper(this);

        explorePresenter = new OtherExploreDetailPresenter(new GetContentFull(new B2CDataRepository()));
        explorePresenter.attach(this);
        explorePresenter.getContent(exploreRView.getId());

        if(savedInstanceState == null){
            // Track GA
            App.getInstance().track(AppConstant.GA_TRACKING_SCREEN_NAME.VENUE_DETAIL.getValue());
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        explorePresenter.detach();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PermissionUtils.EXTERNAL_STORAGE_REQUEST_CODE:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Call share again
                    ShareHelper.getInstance().share(this, exploreRViewItem, detailBitmap);
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        if(requestCode == 9175 && resultCode == RESULT_OK) {
//            finish();
//        }
    }

    private void renderUI(){
        if(exploreRViewItem != null){
            // Description and image
            tvExploreDescription.setText(Html.fromHtml(HtmlUtils.adjustSomeHtmlTag(exploreRViewItem.description), new URLImageParser(tvExploreDescription, this, false, new URLImageParser.IImageParserCallback() {
                boolean isGetImageAtFirstTime;
                @Override
                public void onImageParserDone(String url) {
                    if(!TextUtils.isEmpty(url) && url.contains("http") && !isGetImageAtFirstTime) {
                        isGetImageAtFirstTime = true;
                        GlideHelper.getInstance().loadImage(url,
                                0, ivDetailImage, 0, new BaseTarget<Bitmap>() {
                                    @Override
                                    public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                                        detailBitmap = resource;
                                    }

                                    @Override
                                    public void getSize(SizeReadyCallback cb) {

                                    }
                                });
                    }
                }
            }), new HtmlTagHandler()));
            // Dining name
            if(TextUtils.isEmpty(exploreRViewItem.title)){
                tvExploreName.setVisibility(View.GONE);
            }else{
                tvExploreName.setVisibility(View.VISIBLE);
                tvExploreName.setText(Html.fromHtml(exploreRViewItem.title));
                setTitle(Html.fromHtml(exploreRViewItem.title));
            }

            if(TextUtils.isEmpty(exploreRViewItem.benefit)){
                benefitLayout.setVisibility(View.GONE);
            }else{
                // Benefit
                Spanned benefitSpanned = Html.fromHtml(HtmlUtils.adjustSomeHtmlTag(exploreRViewItem.benefit), new URLImageParser(tvBenefitContent, this, false, null),
                        new HtmlTagHandler());
                if(TextUtils.isEmpty(benefitSpanned.toString().trim())){
                    benefitLayout.setVisibility(View.GONE);
                }else{
                    benefitLayout.setVisibility(View.VISIBLE);
                    tvBenefitContent.setText(benefitSpanned);
                }
            }

            // Terms of Use
            tvTermsOfUse.setText(Html.fromHtml(HtmlUtils.adjustSomeHtmlTag(exploreRViewItem.getDisplayTermsOfUse()), null,
                    new HtmlTagHandler()));
            termsOfUseLayout.setVisibility( View.GONE);
//            tvTermsOfUse.setText(exploreRViewItem.getDisplayTermsOfUse());
        }
    }

    @OnClick({R.id.explore_action_book, R.id.explore_action_share})
    public void onClick(View view){
        switch (view.getId()){
            case R.id.explore_action_book:
                Intent intent = new Intent(this, AskConciergeActivity.class);
                intent.putExtra(IntentConstant.SELECTED_CATEGORY, exploreRViewItem.category);
                intent.putExtra(IntentConstant.AC_SUGGESTED_CONCIERGE, exploreRViewItem.title);
                startActivity(intent);
                break;
            case R.id.explore_action_share:
                ShareHelper.getInstance().share(this, exploreRViewItem, detailBitmap);
                break;
        }
    }

    @Override
    public void onGetContentFullFinished(OtherExploreDetailItem exploreRViewItem) {
        this.exploreRViewItem = exploreRViewItem;
        pbLoading.setVisibility(View.INVISIBLE);
        exploreDetailLayout.setVisibility(View.VISIBLE);
        renderUI();
    }

    @Override
    public void onUpdateFailed() {
        pbLoading.setVisibility(View.INVISIBLE);
    }

    @Override
    public void noInternetMessage() {
        dialogHelper.networkUnavailability(ErrCode.CONNECTIVITY_PROBLEM, null);
    }
}
