package com.ms.androidapp.domain.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.widget.EditText;

import com.ms.androidapp.domain.mapper.profile.MapDataApi;

/**
 * Created by vinh.trinh on 4/27/2017.
 */
public class Profile implements Parcelable {

    public static final int PHONE_MAX_LENGTH = 20;

    private String firstName;
    private String lastName;
    private String email;
    private String phone;
    private String salutation;
    private boolean locationOn;
    private String countryCode;
    private String zipCode;
    private String passCode;

    private String question;

    public Profile() {
        String temp = "";
        firstName = temp;
        lastName = temp;
        email = temp;
        phone = temp;
        salutation = temp;
        countryCode = temp;
        zipCode = temp;
        passCode = temp;
        question = temp;
    }

    public boolean anyIsEmpty() {
        /* || zipCode.length() == 0 */
        return firstName.trim().length() == 0 || lastName.trim().length() == 0 || email.trim().length() == 0 ||
                phone.trim().length() == 0;
    }

    public boolean allIsEmpty() {
        /* && firstSixDigits.length() == 0*/
        return firstName.trim().length() == 0 && lastName.trim().length() == 0 && email.trim().length() == 0 &&
                phone.trim().length() == 0 && zipCode.trim().length() == 0 &&  passCode.trim().length() == 0;
    }

    public boolean anyEmpty(EditText editTextPwd, EditText editeTextConfirmPwd) {
        return getFirstName().length() == 0 || getLastName().length() == 0 || getEmail().length() == 0 ||
                getPhone().length() == 0 || editTextPwd.getText().length() == 0 || editeTextConfirmPwd.getText().length() == 0
                || allIsEmpty();
    }

    public boolean anyEmpty(String editTextPwd, String editTextConfirmPass, String edtTextPassCode) {
        /* || firstSixDigits.length() == 0*/
        return editTextPwd.trim().length() == 0 || editTextConfirmPass.trim().length() == 0
                || edtTextPassCode.trim().length() == 0
                || anyIsEmpty();
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {

        phone = MapDataApi.mapPhoneResponse(phone);

        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDisplayCountryCodeAndPhone(){
        String valPhone = getPhone();
        if (valPhone == null || valPhone.isEmpty()) {
            return "";
        }
        String valuePhone = getPhone().replace("+", "");
        if (valuePhone.length() >= PHONE_MAX_LENGTH) {
            //don't do anything => hold original value phone
        } else {
            valuePhone = "+" + valuePhone;
        }
        return valuePhone;
    }

    public boolean isLocationOn() {
        return locationOn;
    }

    public void locationToggle(boolean on) {
        this.locationOn = on;
    }

    public String getSalutation() {
        return salutation;
    }

    public void setSalutation(String salutation) {
        this.salutation = salutation;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getPassCode() {
        return passCode;
    }

    public void setPassCode(String passCode) { this.passCode = passCode;}

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.firstName);
        dest.writeString(this.lastName);
        dest.writeString(this.email);
        dest.writeString(this.phone);
        dest.writeString(this.salutation);
        dest.writeByte(this.locationOn ? (byte) 1 : (byte) 0);
        dest.writeString(this.countryCode);
        dest.writeString(this.zipCode);
        dest.writeString(this.passCode);
        dest.writeString(this.question);
    }

    protected Profile(Parcel in) {
        this.firstName = in.readString();
        this.lastName = in.readString();
        this.email = in.readString();
        this.phone = in.readString();
        this.salutation = in.readString();
        this.locationOn = in.readByte() != 0;
        this.countryCode = in.readString();
        this.zipCode = in.readString();
        this.passCode = in.readString();
        this.question = in.readString();
    }

    public static final Creator<Profile> CREATOR = new Creator<Profile>() {
        @Override
        public Profile createFromParcel(Parcel source) {
            return new Profile(source);
        }

        @Override
        public Profile[] newArray(int size) {
            return new Profile[size];
        }
    };
}
