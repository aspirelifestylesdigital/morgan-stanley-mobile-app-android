package com.ms.androidapp.domain.mapper.profile;

import android.text.TextUtils;

import com.ms.androidapp.BuildConfig;

import static com.ms.androidapp.domain.model.Profile.PHONE_MAX_LENGTH;

public final class MapDataApi {

    private static final String SALUTATION_MAP_DOCTOR = "Doctor";
    private static final String SALUTATION_DR = "Dr";

    private static final String SALUTATION_MRS_DOT = "Mrs.";
    private static final String SALUTATION_MRS = "Mrs";

    private static final String SALUTATION_MR_DOT = "Mr.";
    private static final String SALUTATION_MR = "Mr";
    /*
    "enum":[
            "Captain",
            "Doctor",
            "Dr. (Miss)",
            "Dr. (Mr)",
            "Dr. (Mrs)",
            "Dr. (Ms)",
            "Fraulein",
            "Herr",
            "Madam",
            "Master",
            "Miss",
            "Mr.",
            "Mrs.",
            "Dr."
            "Ms",
            "Professor"
    ]*/
    /*
    <item>Dr</item>
    <item>Miss</item>
    <item>Mr</item>
    <item>Mrs</item>
    <item>Ms</item>
    */


    public static String mapSalutationRequest(String salutation) {
        if (TextUtils.isEmpty(salutation)) {
            return "";
        }

        String rs;

        if (SALUTATION_DR.equalsIgnoreCase(salutation.replace(".", ""))) {

            rs = SALUTATION_MAP_DOCTOR;

        } else if (SALUTATION_MR.equalsIgnoreCase(salutation.replace(".", ""))) {

            rs = SALUTATION_MR_DOT;

        } else if (SALUTATION_MRS.equalsIgnoreCase(salutation.replace(".", ""))) {

            rs = SALUTATION_MRS_DOT;

        } else {

            rs = salutation;

        }

        return rs;
    }

    public static String mapSalutationResponse(String salutation) {
        if (TextUtils.isEmpty(salutation)) {
            return "";
        }

        String rs;

        if (SALUTATION_MAP_DOCTOR.equalsIgnoreCase(salutation)) {

            rs = SALUTATION_DR;

        } else if (SALUTATION_MRS_DOT.equalsIgnoreCase(salutation)) {

            rs = SALUTATION_MRS;

        } else if (SALUTATION_MR_DOT.equalsIgnoreCase(salutation)) {

            rs = SALUTATION_MR;

        } else {

            rs = salutation;

        }

        return rs;
    }

    public static String mapPhoneResponse(String phone){
        if(TextUtils.isEmpty(phone)){
            return "";
        }

        phone = phone.replace("+", "");

        if(phone.length() > PHONE_MAX_LENGTH){
            phone = phone.substring(0, PHONE_MAX_LENGTH);
        }

        return phone;
    }

    /**
     * force hard code difference value for each project <br><br>
     * field: homeCountry and location[].address.country with value: <br>
     * - BDA: "BRA"         <br>
     * - MDA: "MEX"         <br>
     * - default: "USA"     <br>
     * */
    static String getCountryCode(){
        return "USA";
    }

    /*
    * Sign up (account.username, account.email)
    * Change password
    * Forgot password
    * OKTA Get Token (User)
    * */
    public static String getClientIdEmailApp(String email){
        return BuildConfig.AS_X_ORGANIZATION + "_" + email;
    }
}
