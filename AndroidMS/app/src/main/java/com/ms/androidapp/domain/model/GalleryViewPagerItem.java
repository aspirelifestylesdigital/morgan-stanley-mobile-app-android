package com.ms.androidapp.domain.model;

/**
 * Created by tung.phan on 5/9/2017.
 */

public class GalleryViewPagerItem {

    public final String imageURL;
    public final String title;
    public final String summary;
    public final int color;

    public GalleryViewPagerItem(String imageURL, String title, String summary, int color) {
        this.imageURL = imageURL;
        this.title = title;
        this.summary = summary;
        this.color = color;
    }
}
