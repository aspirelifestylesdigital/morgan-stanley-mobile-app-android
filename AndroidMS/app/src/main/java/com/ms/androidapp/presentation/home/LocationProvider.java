package com.ms.androidapp.presentation.home;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.support.v4.content.ContextCompat;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;

/**
 * Created by vinh.trinh on 7/14/2017.
 */

public class LocationProvider {
    private OnSuccessListener<Location> activity;
    private FusedLocationProviderClient fusedLocationClient;

    LocationProvider(OnSuccessListener<Location> activity) {
        this.activity = activity;
        fusedLocationClient = LocationServices.getFusedLocationProviderClient((Activity) activity);

    }

    void getLocation() {
        if (ContextCompat.checkSelfPermission((Context) activity, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission((Context) activity, Manifest.permission.ACCESS_FINE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {
            activity.onSuccess(null);
            return;
        }
        fusedLocationClient.getLastLocation().addOnSuccessListener((Activity) activity, activity);
    }
}
