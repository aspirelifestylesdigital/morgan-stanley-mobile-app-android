package com.ms.androidapp.presentation.changepass;

import com.api.aspire.common.constant.ErrCode;
import com.ms.androidapp.presentation.base.BasePresenter;

/**
 * Created by vinh.trinh on 4/26/2017.
 */

public interface ChangePassword {

    interface View {
        void showErrorDialog(ErrCode errCode,
                             String extraMsg);
        void showProgressDialog();
        void dismissProgressDialog();
        void showSuccessMessage();
    }

    interface Presenter extends BasePresenter<View> {
        void doChangePassword(String oldPassword, String newPassword);
        void abort();
    }

}
