package com.ms.androidapp.presentation.explore;

import android.os.Parcel;
import android.os.Parcelable;

import com.ms.androidapp.datalayer.entity.geocoder.GeoCoderResponse;
import com.ms.androidapp.domain.model.LatLng;

/**
 * Created by vinh.trinh on 7/28/2017.
 */

public class DiningSortingCriteria implements Parcelable{
    public static final Creator<DiningSortingCriteria> CREATOR = new Creator<DiningSortingCriteria>() {
        @Override
        public DiningSortingCriteria createFromParcel(Parcel in) {
            return new DiningSortingCriteria(in);
        }

        @Override
        public DiningSortingCriteria[] newArray(int size) {
            return new DiningSortingCriteria[size];
        }
    };
    LatLng location;
    GeoCoderResponse.AddressComponent[] addressComponents;
    String cuisine;
//    public float distance;
    boolean isRegion;
    public DiningSortingCriteria(LatLng location, String cuisine) {
        this.location = location;
        this.cuisine = cuisine;
    }
    public DiningSortingCriteria(GeoCoderResponse geoCoderResponse, String cuisine){
        if(geoCoderResponse != null){
            location = new LatLng(geoCoderResponse.location().getLat(), geoCoderResponse.location().getLng());
            addressComponents = geoCoderResponse.addressComponents();
        }
        this.cuisine = cuisine;
    }

    public DiningSortingCriteria(Parcel parcel){
        double latitude = parcel.readDouble();
        double longitude = parcel.readDouble();
        if(latitude != Double.MIN_VALUE){
            location = new LatLng(latitude, longitude);
        }
        Parcelable[] parcelableArray = parcel.readParcelableArray(GeoCoderResponse.AddressComponent.class.getClassLoader());
        if(parcelableArray != null && parcelableArray.length > 0){
            addressComponents = new GeoCoderResponse.AddressComponent[parcelableArray.length];
            for(int index = 0; index < parcelableArray.length; index++) {
                addressComponents[index] = (GeoCoderResponse.AddressComponent)parcelableArray[index];
            }
        }
        cuisine = parcel.readString();
        isRegion = parcel.readInt() == 1;
    }

    void setLocation(LatLng location) {
        this.location = location;
    }

    void setCuisine(String cuisine) {
        this.cuisine = cuisine;
    }

    void setIsRegion(boolean isRegion) {
        this.isRegion = isRegion;
    }

    public boolean isSameCity(String fullAddress){
        if(addressComponents != null && addressComponents.length > 0){
            for(GeoCoderResponse.AddressComponent addressComponent : addressComponents){
                boolean isSameCity = addressComponent.isSameCity(fullAddress);
                if(isSameCity){
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        if(location != null){
            parcel.writeDouble(location.latitude);
            parcel.writeDouble(location.longitude);
        }else{
            parcel.writeDouble(Double.MIN_VALUE);
            parcel.writeDouble(Double.MIN_VALUE);
        }
        parcel.writeParcelableArray(addressComponents, 0);
        parcel.writeString(cuisine);
        parcel.writeInt(isRegion ? 1 : 0);
    }
}
