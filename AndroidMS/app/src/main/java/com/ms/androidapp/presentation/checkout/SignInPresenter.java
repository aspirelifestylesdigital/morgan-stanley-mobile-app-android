package com.ms.androidapp.presentation.checkout;

import android.content.Context;

import com.api.aspire.common.constant.ConstantAspireApi;
import com.api.aspire.common.constant.ErrorApi;
import com.api.aspire.data.datasource.RemoteUserProfileOKTADataStore;
import com.api.aspire.data.preference.PreferencesStorageAspire;
import com.api.aspire.data.repository.ProfileDataAspireRepository;
import com.api.aspire.data.repository.UserProfileOKTADataRepository;
import com.api.aspire.domain.mapper.profile.ProfileLogicCore;
import com.api.aspire.domain.model.ProfileAspire;
import com.api.aspire.domain.repository.ProfileAspireRepository;
import com.api.aspire.domain.usecases.CheckPassCode;
import com.api.aspire.domain.usecases.GetToken;
import com.api.aspire.domain.usecases.SignInCase;
import com.ms.androidapp.App;
import com.ms.androidapp.R;
import com.api.aspire.common.exception.BackendException;
import com.api.aspire.common.constant.ErrCode;
import com.ms.androidapp.domain.usecases.MapProfileApp;

import io.reactivex.Completable;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableCompletableObserver;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by vinh.trinh on 4/26/2017.
 */

public class SignInPresenter implements SignIn.Presenter {

    private CompositeDisposable compositeDisposable;
    private SignIn.View view;
    private SignInCase signInCase;
    private PreferencesStorageAspire preferencesStorage;

    SignInPresenter(Context c) {
        compositeDisposable = new CompositeDisposable();
        MapProfileApp mapLogic = new MapProfileApp();
        this.preferencesStorage = new PreferencesStorageAspire(c);

        PreferencesStorageAspire preferencesStorage = new PreferencesStorageAspire(c);
        ProfileAspireRepository profileRepository = new ProfileDataAspireRepository(preferencesStorage, mapLogic);
        UserProfileOKTADataRepository userProfileOKTADataRepository = new UserProfileOKTADataRepository(new RemoteUserProfileOKTADataStore());
        GetToken getToken = new GetToken(preferencesStorage, mapLogic);
        this.signInCase = new SignInCase(mapLogic,
                profileRepository,
                userProfileOKTADataRepository,
                getToken);
    }

    @Override
    public void attach(SignIn.View view) {
        this.view = view;
    }

    @Override
    public void detach() {
        compositeDisposable.dispose();
        view = null;
    }

    @Override
    public void doSignIn(String email, String password) {
        if (!App.getInstance().hasNetworkConnection()) {
            view.showErrorDialog(ErrCode.CONNECTIVITY_PROBLEM, null);
            return;
        }
        view.showProgressDialog();


        SignInCase.Params params = new SignInCase.Params(email, password);
        Completable.create(e -> {
            //clean reset preference
            preferencesStorage.clear();
            e.onComplete();
        }).andThen(signInCase.loadProfileOKTACheckStatus(params.email).andThen(handleSignInCase(params)))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableCompletableObserver() {
                    @Override
                    public void onComplete() {
                        view.dismissProgressDialog();
                        view.proceedToHome();
                        dispose();
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view == null) return;

                        view.dismissProgressDialog();
                        if (ErrCode.PROFILE_OKTA_LOCKED_OUT.name().equals(e.getMessage())) {
                            view.showErrorDialog(ErrCode.API_ERROR, App.getInstance().getString(R.string.valid_login_okta_profile_error));
                        } else if (ErrCode.USER_PROFILE_OKTA_NOT_EXIST.name().equals(e.getMessage())
                                || ErrorApi.isGetTokenError(e.getMessage())) {
                            view.showErrorDialog(ErrCode.API_ERROR, App.getInstance().getString(R.string.valid_login_error));
                        } else {
                            view.showErrorDialog(ErrCode.UNKNOWN_ERROR, e.getMessage());
                        }

                        dispose();
                    }
                });
    }


    private Completable handleSignInCase(SignInCase.Params params) {
        return signInCase.getProfile(params).toCompletable();
    }

    @Override
    public void abort() {
        compositeDisposable.clear();
    }

}
