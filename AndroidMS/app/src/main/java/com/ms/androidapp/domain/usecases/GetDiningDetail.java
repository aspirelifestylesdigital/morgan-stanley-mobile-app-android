package com.ms.androidapp.domain.usecases;

import com.ms.androidapp.domain.mapper.MappingDetailRViewItem;
import com.ms.androidapp.domain.model.explore.DiningDetailItem;
import com.ms.androidapp.domain.repository.B2CRepository;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;

/**
 * Created by ThuNguyen on 6/6/2017.
 */

public class GetDiningDetail extends UseCase<DiningDetailItem, GetDiningDetail.Params> {

    private B2CRepository b2CRepository;

    public GetDiningDetail(B2CRepository b2CRepository) {
        this.b2CRepository = b2CRepository;
    }

    @Override
    Observable<DiningDetailItem> buildUseCaseObservable(Params params) {
        return null;
    }

    @Override
    Single<DiningDetailItem> buildUseCaseSingle(Params params) {
        return b2CRepository.getQandADetail(params.categoryId, params.itemId)
                .map(MappingDetailRViewItem::transformDiningDetail);
    }

    @Override
    Completable buildUseCaseCompletable(Params params) {
        return null;
    }

    public static final class Params {
        final Integer categoryId;
        final Integer itemId;

        public Params(Integer categoryId, Integer itemId) {
            this.categoryId = categoryId;
            this.itemId = itemId;
        }
    }
}
