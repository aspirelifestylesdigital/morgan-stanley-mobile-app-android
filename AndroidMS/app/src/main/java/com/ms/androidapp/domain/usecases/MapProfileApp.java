package com.ms.androidapp.domain.usecases;

import com.api.aspire.data.entity.profile.UpdateProfileAspireRequest;
import com.api.aspire.data.preference.PreferencesStorageAspire;
import com.api.aspire.domain.model.ProfileAspire;
import com.api.aspire.domain.usecases.MapProfileBase;
import com.ms.androidapp.BuildConfig;
import com.ms.androidapp.common.constant.CityData;
import com.ms.androidapp.domain.mapper.profile.MapDataApi;
import com.ms.androidapp.domain.mapper.profile.MapDataProfile;

import io.reactivex.Completable;
import io.reactivex.Single;

public class MapProfileApp implements MapProfileBase {

    @Override
    public Single<ProfileAspire> mapResponse(ProfileAspire profileAspire,
                                             String secretKeyCurrent,
                                             String userEmailCurrent) {
        return Single.create(emitter -> {
            ProfileAspire profile = MapDataProfile.mapResponse(profileAspire, secretKeyCurrent, userEmailCurrent);
            emitter.onSuccess(profile);
        });
    }

    @Override
    public Completable handlePreferenceApp(PreferencesStorageAspire prefStorage,
                                           ProfileAspire profileAspire) {
        return Completable.create(e -> {

            //-- save select city local
            String nameCity = CityData.cityNameDefaultEmpty();
            prefStorage.editor().selectedCity(nameCity).build().save();

            e.onComplete();
        });
    }

    @Override
    public UpdateProfileAspireRequest mapUpdate(PreferencesStorageAspire pref,
                                                ProfileAspire profile) {
        //binCode local
//        String passCodeLocal = pref.getBinCode();
        return MapDataProfile.mapUpdate(profile);
    }

    @Override
    public String mapSalutation(String profileSalutation) {
        return MapDataApi.mapSalutationRequest(profileSalutation);
    }

    @Override
    public String getXAppId() {
        return BuildConfig.AS_X_APP_ID;
    }

    @Override
    public String getXOrganization() {
        return BuildConfig.AS_X_ORGANIZATION;
    }

    @Override
    public String convertClientIdEmail(String email) {
        return MapDataApi.getClientIdEmailApp(email);
    }

    @Override
    public String getAppUserNamePMA() {
        return BuildConfig.AS_PMA_TOKEN_USERNAME;
    }

    @Override
    public String getAppSecretPMA() {
        return BuildConfig.AS_PMA_TOKEN_SECRET;
    }

    @Override
    public String getTokenServiceUserName() {
        return BuildConfig.AS_TOKEN_USERNAME;
    }

    @Override
    public String getTokenServiceSecret() {
        return BuildConfig.AS_TOKEN_SECRET;
    }
}
