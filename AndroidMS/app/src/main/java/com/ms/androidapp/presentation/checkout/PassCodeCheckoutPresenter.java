package com.ms.androidapp.presentation.checkout;

import android.content.Context;

import com.api.aspire.common.constant.ErrCode;
import com.api.aspire.common.constant.ErrorApi;
import com.api.aspire.data.preference.PreferencesStorageAspire;
import com.api.aspire.data.repository.AuthDataAspireRepository;
import com.api.aspire.data.repository.ProfileDataAspireRepository;
import com.api.aspire.domain.model.ProfileAspire;
import com.api.aspire.domain.repository.AuthAspireRepository;
import com.api.aspire.domain.usecases.CheckPassCode;
import com.api.aspire.domain.usecases.GetAccessToken;
import com.api.aspire.domain.usecases.GetToken;
import com.api.aspire.domain.usecases.LoadProfile;
import com.api.aspire.domain.usecases.SaveProfile;
import com.ms.androidapp.App;
import com.ms.androidapp.domain.usecases.MapProfileApp;


import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableCompletableObserver;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class PassCodeCheckoutPresenter implements PassCodeCheckout.Presenter {

    private CompositeDisposable compositeDisposable;
    private PassCodeCheckout.View view;

    private CheckPassCode checkPassCode;
    private PreferencesStorageAspire preferencesStorage;

    public PassCodeCheckoutPresenter(Context c) {
        this.compositeDisposable = new CompositeDisposable();
        MapProfileApp mapLogic = new MapProfileApp();
        preferencesStorage = new PreferencesStorageAspire(c);
        AuthAspireRepository repositoryAuth = new AuthDataAspireRepository();
        ProfileDataAspireRepository profileRepository = new ProfileDataAspireRepository(preferencesStorage, mapLogic);
        LoadProfile mLoadProfile = new LoadProfile(profileRepository);
        GetToken getToken = new GetToken(preferencesStorage, mapLogic);
        GetAccessToken mGetAccessToken = new GetAccessToken(mLoadProfile, getToken);
        SaveProfile saveProfile = new SaveProfile(profileRepository);
        this.checkPassCode = new CheckPassCode(mapLogic,
                preferencesStorage,
                repositoryAuth,
                mGetAccessToken,
                mLoadProfile,
                saveProfile,
                getToken
        );
    }

    @Override
    public void attach(PassCodeCheckout.View view) {
        this.view = view;
    }

    @Override
    public void detach() {
        compositeDisposable.dispose();
        view = null;
    }

    private boolean isNoInternet() {
        boolean status;
        if (!App.getInstance().hasNetworkConnection()) {
            view.dismissProgressDialog();
            view.showErrorDialog(ErrCode.CONNECTIVITY_PROBLEM, null);
            status = true;
        } else {
            status = false;
        }
        return status;
    }

    @Override
    public void checkoutPassCode(final String input) {
        if (isNoInternet()) {
            return;
        }

        view.showProgressDialog();
        compositeDisposable.add(
                checkPassCode.buildUseCaseSingle(new CheckPassCode.Params(input))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new PassCodeObserver())
        );
    }

    private final class PassCodeObserver extends DisposableSingleObserver<Boolean> {

        @Override
        public void onSuccess(Boolean aBoolean) {
            view.dismissProgressDialog();
            if (aBoolean != null && aBoolean)
                view.onCheckPassCodeSuccessfully();
            else
                view.onCheckPassCodeFailure();
            dispose();
        }

        @Override
        public void onError(Throwable e) {
            view.dismissProgressDialog();
            view.onCheckPassCodeFailure();
            dispose();
        }
    }

    @Override
    public void handleSaveUserPrefPassCode(String newPassCode) {
        if (isNoInternet()) {
            return;
        }
        view.showProgressDialog();
        compositeDisposable.add(checkPassCode.saveBinCodeInProfile(newPassCode)
                .andThen(Completable.create(e -> {

                    preferencesStorage.editor().binCode(newPassCode).build().save();
                    e.onComplete();
                }))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(completableUpdateBinCode)
        );
    }

    @Override
    public void handleUpdateBinCodeSignIn(String newPassCode, ProfileAspire profileSignIn) {
        if (isNoInternet()) {
            return;
        }
        view.showProgressDialog();
        compositeDisposable.add(checkPassCode.updateBinCodeSignIn(newPassCode, profileSignIn)
                .andThen(Completable.create(e -> {

                    preferencesStorage.editor().binCode(newPassCode).build().save();
                    e.onComplete();
                }))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(completableUpdateBinCode)
        );
    }

    private DisposableCompletableObserver completableUpdateBinCode = new DisposableCompletableObserver() {
        @Override
        public void onComplete() {
            if(view == null) return;
            view.dismissProgressDialog();
            view.updatePassCodeApiCompleted();
            dispose();
        }

        @Override
        public void onError(Throwable e) {
            if(view == null) return;
            view.dismissProgressDialog();
            if (ErrorApi.isGetTokenError(e.getMessage())) {
                view.onErrorGetToken();
            } else {
                view.onCheckPassCodeFailure();
            }
            dispose();
        }
    };

    //-- Splash Activity
    @Override
    public void handleSplashCheckPassCode() {
        if (isNoInternet()) {
            return;
        }
        view.showProgressDialog();
        compositeDisposable.add(
                checkPassCode.handleCheckPassCode()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new SplashPassCodeObserver())
        );
    }

    private class SplashPassCodeObserver extends DisposableSingleObserver<CheckPassCode.ResultPassCode> {
        @Override
        public void onSuccess(CheckPassCode.ResultPassCode resultPassCode) {
            view.dismissProgressDialog();
            if (resultPassCode == null) {
                view.onCheckPassCodeFailure();
            } else {
                switch (resultPassCode.passCodeStatus) {
                    case VALID:
                        view.onCheckPassCodeSuccessfully();
                        break;
                    case INVALID:
                        view.onCheckPassCodeFailure();
                        break;
                    case NONE:
                    default:
                        view.onCheckPassCodeNone();
                        break;
                }
            }
            dispose();
        }

        @Override
        public void onError(Throwable e) {
            view.dismissProgressDialog();
            if (ErrorApi.isGetTokenError(e.getMessage())) {
                view.onErrorGetToken();
            } else {
                view.onCheckPassCodeFailure();
            }
            dispose();
        }
    }

    public PreferencesStorageAspire getPreferencesStorage() {
        return preferencesStorage;
    }
}
