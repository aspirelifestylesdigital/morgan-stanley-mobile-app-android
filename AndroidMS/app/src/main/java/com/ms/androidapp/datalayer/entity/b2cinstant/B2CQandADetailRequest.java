package com.ms.androidapp.datalayer.entity.b2cinstant;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ms.androidapp.BuildConfig;

/**
 * Created by Thu Nguyen on 6/5/2017.
 */

public class B2CQandADetailRequest {
    @SerializedName("subDomain")
    @Expose
    private String subDomain;
    @SerializedName("password")
    @Expose
    private String secretKey;
    @SerializedName("categoryID")
    @Expose
    private Integer categoryID;
    @SerializedName("questionID")
    @Expose
    private Integer questionID;

    public B2CQandADetailRequest(){
        subDomain = BuildConfig.WS_B2C_SUBDOMAIN;
        secretKey = BuildConfig.WS_B2C_SECRET;
    }
    public String getSubDomain() {
        return subDomain;
    }

    public void setSubDomain(String subDomain) {
        this.subDomain = subDomain;
    }

    public String getPassword() {
        return secretKey;
    }

    public void setPassword(String password) {
        this.secretKey = password;
    }

    public Integer getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(Integer categoryID) {
        this.categoryID = categoryID;
    }

    public Integer getQuestionID() {
        return questionID;
    }

    public void setQuestionID(Integer questionID) {
        this.questionID = questionID;
    }
}
