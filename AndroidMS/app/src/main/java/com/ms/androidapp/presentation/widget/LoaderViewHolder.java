package com.ms.androidapp.presentation.widget;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.ms.androidapp.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by vinh.trinh on 6/6/2017.
 */

public class LoaderViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.progressbar)
    ProgressBar mProgressBar;

    public LoaderViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}