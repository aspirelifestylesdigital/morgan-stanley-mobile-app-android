package com.ms.androidapp.presentation.changepass;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatButton;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import com.api.aspire.common.constant.ErrorApi;
import com.ms.androidapp.App;
import com.ms.androidapp.R;
import com.ms.androidapp.common.constant.AppConstant;
import com.api.aspire.common.constant.ErrCode;
import com.ms.androidapp.common.logic.Validator;
import com.ms.androidapp.presentation.base.CommonActivity;
import com.ms.androidapp.presentation.home.HomeActivity;
import com.ms.androidapp.presentation.widget.DialogHelper;
import com.ms.androidapp.presentation.widget.PasswordInputFilter;
import com.ms.androidapp.presentation.widget.ViewKeyboardListener;
import com.ms.androidapp.presentation.widget.ViewUtils;
import com.support.mylibrary.widget.ErrorIndicatorEditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ChangePasswordActivity extends CommonActivity implements ChangePassword.View {
    @BindView(R.id.edt_old_pwd)
    ErrorIndicatorEditText edtOldSecret;
    @BindView(R.id.edt_new_pwd)
    ErrorIndicatorEditText edtNewSecret;
    @BindView(R.id.edt_retype_pwd)
    ErrorIndicatorEditText edtRetypeSecret;
    @BindView(R.id.btn_submit)
    AppCompatButton btnSubmit;
    @BindView(R.id.btn_cancel)
    AppCompatButton btnCancel;
    @BindView(R.id.content_wrapper)
    LinearLayout contentWrapper;
    @BindView(R.id.scroll_view)
    ScrollView scroll;
    @BindView(R.id.contentSubmit)
    LinearLayout contentSubmit;
    @BindView(R.id.llParent)
    LinearLayout llParent;
    @BindView(R.id.llChangePassRoot)
    LinearLayout llRootView;
    DialogHelper dialogHelper;
    ChangePasswordPresenter presenter;
    Validator validator;
    String errMessage;
    private boolean hasForgotSecret;

    private ChangePasswordPresenter presenter() {
        return new ChangePasswordPresenter(getApplicationContext());
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        ButterKnife.bind(this);
        setTitle(R.string.title_change_password);
        hasForgotSecret = !TextUtils.isEmpty(getIntent().getStringExtra(Intent.EXTRA_REFERRER));
        if (hasForgotSecret) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            disableSwipe();
            back.setVisibility(View.INVISIBLE);
        }

        dialogHelper = new DialogHelper(this);
        presenter = presenter();
        presenter.attach(this);
        validator = new Validator();

        keyboardInteractListener();
//        edtRetypeSecret.addTextChangedListener(new DiscardSpaceTextWatcher(edtRetypeSecret, new DiscardSpaceTextWatcher.ITextWatcherListener() {
//            @Override
//            public void onWatcherFinished() {
//                enableButton();
//            }
//        }));
//        edtNewSecret.addTextChangedListener(new DiscardSpaceTextWatcher(edtNewSecret, new DiscardSpaceTextWatcher.ITextWatcherListener() {
//            @Override
//            public void onWatcherFinished() {
//                enableButton();
//            }
//        }));
//        edtOldSecret.addTextChangedListener(new DiscardSpaceTextWatcher(edtOldSecret, new DiscardSpaceTextWatcher.ITextWatcherListener() {
//            @Override
//            public void onWatcherFinished() {
//                enableButton();
//            }
//        }));
        InputFilter[] filtersPassword = new InputFilter[]{new PasswordInputFilter(), new InputFilter.LengthFilter(getResources().getInteger(R.integer.password_max_length_characters))};
        edtOldSecret.setFilters(filtersPassword);
        edtNewSecret.setFilters(filtersPassword);
        edtRetypeSecret.setFilters(filtersPassword);

        edtOldSecret.addTextChangedListener(textWatcher);
        edtNewSecret.addTextChangedListener(textWatcher);
        edtRetypeSecret.addTextChangedListener(textWatcher);
        if (savedInstanceState == null) {
            // Track GA
            App.getInstance().track(AppConstant.GA_TRACKING_SCREEN_NAME.CHANGE_PASSWORD.getValue());
        }

        scroll.setOnTouchListener(this::onTouchScroll);
        handleButtonEnableView(false);
        //initTextInteractListener();
    }

    /**
     * handle click on back hide keyboard close cursor
     */
    private void keyboardInteractListener() {
        ViewKeyboardListener.KeyboardEvent event = new ViewKeyboardListener.KeyboardEvent() {
            @Override
            public void showKeyboard() {
                //dummy do nothing
            }

            @Override
            public void hideKeyboard() {
                //dummy do nothing
            }

            @Override
            public View getCurrentFocus() {
                return ChangePasswordActivity.this.getCurrentFocus();
            }
        };
        //-- add listen keyboard
        ViewKeyboardListener keyboardListener = new ViewKeyboardListener(llRootView, event);
        keyboardListener.setViewFocusChangeListener(llParent);

    }

    //<editor-fold desc="handle button UnEnable">
    private long lastTouchDown = 0;
    private static final int CLICK_ACTION_HOLD = 200;

    private boolean onTouchScroll(View view, MotionEvent ev) {
        if (ev.getAction() == MotionEvent.ACTION_DOWN) {
            lastTouchDown = System.currentTimeMillis();
        } else if (ev.getAction() == MotionEvent.ACTION_UP) {
            if (System.currentTimeMillis() - lastTouchDown < CLICK_ACTION_HOLD) {
                onClick(view);
            }
            lastTouchDown = 0;
        }
        return false;
    }

    @OnClick({R.id.llChangePassRoot, R.id.content_wrapper, R.id.llParent, R.id.scroll_view, R.id.contentSubmit, R.id.toolbar})
    public void onClick(View view) {
        ViewUtils.hideSoftKey(view);
        if (getCurrentFocus() != null) {
            getCurrentFocus().clearFocus();
        }
    }
    //</editor-fold>

    @Override
    protected void onDestroy() {
        presenter.detach();
        super.onDestroy();
    }

    @OnClick(R.id.btn_submit)
    public void submitClick(View view) {
        hideErrorLine();
        final ErrorIndicatorEditText validateFields = validateFields(edtOldSecret.getText().toString(), edtNewSecret.getText().toString(), edtRetypeSecret.getText().toString());
        if (validateFields == null) {
            presenter.doChangePassword(edtOldSecret.getText().toString(), edtNewSecret.getText().toString());
        } else {
            dialogHelper.profileDialog(errMessage, dialog -> {
                if (view != null) {
                    view.postDelayed(() -> {
                        validateFields.requestFocus();
                        ViewUtils.showSoftKey(validateFields);
                    }, 130);
                }
            });
        }
    }

    private void hideErrorLine() {
        edtOldSecret.setError(null);
        edtNewSecret.setError(null);
        edtRetypeSecret.setError(null);
    }

    @OnClick(R.id.btn_cancel)
    public void cancelClick(View view) {
        ViewUtils.hideSoftKey(view);
        llParent.requestFocus();
        resetView();
    }

    private void enableButton() {
        if (!TextUtils.isEmpty(edtOldSecret.getText().toString()) || !TextUtils.isEmpty(edtRetypeSecret.getText().toString()) || !TextUtils.isEmpty(edtNewSecret.getText().toString())) {
            handleButtonEnableView(true);
        } else {
            handleButtonEnableView(false);
        }
    }

    private void handleButtonEnableView(boolean enable) {
        btnCancel.setEnabled(enable);
        btnSubmit.setEnabled(enable);
        btnCancel.setClickable(enable);
        btnSubmit.setClickable(enable);
    }

    private void resetView() {
        edtOldSecret.setText("");
        edtNewSecret.setText("");
        edtRetypeSecret.setText("");
        edtOldSecret.setError(null);
        edtNewSecret.setError(null);
        edtRetypeSecret.setError(null);
    }

    private void proceedToHome() {
        Intent intent = new Intent(this, HomeActivity.class);
        startActivity(intent);
    }

    // presenter
    @Override
    public void showErrorDialog(ErrCode errCode, String extraMsg) {
        if (dialogHelper.networkUnavailability(errCode, extraMsg)) return;
        if (ErrorApi.isChangePasswordNotMatch(extraMsg)) {
            showDialogErrorApi(App.getInstance().getString(R.string.input_err_fields),
                    App.getInstance().getString(R.string.change_password_api_not_match));
        } else if (ErrorApi.isChangePasswordWrong(extraMsg)) {
            showDialogErrorApiNewPassWord(App.getInstance().getString(R.string.invalid_create_pwd_title),
                    App.getInstance().getString(R.string.invalid_create_pwd));
        } /*else if (errCode == ErrCode.API_ERROR) {
            dialogHelper.alert(App.getInstance().getString(R.string.errorTitle), extraMsg);
        } */ else {
            dialogHelper.showGeneralError();
        }
    }

    private void showDialogErrorApi(String title, String errMessage) {
        dialogHelper.alert(title, errMessage,
                dialogEventOk -> edtOldSecret.postDelayed(() -> {
                    edtOldSecret.setError("");
                    edtOldSecret.requestFocus();
                    ViewUtils.showSoftKey(edtOldSecret);
                }, 100));
    }
    private void showDialogErrorApiNewPassWord(String title, String errMessage) {
        dialogHelper.alert(title, errMessage,
                dialogEventOk -> edtNewSecret.postDelayed(() -> {
                    edtNewSecret.setError("");
                    edtNewSecret.requestFocus();
                    ViewUtils.showSoftKey(edtNewSecret);
                }, 100));
    }

    // presenter
    @Override
    public void showProgressDialog() {
        if (!isDestroyed() && !isFinishing()) {
            dialogHelper.showProgress();
        }
    }

    // presenter
    @Override
    public void dismissProgressDialog() {
        if (!isDestroyed() && !isFinishing()) {
            dialogHelper.dismissProgress();
        }
    }

    @Override
    public void showSuccessMessage() {
        dialogHelper.alert(null, getString(R.string.change_password_message), dialogInterface -> {
            if (hasForgotSecret) proceedToHome();
            finish();
        });
    }

    private ErrorIndicatorEditText validateFields(String oldPassword, String newPassword, String retypePassword) {
        ErrorIndicatorEditText valid = null;
        StringBuilder stringBuilder = new StringBuilder();
        boolean invalidNewPwd = false;
        boolean oldPwdEmpty = TextUtils.isEmpty(oldPassword.trim());
        boolean newPwdEmpty = TextUtils.isEmpty(newPassword.trim());
        boolean retypePwdEmpty = TextUtils.isEmpty(retypePassword.trim());

        if (oldPwdEmpty || newPwdEmpty || retypePwdEmpty) {
            //-- old pwd
            if (oldPwdEmpty) {
                edtOldSecret.setError("");
                valid = edtOldSecret;
            } else {
                edtOldSecret.setError(null);
            }

            //-- new pwd
            if (newPwdEmpty) {
                edtNewSecret.setError("");
                if (valid == null) {
                    valid = edtNewSecret;
                }
            } else {
                edtNewSecret.setError(null);
            }

            //-- retype pwd
            if (retypePwdEmpty) {
                edtRetypeSecret.setError("");
                if (valid == null) {
                    valid = edtRetypeSecret;
                }
            } else {
                edtRetypeSecret.setError(null);
            }
            stringBuilder.append(getString(R.string.input_err_required));
        }


//        if (!oldPwdEmpty && oldPassword.length() < 5) {
//            edtOldSecret.setError("");
//            if (stringBuilder.length() > 0) stringBuilder.append("\n");
//            stringBuilder.append(getString(R.string.input_err_old_pwd_failed));
//            if (valid == null)
//                valid = edtOldSecret;
//        }

//        if (!newPwdEmpty && !validator.secretValidator(newPassword.trim())) {
//            edtNewSecret.setError("");
//            if (stringBuilder.length() > 0) stringBuilder.append("\n");
//            stringBuilder.append(getString(R.string.invalid_create_pwd_2));
//            if (valid == null) {
//                valid = edtNewSecret;
//            } else {
//                invalidNewPwd = true;
//            }
//        }
        if (!retypePwdEmpty) {
            boolean validPasswordCompare = validator.password(newPassword, retypePassword);
            if (!validPasswordCompare) {
                edtRetypeSecret.setError("");
                if (stringBuilder.length() > 0) stringBuilder.append("\n");
                stringBuilder.append(getString(R.string.input_err_pwd_confirmation_failed));
                if (valid == null)
                    valid = edtRetypeSecret;
            }//else do nothing

            //case the same pass & retypePass but retype not show => show both
//            if(validPasswordCompare && edtNewSecret.isError() && !edtRetypeSecret.isError()){
//                edtRetypeSecret.setError("");
//            }
        }
        //priority focus error
        if (valid != null) {
            //case: retypePwd empty but newPwd invalidate -> focus newPwd (no focus retypePwd)
            if (invalidNewPwd && valid.getId() == edtRetypeSecret.getId()) {
                valid = edtNewSecret;
            }
        }

        errMessage = stringBuilder.toString();
        return valid;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            super.onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (!hasForgotSecret) super.onBackPressed();
    }

    TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            enableButton();
        }
    };
}
