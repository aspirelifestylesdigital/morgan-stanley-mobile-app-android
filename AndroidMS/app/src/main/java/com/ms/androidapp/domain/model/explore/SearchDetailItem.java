package com.ms.androidapp.domain.model.explore;

import android.os.Parcel;

/**
 * Created by vinh.trinh on 6/23/2017.
 */

public class SearchDetailItem extends ExploreRView {

    public final Integer ID;
    public final Integer secondaryID;
    public final String title;
    public final String category; // IA, ??
    public int cityGuideSubCategoryIndex = -1;

    public SearchDetailItem(Integer ID, Integer secondaryID, String title, String category) {
        this.ID = ID;
        this.secondaryID = secondaryID;
        this.title = title;
        this.category = category;
    }

    protected SearchDetailItem(Parcel in) {
        this.ID = in.readInt();
        this.secondaryID = in.readInt();
        this.title = in.readString();
        this.category = in.readString();
        this.cityGuideSubCategoryIndex = in.readInt();
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public String getDescription() {
        return null;
    }

    @Override
    public String getImageUrl() {
        return null;
    }

    @Override
    public Integer getId() {
        return ID;
    }

    @Override
    public String getSuggestedToAC() {
        return null;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(ID);
        dest.writeInt(secondaryID);
        dest.writeString(title);
        dest.writeString(category);
        dest.writeInt(cityGuideSubCategoryIndex);
    }

    public static final Creator<SearchDetailItem> CREATOR = new Creator<SearchDetailItem>() {
        @Override
        public SearchDetailItem createFromParcel(Parcel in) {
            return new SearchDetailItem(in);
        }

        @Override
        public SearchDetailItem[] newArray(int size) {
            return new SearchDetailItem[size];
        }
    };
}
