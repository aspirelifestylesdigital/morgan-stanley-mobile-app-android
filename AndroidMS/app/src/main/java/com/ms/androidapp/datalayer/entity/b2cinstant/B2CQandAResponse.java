package com.ms.androidapp.datalayer.entity.b2cinstant;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ms.androidapp.datalayer.entity.GetQuestionsAndAnswersResult;

/**
 * Created by tung.phan on 6/1/2017.
 */

public class B2CQandAResponse {

    @SerializedName("GetQuestionsAndAnswersResult")
    @Expose
    private GetQuestionsAndAnswersResult getQuestionsAndAnswersResult;

    public GetQuestionsAndAnswersResult getGetQuestionsAndAnswersResult() {
        return getQuestionsAndAnswersResult;
    }

    public void setGetQuestionsAndAnswersResult(GetQuestionsAndAnswersResult getQuestionsAndAnswersResult) {
        this.getQuestionsAndAnswersResult = getQuestionsAndAnswersResult;
    }

}
