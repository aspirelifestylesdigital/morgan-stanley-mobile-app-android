package com.ms.androidapp.presentation.selectcategory;

import android.content.Context;

import com.api.aspire.data.preference.PreferencesStorageAspire;
import com.api.aspire.data.repository.ProfileDataAspireRepository;
import com.api.aspire.domain.model.ProfileAspire;
import com.api.aspire.domain.repository.ProfileAspireRepository;
import com.ms.androidapp.common.GPSChecker;
import com.api.aspire.domain.usecases.LoadProfile;
import com.ms.androidapp.domain.usecases.MapProfileApp;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by tung.phan on 5/31/2017.
 */

public class CategoryPresenter implements Category.Presenter {
    public static boolean CATEGORY_ALL = true;
    private CompositeDisposable disposables;
    private Category.View view;
    private LoadProfile loadProfile;
    private ProfileAspire profile;

    CategoryPresenter(Context context) {
        PreferencesStorageAspire pref = new PreferencesStorageAspire(context);
        ProfileAspireRepository profileRepository = new ProfileDataAspireRepository(pref, new MapProfileApp());
        this.loadProfile = new LoadProfile(profileRepository);
    }

    @Override
    public void attach(Category.View view) {
        this.view = view;
        disposables = new CompositeDisposable();
    }

    @Override
    public void detach() {
        disposables.dispose();
        this.view = null;
    }

    @Override
    public void loadProfile() {
        loadProfile.param(null)
                .on(Schedulers.io(), AndroidSchedulers.mainThread())
                .execute(new DisposableSingleObserver<ProfileAspire>() {
                    @Override
                    public void onSuccess(ProfileAspire profile) {
                        CategoryPresenter.this.profile = profile;
                    }

                    @Override
                    public void onError(Throwable e) {
                    }
                });
    }

    @Override
    public boolean isLocationProfileSettingOn() {
        return profile != null && profile.isLocationOn();
    }

    @Override
    public void locationServiceCheck(Context context) {
        if (GPSChecker.GPSEnable(context)) {
            view.proceedWithDiningCategory();
        } else {
            view.askForLocationSetting();
        }
    }
}
