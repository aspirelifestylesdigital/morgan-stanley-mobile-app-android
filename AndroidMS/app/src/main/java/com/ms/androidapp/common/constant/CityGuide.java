package com.ms.androidapp.common.constant;

import android.util.SparseIntArray;

/**
 * Created by vinh.trinh on 6/7/2017.
 */

public class CityGuide {
    /*public static final int ACCOMMODATION_LIST_INDEX = 0;*/
    public static final int BAR_LIST_INDEX = 0;
    public static final int SPA_LIST_INDEX = 4;
    public static final int DINNING_LIST_INDEX = 2;
    public static final int SHOPPING_LIST_INDEX = 3;
    public static final int CULTURE_LIST_INDEX = 1;
//    public final int accommodation;
    public final int dining;
    public final int bar;
    public final int spa;
    public final int shopping;
    public final int culture;
    private final SparseIntArray guides;

    /*CityGuide(int accommodation) {
        this.accommodation = accommodation;
        dining = 0;
        bar = 0;
        spa = 0;
        shopping = 0;
        culture = 0;
        guides = new SparseIntArray(1);
        guides.append(ACCOMMODATION_LIST_INDEX, accommodation);
    }*/

    CityGuide(int dining, int bar, int spa, int shopping, int culture) {
//        this.accommodation = accommodation;
        this.dining = dining;
        this.bar = bar;
        this.spa = spa;
        this.shopping = shopping;
        this.culture = culture;
        guides = new SparseIntArray(5);
//        guides.append(ACCOMMODATION_LIST_INDEX, accommodation);
        guides.append(DINNING_LIST_INDEX, dining);
        guides.append(BAR_LIST_INDEX, bar);
        guides.append(SPA_LIST_INDEX, spa);
        guides.append(SHOPPING_LIST_INDEX, shopping);
        guides.append(CULTURE_LIST_INDEX, culture);
    }

    int index(int val) {
        for (int i = 0; i < guides.size(); i++) {
            Integer key = guides.keyAt(i);
            if(guides.get(key) == val) return key;
        }
        return -1;
    }

    int at(int index) {
        if(index < 0 || index >= guides.size()) {
            throw new IllegalArgumentException("index out of bound");
        }
        return guides.get(index);
    }

    boolean has(int id) {
        return guides.indexOfValue(id) >=0;
    }

    public int size() {
        return guides.size();
    }
}
