package com.ms.androidapp.presentation.checkout;

import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;

import com.api.aspire.common.constant.ErrCode;
import com.api.aspire.data.preference.PreferencesStorageAspire;
import com.api.aspire.domain.model.ProfileAspire;
import com.ms.androidapp.App;
import com.ms.androidapp.R;
import com.ms.androidapp.common.constant.IntentConstant;
import com.ms.androidapp.common.logic.Validator;
import com.ms.androidapp.presentation.base.CommonActivity;
import com.ms.androidapp.presentation.changepass.ChangePasswordActivity;
import com.ms.androidapp.presentation.home.HomeActivity;
import com.ms.androidapp.presentation.profile.signUpV2.SignUpV2Activity;
import com.ms.androidapp.presentation.widget.DialogHelper;
import com.ms.androidapp.presentation.widget.ViewKeyboardListener;
import com.ms.androidapp.presentation.widget.ViewScrollViewListener;
import com.ms.androidapp.presentation.widget.ViewUtils;
import com.support.mylibrary.widget.ErrorIndicatorEditText;
import com.support.mylibrary.widget.LetterSpacingTextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by son.ho on 10/23/2017.
 */

public class NewPassCodeActivity extends CommonActivity implements PassCodeCheckout.View {

    @BindView(R.id.edt_passcode)
    ErrorIndicatorEditText edtPasscode;
    @BindView(R.id.btn_submit_passcode)
    Button btnPassCodeSubmit;
    PassCodeCheckoutPresenter presenter;
    DialogHelper dialogHelper;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.title)
    LetterSpacingTextView tittle;
    @BindView(R.id.scrollView)
    ScrollView mainScrollView;
    @BindView(R.id.content_wrapper)
    LinearLayout contentWrapper;
    @BindView(R.id.llParent)
    LinearLayout llParent;
    @BindView(R.id.root_view)
    RelativeLayout llProfileParentView;
    //-- variable
    private boolean navigateFromSignUp;
    private ProfileAspire profileSignIn;
    private ViewKeyboardListener keyboardListener;

    private PassCodeCheckoutPresenter passCodeCheckoutPresenter() {
        return new PassCodeCheckoutPresenter(this);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_passcode);
        ButterKnife.bind(this);

        tittle.setText("CONCIERGE");
        keyboardInteractListener();
        btnPassCodeSubmit.setEnabled(false);
        presenter = passCodeCheckoutPresenter();
        presenter.attach(this);
        dialogHelper = new DialogHelper(this);

        edtPasscode.setTransformationMethod(PasswordTransformationMethod.getInstance());

        edtPasscode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                int count = editable.toString().length();
                if (count > 0) {
                    btnPassCodeSubmit.setEnabled(true);
                } else {
                    btnPassCodeSubmit.setEnabled(false);
                }
            }
        });

        // Get extra
        navigateFromSignUp = getIntent().getBooleanExtra(IntentConstant.SIGN_UP,false);
        if (getIntent().hasExtra(IntentConstant.SIGN_IN_PROFILE)) {
            String profileStr = getIntent().getStringExtra(IntentConstant.SIGN_IN_PROFILE);
            if (!TextUtils.isEmpty(profileStr)) {
                this.profileSignIn = new ProfileAspire(profileStr);
            }
        }

        boolean isShowBack = false;
        if(navigateFromSignUp || profileSignIn != null){
            //SignUp or SignIn
            isShowBack = true;
        }

        if( getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(isShowBack);
        }

        if(!isShowBack){
            //-- no flow SignUp or SignIn
            disableSwipe();
        }

        hideToolbarElevation();
        new ViewScrollViewListener(mainScrollView, view -> ViewUtils.hideSoftKey(view));

    }

    @OnClick(R.id.btn_submit_passcode)
    public void onSubmit(View view) {
        ViewUtils.hideSoftKey(view);
        Validator validator = new Validator();
        String passCodeData = edtPasscode.getText().toString();
        edtPasscode.hideErrorColorLine();
        if (TextUtils.isEmpty(passCodeData)) {
            edtPasscode.showErrorColorLine();
        } else {
            //- case wrong passCode
            if (!validator.passCode(passCodeData)) {
                edtPasscode.showErrorColorLine();
                checkPassCodeError();
            } else {
                toSignUpV2Screen();
            }
        }
    }

    private void keyboardInteractListener() {
        ViewKeyboardListener.KeyboardEvent event = new ViewKeyboardListener.KeyboardEvent() {
            @Override
            public void showKeyboard() {
                //dummy do nothing

            }

            @Override
            public void hideKeyboard() {
            }

            @Override
            public View getCurrentFocus() {
                if (NewPassCodeActivity.this == null) return null;
                return NewPassCodeActivity.this.getCurrentFocus();
            }
        };
        //-- add listen keyboard
        keyboardListener = new ViewKeyboardListener(llProfileParentView, event);
        keyboardListener.setViewFocusChangeListener(llParent);
    }


    @OnClick({R.id.holder,R.id.content_wrapper,R.id.scrollView,R.id.root_view, R.id.toolbar})
    public void onClick(View view) {
        ViewUtils.hideSoftKey(view);
    }

    @Override
    public void onStop() {
        ViewUtils.hideSoftKey(getCurrentFocus());
        keyboardListener.removeListener();
        super.onStop();
    }
    /*hide keyboard when touch on screen*/

    void invokeSoftKey(View view) {
        if (edtPasscode != null) {
            edtPasscode.setText("");
            edtPasscode.requestFocus();
            edtPasscode.setCursorVisible(true);
            edtPasscode.setSelection(edtPasscode.getText().toString().length());
        }
        ViewUtils.showSoftKey(view);
    }

    @Override
    public void showErrorDialog(ErrCode errCode, String extraMsg) {
        if (dialogHelper.networkUnavailability(errCode, extraMsg)) return;
        if (errCode == ErrCode.API_ERROR) dialogHelper.alert("ERROR!", extraMsg);
        else dialogHelper.showGeneralError();
    }

    @Override
    public void showProgressDialog() {
        dialogHelper.showProgress();
    }

    @Override
    public void dismissProgressDialog() {
        dialogHelper.dismissProgress();
    }

    @Override
    public void onCheckPassCodeSuccessfully() {
        //-- dummy code here
//        String passCode = edtPasscode.getText().toString();
//        if (!TextUtils.isEmpty(passCode)) {
//
//            //don't check binCode
//            if (navigateFromSignUp) {
//
//            }else{
//                //-- flow case update binCode SignIn or Splash Screen
//                //-- Sign In
//                if (profileSignIn != null) {
//                    presenter.handleUpdateBinCodeSignIn(passCode, profileSignIn);
//                } else {
//                    //-- Splash Screen
//                    presenter.handleSaveUserPrefPassCode(passCode);
//                }
//            }
//        }
    }

    @Override
    public void updatePassCodeApiCompleted() {
        boolean hasForgotPwd = new PreferencesStorageAspire(getApplicationContext()).hasForgotPwd();
        Intent intent;
        if (hasForgotPwd) {
            intent = new Intent(this, ChangePasswordActivity.class);
            intent.putExtra(Intent.EXTRA_REFERRER, "a");
        } else {
            intent = new Intent(this, HomeActivity.class);
        }
        startActivity(intent);
        finish();
    }

    @Override
    public void onCheckPassCodeFailure() {
        checkPassCodeError();
    }

    @Override
    public void onCheckPassCodeNone() {
        checkPassCodeError();
    }

    private void checkPassCodeError() {
        if (dialogHelper != null)
            dialogHelper.action(App.getInstance().getString(R.string.dialogTitleError), getString(R.string.dialogErrorNonePassCode), getString(R.string.text_ok),getString(R.string.text_cancel),
                    (dialogInterface, i) -> invokeError(),
                    (dialogInterface, i) -> toSignInScreen());
    }

    private void onCancelError() {
        if (edtPasscode != null) {
            edtPasscode.postDelayed(() -> ViewUtils.hideSoftKey(edtPasscode), 100);
        }
    }

    private void invokeError() {
        if (edtPasscode != null) {
            edtPasscode.postDelayed(() -> invokeSoftKey(edtPasscode), 100);
        }
    }

    private void toSignInScreen() {
        Intent intent = new Intent(this, SignInActivity.class);
        startActivity(intent);
        finish();

    }

    private void toSignUpV2Screen() {
        Intent intent = new Intent(this, SignUpV2Activity.class);
        startActivity(intent);
    }

    @Override
    public void onErrorGetToken() {
        if (dialogHelper != null) {
            dialogHelper.showGetTokenError();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            ViewUtils.hideSoftKey(getCurrentFocus());
            super.onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
