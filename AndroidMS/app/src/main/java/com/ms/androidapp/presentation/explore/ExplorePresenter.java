package com.ms.androidapp.presentation.explore;


import android.text.TextUtils;

import com.ms.androidapp.App;
import com.ms.androidapp.common.constant.CityData;
import com.ms.androidapp.domain.model.LatLng;
import com.ms.androidapp.domain.model.explore.ExploreRView;
import com.ms.androidapp.domain.model.explore.ExploreRViewItem;
import com.ms.androidapp.domain.usecases.AccommodationSearch;
import com.ms.androidapp.domain.usecases.GetAccommodationByCategories;
import com.ms.androidapp.domain.usecases.GetCityGuides;
import com.ms.androidapp.domain.usecases.GetDinningList;
import com.ms.androidapp.domain.usecases.UseCase;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by tung.phan on 5/8/2017.
 */

public class ExplorePresenter implements Explore.Presenter {

    public static final int DEFAULT_PAGE = 1;
    private final Map<String, String[]> categoryMapper = new HashMap<>();

    private CompositeDisposable disposables;
    private GetAccommodationByCategories getAccommodationByCategories;
    private GetCityGuides getCityGuides;
    private GetDinningList getDinningList;
    private AccommodationSearch accommodationSearch;
    private Explore.View view;
    private UseCase executingUseCase;

    private Single<List<ExploreRViewItem>> historyData;
    private int paging = DEFAULT_PAGE;
    private boolean reachEnd = false;
    // for other categories
    private String selectedCategory = "all";
    private String lastSelectedCity = "";
    // for city guide
    private int cityGuideIndex = -1;
    // for search
    private String term;
    private boolean withOffers;
    private boolean isDialogOffline = false;
    private DiningSortingCriteria diningSortingCriteria = new DiningSortingCriteria((LatLng)null, null);

    ExplorePresenter(GetAccommodationByCategories other, GetDinningList dining,
                     GetCityGuides cityGuides, AccommodationSearch search) {
        disposables = new CompositeDisposable();
        getAccommodationByCategories = other;
        getDinningList = dining;
        getCityGuides = cityGuides;
        accommodationSearch = search;
        buildCategoryMapper();
    }

    boolean inSearchingMode() {
        return !TextUtils.isEmpty(term) || withOffers;
    }

    String searchTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    boolean isWithOffers() {
        return withOffers;
    }

    public void setWithOffers(boolean withOffers) {
        this.withOffers = withOffers;
    }

    public void setLastSelectedCity(String lastSelectedCity) {
        this.lastSelectedCity = lastSelectedCity;
    }

    public int getPaging() {
        return paging;
    }

    public void setPaging(int paging) {
        this.paging = paging;
    }

    public int getCityGuideIndex() {
        return cityGuideIndex;
    }

    public void setCityGuideIndex(int cityGuideIndex) {
        this.cityGuideIndex = cityGuideIndex;
    }

    public String getSelectedCategory() {
        return selectedCategory;
    }

    public void setSelectedCategory(String selectedCategory) {
        this.selectedCategory = selectedCategory;
    }

    @Override
    public void attach(Explore.View view) {
        this.view = view;
    }

    @Override
    public void detach() {
        disposables.dispose();
        this.view = null;
    }

    @Override
    public void getAccommodationData() {
        handleCategorySelection(selectedCategory);
    }

    @Override
    public void handleCitySelection() {
        if(lastSelectedCity.equals(CityData.cityName())) {
            return;
        }
        reachEnd = false;
        int diningCode = CityData.diningCode();
        lastSelectedCity = CityData.cityName();
        if(!TextUtils.isEmpty(term) || withOffers) {
            view.clearSearch(false);
        }
        if(selectedCategory.equals("dining")) {
            getDiningList(diningCode, DEFAULT_PAGE);
            return;
        }
        if(cityGuideIndex > -1) {
            getCityGuide(cityGuideIndex, DEFAULT_PAGE);
        } else if(this.selectedCategory.equals("all")) {
            getByAllCategories(DEFAULT_PAGE);
        } else {
            getAccommodationData(this.selectedCategory, DEFAULT_PAGE);
        }
    }

    @Override
    public void handleCategorySelection(String category) {
        reachEnd = false;
        cityGuideIndex = -1;
        this.selectedCategory = category.toLowerCase();

        if(inSearchingMode()) {
            view.clearSearch(false);
        }
        if(this.selectedCategory.equals("dining")) {
            getDiningList(CityData.diningCode(), DEFAULT_PAGE);
        } else if(!this.selectedCategory.equals("all")) {
            getAccommodationData(this.selectedCategory, DEFAULT_PAGE);
        } else {
            getByAllCategories(DEFAULT_PAGE);
        }
    }

    @Override
    public void cityGuideCategorySelection(int index) {
        view.clearSearch(false);
        reachEnd = false;
        selectedCategory = "";
        cityGuideIndex = index;
        getCityGuide(cityGuideIndex, DEFAULT_PAGE);
    }

    @Override
    public void searchByTerm(String term, boolean withOffers) {
        reachEnd = false;
        cityGuideIndex = -1;
        this.term = term;
        this.withOffers = withOffers;
        handleSearchCategory();
    }

    @Override
    public void clearSearch() {
        this.term = "";
        withOffers = false;
    }

    @Override
    public void offHasOffers() {
        reachEnd = false;
        withOffers = false;
        if(TextUtils.isEmpty(this.term)) {
            view.clearSearch(true);
            return;
        }
        handleSearchCategory();
    }

    private void handleSearchCategory() {
        if(selectedCategory.equals("all") || selectedCategory.equals("dining"))
            searchByTerm(DEFAULT_PAGE, term, withOffers, lastSelectedCity, this.selectedCategory);
        else
            getAccommodationData(this.selectedCategory, DEFAULT_PAGE);
    }

    @Override
    public void loadMore() {
        if(reachEnd || paging == DEFAULT_PAGE) return;
        if(executingUseCase == null && selectedCategory.equals("all")) {
            getByAllCategories(paging);
            return;
        }
        if(executingUseCase == null && selectedCategory.equals("dining")) {
            getDiningList(CityData.diningCode(), paging);
            return;
        }
        if(executingUseCase instanceof GetCityGuides) {
            getCityGuide(cityGuideIndex, paging);
        } else if(executingUseCase instanceof GetAccommodationByCategories) {
//            getAccommodationData(selectedCategory, paging);
        } else if(executingUseCase instanceof AccommodationSearch) {
            searchByTerm(paging, term, withOffers, lastSelectedCity, selectedCategory);
        }
    }

    @Override
    public ExploreRView detailItem(ExploreRView.ItemType type, int index) {
        /*if(executingUseCase == null && !this.selectedCategory.equals("all")) return null;*/
        if(type == ExploreRView.ItemType.CITY_GUIDE) {
            return getCityGuides.getItemView(index);
        } else if(type == ExploreRView.ItemType.DINING) {
            return getDinningList.getItemView(index);
        } else if(type == ExploreRView.ItemType.SEARCH) {
            return accommodationSearch.getItemView(index);
        } else {
            return getAccommodationByCategories.getItemView(index);
        }
    }

    @Override
    public void applyDiningSortingCriteria(LatLng location, String cuisine) {
        diningSortingCriteria.setLocation(location);
        diningSortingCriteria.setCuisine(cuisine);
        diningSortingCriteria.setIsRegion(CityData.regions.contains(CityData.cityName()));
        if(selectedCategory.equals("dining")) {
            List<ExploreRViewItem> viewData = view.historyData();
            if(viewData == null || viewData.size() == 0) return;

            Single<List<ExploreRViewItem>> singleSort = Single.create(e -> {
                List<ExploreRViewItem> sort = DiningSorting.sort(viewData, diningSortingCriteria);
                e.onSuccess(sort);
            });
            singleSort.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new DisposableSingleObserver<List<ExploreRViewItem>>() {
                        @Override
                        public void onSuccess(List<ExploreRViewItem> exploreRViewItems) {
                            view.updateRecommendAdapter(exploreRViewItems);
                            dispose();
                        }

                        @Override
                        public void onError(Throwable e) {
                            dispose();
                        }
                    });
        }
    }

    /**
     * city guide API
     * @param cityGuideCategoryIndex cityGuideCategoryIndex
     */
    private void getCityGuide(int cityGuideCategoryIndex, int page) {
        if(!preExecute(page)) return;
        executingUseCase = getCityGuides;

        int code = CityData.specificCityGuideCode(cityGuideCategoryIndex);
        if(code == -1) {
            view.hideLoading(paging > DEFAULT_PAGE);
            view.emptyData();
            view.cityGuideNotAvailable();
            return;
        }
        Single<List<ExploreRViewItem>> executor = getCityGuides.buildUseCaseSingle(
                new GetCityGuides.Params(code).paging(page));
        executeAndMerge(executor);
    }

    /**
     * dining list API
     * @param diningCode selectedCategory ID
     */
    private void getDiningList(int diningCode, int page) {
        if(!preExecute(page)) return;
        executingUseCase = null;
        Single<List<ExploreRViewItem>> pureDiningList;
        if (diningCode > 0) {
            pureDiningList = getDinningList.buildUseCaseSingle(
                    new GetDinningList.Params(diningCode, page));
        } else {
            pureDiningList = Single.just(new ArrayList<ExploreRViewItem>());
        }

        Single<List<ExploreRViewItem>> ccaDiningList;
        if (page == DEFAULT_PAGE) {
            ccaDiningList = getAccommodationByCategories
                    .buildUseCaseSingle(new GetAccommodationByCategories.Params(
                            "dining",
                            page,
                            null,
                            categoryMapper.get("dining")
                    ));
        } else {
            ccaDiningList = Single.just(new ArrayList<ExploreRViewItem>());
        }
        Single<List<ExploreRViewItem>> executor = Single.zip(pureDiningList, ccaDiningList,
                (pureDiningItems, ccaDiningItems) -> {
                    pureDiningItems.addAll(ccaDiningItems);
                    return pureDiningItems;
                });
        executeAndMerge(executor);
    }

    private void getAccommodationData(String categoryName, int page) {
        String[] categories = categoryMapper.get(categoryName);
        if(categories == null) {
            view.emptyData();
            return;
        }
        getAccommodationData(categoryName, page, categories);
    }

    /**
     *
     * @param categories all, flowers... except dining
     */
    private void getAccommodationData(String catName, int page, String... categories) {
        if(!preExecute(page)) return;
        executingUseCase = getAccommodationByCategories;
        Single<List<ExploreRViewItem>> executor = getAccommodationByCategories.buildUseCaseSingle(
                new GetAccommodationByCategories.Params(
                        catName,
                        page,
                        inSearchingMode() ? new SearchConditions(term, withOffers) : null,
                        categories));
        executeAndMerge(executor);
    }

    private void getByAllCategories(int page) {
        if(!preExecute(page)) return;
        executingUseCase = null;
        List<Single<List<ExploreRViewItem>>> executors = new ArrayList<>();

        // accommodation
        if(page == 1) {
            Single<List<ExploreRViewItem>> getAccommodations =
                    getAccommodationByCategories.buildUseCaseSingle(
                            new GetAccommodationByCategories.Params(
                                    "all",
                                    page,
                                    inSearchingMode() ? new SearchConditions(term, withOffers) : null,
                                    categoryMapper.get("all"))
                    );
            executors.add(getAccommodations);
        }

        // dining
        int diningCode = CityData.citySelected() ? CityData.diningCode() : 0;
        Single<List<ExploreRViewItem>> getDining;
        if(diningCode > 0) {
            getDining = getDinningList.buildUseCaseSingle(
                    new GetDinningList.Params(diningCode, page)
            );
            executors.add(getDining);
        }

        if(executors.size() == 0) {
            view.hideLoading(paging > DEFAULT_PAGE);
            return;
        }

        Single<List<ExploreRViewItem>> mainExecutor = Single.zip(executors, objects -> {
            List<ExploreRViewItem> result = new ArrayList<>();
            for(Object obj : objects) {
                List<ExploreRViewItem> eachList = (List<ExploreRViewItem>) obj;
                result.addAll(eachList);
            }
            return result;
        });
        executeAndMerge(mainExecutor);
    }

    private void searchByTerm(int page, String term, boolean withOffers, String city, String category) {
        if(!preExecute(page)) return;
        executingUseCase = accommodationSearch;
        AccommodationSearch.Params params = new AccommodationSearch.Params(term, page, withOffers, city, selectedCategory.equals("dining"), category);
        disposables.add(accommodationSearch.buildUseCaseSingle(params)
                .zipWith(historyData, (searchResult, history) -> {
                    history.addAll(searchResult.exploreRViewItems);
                    if(selectedCategory.equals("dining")) history = DiningSorting.sort(history, diningSortingCriteria);
                    else Collections.sort(history);
                    return new AccommodationSearch.SearchResult(history, searchResult.reachEnd);
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new SearchObserver()));
    }

    private void executeAndMerge(Single<List<ExploreRViewItem>> executor) {
        disposables.add(executor.zipWith(historyData, (exploreRViewItems, history) -> {
            if(exploreRViewItems.size() == 0) return exploreRViewItems;
            history.addAll(exploreRViewItems);
            if(selectedCategory.equals("dining")) history = DiningSorting.sort(history, diningSortingCriteria);
            else Collections.sort(history);
            return history;
        })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new AccommodationDataObserver()));
    }

    private boolean preExecute(int page) {
        if(!App.getInstance().hasNetworkConnection()) {

            if(page == DEFAULT_PAGE) {
                view.emptyData();
                view.noInternetMessage();
            }else{
                if(!isDialogOffline){
                    isDialogOffline = true;
                    view.noInternetMessage();
                }else{
                    view.loadDataDone();
                }
            }
            return false;
        }
        isDialogOffline = false;

        disposables.clear();
        if(page == DEFAULT_PAGE) {
            view.emptyData();
        }
        historyData = Single.just(view.historyData());
        paging = page;
        view.showLoading(page > DEFAULT_PAGE);
        return true;
    }

    private void buildCategoryMapper() {
        categoryMapper.put("all", new String[] { "Specialty Travel", "Hotels", "Flowers", "Tickets",
                "Golf", "Golf Merchandise", "Vacation Packages", "Cruises", "VIP Travel Services",
                "Private Jet Travel", "Transportation","Wine", "Dining", "Retail Shopping" });
        categoryMapper.put("dining", new String[] {"Dining", "Specialty Travel"});
        categoryMapper.put("hotels", new String[] {"Hotels"});
        categoryMapper.put("flowers", new String[] {"Flowers"});
        categoryMapper.put("entertainment", new String[] {"Tickets", "Specialty Travel"});
        categoryMapper.put("golf", new String[] {"Golf", "Golf Merchandise", "Specialty Travel"});
        categoryMapper.put("vacation packages", new String[] {"Vacation Packages"});
        categoryMapper.put("cruise", new String[] {"Cruises"});
        categoryMapper.put("tours", new String[] {"VIP Travel Services"});
        categoryMapper.put("travel", new String[] {"Private Jet Travel"});
        categoryMapper.put("airport services", new String[] {"VIP Travel Services"});
        categoryMapper.put("travel services", new String[] {"VIP Travel Services"});
        categoryMapper.put("transportation", new String[] {"Transportation"});
        categoryMapper.put("shopping", new String[] {"Golf Merchandise", "Flowers", "Wine", /*"Golf",*/ "Retail Shopping"});
    }

    private final class AccommodationDataObserver extends DisposableSingleObserver<List<ExploreRViewItem>> {

        @Override
        public void onSuccess(List<ExploreRViewItem> exploreRViewItems) {
            view.hideLoading(paging > DEFAULT_PAGE);
            if(exploreRViewItems.size()>0) {
                paging++;
            } else {
                if(inSearchingMode()) {
                    view.onSearchNoData();
                }
                reachEnd = true;
                return;
            }
            view.updateRecommendAdapter(exploreRViewItems);
            dispose();
        }

        @Override
        public void onError(Throwable e) {
            view.hideLoading(paging > DEFAULT_PAGE);
            view.onUpdateFailed(e.getMessage());
            dispose();
        }
    }

    private final class SearchObserver extends DisposableSingleObserver<AccommodationSearch.SearchResult> {

        private int pageCount = 1;

        @Override
        public void onSuccess(AccommodationSearch.SearchResult searchResult) {
            view.hideLoading(paging > DEFAULT_PAGE);
            int count = searchResult.exploreRViewItems.size();
            if(searchResult.reachEnd) {
                reachEnd = true;
            }
            if(searchResult.reachEnd && count == 0) {
                view.hideLoading(paging > DEFAULT_PAGE);
                view.onSearchNoData();
            } else {
                paging++;
                view.updateRecommendAdapter(searchResult.exploreRViewItems);
                if(count < pageCount*10) {
                    loadMore();
                } else {
                    pageCount++;
                }
            }
            dispose();
        }

        @Override
        public void onError(Throwable e) {
            view.hideLoading(paging > DEFAULT_PAGE);
            view.onSearchNoData();
            dispose();
        }
    }
}