package com.ms.androidapp.presentation.changepass;

import android.content.Context;

import com.api.aspire.data.preference.PreferencesStorageAspire;
import com.api.aspire.data.repository.ProfileDataAspireRepository;
import com.api.aspire.domain.usecases.LoadProfile;
import com.api.aspire.domain.usecases.ResetPassword;
import com.ms.androidapp.App;
import com.api.aspire.common.constant.ErrCode;
import com.ms.androidapp.domain.usecases.MapProfileApp;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableCompletableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by vinh.trinh on 4/26/2017.
 */

public class ChangePasswordPresenter implements ChangePassword.Presenter {

    private CompositeDisposable compositeDisposable;
    private ChangePassword.View view;
    private ResetPassword changePassword;

    ChangePasswordPresenter(Context c) {
        compositeDisposable = new CompositeDisposable();
        PreferencesStorageAspire preferencesStorage = new PreferencesStorageAspire(c);
        MapProfileApp mapLogic = new MapProfileApp();
        this.changePassword = new ResetPassword(mapLogic,
                preferencesStorage,
                new LoadProfile(new ProfileDataAspireRepository(preferencesStorage,mapLogic))
        );
    }

    @Override
    public void attach(ChangePassword.View view) {
        this.view = view;
    }

    @Override
    public void detach() {
        compositeDisposable.dispose();
        view = null;
    }

    @Override
    public void doChangePassword(String oldPassword, String newPassword) {
        if (!App.getInstance().hasNetworkConnection()) {
            view.showErrorDialog(ErrCode.CONNECTIVITY_PROBLEM, null);
            return;
        }
        view.showProgressDialog();

        ResetPassword.Params params = new ResetPassword.Params(oldPassword, newPassword);
        compositeDisposable.add(
                changePassword.buildUseCaseCompletable(params)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new DisposableCompletableObserver() {
                            @Override
                            public void onComplete() {
                                if (view != null) {
                                    view.dismissProgressDialog();
                                    view.showSuccessMessage();
                                }
                            }

                            @Override
                            public void onError(Throwable e) {
                                if (view != null) {
                                    view.dismissProgressDialog();
                                    view.showErrorDialog(ErrCode.UNKNOWN_ERROR, e.getMessage());
                                }
                            }
                        })
        );

    }

    @Override
    public void abort() {
        compositeDisposable.clear();
    }

}
