package com.ms.androidapp.common.constant;

/**
 * Created by tung.phan on 5/5/2017.
 */

public interface SharedPrefConstant {

    String PROFILE = "ppfile";
    String PREFERENCES = "preferences";
    String METADATA = "pMetadata";
    String SELECTED_CITY = "city";
    String FORGOT_SECRET = "hasFP";
}
