package com.ms.androidapp.domain.mapper.profile;

import android.text.TextUtils;

import com.api.aspire.common.utils.CommonUtils;
import com.api.aspire.data.entity.profile.CreateProfileAspireRequest;
import com.api.aspire.data.entity.profile.ProfileAspireResponse;
import com.api.aspire.domain.model.ProfileAspire;
import com.api.aspire.domain.model.UserProfileOKTA;
import com.api.aspire.domain.usecases.ChangeSecurityQuestion;
import com.api.aspire.domain.usecases.CreateProfileCase;
import com.ms.androidapp.BuildConfig;
import com.ms.androidapp.domain.model.Profile;

public class MapProfile {

    public MapProfile() {
    }

    private String handlePartyId(String partyId) {
        String valPartyId;
        if (TextUtils.isEmpty(partyId)) {
            valPartyId = null;
        } else {
            valPartyId = partyId;
        }
        return valPartyId;
    }

    /**
     * Check Party Id
     * Create Object ProfileAspire
     * Create Param Request
     */
    public CreateProfileCase.Params createProfile(Profile profile,
                                                  String partyId,
                                                  ChangeSecurityQuestion.Param securityQuestionParam) {
        //- check partyId
        String valPartyId = handlePartyId(partyId);

        //- object request
        ProfileAspire profileAspire = new ProfileAspire();
        //- secret in profile
        profileAspire.setSecretKey(securityQuestionParam.secret);

        //- map profile
        profileAspire.setFirstName(profile.getFirstName());
        profileAspire.setLastName(profile.getLastName());
        profileAspire.setEmail(profile.getEmail());
        profileAspire.setPhone(profile.getPhone());
        profileAspire.setSalutation(profile.getSalutation());
        profileAspire.locationToggle(profile.isLocationOn());
        profileAspire.setCountryCode(profile.getCountryCode());
        //- special
        profileAspire.setBinCodeLocal(profile.getPassCode());

        ProfileAspireResponse profileAspireResponse = MapDataProfile.mapRequest(profileAspire);

        //-*- set bin verifyMetadata
        int binNumber = BuildConfig.AS_BIN;

        String clientEmail = MapDataApi.getClientIdEmailApp(profile.getEmail());
        CreateProfileAspireRequest createProfileRequest = new CreateProfileAspireRequest(valPartyId,
                clientEmail,
                securityQuestionParam.secret,
                profileAspireResponse,
                binNumber);

        return new CreateProfileCase.Params(profileAspire, createProfileRequest, securityQuestionParam);
    }

    public Profile getProfile(ProfileAspire profileAspire) {
        Profile profile = new Profile();

        //- map profile
        profile.setFirstName(profileAspire.getFirstName());
        profile.setLastName(profileAspire.getLastName());
        profile.setEmail(profileAspire.getEmail());
        profile.setPhone(profileAspire.getPhone());
        profile.setSalutation(profileAspire.getSalutation());
        profile.locationToggle(profileAspire.isLocationOn());
        profile.setCountryCode(profileAspire.getCountryCode());
        //- special
        profile.setPassCode(profileAspire.getBinCodeLocal());

        return profile;
    }

    public void getProfileRecoveryQuestion(Profile profile, UserProfileOKTA userProfileOKTA) {
        if (profile == null || userProfileOKTA == null ||
                userProfileOKTA.getCredentials() == null ||
                userProfileOKTA.getCredentials().getRecoveryQuestion() == null)
            return;

        //- map profile
        profile.setQuestion(userProfileOKTA.getCredentials().getRecoveryQuestion().getQuestion());
    }

    public ProfileAspire updateProfile(ProfileAspire profileAspire, Profile profileUpdated) {

        //- map profile
        profileAspire.setFirstName(profileUpdated.getFirstName());
        profileAspire.setLastName(profileUpdated.getLastName());
        profileAspire.setEmail(profileUpdated.getEmail());
        profileAspire.setPhone(profileUpdated.getPhone());
        profileAspire.setSalutation(profileUpdated.getSalutation());
        profileAspire.locationToggle(profileUpdated.isLocationOn());
        profileAspire.setCountryCode(profileUpdated.getCountryCode());
        //- special
        profileAspire.setBinCodeLocal(profileUpdated.getPassCode());

        return profileAspire;
    }
}
